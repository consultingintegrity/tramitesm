<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 0);
ini_set('memory_limit','2048M');
class Migration extends CI_Controller {



	public function index()
	{

		$this->load->library('migration');
		//this->migration->version(2) ejecutará el método up de las miraciones 001 y 002 y el metodo down de las superiores

		if ($this->migration->version(52)) {

			show_error($this->migration->error_string());
		}//if
		else{
			echo "Base de datos Cargada";
		}//else
	}//index


}//class
/* End of file Migration.php */
/* Location: ./application/controllers/migrations/Migration.php */
