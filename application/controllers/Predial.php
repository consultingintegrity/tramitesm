<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Predial extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('Predial_M');
  }
  function index()
  {
    //$this->load->library('license/license');//libreria para validar la conraseña del uso de la plataforma
    // $this->license->authenticate();
    $data["title"] = "Pago del Predial";
    $this->load->view('head', $data);
    $this->load->view('header');
    $this->load->view('predial/content');
    $this->load->view('footer');
    $this->load->view('scripts/js');
    $this->load->view('scripts/utils');
    $this->load->view('scripts/predio');
  } //index

  public function consultaPredial()
  {
    //obtengo datos del ajax
    $claveCatastral = (!empty($this->input->post('clave'))) ? addslashes($this->input->post('clave')) : 0;
    //consulto info en la db
    $claveCatastral = str_replace(' ', '', $claveCatastral);
    $dataPredial = $this->Predial_M->ConsultaInfo($claveCatastral);
    //valido que la clave catastral tenga registros
    if (!empty($dataPredial)) {
      $dataPredial->total = number_format($dataPredial->total, 2);
      $dataPredial->impuestoAno = number_format($dataPredial->impuestoAno, 2);
      $dataPredial->adicional = number_format($dataPredial->adicional, 2);
      $dataPredial->actualización = number_format($dataPredial->actualización, 2);
      $dataPredial->Recargo = number_format($dataPredial->Recargo, 2);
      $dataPredial->requerimientoGastoEjecucion = number_format($dataPredial->requerimientoGastoEjecucion, 2);
      $dataPredial->embargoGastosEjecucion = number_format($dataPredial->embargoGastosEjecucion, 2);
      $dataPredial->multa = number_format($dataPredial->multa, 2);
      $dataPredial->descuento = number_format($dataPredial->descuento, 2);
      $dataPredial->rezagototal = number_format($dataPredial->rezagoAnoTranscurre + $dataPredial->rezagoAnosAnteriores, 2);

      $jsonResponse["estatus"] = 200;
      $jsonResponse["msg"] = "Verifica La información";
      $jsonResponse["predial"] = $dataPredial;
    } //if si existe la clave castral
    else {
      $jsonResponse["estatus"] = 500;
      $jsonResponse["msg"] = "La clave catastral no Existe";
    }

    echo json_encode($jsonResponse);
  } //consultaPredial

  public function consultaTotal()
  {
    $id_predial = (!empty($this->input->post('id'))) ? addslashes($this->input->post('id')) : 0;
    $totalPredio = $this->Predial_M->getMonto($id_predial);

    $jsonResponse["total"] = $totalPredio;

    echo json_encode($jsonResponse);
  } //consultaTotal

  //funcion que realiza al modificacion del estatus de pago en predio solicitud
  public function realizaPagoPredio()
  {
    $id_predial = (!empty($this->input->post('id'))) ? addslashes($this->input->post('id')) : 0;
    if ($this->Predial_M->uptadeEstPago($id_predial)) {

      $jsonResponse["response"] = 200;
      $jsonResponse["msg"] = "Estado de pago actualizado";
    } else {
      $jsonResponse["estatus"] = 500;
      $jsonResponse["msg"] = "No actualizo estado de pago";
    }
    echo json_encode($jsonResponse);
  } //realizaPagoPredio

  public function realizaCortePredial()
  {
    $insertCortePredial = $this->Predial_M->insertCortePredial();
    if ($insertCortePredial == true) {

      $this->Predial_M->generaCorteP();
      $jsonResponse["response"] = 200;
      $jsonResponse["msg"] = "se genero el corte de predial";
    } else {
      $jsonResponse["response"] = 500;
      $jsonResponse["msg"] = "No hay cortes";
    }
    echo json_encode($jsonResponse);
  } //realizaCortePredial

  public function insertarPredial()
  {
    //obtengo mis valores
    $nomPersona = (!empty($this->input->post('nombre'))) ? addslashes($this->input->post('nombre')) : 0;
    $cCatastral = (!empty($this->input->post('cCatastral'))) ? addslashes($this->input->post('cCatastral')) : 0;
    $folio = (!empty($this->input->post('folio'))) ? addslashes($this->input->post('folio')) : 0;
    $lCaptura = (!empty($this->input->post('lCaptura'))) ? addslashes($this->input->post('lCaptura')) : 0;
    $total = (!empty($this->input->post('total'))) ? addslashes($this->input->post('total')) : 0;
    $url = (!empty($this->input->post('url'))) ? addslashes($this->input->post('url')) : 0;
    $fechaEmision = (!empty($this->input->post('fechaEmision'))) ? addslashes($this->input->post('fechaEmision')) : 0;
    $fechaVencimiento = (!empty($this->input->post('fechaVencimiento'))) ? addslashes($this->input->post('fechaVencimiento')) : 0;

    //convierto fechas para poder insertar
    $fechaEmision = date('Y-m-d', strtotime($fechaEmision));
    $fechaVencimiento = date('Y-m-d', strtotime($fechaVencimiento));

    $resultado = $this->Predial_M->insertaPredialSol($nomPersona, $cCatastral, $folio, $lCaptura, $total, $url, $fechaEmision, $fechaVencimiento);

    if (!empty($resultado)) {
      $jsonResponse["estatus"] = 200;
      $jsonResponse["msg"] = "ok";
    } else {
      $jsonResponse["estatus"] = 500;
      $jsonResponse["msg"] = "Algo salio mal con la base de datos";
    }
    echo json_encode($jsonResponse);
  } //insert
}//class
