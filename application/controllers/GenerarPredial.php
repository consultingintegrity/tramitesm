<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenerarPredial extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->library('Pdf');
  }//.index

  public function generaPdfCorte(){

    $this->load->model('Predial_M');
    $claveCatastral = (!empty($this->input->post('clave'))) ? addslashes($this->input->post('clave')) : 0;
    $claveCatastral = str_replace(' ','',$claveCatastral);
    if($claveCatastral != 0){
      $dataPredialExiste = $this->Predial_M->consultaExiste($claveCatastral);
      if($dataPredialExiste == null){

      
      $dataPredial = $this->Predial_M->ConsultaInfo($claveCatastral);
    //fecha

    $fechaTabla = date("d-m-Y");
      //fecha de vencimiento del documento
    $dia = date('d', strtotime("+1 days"));
    $mes = date('F', strtotime("+1 days"));
    $ano = date('Y', strtotime("+1 days"));

    $mes = $this->name($mes);

    $fecha_vencimiento = $dia." de ".$mes." de ".$ano;
    //fecha de documento
    $mes2 = $this->name(date('F'));
    $fecha = date("d")." de ".$mes2." de ".date("Y");
    $fechaFol= date("my", strtotime($fechaTabla));//se convierte la fecha al formato de captura
      //folio del documento
    $folio ='OP/'.$dataPredial->id."-".$fechaFol;
    $fechaCap= date("dmy", strtotime($fechaTabla));
      //calculo para la linea de captura
    $ide=9;
    $idef= str_pad($ide,2,0,STR_PAD_LEFT);
    $final= $this->generaCadena($idef.$claveCatastral);
    $pdf = new Pdf();
    $pdf->AddPage();
    $pdf->SetMargins(15, 25 , 15, 20);
    $pdf->AliasNbPages();

    $pdf->SetTitle(utf8_decode("Recibo de pago Predial"));
    $pdf->SetFont('Times', 'B', 14);
    $pdf->Cell(0,25,utf8_decode('MUNICIPIO DE JALPAN DE SERRA QUERÉTARO'),0,0,'C');
    $pdf->SetFont('Times', 'B', 13);
    $pdf->Ln(17);
    $pdf->Cell(0,0,utf8_decode('Dirección de Finanzas Publicas'),0,0,'C');
    $pdf->SetFont('Arial','',12);
    $pdf->Ln(10);
    $pdf->Cell(0,5,utf8_decode('Jalpan de Serra, Qro., a '.$fecha),0,0,'R');
    $pdf->Ln(7);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(0,0,utf8_decode("FOLIO: ".$folio),0,0,'R');
    $pdf->Ln(10);
    $pdf->SetFont('Times', 'B', 10);
    $pdf->setX(35);
    $pdf->Cell(70,0,utf8_decode('Fecha de Impresión:'),0,0,'L');
    $pdf->SetFont('Times', '', 9);
    $pdf->Cell(0,0,utf8_decode($fecha),0,0,'L');
    $pdf->Ln(5);
    $pdf->SetFont('Times', 'B', 10);
    $pdf->setX(35);
    $pdf->Cell(70,0,utf8_decode('Clave Catastral:'),0,0,'L');
    $pdf->SetFont('Times', '', 9);
    $pdf->Cell(0,0,utf8_decode($dataPredial->ClaveCatastral),0,0,'L');
    $pdf->Ln(5);
    $pdf->SetFont('Times', 'B', 10);
    $pdf->setX(35);
    $pdf->Cell(70,0,utf8_decode('Nombre Propietario:'),0,0,'L');
    $pdf->SetFont('Times', '', 9);
    $pdf->Cell(0,0,utf8_decode($dataPredial->nombreContribuyente),0,0,'L');
    $pdf->Ln(5);
    $pdf->SetFont('Times', 'B', 10);
    $pdf->setX(35);
    $pdf->Cell(70,0,utf8_decode('Ubicacion del Predio:'),0,0,'L');
    $pdf->SetFont('Times', '', 9);
    $pdf->Cell(0,0,utf8_decode($dataPredial->coloniaPredio." #".$dataPredial->numeroExterior),0,0,'L');
    $pdf->Ln(2);
    $pdf->setX(105);
    $pdf->Cell(0,5,utf8_decode($dataPredial->ubicacionPredio),0,0,'L');
    $pdf->Ln(8);
    $pdf->SetFont('Times', 'B', 10);
    $pdf->setX(35);
    $pdf->Cell(70,0,utf8_decode('Vigencia de Pago:'),0,0,'L');
    $pdf->SetFont('Times', '', 9);
    $pdf->Cell(0,0,utf8_decode($fecha_vencimiento),0,0,'L');
    $pdf->Ln(6);
    $pdf->SetFont('Times', 'B', 10);
    $pdf->setX(35);
    $pdf->Cell(0,0,utf8_decode('ADEUDO DE PREDIAL'),0,0,'L');
    $pdf->Ln(5);
    $pdf->setX(35);
    $pdf->Cell(70,0,utf8_decode('Periodo Inicial:'),0,0,'L');
    $pdf->Cell(20,0,utf8_decode('Año: '.$dataPredial->anoInicialAdeudo),0,0,'L');
    $pdf->Cell(10,0,utf8_decode('Bimestre: '.$dataPredial->bimestreInicialAdeudo),0,0,'L');
    $pdf->Ln(5);
    $pdf->setX(35);
    $pdf->Cell(70,0,utf8_decode('Periodo Final:'),0,0,'L');
    $pdf->Cell(20,0,utf8_decode('Año: '.$dataPredial->anofinalAdeudo),0,0,'L');
    $pdf->Cell(10,0,utf8_decode('Bimestre: '.$dataPredial->bimestrefinalAdeudo),0,0,'L');
    $pdf->Ln(10);
    $pdf->Cell(0,0,utf8_decode($dataPredial->nombreContribuyente),0,0,'C');
    $pdf->Ln(8);
    $pdf->SetFont('courier', '', 11);
    $pdf->Cell(0,0,utf8_decode('TITULAR'),0,0,'C');
    $pdf->Ln(8);
    $pdf->SetFont('Times', 'B', 14);
    $pdf->Cell(0,0,utf8_decode('$'.number_format($dataPredial->total,2)),0,0,'C');
    $pdf->Ln(8);
    $pdf->SetFont('courier', '', 11);
    $pdf->Cell(0,0,utf8_decode('TOTAL'),0,0,'C');
    $pdf->Ln(5);
    $pdf->SetFont('Arial','B',11);
    $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
    $pdf->Ln(5);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
    $pdf->Ln(5);
    $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
    $pdf->Ln(5);
    $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
    $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',160,180,20);
    $pdf->Ln(10);
    $pdf->Ln(3);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
    $pdf->Ln(6);
    $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
    $pdf->Ln(4);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(120,8,'1600826','L,R',0,'L',0);//linea de captura
    $pdf->Ln(4);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
    $pdf->Ln(4);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(120,8,$final,'L,R',0,'L',0);
    $pdf->Ln(6);
    $pdf->SetFont('Arial','B',13);
    $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
    $pdf->Ln(6);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
    $pdf->Ln(4);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(120,8,'012914002016008262','L,R',0,'L',0);
    $pdf->Ln(4);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
    $pdf->Ln(4);
    $pdf->SetFont('Arial','',10);
    $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
    $pdf->Ln(10);
    $pdf->SetFont('Times', 'B', 8);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('DESGLOSE:'),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->SetFont('Times', '', 7);
    $pdf->Cell(25,5,utf8_decode('Impuesto:'),0,0,'L');
    $pdf->Cell(20,5,utf8_decode(number_format($dataPredial->impuestoAno,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Adicional:'),0,0,'L');
    $pdf->Cell(20,5,utf8_decode(number_format($dataPredial->adicional,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Actualización:'),0,0,'L');
    $pdf->Cell(20,5,utf8_decode(number_format($dataPredial->actualización,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Recargos:'),0,0,'L');
    $pdf->Cell(20,5,utf8_decode(number_format($dataPredial->Recargo,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Requerimientos:'),0,0,'L');
    $pdf->Cell(20,5,utf8_decode(number_format($dataPredial->requerimientoGastoEjecucion,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Embargo:'),0,0,'L');
    $pdf->Cell(20,5,utf8_decode(number_format($dataPredial->embargoGastosEjecucion,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Multa:'),0,0,'L');
    $pdf->Cell(20,5,utf8_decode(number_format($dataPredial->multa,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Rezago:'),0,0,'L');
    $pdf->Cell(25,5,utf8_decode(number_format($dataPredial->rezagoAnoTranscurre+$dataPredial->rezagoAnosAnteriores,2)),0,0,'L');
    $pdf->Ln(3);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('Descuento:'),0,0,'L');
    $pdf->Cell(25,5,utf8_decode(number_format($dataPredial->descuento,2)),0,0,'L');
    $pdf->Ln(5);
    $pdf->SetX(150);
    $pdf->Cell(25,5,utf8_decode('TOTAL:'),0,0,'L');
    $pdf->Cell(25,5,utf8_decode("$".number_format($dataPredial->total,2)),1,0,'C');
    $pdf->Ln(3);
    $pdf->SetFont('Times', '', 6);
    $pdf->Cell(0,5,utf8_decode('ESTE DOCUMENTO NO ES UN COMPROBANTE FISCAL:'),0,0,'C');
    /*
      * Se manda el pdf al navegador
      *
      * $this->pdf->Output(nombredelarchivo, destino);
      *
      * I = Muestra el pdf en el navegador
      * D = Envia el pdf para descarga
      * F = guarda el archivo dn una ruta
      */
      $name = "predialDocs/predial_{$dataPredial->ClaveCatastral}{$fechaCap}.pdf";
     $mesAno=date("m-Y");
      $fecha_vencimientoTabla = date('d-m-Y', strtotime("+1 days"));
      $pdf->close();
      $pdf->OutPut(''.$name,'F');
      
      if(!is_null($name)){
          $jsonResponse["response"] = 200;
          $jsonResponse["msg"] = "Documento Generado Exitosamente";
          $jsonResponse["url"] = $name;
          $jsonResponse["lineaCaptura"] = $final;
          $jsonResponse["folio"] = $folio;
          $jsonResponse["fecha_vencimiento"] = $fecha_vencimientoTabla;
          $jsonResponse["fecha_emision"] = $fechaTabla;
          $jsonResponse["Total"] = $dataPredial->total;
          $jsonResponse["nombre"] = $dataPredial->nombreContribuyente;
          $jsonResponse["claveCatastral"] = $dataPredial->ClaveCatastral;
      }
      else{

        $jsonResponse["response"] = 500;
        $jsonResponse["msg"] = "algo salio mal";
      }//name
    }//predial existe
    else{
      $fechavence=date('d-m-Y', strtotime($dataPredialExiste->fecha_vencimiento));
      $jsonResponse["response"] = 600;
      $jsonResponse["msg"] = "Ya Cuentas con una solicitud Generada \n vence ".$fechavence;
      $jsonResponse["url"] = $dataPredialExiste->url;
    }
    
    }//clave !=null
    else{

      $jsonResponse["response"] = 500;
      $jsonResponse["msg"] = "algo salio mal";
  
    }
    echo json_encode($jsonResponse);
  }//generaPdfCorte

  public function generaCadena($cadena_captura){
    $linea_captura = 0;
    $r = str_pad($cadena_captura,18,0,STR_PAD_RIGHT);
    
    $cadena1 = substr($r,0,1); 
    $cadena2 = substr($r,1,1); 
    $cadena3 = substr($r,2,1);
    $cadena4 = substr($r,3,1);
    $cadena5 = substr($r,4,1);
    $cadena6 = substr($r,5,1);
    $cadena7 = substr($r,6,1);
    $cadena8 = substr($r,7,1);
    $cadena9 = substr($r,8,1);
    $cadena10 = substr($r,9,1);
    $cadena11 = substr($r,10,1);
    $cadena12 = substr($r,11,1);
    $cadena13 = substr($r,12,1);
    $cadena14 = substr($r,13,1);
    $cadena15 = substr($r,14,1);
    $cadena16 = substr($r,15,1);
    $cadena17 = substr($r,16,1);
    $cadena18 = substr($r,17,1);
    $cadena19 = substr($r,18,1);
    
    $rcadena1 = $cadena1 * 1;
    if($rcadena1>9){
      $rscadena1 = substr($rcadena1,0,1) + substr($rcadena1,1,1);
    }
    else {
      $rscadena1 = $rcadena1;
    }
    
    $rcadena2 = $cadena2 * 2;
    if($rcadena2>9){
      $rscadena2 = substr($rcadena2,0,1) + substr($rcadena2,1,1);
    }
    else {
      $rscadena2 = $rcadena2;
    }
    
    $rcadena3 = $cadena3 * 1;
    if($rcadena3>9){
      $rscadena3 = substr($rcadena3,0,1) + substr($rcadena3,1,1);
    }
    else {
      $rscadena3 = $rcadena3;
    }
    
    $rcadena4 = $cadena4 * 2;
    if($rcadena4>9){
      $rscadena4 = substr($rcadena4,0,1) + substr($rcadena4,1,1);
    }
    else {
      $rscadena4 = $rcadena4;
    }
    
    $rcadena5 = $cadena5 * 1;
    if($rcadena5>9){
      $rscadena5 = substr($rcadena5,0,1) + substr($rcadena5,1,1);
    }
    else {
      $rscadena5 = $rcadena5;
    }
    
    $rcadena6 = $cadena6 * 2;
    if($rcadena6>9){
      $rscadena6 = substr($rcadena6,0,1) + substr($rcadena6,1,1);
    }
    else {
      $rscadena6 = $rcadena6;
    }
    
    $rcadena7 = $cadena7 * 1;
    if($rcadena7>9){
      $rscadena7 = substr($rcadena7,0,1) + substr($rcadena7,1,1);
    }
    else {
      $rscadena7 = $rcadena7;
    }
    
    $rcadena8 = $cadena8 * 2;
    if($rcadena8>9){
      $rscadena8 = substr($rcadena8,0,1) + substr($rcadena8,1,1);
    }
    
    else {
      $rscadena8 = $rcadena8;
    }
    
    $rcadena9 = $cadena9 * 1;
    if($rcadena9>9){
      $rscadena9 = substr($rcadena9,0,1) + substr($rcadena9,1,1);
    }
    else {
      $rscadena9 = $rcadena9;
    }
    
    $rcadena10 = $cadena10 * 2;
    if($rcadena10>9){
      $rscadena10 = substr($rcadena10,0,1) + substr($rcadena10,1,1);
    }
    else {
      $rscadena10 = $rcadena10;
    }
    
    $rcadena11 = $cadena11 * 1;
    if($rcadena11>9){
      $rscadena11 = substr($rcadena11,0,1) + substr($rcadena11,1,1);
    }
    else {
      $rscadena11 = $rcadena11;
    }
    
    $rcadena12 = $cadena12 * 2;
    if($rcadena12>9){
      $rscadena12 = substr($rcadena12,0,1) + substr($rcadena12,1,1);
    }
    else {
      $rscadena12 = $rcadena12;
    }
    
    $rcadena13 = $cadena13 * 1;
    if($rcadena13>9){
      $rscadena13 = substr($rcadena13,0,1) + substr($rcadena13,1,1);
    }
    else {
      $rscadena13 = $rcadena13;
    }
    
    $rcadena14 = $cadena14 * 2;
    if($rcadena14>9){
      $rscadena14 = substr($rcadena14,0,1) + substr($rcadena14,1,1);
    }
    else {
      $rscadena14 = $rcadena14;
    }
    
    $rcadena15 = $cadena15 * 1;
    if($rcadena15>9){
      $rscadena15 = substr($rcadena15,0,1) + substr($rcadena15,1,1);
    }
    else {
      $rscadena15 = $rcadena15;
    }
    
    $rcadena16 = $cadena16 * 2;
    if($rcadena16>9){
      $rscadena16 = substr($rcadena16,0,1) + substr($rcadena16,1,1);
    }
    else {
      $rscadena16 = $rcadena16;
    }
    
    $rcadena17 = $cadena17 * 1;
    if($rcadena17>9){
      $rscadena17 = substr($rcadena17,0,1) + substr($rcadena17,1,1);
    }
    else {
      $rscadena17 = $rcadena17;
    }
    
    $rcadena18 = $cadena18 * 2;
    if($rcadena18>9){
      $rscadena18 = substr($rcadena18,0,1) + substr($rcadena18,1,1);
    }
    else {
      $rscadena18 = $rcadena18;
    }
    
    
    $cadenaf = $rscadena1+$rscadena2+$rscadena3+$rscadena4+$rscadena5+$rscadena6+$rscadena7+$rscadena8+$rscadena9+$rscadena10+$rscadena11+$rscadena12+$rscadena13+$rscadena14+
          $rscadena15+$rscadena16+$rscadena17+$rscadena18;
    
    $cadenafinal = ceil($cadenaf/10)*10;
    $digito_verificador = $cadenafinal - $cadenaf;
    $linea_captura = $r.$digito_verificador;
    
    return $linea_captura;
    
    
      }

      public function name($mes){
        
        if ($mes=="January") $mes="enero";
        if ($mes=="February") $mes="febrero";
        if ($mes=="March") $mes="marzo";
        if ($mes=="April") $mes="abril";
        if ($mes=="May") $mes="mayo";
        if ($mes=="June") $mes="junio";
        if ($mes=="July") $mes="julio";
        if ($mes=="August") $mes="agosto";
        if ($mes=="September") $mes="septiembre";
        if ($mes=="October") $mes="octubre";
        if ($mes=="November") $mes="noviembre";
        if ($mes=="December") $mes="diciembre";

        return $mes;
      }

}//class
?>
