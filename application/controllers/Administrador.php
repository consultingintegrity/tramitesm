<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @author Guadalupe Valerio
 * @version 2.0
 */
class Administrador extends CI_Controller
{
  public function __construct()
  {
      parent::__construct();
      $this->load->model(array('M_Catalogo',"Solicitud_M","Predial_M"));
      $this->load->model("Funcionario");
      $this->load->library('CorreoElectronico');
      //librerias
      $this->load->library(array('Validation','Json'));
      if (!$this->session->userdata('logged_in')) {
        $this->session->sess_destroy();
        redirect(base_url()."Auth","refresh");
      }//if
      else {
        if ($this->session->userdata('rol')[0]->nombre != "Administrador"){
          $this->session->sess_destroy();
          redirect(base_url()."Auth","refresh");
        }
      }//else
  }//construct

    public function index($opt = "",$id_persona = "")
    {
      //cargamos las vistas del administrador
      switch ($opt) {
        case '':
          $currrent = date("d-m-Y");
          $str_fecha = $this->validation->formatoFechaDia($currrent);
          $str_fecha .= " " . $this->validation->formatoFechaMes($currrent);
          $str_fecha .= " del " . date("Y");
          $solicitud_revision = $this->Solicitud_M->getAllSolicitud(4,date("m"));//obenemos las solicitudes en revision
          $solicitud_terminada = $this->Solicitud_M->getAllSolicitud(1,date("m"));//obenemos las solicitudes en terminadas
          $solicitud_cancelada = $this->Solicitud_M->getAllSolicitud(0,date("m"));//obenemos las solicitudes canceladas
          $dinero_generado = $this->Solicitud_M->getDineroGenerado(date("m"));//obenemos el dinero generado
          $solicitudes = $this->Solicitud_M->solicitudes(date("m"));//obenemos las solicitudes
          $solicitudes_progreso = $this->Solicitud_M->progresoSolictud(date("m"));//obenemos el progreo de las  solicitudes
          $documentos_generados = $this->Solicitud_M->getCountdocumentosGenerados(date("m"));//obenemos los documentos generados de las solicitudes
          $visitas = (int)$this->Solicitud_M->contarVisitas();
          $data = array(
            'navBar' => "Dashboard", "fecha" => $this->validation->formatoFechaMes(date("M"))." del " . date("Y"),
            'solicitudes_proceso' => count($solicitud_revision), 'solicitudes_terminadas' => count($solicitud_terminada), 'solicitudes_canceladas' => count($solicitud_cancelada), 'dinero_generado' => $dinero_generado,
            'solicitudes'=> $solicitudes, "progreso_solicitudes" => $solicitudes_progreso, "documentos_generados" => $documentos_generados,"Visitas_total"=>$visitas);
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/content',$data);
          $this->load->view('administrador/footer');
          $this->load->view('scripts/ws-admin');
          $this->load->view('administrador/js');
          $this->load->view('scripts/admin');
          break;
        case 'registro':
          //Cargamos los estados
          $estados = $this->M_Catalogo->getEstados();
          //cargamos los roles
          $rol = $this->M_Catalogo->getRol();
          //cargamos la direcciones
          $direccion = $this->M_Catalogo->getDireccion();

          $data = array('navBar' => $opt, 'estados' => $estados, 'rol' => $rol, 'direcciones' => $direccion);
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/funcionarios/content-registro',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/catalogo');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'consultar':
          $funcionarios = $this->Funcionario->getFun();
          $data = array('navBar' => $opt, 'funcionarios' => $funcionarios);
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/funcionarios/content-consultar',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/catalogo');
          $this->load->view('scripts/ws-admin');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'editar':
          //Cargamos los estados
          $estados = $this->M_Catalogo->getEstados();
          //cargamos los roles
          $rol = $this->M_Catalogo->getRol();
          //cargamos la direcciones
          $direccion = $this->M_Catalogo->getDireccion();
          //Obtenemos el id del funcionario
          $funcionario  = $this->Funcionario->getFunId($id_persona);
          $data = array('navBar' => $opt, 'estados' => $estados, 'rol' => $rol, 'direcciones' => $direccion, 'funcionario' => $funcionario);

          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/funcionarios/content-registro',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/catalogo');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'seguimiento-soilcitud':
          $solicitud_terminada = $this->Solicitud_M->getAllSolicitud(1,date("m"));//obenemos las solicitudes en terminadas
          $solicitud_cancelada = $this->Solicitud_M->getAllSolicitud(0,date("m"));//obenemos las solicitudes canceladas
          $solicitudes = $this->Solicitud_M->solicitudes(date("m"));//obenemos las solicitudes de las solicitudes

          $data = array(
            'navBar' => $opt,
            'solicitudes_terminadas' => count($solicitud_terminada), 'solicitudes_canceladas' => count($solicitud_cancelada),
            'solicitudes'=> $solicitudes);
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/historial/seguimiento-solicitud',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'historial-solicitudes':
          $solicitud_terminada = $this->Solicitud_M->getCountSolicitudes(1);//obenemos las solicitudes terminadas
          $solicitud_cancelada = $this->Solicitud_M->getCountSolicitudes(0);//obenemos las solicitudes canceladas
          $solicitud_proceso = $this->Solicitud_M->getCountSolicitudes(4);//obenemos las solicitudes en proceso
          $solicitud_caja = $this->Solicitud_M->getCountSolicitudes(3);//obenemos las solicitudes en espera en caja
          $solicitudes = $this->Solicitud_M->solicitudes();//obenemos las solicitudes de las solicitudes

          $data = array(
            'navBar' => $opt,
            'solicitudes_terminadas' => $solicitud_terminada, 'solicitudes_canceladas' => $solicitud_cancelada,
            'solicitudes_proceso' => $solicitud_proceso, 'solicitudes_caja' => $solicitud_caja,
            'solicitudes'=> $solicitudes);
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/historial/historial-solicitud',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'solicitud-expediente':
          $id_solicitud = $id_persona;//Obtenemos el segundo parmetro que sería el Id de la solicitud :v.... Esto para no agregar un tercer parametro

          //Obtenemos los requisitos de la solicitud
          $requisitos_solicitud = $this->Solicitud_M->getRequisitosSolicitud($id_solicitud);
          //obteneoms el estatus actual de la solicitud :v
          $estatus = $this->Solicitud_M->getSolicitudCosto($id_solicitud)->estatus;
          //obtenemos la información de la solicitud
          $info_solicitud = $this->Solicitud_M->infoSolicitud($id_solicitud,$estatus);
          //Obtenemos los funcionarios que revisarón una solicitud
          $funcionarios_solicitud = $this->Solicitud_M->seguimientoFuncionarios($id_solicitud);


          $data = array(
            'navBar' => $opt,
            'requisitos_solicitud' => $requisitos_solicitud, "info_solicitud" => $info_solicitud, "funcionarios_solicitud" => $funcionarios_solicitud);
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/historial/expediente-solicitud',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'perfil-info':
          $data = array('navBar' => "Perfil", "title" => "Perfil");
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/perfil-info',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/catalogo');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'cambio-contrasenia':
          $data = array('navBar' => "Contraseña", "title" => "Contraseña");
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/perfil-info',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/catalogo');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'imagen-perfil':
          $data = array('navBar' => "Imagen de Perfil", "title" => "Imagen de Perfil");
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/perfil-info',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/catalogo');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          break;
        case 'historial-tramites':
          $data = array(
            'navBar' => $opt);
          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/historial/historial-tramites',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/ws-admin');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          $this->load->view('scripts/charts');
          break;

        case 'historial-tramites-licencia-construccion':
          $data = array(
            'navBar' => $opt);
          //cargaos la información de los trámites
          $solicitud_construccion = $this->Solicitud_M->infoSolicitudByTramite(7);//obenemos las solicitudes de licencia de construcción
          $data["infoSolicitud"] = $solicitud_construccion;

          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/historial/historial-tramites-licenci-construccion.php',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/ws-admin');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          $this->load->view('scripts/charts');
          break;

        case 'historial-tramites-licencia-dictamen-uso-suelo':
          $data = array(
            'navBar' => $opt);
          //cargaos la información de los trámites
          $solicitudes_tipo_dictamen = $this->Solicitud_M->getSolicitudSubTramite(2);//obenemos las solicitudes de licencia de construcción
          $data["tipoDictamen"] = $solicitudes_tipo_dictamen;


          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/historial/historial-tramites-licencia-dictamen-uso-suelo.php',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/ws-admin');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          $this->load->view('scripts/charts');
          break;
          //se cargan vistas y js para historial de predial
          case 'historial-predial':
          //array
          $prediales = $this->Predial_M->consultaTodo();
          $data = array(
            'navBar' => $opt,
            'Predial' => $prediales
          );

          $this->load->view('administrador/head',$data);
          $this->load->view('administrador/header');
          $this->load->view('administrador/sidebar',$data);
          $this->load->view('administrador/header-desktop');
          $this->load->view('administrador/historial/historial-predial.php',$data);
          $this->load->view('administrador/footer');
          $this->load->view('administrador/js');
          $this->load->view('scripts/ws-admin');
          $this->load->view('scripts/admin');
          $this->load->view('scripts/utils');
          $this->load->view('scripts/charts');
          break;
        default:
          $data = array('heading' => "Error 404 Página no encontrada", 'message' => "La página que estas solicitando no existe." );
          $this->load->view('errors/html/error_404',$data);
          break;
      }//switch
    } //fin de la funcion index
    //funcion que actualiza el status (eliminar) de un usuario en base a el id de persona
    public function actualizaStatus($id_usuario,$estatus)
    {
      $estatus = ($estatus == 1) ? 0 : 1;


        //se carga el modelo de funcionario
        $this->load->model('Funcionario');
        //Llamamos la funcion de actualizar status  que retorna un true o false
        if ($this->Funcionario->actualizaStatus($id_usuario,$estatus)) {
            redirect(base_url() . "Administrador/index/consultar", '');
        } //fin del if de mandar a llamar la funcion del modelo actualiza status



    } //fin de la funcion actualizar status
    public function enviarCorreo()
    {
        $CorreoFuncionario = (!empty($this->input->post("CorreoFuncionario"))) ? addslashes($this->input->post("CorreoFuncionario")) : "";
        $txtmsj            = (!empty($this->input->post("txtmsj"))) ? addslashes($this->input->post("txtmsj")) : "";
        $json_response = array();
        $header=base_url() . 'plantilla/images/correo/header.png';
        $logos=base_url() . 'plantilla/images/correo/logos.png';
        //se carga la libreria de correo electronico
        $this->load->library('CorreoElectronico');
        $contenido = '<body style="font-family: Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;">
                                    <div style="margin-top: 0;margin-bottom: 0;margin-left: auto;margin-right: auto;background-color: #F2F2F2;width: 600px;min-height: 500px;position: relative">';
        $contenido .=' <header> <img src="';
        $contenido .=  $header;
        $contenido .='" width="600" height="auto"></header>
                        <div style="padding:40px">
                            <h2>Mensaje de administrador</h2>
                            <p style="text-align:justify; line-height: 2em;">';
        //se concatena el contenido de el text area a la variable contenido
        $contenido .= $txtmsj;
        $contenido .= '</p>';
        $contenido .= '<div align="center"><img src="';
        $contenido .=  $logos;
        $contenido .= '" width="215" height="107"></div>
                  </div>


                                           <div style="position:relative; left: 0; bottom: 0; width: 100%; background-color: black; color: white;text-align: center;font-size: 8px;">
                            <div style="padding:40px">
                              <table width="100%" border="0">
                                <tbody>
                                  <tr>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Atención Ciudadana</strong><br><br> Queretarp</p></td>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Síguenos en redes sociales</strong><br><br> </p></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>

                        </div>
                      </body>
                    ';
         //if para comprobar que el correo se envio con exito
        if ($this->correoelectronico->mandarCorreo($CorreoFuncionario, "Aclaración sobre tramite en curso", $contenido)) {
          $json_response["status"] = 200;
          $json_response["msg"] = "Se envió con exito el mensaje.";
          $json_response["title"] = "Envio de correo.";
        } else {
          $json_response["status"] = 400;
          $json_response["msg"] = "Ocurrio un error al mandar el correo electrónico";
          $json_response["title"] = "Envío de correo.";
        }
        echo json_encode($json_response);
    } //fin de la funcion enviar correo


    //funcion que modifica un funcionario existente
    public function modificar_fun()
    {
        //se resiben los datos del formulario y se guardan en una variable que mas tarde se ocupara
        $txtPrimerNom  = (!empty($this->input->post("txtPrimerNom"))) ? addslashes($this->input->post("txtPrimerNom")) : "";
        $txtSegundoNom = (!empty($this->input->post("txtSegundoNom"))) ? addslashes($this->input->post("txtSegundoNom")) : "";
        $txtPrimerAp   = (!empty($this->input->post("txtPrimerAp"))) ? addslashes($this->input->post("txtPrimerAp")) : "";
        $txtSegundoAp  = (!empty($this->input->post("txtSegundoAp"))) ? addslashes($this->input->post("txtSegundoAp")) : "";
        $txtCorreo     = (!empty($this->input->post("txtCorreo"))) ? addslashes($this->input->post("txtCorreo")) : "";
        $optGenero     = (!empty($this->input->post("optGenero"))) ? addslashes($this->input->post("optGenero")) : "";
        $txtFecha      = (!empty($this->input->post("txtFecha"))) ? addslashes($this->input->post("txtFecha")) : "";
        $optRol        = (!empty($this->input->post("optRol"))) ? addslashes($this->input->post("optRol")) : "";
        $optMunicipio  = (!empty($this->input->post("optMunicipio"))) ? addslashes($this->input->post("optMunicipio")) : "";
        $txtCalle      = (!empty($this->input->post("txtCalle"))) ? addslashes($this->input->post("txtCalle")) : "";
        $txtColonia    = (!empty($this->input->post("txtColonia"))) ? addslashes($this->input->post("txtColonia")) : "";
        $txtNumeroI    = (!empty($this->input->post("txtNumeroI"))) ? addslashes($this->input->post("txtNumeroI")) : "";
        $txtNumeroE    = (!empty($this->input->post("txtNumeroE"))) ? addslashes($this->input->post("txtNumeroE")) : "";
        $id_persona    = (!empty($this->input->post("persona"))) ? addslashes($this->input->post("persona")) : "";
        $id_usuario    = (!empty($this->input->post("funcionario"))) ? addslashes($this->input->post("funcionario")) : "";
        $txtCorreoActual    = (!empty($this->input->post("txtCorreoActual"))) ? addslashes($this->input->post("txtCorreoActual")) : "";
        $txtMovil    = (!empty($this->input->post("txtMovil"))) ? addslashes($this->input->post("txtMovil")) : "";
        $txtTelOficina    = (!empty($this->input->post("txtTelOficina"))) ? addslashes($this->input->post("txtTelOficina")) : "";
        $txtExtension    = (!empty($this->input->post("txtExtension"))) ? addslashes($this->input->post("txtExtension")) : "";
        $optDireccion    = (!empty($this->input->post("optDireccion"))) ? addslashes($this->input->post("optDireccion")) : "";
        $optDependencia    = (!empty($this->input->post("optDependencia"))) ? addslashes($this->input->post("optDependencia")) : "";

        //array de validacion para los campos resividos del formulario
        $arr_validations = [
          array(
              "field" => "txtPrimerNom",
              "label" => "Primer Nombre",
              "rules" => "required|max_length[65]|trim",
          ),
          array(
              "field" => "txtSegundoNom",
              "label" => "Segundo Nombre",
              "rules" => "max_length[65]|trim",
          ),
          array(
              "field" => "txtPrimerAp",
              "label" => "Primer Apellido",
              "rules" => "required|max_length[65]|trim",
          ),
          array(
              "field" => "txtSegundoAp",
              "label" => "Segundo Apellido",
              "rules" => "max_length[65]|trim",
          ),
          array(
              "field" => "optGenero",
              "label" => "Genero",
              "rules" => "required|trim|in_list[m,M,f,F]",
          ),
          array(
              "field" => "txtFecha",
              "label" => "Fecha de nacimiento",
              "rules" => "required|max_length[65]|trim|min_length[6]",
          ),
          array(
              "field" => "optMunicipio",
              "label" => "Municipio",
              "rules" => "required|trim|is_natural",
          ),
          array(
              "field" => "txtCalle",
              "label" => "Calle",
              "rules" => "required|trim",
          ),
          array(
              "field" => "txtColonia",
              "label" => "Colonia",
              "rules" => "required|trim",
          ),
          array(
              "field" => "txtNumeroI",
              "label" => "Numero Interior",
              "rules" => "trim",
          ),
          array(
              "field" => "txtNumeroE",
              "label" => "Numero Exterior",
              "rules" => "required|is_natural|trim",
          ),
          array(
              "field" => "optRol",
              "label" => "Rol",
              "rules" => "required|trim|is_natural",
          ),
        ];
        //array que guarda los mensajes correspondientes a las respectivas validaciones
        $arr_msg = [
          array(
                "required"             => "EL campo %s es requerido para continuar",
                "valid_email"          => "EL campo %s debe ser un correo electrónco valido",
                "min_length[2]"        => "El campo %s debe tener mas de 2 caracteres",
                "min_length[6]"        => "El campo %s debe tener mas de 6 caracteres", "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
                "is_unique"            => "Error ese %s ya se encuentra registrado",
                "valid_date"           => "El campo %s debe estar en formato correcto",
                "is_natural"           => "El campo %s solo puede contener numeros",
                "alpha_numeric_spaces" => "El campo %s solo puede contener letras numeros y espacios",

            ),
        ];
        //if de validacion que comprueba si el campo correo electronico se modifico
        if ($txtCorreo != $txtCorreoActual) {
          //se hacen las respectivas validaciones de correo
          $this->form_validation->set_rules('txtCorreo', 'correo electrónico', 'required|valid_email|max_length[65]|trim|is_unique[usuario.correo_electronico]',
              array(
                  "required"       => "EL campo %s es requerido para continuar",
                  "valid_email"    => "EL campo %s debe ser un correo electrónico valido",
                  "min_length[7]"  => "El campo %s debe tener mas de 7 caracteres",
                  "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
                  "is_unique"      => "Error ese %s ya se encuentra registrado",
              )
          ); //set rules
        } //if  validacion es el mismo correo
        //se carga la libreria validation
        $this->load->library('validation');
        //se guarda el resusltado de lla funcion de valid for en una variable (true o false)
        $form_validation = $this->validation->validForm($arr_validations, $arr_msg);
        //validacion de varios campos para saber si estan en formato correcto mediante una funcion llamada acento

        //creamos variable para guardar el json Response
        $json_response = array();
        if ($this->validation->acento($txtPrimerNom)) {
          $json_response["status"] = 400;
          $json_response["msg"] = "El campo primer nombre solo debe contener letras.";
          $json_response["title"] = "Error de validación.";
        } //if validacion de nombre1
        elseif ($this->validation->acento($txtSegundoNom) && $txtSegundoNom != null) {
          $json_response["status"] = 400;
          $json_response["msg"] = "El campo segundo nombre solo debe contener letras.";
          $json_response["title"] = "Error de validación.";
        } //elseif validacion de nombre 2
        elseif ($this->validation->acento($txtPrimerAp)) {
          $json_response["status"] = 400;
          $json_response["msg"] = "El campo primer apellido solo debe contener letras.";
          $json_response["title"] = "Error de validación.";
        } //elseif validacion ape1
        elseif ($this->validation->acento($txtSegundoAp) && $txtSegundoAp != null) {
          $json_response["status"] = 400;
          $json_response["msg"] = "El campo segundo apellido solo debe contener letras.";
          $json_response["title"] = "Error de validación.";
        } //elseif validacion ape2
        elseif ($this->validation->fecha($txtFecha) == false) {
          $json_response["status"] = 400;
          $json_response["msg"] = "El campo fecha debe estar en formato correcto.";
          $json_response["title"] = "Error de validación.";
        } //elseif validacion fecha
        //inicio de el else de las validaciones de los campos anteriores
        else {
          //if que valida el return del form validations
          if ($form_validation) {
            //se carga el modelo de funcionario
            $this->load->model('Funcionario');
            //Llamamos la funcion de registro
            if ($this->Funcionario->modFun($txtPrimerNom, $txtSegundoNom, $txtPrimerAp, $txtSegundoAp, $optGenero, $optMunicipio, $id_persona, $txtCorreo, $optRol,
                                            $txtNumeroI, $txtNumeroE, $txtCalle, $txtColonia, $txtFecha, $id_usuario,
                                            $txtMovil, $txtTelOficina, $txtExtension, $optDireccion, $optDependencia)) {

              $json_response["status"] = 200;
              $json_response["msg"] = "Se modificó la información del funcionario.";
              $json_response["title"] = "Modificación Exitosa.";
            } //fin del if de registro de funcionario

          } //fin del if de validacion form
          //else del form validation
          else {
            //se envian los errores  del form validation
            $json_response["status"] = 400;
            $json_response["msg"] =  validation_errors();
            $json_response["title"] = "Error de validación.";
          } //fin del else de form validations
        } //fin del else validaciones acento

        echo json_encode($json_response);
    } //fin de la funcion modificar funcionario

    //funcion que me permite cambiar la contraseña
    public function cambiarContrasenia()
    {

        ////se resiven los datos del formulario y se guardan en una variable que mas tarde se ocupara
        $txtPwd         = (!empty($this->input->post("txtPwd"))) ? addslashes($this->input->post("txtPwd")) : "";
        $txtConfirmPass = (!empty($this->input->post("txtConfirmPwd"))) ? addslashes($this->input->post("txtConfirmPwd")) : "";
        $id_usuario     = (!empty($this->input->post("id_usuario"))) ? addslashes($this->input->post("id_usuario")) : "";
        $json_response = array();
        //array de validacion para los campos resibidos del formulario
        $arr_validations = [
          array(
              "field" => "txtPwd",
              "label" => "Contraseña",
              "rules" => "required|min_length[6]|matches[txtConfirmPwd]",
          ),
          array(
              "field" => "txtConfirmPwd",
              "label" => "Confirmar Contraseña",
              "rules" => "required|min_length[6]",
          ),
        ];
        //array que guarda los mensajes correspondientes a las respectivas validaciones
        $arr_msg = [
          array(
            "required"      => "EL campo %s es requerido para continuar",
            "min_length[6]" => "El campo %s debe tener mas de 6 caracteres",
          ),
        ];
        //se carga la libreria validation
        $this->load->library('validation');
        //se guarda el resusltado de lla funcion de valid for en una variable (true o false)
        $form_validation = $this->validation->validForm($arr_validations, $arr_msg);
        //if que valida el return del form validations
        if ($form_validation) {
          //se carga el modelo de funcionario
          $this->load->model('Funcionario');
          //Llamamos la funcion de cambiarcontra
          if ($this->Funcionario->cambiarcontra(hash('sha512', $txtPwd), $id_usuario)) {

            //Mandamos correo electronico al funcionario
            $subTitulo = "Cambio de contraseña";
            //cargamos modelo de usuario para obtener el correo del ciudadano
            $this->load->model('Usuario');
            //obtenemos información de usuario
            $usuario = $this->Usuario->getUsuario($id_usuario);
            //obtenemos información del Trámite
            $texto = "¡Hola " . $usuario->primer_nombre . "! " . "Tu contraseña fue modificada por el administrador. Tu nueva contraseña es: ". $txtConfirmPass .". No la compartas con nadie";
            $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
            $direccion = $usuario->correo_electronico;
            //mandamos correo eléctronico
            //se comenta porque el correo no se probo
            //$this->correoelectronico->mandarCorreo($direccion, "", $contenido_mail);

            $json_response["status"] = 200 ;
            $json_response["msg"] = "Se modificó la contraseña del funcionario." ;
            $json_response["title"] = "Modifiación Exitosa." ;
          } //fin if de la funcion cambiar contraseña
        } //fin del if form validacion
        //else del form validation
        else {
          $json_response["status"] = 200 ;
          $json_response["msg"] = validation_errors();
          $json_response["title"] = "Error de Validación";
        } //fin del else de validaciones de form_validation
        echo json_encode($json_response);
    } //fin de la funcion modContra
    //funcion que me permite registrar un nuevo funcionario
    public function registrar_fun()
    {
      //se resiben los datos del formulario y se guardan en una variable que mas tarde se ocupara
      $txtPrimerNom       = (!empty($this->input->post("txtPrimerNom"))) ? addslashes($this->input->post("txtPrimerNom")) : "";
      $txtSegundoNom      = (!empty($this->input->post("txtSegundoNom"))) ? addslashes($this->input->post("txtSegundoNom")) : "";
      $txtPrimerAp        = (!empty($this->input->post("txtPrimerAp"))) ? addslashes($this->input->post("txtPrimerAp")) : "";
      $txtSegundoAp       = (!empty($this->input->post("txtSegundoAp"))) ? addslashes($this->input->post("txtSegundoAp")) : "";
      $txtCorreo          = (!empty($this->input->post("txtCorreo"))) ? addslashes($this->input->post("txtCorreo")) : "";
      $optGenero          = (!empty($this->input->post("optGenero"))) ? addslashes($this->input->post("optGenero")) : "";
      $txtContra          = (!empty($this->input->post("txtContra"))) ? addslashes($this->input->post("txtContra")) : "";
      $txtFecha           = (!empty($this->input->post("txtFecha"))) ? addslashes($this->input->post("txtFecha")) : "";
      $optRol             = (!empty($this->input->post("optRol"))) ? addslashes($this->input->post("optRol")) : "";
      $optMunicipio       = (!empty($this->input->post("optMunicipio"))) ? addslashes($this->input->post("optMunicipio")) : "";
      $txtCalle           = (!empty($this->input->post("txtCalle"))) ? addslashes($this->input->post("txtCalle")) : "";
      $txtColonia         = (!empty($this->input->post("txtColonia"))) ? addslashes($this->input->post("txtColonia")) : "";
      $txtNumeroI         = (!empty($this->input->post("txtNumeroI"))) ? addslashes($this->input->post("txtNumeroI")) : "";
      $txtNumeroE         = (!empty($this->input->post("txtNumeroE"))) ? addslashes($this->input->post("txtNumeroE")) : "";
      $txtConfirmarContra = (!empty($this->input->post("txtConfirmarContra"))) ? addslashes($this->input->post("txtConfirmarContra")) : "";
      $txtMovil           = (!empty($this->input->post("txtMovil"))) ? addslashes($this->input->post("txtMovil")) : "";
      $txtTelOficina      = (!empty($this->input->post("txtTelOficina"))) ? addslashes($this->input->post("txtTelOficina")) : "";
      $txtExtension       = (!empty($this->input->post("txtExtension"))) ? addslashes($this->input->post("txtExtension")) : "";
      $id_dependencia     = (!empty($this->input->post("optDependencia"))) ? addslashes($this->input->post("optDependencia")) : "";

      //array de validacion para los campos resibidos del formulario
      $arr_validations = [

                array(
                    "field" => "txtPrimerNom",
                    "label" => "Primer Nombre",
                    "rules" => "required|max_length[65]|trim",
                ),
                array(
                    "field" => "txtSegundoNom",
                    "label" => "Segundo Nombre",
                    "rules" => "max_length[65]|trim",
                ),
                array(
                    "field" => "txtPrimerAp",
                    "label" => "Primer Apellido",
                    "rules" => "required|max_length[65]|trim",
                ),
                array(
                    "field" => "txtSegundoAp",
                    "label" => "Segundo Apellido",
                    "rules" => "max_length[65]|trim",
                ),
                array(
                    "field" => "txtCorreo",
                    "label" => "Correo Electronico",
                    "rules" => "required|max_length[65]|trim|valid_email|is_unique[usuario.correo_electronico]",

                ),
                array(
                    "field" => "txtConfirmarContra",
                    "label" => "Confirmar Contraseña",
                    "rules" => "required|min_length[6]",
                ),
                array(
                    "field" => "txtContra",
                    "label" => "Contraseña",
                    "rules" => "required|min_length[6]|matches[txtConfirmarContra]",
                ),
                array(
                    "field" => "optGenero",
                    "label" => "Genero",
                    "rules" => "required|trim|in_list[m,M,f,F]",
                ),
                array(
                    "field" => "txtFecha",
                    "label" => "Fecha de nacimiento",
                    "rules" => "required|max_length[65]|trim|min_length[6]",
                ),
                array(
                    "field" => "optMunicipio",
                    "label" => "Municipio",
                    "rules" => "required|trim|is_natural",
                ),
                array(
                    "field" => "txtCalle",
                    "label" => "Calle",
                    "rules" => "required|alpha_numeric_spaces|trim",
                ),
                array(
                    "field" => "txtColonia",
                    "label" => "Colonia",
                    "rules" => "required|trim",
                ),
                array(
                    "field" => "txtNumeroI",
                    "label" => "Numero Interior",
                    "rules" => "trim",
                ),
                array(
                    "field" => "txtNumeroE",
                    "label" => "Numero Exterior",
                    "rules" => "required|trim",
                ),
                array(
                    "field" => "optRol",
                    "label" => "Rol",
                    "rules" => "required|trim|is_natural",
                ),];
      //array que guarda los mensajes correspondientes a las respectivas validaciones
      $arr_msg = [
          array(
              "required"      => "EL campo %s es requerido para continuar",
              "valid_email"   => "EL campo %s debe ser un correo electrónco valido",
              "min_length[2]" => "El campo %s debe tener mas de 2 caracteres",
              "min_length[6]" => "El campo %s debe tener mas de 6 caracteres", "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
              "is_unique"     => "Error ese %s ya se encuentra registrado",
              "valid_date"    => "El campo %s debe estar en formato correcto",
              "is_natural"    => "El campo %s solo puede contener numeros",
          ),
      ];
      //se carga la libreria validation
      $this->load->library('validation');
      //se guarda el resusltado de lla funcion de valid for en una variable (true o false)
      $form_validation = $this->validation->validForm($arr_validations, $arr_msg);
      //creamos variable para guardar el json Response
      $json_response = array();
      //validacion de varios campos para saber si estan en formato correcto mediante una funcion llamada acento
      if ($this->validation->acento($txtPrimerNom)) {
        $json_response["msg"] = "El campo primer nombre solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
        $json_response["status"] = 400;
      } //if validacion de nombre1
      elseif ($this->validation->acento($txtSegundoNom) && $txtSegundoNom != null) {
        $json_response["msg"] = "El campo segundo nombre solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
        $json_response["status"] = 400;
      } //elseif validacion de nombre 2
      elseif ($this->validation->acento($txtPrimerAp)) {
        $json_response["msg"] = "El campo primer apellido solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
        $json_response["status"] = 400;
      } //elseif validacion ape1
      elseif ($this->validation->acento($txtSegundoAp) && $txtSegundoAp != null) {
        $json_response["msg"] = "El campo segundo apellido solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
        $json_response["status"] = 400;
      } //elseif validacion ape2
      elseif ($this->validation->fecha($txtFecha) == false) {
        $json_response["msg"] = "El campo fecha debe estar en formato correcto.";
        $json_response["title"] = "Error de validación.";
        $json_response["status"] = 400;
        $this->session->set_flashdata('error', "");
      } //elseif validacion fecha
      //inicio de el else de las validaciones de los campos anteriores
      else {
        //if que valida el return del form validations
        if ($form_validation) {
            //se carga el modelo de funcionario
            $this->load->model('Funcionario');
            //Llamamos la funcion de registro
            if ($this->Funcionario->registro($txtPrimerNom, $txtSegundoNom, $txtPrimerAp, $txtSegundoAp, $optGenero, $optMunicipio, $txtCorreo, hash('sha512', $txtContra), $optRol, $txtNumeroI, $txtNumeroE, 1, $txtCalle, $txtColonia, $txtFecha, $txtMovil, $txtTelOficina, $txtExtension, $id_dependencia)) {
                //mandamos un mensaje mediante flashdata
                $this->session->set_flashdata('good', " ");
                $json_response["msg"] = "Se mandó un correo electrónico con los datos de acceso a la plataforma al correo que registró.";
                $json_response["title"] = "Registro Exitoso";
                $json_response["status"] = 200;
                //Mandamos correo electronico al funcionario
                $subTitulo = "Bienvenido a la plataforma TX Digital";
                //obtenemos información del Trámite
                $texto = "¡Hola " . $txtPrimerNom . "! " . "Tu acceso a la plataforma es el siguiente. Correo electrónico: ".$txtCorreo." Contraseña: ".$txtContra.". No la compartas con nadie. Acceso a la plataforma: /n" . base_url();
                $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
                $Titulo = "Usted a sido registrado como funcionario";
                //mandamos correo eléctronico
                //se comenta porque no tenemos correo
                //$this->correoelectronico->mandarCorreo($txtCorreo, $Titulo, $contenido_mail);
            } //fin del if de la funcion registro
        } //fin del if de validacion form
        //else del form validation
        else {
          $json_response["msg"] = validation_errors();
          $json_response["title"] = "Error de validación.";
          $json_response["status"] = 400;
        } //fin del else de form validations
      } //fin del else de validacion de acentos
      echo json_encode($json_response);
    } //fin de la funcion registrar funcionario

    /**
     * Modificar información de Adminisradores
     */
    public function updateAdministrador(){
      //Variables con informaión del adminsitrador
      $primer_nombre    = (!empty($this->input->post("txtPrimerNom"))) ? addslashes($this->input->post("txtPrimerNom")) : "";
      $segundo_nombre   = (!empty($this->input->post("txtSegundoNom"))) ? addslashes($this->input->post("txtSegundoNom")) : "";
      $primer_apellido  = (!empty($this->input->post("txtPrimerAp"))) ? addslashes($this->input->post("txtPrimerAp")) : "";
      $segundo_apellido = (!empty($this->input->post("txtSegundoAp"))) ? addslashes($this->input->post("txtSegundoAp")) : "";
      $correo           = (!empty($this->input->post("txtCorreo"))) ? addslashes($this->input->post("txtCorreo")) : "";
      $correo_actual    = (!empty($this->input->post("txtCorreoActual"))) ? addslashes($this->input->post("txtCorreoActual")) : "";
      $optGenero     = (!empty($this->input->post("optGenero"))) ? addslashes($this->input->post("optGenero")) : "";
      $id_persona       = $this->session->userdata("id_persona");

      //array de validaciones
      $arr_validations = [
        array(
            "field" => "txtPrimerNom",
            "label" => "Primer Nombre",
            "rules" => "required|max_length[65]|trim",
        ),
        array(
            "field" => "txtSegundoNom",
            "label" => "Segundo Nombre",
            "rules" => "max_length[65]|trim",
        ),
        array(
            "field" => "txtPrimerAp",
            "label" => "Primer Apellido",
            "rules" => "required|max_length[65]|trim",
        ),
        array(
            "field" => "txtSegundoAp",
            "label" => "Segundo Apellido",
            "rules" => "max_length[65]|trim",
        ),
        array(
            "field" => "optGenero",
            "label" => "Genero",
            "rules" => "required|trim|in_list[m,M,f,F]",
        )
      ];
      //array que guarda los mensajes correspondientes a las respectivas validaciones
      $arr_msg = [
        array(
          "required"             => "EL campo %s es requerido para continuar",
          "valid_email"          => "EL campo %s debe ser un correo electrónco valido",
          "min_length[2]"        => "El campo %s debe tener mas de 2 caracteres",
          "min_length[6]"        => "El campo %s debe tener mas de 6 caracteres", "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
          "is_unique"            => "Error ese %s ya se encuentra registrado",
          "valid_date"           => "El campo %s debe estar en formato correcto",
          "is_natural"           => "El campo %s solo puede contener numeros",
          "alpha_numeric_spaces" => "El campo %s solo puede contener letras numeros y espacios",
          ),
      ];
      //validaos el correo electrónico
      if ($correo != $correo_actual) {
        //se hacen las respectivas validaciones de correo
        $this->form_validation->set_rules('txtCorreo', 'correo electrónico', 'required|valid_email|max_length[65]|trim|is_unique[usuario.correo_electronico]',
            array(
                "required"       => "EL campo %s es requerido para continuar",
                "valid_email"    => "EL campo %s debe ser un correo electrónico valido",
                "min_length[7]"  => "El campo %s debe tener mas de 7 caracteres",
                "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
                "is_unique"      => "Error ese %s ya se encuentra registrado",
            )
        ); //set rules
      } //if  validacion es el mismo correo
      //se carga la libreria validation
      $this->load->library('validation');
      //se guarda el resusltado de lla funcion de valid for en una variable (true o false)
      $form_validation = $this->validation->validForm($arr_validations, $arr_msg);

      //creamos variable para guardar el json Response
      $json_response = array();
      if ($this->validation->acento($primer_nombre)) {
        $json_response["status"] = 400;
        $json_response["msg"] = "El campo primer nombre solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
      } //if validacion de nombre1
      elseif ($this->validation->acento($segundo_nombre) && $segundo_nombre != null) {
        $json_response["status"] = 400;
        $json_response["msg"] = "El campo segundo nombre solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
      } //elseif validacion de nombre 2
      elseif ($this->validation->acento($primer_apellido)) {
        $json_response["status"] = 400;
        $json_response["msg"] = "El campo primer apellido solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
      } //elseif validacion ape1
      elseif ($this->validation->acento($segundo_apellido) && $segundo_apellido != null) {
        $json_response["status"] = 400;
        $json_response["msg"] = "El campo segundo apellido solo debe contener letras.";
        $json_response["title"] = "Error de validación.";
      } //elseif validacion ape2
      //inicio de el else de las validaciones de los campos anteriores
      else {
        //if que valida el return del form validations
        if ($form_validation) {
          //Llamamos la funcion de registro
          if ($this->Funcionario->actualizarInfoAdministrador($primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido, $optGenero,$id_persona, $correo)) {
            //actualizamos los datos de sesión
            $this->session->set_userdata('primer_nombre',$primer_nombre);
            $this->session->set_userdata('segundo_nombre',$segundo_nombre);
            $this->session->set_userdata('primer_apellido',$primer_apellido);
            $this->session->set_userdata('segundo_apellido',$segundo_apellido);
            $this->session->set_userdata('genero',$optGenero);
            $this->session->set_userdata('correo_electronico',$correo);
            $json_response["status"] = 200;
            $json_response["msg"] = "Se modificó la información.";
            $json_response["title"] = "Modificación Exitosa.";
          } //fin del if de registro de funcionario

        } //fin del if de validacion form
        //else del form validation
        else {
          //se envian los errores  del form validation
          $json_response["status"] = 400;
          $json_response["msg"] =  validation_errors();
          $json_response["title"] = "Error de validación.";
        } //fin del else de form validations
      } //fin del else validaciones acento

      echo json_encode($json_response);

    }//updateAdministrador

    public function imagen(){
      $txtImagenPerfil = ($this->input->post("txtImg") != "") ? addslashes($this->input->post("txtImg")) : "";
      $json_response= [];

      $this->load->library('SubirImagen');
      //Url para guardar la ruta de los archivos

      $urlImgPerfil = "plantilla/images/user-perfil/";
      $this->subirimagen->dir_exist($urlImgPerfil);
      //Verificamos que los directorios donde se guardaran las imagenes existan, en caso contrario se crean antes de subir los archivos

      $this->subirimagen->dir_exist($urlImgPerfil);

      $id_user_ses = $this->session->userdata('id_usuario');

      $nom1_ses1    = $this->session->userdata('primer_nombre');
      $nom1_ses    =    str_replace(' ','_',$nom1_ses1);
      $txtNomIma   = $id_user_ses . "img" . $nom1_ses;

      $ruta1 = $urlImgPerfil . $txtNomIma . ".jpg";
      $ruta2 = $urlImgPerfil . $txtNomIma . ".png";
      //comprobamos si el archvo existe en casi de que exista se elimina
      if (file_exists($ruta1)) {
          unlink($ruta1);
      }
      //se comprueba que el archivo exista en caso de que exita se elimnia
      if (file_exists($ruta2)) {
            unlink($ruta2);
      }
      if (!empty($_FILES["txtImg"]["name"])) {
          $config["upload_path"]   = $urlImgPerfil;
          $config['allowed_types'] = 'jpg|png';
          $config['max_size']      = 525542880;
          $config['file_name']     = $txtNomIma;
          //Subiendo imagen al server
          $respondImg = $this->subirimagen->upload_files($config, "txtImagenPerfil");
          $nameImg    = $respondImg["uploadSuccess"]["file_name"];

      }
      $extImg = $this->upload->data('file_ext');
      $ruta   = $urlImgPerfil . $txtNomIma . $extImg;
      if ($respondImg["valid"]) {
          //Cargamos el modelo para insertar los archivos
          $this->load->model('Usuario');
          if ($this->Usuario->ModImg($ruta, $id_user_ses)) {
            $this->session->userdata('img',$ruta);
            $json_response["status"] = 200;
            $json_response["msg"] = "Se modificó la imagen.";
            $json_response["title"] = "Modificación Exitosa.";
            $json_response["ruta"] = base_url().$ruta;
          }
      } else {
        $json_response["status"] = 400;
        $json_response["msg"] = "Error al modifica la imagen.";
        $json_response["title"] = "Error.";
        $json_response["ruta"] = "Modificación Exitosa.";
      } //else
      echo json_encode($json_response);

    }//imagen

    //elimino funcionario de plataforma solo cambio estatus no puedo borrar por historial
    public function eliminaFuncionario($idFuncionario){
      $funcionario  = $this->Funcionario->getFunId($idFuncionario);
      //se carga el modelo de funcionario
        $estatus = 3;
        $this->load->model('Funcionario');
      if($this->Funcionario->eliminarFuncionario($funcionario->id_usuario,$estatus)){
        redirect(base_url() . "Administrador/index/consultar", '');
      }
    }
} //class
