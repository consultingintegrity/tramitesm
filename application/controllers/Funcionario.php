<?php
/**
 * @author Esau Krack
 * @version 1.0
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionario extends CI_Controller {

	public function index()
	{
		if (!empty($this->session->userdata())
				&& $this->session->userdata('logged_in') && $this->session->userdata('rol')[0]->nombre != "Ciudadano") {
			//Cargamos el modelo de tramites para mostrarlos en la vista
			$this->load->model('TramitesConsulta');
			$dataTramites = $this->TramitesConsulta->ConsultaPendientes($this->session->userdata('id_usuario'),$this->session->userdata('rol')[0]->id);
			//Obtenemos el id de los trámites correspondientes a opinión técnica para permiso de bébidas alcoholicas
			$arrayIdSolicitud = (!empty($this->TramitesConsulta->opinionTecnica($this->session->userdata('id_usuario')))) ? $this->TramitesConsulta->opinionTecnica($this->session->userdata('id_usuario')) : 0;
			//echo $arrayIdSolicitud;
			$arrayIdSolicitud = (!empty($arrayIdSolicitud)) ? $this->obtenerIdSolicitudes($arrayIdSolicitud):  0;
			$dataTramitesOpinionTecnica = $this->TramitesConsulta->solicitudesOpinionTecnica($arrayIdSolicitud);
			//$dataCorte = $this->Funcionario->getCorte();


		//$this->load->model('Funcionario');
	    $this->load->model('Solicitud_M');
			$id_funcionario = $this->session->userdata('id_usuario');
      $pases_caja = $this->Solicitud_M->pasesCaja($id_funcionario);
	  $data["title"] = "Pases a caja";
	  //$data["datosTabla"] = $dataCorte;
			$dataTitle = "Bienvenido!";
			$data = [
				'tramites' => $dataTramites,
				'title' => $dataTitle,
				'pases_caja'=>$pases_caja,
				'solicitud_opinion_tecnica' => $dataTramitesOpinionTecnica
			];

            $this->load->view('head',$data);
            $this->load->view('funcionario/header');
            if ($this->session->userdata('rol')[0]->id != 25) { // Rol encargado de subir base de datos a predio
            	$this->load->view('funcionario/content',$data);
            }//if
            else{
            	$this->load->view('predial/cargarExcel');
            }//else
            			$this->load->view('footer');
						$this->load->view('scripts/js');
						$this->load->view('scripts/pago');
						$this->load->view('scripts/utils');
			      $this->load->view('scripts/evaluacion');
			      $this->load->view('scripts/dashboard');
		}//if
		else{
			$this->session->sess_destroy();
			redirect(base_url()."Auth",'refresh');
		}//else
	}//index

	/**
	 * Muestra solo a rol inspección de protección civil
	 */
	public function correcciones()
	{
		if ($this->session->userdata('rol')[0]->id == 8 && $this->session->userdata('id_dependencia') == 3) {
			//guardamos el id del funcionario actual
			$id_funcionario = $this->session->userdata('id_usuario');
			$this->load->model('TramitesConsulta');
			$dataTramites =$this->TramitesConsulta->solicitudCorreccion($this->session->userdata('id_usuario'),5);

			$data = array("title"=>"Correcciones de solicitudes","correcciones"=>$dataTramites);

			$this->load->view('head',$data);
			$this->load->view('funcionario/header');
			$this->load->view('funcionario/correcciones',$data);
			$this->load->view('footer');
			$this->load->view('scripts/js');
			$this->load->view('scripts/utils');
		}
		else{
			redirect(base_url()."Auth",'');
		}//else

	}//entrega



	/**
	 * Muestra tabla con solicitudes que tienen  correcciones en la fase de inspección
	 */
	public function correccionesCiudadano()
	{
		if ($this->session->userdata('rol')[0]->id >= 3) {
			//guardamos el id del funcionario actual
			$id_funcionario = $this->session->userdata('id_usuario');
			$this->load->model('TramitesConsulta');
			$dataTramites =$this->TramitesConsulta->solicitudCorreccionCiudadano($this->session->userdata('id_usuario'),6);

			$data = array("title"=>"Correcciones de solicitudes","correcciones"=>$dataTramites);

			$this->load->view('head',$data);
			$this->load->view('funcionario/header');
			$this->load->view('funcionario/correccionesCiudadano',$data);
			$this->load->view('footer');
			$this->load->view('scripts/js');
			$this->load->view('scripts/utils');
		}
		else{
			redirect(base_url()."Auth",'');
		}//else

	}//entrega

	public function switchRole($id_rol = "")
	{
		if ($id_rol != "") {
			$nuevo_rol = array();
			for ($i=0; $i < count($this->session->userdata('rol')); $i++) {
				$rol = $this->session->userdata('rol');
				if ($rol[$i]->id == $id_rol) {
					$nuevo_rol[0] = $rol[$i];
				}//if
				else{
					$sig = $i + 1;
					$nuevo_rol[$sig] = $rol[$i];
				}//else
			}//for
			$this->session->unset_userdata('rol');
			$this->session->set_userdata('rol',$nuevo_rol);
			redirect(base_url()."Funcionario",'');
		}//if
		else{
			redirect(base_url()."Funcionario",'');
		}//else
	}//switchRole

	/**
   * @param array $arr_solicitudes
   */
  public function obtenerIdSolicitudes($arr_solicitudes)
  {
    $idSolicitudes = array();
		if ($arr_solicitudes == null) {
			array_push($idSolicitudes, 0);
		}//if
		else {
			foreach ($arr_solicitudes as $id_sol) {
				array_push($idSolicitudes, $id_sol->id_solicitud);
			}//foreach
		}
      return $idSolicitudes;
  }//obtener_pestania

  public function verCorte()//metodo de corte
  {
    if ($this->session->userdata('rol')[0]->id == 3) {
		//guardamos el id del funcionario actual
		$id_funcionario = $this->session->userdata('id_usuario');
		$this->load->model('Solicitud_M');
		$dataCorte =$this->Solicitud_M->getCorte();
		$data = ["title"=>"Corte de caja", "datoscorte" => $dataCorte];
		
		$this->load->view('head',$data);
		$this->load->view('funcionario/header');
		$this->load->view('funcionario/corte',$data);
		$this->load->view('footer');
		$this->load->view('scripts/js');
		$this->load->view('scripts/pago');
		$this->load->view('scripts/utils');
	}
	else{
		redirect(base_url()."Auth",'');
	}//else
  }//obtener_pestania

  public function predial()//metodo de corte
  {
    if ($this->session->userdata('rol')[0]->id == 3) {
		//guardamos el id del funcionario actual
		$id_funcionario = $this->session->userdata('id_usuario');
		$this->load->model('Predial_M');
		$dataPredial =$this->Predial_M->getPredialSol();
		$data = ["title"=>"Pago Predial", "datospredial" => $dataPredial];
		
		$this->load->view('head',$data);
		$this->load->view('funcionario/header');
		$this->load->view('funcionario/predial',$data);
		$this->load->view('footer');
		$this->load->view('scripts/js');
		$this->load->view('scripts/pago');
		$this->load->view('scripts/utils');
	}
	else{
		redirect(base_url()."Auth",'');
	}//else
  }//obtener_pestania

  public function validPredial(){
	$dataTitle = "Validación de Predio!";
	$data=[];
	$this->load->model('predio');
	$datapredio = $this->predio->verifica();
	if(!empty($datapredio)){
		$data =[
			'prediales' => $datapredio,
			'title' => $dataTitle
		];
	}else{
		$data=['title' => $dataTitle];
	}
	
	$this->load->view('head',$data);
	$this->load->view('funcionario/header');
	$this->load->view('predial/validPredio',$data);
	$this->load->view('scripts/db');
	$this->load->view('footer');
	$this->load->view('scripts/js');
	$this->load->view('scripts/pago');
	$this->load->view('scripts/utils');
	$this->load->view('scripts/evaluacion');
	$this->load->view('scripts/dashboard');

  }
  

}//class

/* End of file Funcionario.php */
/* Location: ./application/controllers/Funcioanrio.php */
