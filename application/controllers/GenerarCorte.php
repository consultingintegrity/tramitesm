<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenerarCorte extends CI_Controller {

  public function __construct(){
    parent::__construct();
  }//.index

  public function generaPdfCorte(){
    $this->load->model('Solicitud_M');
    $this->load->library('Pdf');
    //fecha
    $dia = date("d");
    $mes = date("F");
    $ano = date("Y");

    if ($mes=="January") $mes="enero";
    if ($mes=="February") $mes="febrero";
    if ($mes=="March") $mes="marzo";
    if ($mes=="April") $mes="abril";
    if ($mes=="May") $mes="mayo";
    if ($mes=="June") $mes="junio";
    if ($mes=="July") $mes="julio";
    if ($mes=="August") $mes="agosto";
    if ($mes=="September") $mes="septiembre";
    if ($mes=="October") $mes="octubre";
    if ($mes=="November") $mes="noviembre";
    if ($mes=="December") $mes="diciembre";

    $fecha = $dia." de ".$mes." de ".$ano;
    $pdf = new Pdf();
    $pdf->AddPage();
    $pdf->SetMargins(15, 25 , 15, 20);
    if($consulta_corte = $this->Solicitud_M->corte()){
    $sumCorte = $this->Solicitud_M->sumCorte();

    

    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(0,0,utf8_decode($fecha),0,0,'C');
    $pdf->SetFont('Arial', 'B', 15);
    $pdf->SetTitle(utf8_decode("Corte de Caja"));
    $pdf->getX(105);
    $pdf->SetY(15);
    $pdf->Cell(180,10,"Corte de Caja",0,0,'C');
    $pdf->Ln(20);
    $pdf->SetFont('Arial','B',20);
    $pdf->Cell(20,10,'TOTAL',0,0,'L');
    $pdf->Cell(160,10,utf8_decode('$'.number_format($sumCorte,2)),0,0,'R');
    $pdf->SetDrawColor(0,0,0);
    $pdf->SetLineWidth(1);
    $pdf->Line(15,45,195,45);
    $pdf->Ln(15);
    $pdf->SetFont('Arial','',12);
    $pdf->SetLineWidth(0);
    $pdf->SetFillColor(247, 247, 247 );
    $pdf->Cell(15,10,'#','L',0,'C',True);
    $pdf->Cell(30,10,'Tipo de Pago','L',0,'C',True);
    $pdf->Cell(70,10,'Folio / Referencia Bancaria','L',0,'C',True);
    $pdf->Cell(25,10,'Fecha','L',0,'C',True);
    $pdf->Cell(5,10,'','L',0,'C',True);
    $pdf->Cell(35,10,'Costo',0,0,'C',True);
    $pdf->Line(15,60,195,60);
    $pdf->Ln(5);
    
    foreach ($consulta_corte as $corte) {
        $pdf->Ln(7);
        $pdf->SetFont('Arial','',9);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(15,10,utf8_decode($corte->id),'L',0,'C',True); 
        $pdf->Cell(30,10,utf8_decode($corte->tipo_pago),'L',0,'C',True);
        $pdf->Cell(70,10,utf8_decode($corte->folio),'L',0,'C',True);
        $pdf->Cell(25,10,utf8_decode($corte->fecha),'L',0,'C',True);
        $pdf->Cell(5,10,'$','L',0,'C',True);
        $pdf->Cell(35,10,number_format($corte->costo_corte,2),0,0,'R',True);
    }
    $pdf->Output('corte.pdf','I');
    }
    else{
       
        $pdf->SetFont('Arial', 'B', 15);
        $pdf->SetTitle(utf8_decode("Corte de Caja"));
        $pdf->getX(105);
        $pdf->SetY(15);
        $pdf->Cell(180,10,'CORTE DE CAJA',0,0,'C');
        $pdf->Ln(20);
        $pdf->SetFont('Arial','B',20);
        $pdf->Cell(20,10,'TOTAL',0,0,'L');
        $pdf->Cell(145,10,'',0,0,'R');
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetLineWidth(1);
        $pdf->Line(15,45,195,45);
        $pdf->Ln(15);
        $pdf->SetFont('Arial','',12);
        $pdf->SetLineWidth(0);
        $pdf->SetFillColor(247, 247, 247 );
        $pdf->Cell(15,10,'#',0,0,'C',True);
        $pdf->Cell(30,10,'Tipo de Pago',0,0,'C',True);
        $pdf->Cell(70,10,'Folio / Referencia Bancaria',0,0,'C',True);
        $pdf->Cell(35,10,'Fecha',0,0,'C',True);
        $pdf->Cell(30,10,'Costo',0,0,'C',True);
        $pdf->Line(15,60,195,60);
        $pdf->Ln(50);
        $pdf->SetFont('Arial','',20);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(180,10,'NO SE HAN REALIZADO CORTES',0,0,'C');
        $pdf->OutPut();
    }


  }//generaPdfCorte

}//class
?>
