<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenerarDocs extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->library('Json');
  }//.index

  //generar pdf por id de tramite
  public function generarPdf(){
    //variables que ocuparemos para la actalizción de la fase del trámite
    $final = "";
    $folio = "";
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;
    $id_fase = (!empty($this->input->post('no_fase'))) ? addslashes($this->input->post('no_fase')) : 0;
    $id_ciudadano = (!empty($this->input->post('id_ciudadano'))) ? addslashes($this->input->post('id_ciudadano')) : 0;
    $id_tramite = (!empty($this->input->post('id_tramite'))) ? addslashes($this->input->post('id_tramite')) : 0;
    $descripcion = (!empty($this->input->post('descripcion'))) ? addslashes($this->input->post('descripcion')) : "";
    $asunto = (!empty($this->input->post('asunto'))) ? addslashes($this->input->post('asunto')) : 0;
    $clave = (!empty($this->input->post('clave'))) ? addslashes($this->input->post('clave')) : 0;
    $interesado = (!empty($this->input->post('interesado'))) ? addslashes($this->input->post('interesado')) : 0;
    $cantidad = (!empty($this->input->post('costoTramite'))) ? addslashes($this->input->post('costoTramite')) : 0;
    $cantidad = str_replace(',','',$cantidad);
    //campos para numero oficial
    $asignoNo = (!empty($this->input->post('asignoNO'))) ? addslashes($this->input->post('asignoNO')) : 0;
    $comprobanteNo = (!empty($this->input->post('comprobanteNO'))) ? addslashes($this->input->post('comprobanteNO')) : 0;
    //campos para uso de suelo
    $usZona = (!empty($this->input->post('usZona'))) ? addslashes($this->input->post('usZona')) : 0;
    $organismo = (!empty($this->input->post('organismo'))) ? addslashes($this->input->post('organismo')) : 0;

    //proteccion civil
    $giroNegocio = (!empty($this->input->post('giroNegocio'))) ? addslashes($this->input->post('giroNegocio')) : 0;
    //proteccion y factibilidad
    $nombreNegocio = (!empty($this->input->post('nombreNegocio'))) ? addslashes($this->input->post('nombreNegocio')) : 0;
    //factibilidad de giro
    $noDictamen = (!empty($this->input->post('noDictamen'))) ? addslashes($this->input->post('noDictamen')) : 0;
    //licencia de funcionamiento
    $tipoLicencia = (!empty($this->input->post('tipoLicencia'))) ? addslashes($this->input->post('tipoLicencia')) : 0;
    //licencia de contruccion
    $noLicencia = (!empty($this->input->post('noLicencia'))) ? addslashes($this->input->post('noLicencia')) : 0;
    $noAlineamiento = (!empty($this->input->post('noAlineamiento'))) ? addslashes($this->input->post('noAlineamiento')) : 0;
    $vigencia = (!empty($this->input->post('vigencia'))) ? addslashes($this->input->post('vigencia')) : 0;
    $delegacion = (!empty($this->input->post('delegacion'))) ? addslashes($this->input->post('delegacion')) : 0;
    $observaciones = (!empty($this->input->post('observaciones'))) ? addslashes($this->input->post('observaciones')) : 0;
    //informe uso de suelo
    $predioZona = (!empty($this->input->post('predioZona'))) ? addslashes($this->input->post('predioZona')) : 0;
    //models
    $this->load->model('TramitesConsulta');
    $this->load->model('Solicitud_M');
    $this->load->model('M_Catalogo');
    $this->load->model('Formulario');
    $this->load->model('Documentos');

    
    
    //obtenemos la solicitud
    $data_solicitud = $this->Solicitud_M->getDataSolicitud($id_solicitud);
    //Obtenemos la información de la solicitud
    $data_informacion_solicitud = json_decode($data_solicitud->informacion,1);
    //campos del formulario
    $name = "";
    //fecha
    $dia = date("d");
    $mes = date("F");
    $ano = date("Y");

    if ($mes=="January") $mes="enero";
    if ($mes=="February") $mes="febrero";
    if ($mes=="March") $mes="marzo";
    if ($mes=="April") $mes="abril";
    if ($mes=="May") $mes="mayo";
    if ($mes=="June") $mes="junio";
    if ($mes=="July") $mes="julio";
    if ($mes=="August") $mes="agosto";
    if ($mes=="September") $mes="septiembre";
    if ($mes=="October") $mes="octubre";
    if ($mes=="November") $mes="noviembre";
    if ($mes=="December") $mes="diciembre";

    $fecha = $dia." de ".$mes." de ".$ano;
    $fechaTabla = date("d-m-Y");
    //se valida que el dia que se genera el recivo sea lunes para asi sumas solo 5 dias a la fecha de vencimiento
    if (date("1")=="Monday") {
              //se suman solo 5 dias
              $fecha_vencimiento = date('d-m-Y', strtotime("+5 days"));
    }else {
              //se suman 7 dias
              $fecha_vencimiento = date('d-m-Y', strtotime("+7 days"));
    }
    $Subt = $this->TramitesConsulta->subTramitePago($id_solicitud);
    //se l da formato a la fecha y se parsea a string
    //$fecha_vencimiento = date("d-m-y", $fecha_ven);
    // Se carga la libreria fpdf
    $this->load->library('Pdf');
    // Creacion del PDF
    $pdf = new Pdf();
    // Agregamos una página
    $pdf->AddPage();
    $pdf->SetMargins(15, 25 , 15, 20);
    // Define el alias para el número de página que se imprimirá en el pie
    $pdf->AliasNbPages();
    switch ($id_tramite) {
      
    //info del tramite

      case '1':
        //No Oficial
        $tramite = $this->TramitesConsulta->descTramite($id_tramite);
        $folio = "N°. ANOF-0{$id_tramite}{$id_solicitud}/{$ano}";
        $tipoTramite = $this->Solicitud_M->getTipoSolicitud($id_solicitud);
        $fetein = 30202;
        switch ($id_fase) {
          //fase
          
          case '5':
            /* Se define el titulo, márgenes izquierdo, derecho y
              * el color de relleno predeterminado
              */
              if(!empty($asignoNo) && !empty($comprobanteNo)){
              $pdf->SetTitle(utf8_decode("Alineamiento y Número Oficial"));
              $pdf->SetFillColor(200,200,200);
              $pdf->SetFont('Arial','B',12);
              $pdf->getY(15);
              $pdf->Ln('8');
              $pdf->setX(55);
              $pdf->MultiCell(100,5,utf8_decode("SECRETARIA DE DESARROLLO URBANO, VIVIENDA Y OBRAS PÚBLICAS "),0,'C',0);
              $pdf->Ln(20);

              //se pinta la fecha con el lugar donde se emite el documento
              $pdf->SetFont('Arial','',12);
              $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,0,'R');
              $pdf->Ln('5');
              $pdf->SetFont('Arial','B',12);
              $pdf->Cell(0,5,utf8_decode($folio),0,0,'R');

              // Se define el formato de fuente: Arial, negritas, tamaño 9
              //$this->pdf->setFond();
              /*
              * $this->pdf->Cell(Ancho, Alto,texto,borde,posición,alineación,relleno);
              */
              //se forma el contexto del documento
                $pdf->Ln(5);
                $pdf->SetFont('Arial','B',14);
                $pdf->Cell(20,3,utf8_decode('Asunto'),0,0,'L');
                $pdf->Ln(7);
                $pdf->SetFont('Arial','',13);
                $pdf->Cell(0,3,utf8_decode($tramite->nombre."."),0,0,'L');
                $pdf->Ln(5);
                $pdf->Cell(0,3,utf8_decode("(".$Subt->nombreSubTramite.")."),0,0,'L');
                $pdf->Ln(15);
                $pdf->Rect(13, 80, 185,48, 5, '13', 'Do');
                $pdf->SetFont('Arial','B',12);
                $pdf->SetX(60);
                $pdf->cell(90,4,utf8_decode('DATOS DEL SOLICITANTE'),0,0,'C');
                $pdf->Ln(13);
                $pdf->SetFont('Arial','B',12);
                $pdf->cell(35,4,utf8_decode('Clave catastral: '),0,0,'L');
                $this->json->formData($data_informacion_solicitud,"frm_no_oficial");//frm_ubicacion_predio
                $this->json->valueFieldForm("txtClaveCatastral");
                $pdf->SetFont('Arial','',12);
                $pdf->cell(65,4,utf8_decode( $this->json->getFieldValue().'.'),0,0,'L');
                $pdf->Ln(8);
                $pdf->SetFont('Arial','B',12);
                $pdf->cell(50,4,utf8_decode('Nombre del propietario'),0,0,'L');
                $pdf->SetFont('Arial','',12);
                $this->json->setFieldValue("");
                $presente ="";
                $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
               if($representante == 1){
                  $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }else if($representante == 59){
                  $this->json->formData($data_informacion_solicitud,"frm_datos_solicitudNOOficial");//frm_ubicacion_predio
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtPrimerNombre");
                  $presente = $this->json->getFieldValue();
                  $this->json->valueFieldForm("txtSegundoNombre");
                  if($this->json->getFieldValue()!= ""){
                    $presente .= " " . $this->json->getFieldValue();
                  }else{
                  $this->json->valueFieldForm("txtPrimerApellido");
                  $presente .= " " . $this->json->getFieldValue();
                  $this->json->valueFieldForm("txtSegundoApellido");
                  $presente .= " " . $this->json->getFieldValue();
                  }
                }
                
                $pdf->cell(40,4,utf8_decode($presente.'.'),0,0,'L');
                $pdf->Ln(8);
                $pdf->SetFont('Arial','B',12);
                $pdf->cell(51,4,utf8_decode('Domicilio del propietario'),0,0,'L');
                $pdf->SetFont('Arial','',12);
                $this->json->setFieldValue("");
                $this->json->formData($data_informacion_solicitud,"frm_datos_solicitudNOOficial");//frm_ubicacion_predio
                $this->json->valueFieldForm("txtDomicilioPropietario");
                $direccion= $this->json->getFieldValue();
                $this->json->valueFieldForm("txtNumero");
                $direccion .= " " . $this->json->getFieldValue();
                $this->json->valueFieldForm("txtColonia");
                $direccion .= " " . $this->json->getFieldValue();
                $this->json->valueFieldForm("txtDelegacion");
                $direccion .= " " . $this->json->getFieldValue();
                $pdf->cell(80,4,utf8_decode($direccion. '.'),0,0,'L');
                $pdf->Ln(25);
                $pdf->Rect(13, 133, 185, 48, 5, '13', 'Do');
                $pdf->SetFont('Arial','b',12);
                $pdf->SetX(40);
                $pdf->cell(115,4,utf8_decode('DIRECCIÓN DE DESARROLLO URBANO Y VIVIENDA'),0,0,'C');
                $pdf->Ln(13);
                $pdf->SetFont('Arial','',12);
                $pdf->cell(115,4,utf8_decode('SECCIÓN DE NÚMERO OFICIAL COMPROBANTE NÚM. __________________________'),0,0,'L');
                $pdf->SetFont('Arial','B',12);
                $pdf->Cell(40,3,utf8_decode(''.$comprobanteNo),0,1,'C');
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',12);
                $pdf->cell(58,4,utf8_decode('Se asignó el Número Oficial ______________________________________________'),0,0,'L');
                $pdf->SetFont('Arial','B',12);
                $pdf->Cell(108,3,utf8_decode(''.$asignoNo),0,0,'C');
                $pdf->Ln(7);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(52,4,utf8_decode('Para el predio ubicado en  _________________________________________________'),0,0,'L');
                $pdf->SetFont('Arial','B',12);
                $this->json->formData($data_informacion_solicitud,"frm_no_oficial");//frm_ubicacion_predio
                $this->json->setFieldValue("");
                $this->json->valueFieldForm("txtCalle");
                $pdf->Cell(115,3,utf8_decode($this->json->getFieldValue().'.'),0,0,'C');
                $pdf->Ln(7);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(18,4,utf8_decode('Colonia ________________________________________________________________'),0,0,'L');
                $pdf->SetFont('Arial','B',12);
                $this->json->setFieldValue("");
                $this->json->valueFieldForm("txtColoniaPredio");
                $ubicacion = $this->json->getFieldValue();
                $this->json->setFieldValue("");
                $this->json->valueFieldForm("txtLlote");
                $ubicacion .=" ". $this->json->getFieldValue();
                $this->json->setFieldValue("");
                $this->json->valueFieldForm("txtManzana");
                $ubicacion .=" ". $this->json->getFieldValue();
                $pdf->Cell(149,3,utf8_decode($ubicacion.'.'),0,0,'C');
                $pdf->Ln(34);
                $pdf->SetFont('Arial','',15);
                $pdf->Cell(175,3,utf8_decode("LUGAR PARA EL CROQUIS (Deberá hacerse con tinta)"),0,0,'C');                
                $pdf->Ln(6);
                $pdf->Cell(179,3,utf8_decode("DATOS QUE DEBE CONTENER CROQUIS DE MANZANA Y PREDIO:"),0,0,'C');                
                $pdf->Ln(5);
                $pdf->MultiCell(179,5,utf8_decode("Nombres de las calles que limitan la manzana, distancia de las dos esquinas a los linderos del predio, distancia de una esquina a la mitad de la entrada del predio, medidas del frente o frentes, fondo del predio y orientación"),0,'C',0);                
                ///Entra la nueva pagina////
                $pdf->AddPage();
                $pdf->Rect(13, 39, 185, 130, 50, '13', 'Do');
                $pdf->SetY(-125);
                $pdf->SetX(90);
                $pdf->Cell(18,4,utf8_decode('OBSERVACIONES'),0,0,'C');
                $pdf->Ln(10);
                $pdf->SetFont('Arial','',12);
                $pdf->MultiCell(179,4,utf8_decode($descripcion), 0,'J',false);
                $pdf->Ln(10);
                $pdf->SetY(250);
                $pdf->SetX(30);
                $pdf->Cell(55,3,utf8_decode('____________________________'),0,0,'L');
                $pdf->SetX(120);
                $pdf->Cell(55,3,utf8_decode('____________________________'),0,0,'L');
                $pdf->Ln(6);
                $pdf->SetX(50);
                $pdf->Cell(15,3,utf8_decode('REVISÓ '),0,0,'C');
                $pdf->SetX(143);
                $pdf->Cell(15,3,utf8_decode('AUTORIZÓ '),0,0,'C');
                $pdf->Ln(12);
                $pdf->SetY(268);
                $pdf->SetX(38);
                $pdf->Cell(130,3,utf8_decode('NOTA: Este documento no debe tener raspaduras ni enmendaduras.'),0,0,'C');
                //se limpia y cierra para evitar errores
                //
                $pdf->close();
              /*
              * Se manda el pdf al navegador
              *
              * $this->pdf->Output(nombredelarchivo, destino);
              *
              * I = Muestra el pdf en el navegador
              * D = Envia el pdf para descarga
              * F = guarda el archivo dn una ruta
              */
              //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
              $name = "pdf/Numero_Oficial_{$id_tramite}_{$id_solicitud}.pdf";
              $pdf->Output(''.$name, 'f');
              }else{
                $name = null;
              }
              //retorno la url
            break;
          case '8':
          //orden de pago
            $folio = 'OP/0'.$id_tramite.'-'.$id_solicitud;
            $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
            $cadena = $fechaCap.$fetein.$id_solicitud;
            $ca = (int)$cadena;
            
            $final= $this->generaCadena($ca);
          if(!empty($cantidad)){
            

            $pdf->SetTitle(utf8_decode("Pase a Caja"));
            $pdf->SetFillColor(200,200,200);
            $pdf->getX(105);
            $pdf->SetY(18);
            $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
            $pdf->Ln('8');
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(30);
            $pdf->Ln(20);

            //se pinta la fecha con el lugar donde se emite el documento
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
            $pdf->Ln(5); 
            $pdf->SetFont('Arial','b',12);
            $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
            $pdf->Ln(21);
            $pdf->SetFont('Arial','B',12);
            $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',12);
            //podemos mandar id de form o nombre del formulario
            $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_datos_solicitudNOOficial");//frm_ubicacion_predio
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtPrimerNombre");
              $presente = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtSegundoNombre");
              if($this->json->getFieldValue()!= ""){
                $presente .= " " . $this->json->getFieldValue();
              }else{
              $this->json->valueFieldForm("txtPrimerApellido");
              $presente .= " " . $this->json->getFieldValue();
              $this->json->valueFieldForm("txtSegundoApellido");
              $presente .= " " . $this->json->getFieldValue();
              }
            }
          
            $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',12);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',14);
            $pdf->Cell(190,4,utf8_decode($tramite->nombre.'.'),0,'J',false);
            $pdf->Ln(7);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(190,4,utf8_decode('('.$Subt->nombreSubTramite.').'),0,'J',false);
            $pdf->Ln(13);
            $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
            $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
            $pdf->Ln(8);
            $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
            //seccion cadena de pago
           
            $pdf->Ln(35);
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
            $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
            $pdf->Ln(15);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
            $pdf->Ln(6);
            $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'L,R',0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
           
             //se limpia y cierra para evitar errores

             $pdf->close();
           /*
           * Se manda el pdf al navegador
           *
           * $this->pdf->Output(nombredelarchivo, destino);
           *
           * I = Muestra el pdf en el navegador
           * D = Envia el pdf para descarga
           * F = guarda el archivo dn una ruta
           */
           //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
         $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
             $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }
            break;

          default:
            # code...
            break;
        }//numero oficial

        break;

        case '2':
        //dictamen Uso de suelo
        $tramite = $this->TramitesConsulta->descTramite($id_tramite);
        $folio = "N.º US-0{$id_tramite}{$id_solicitud}/{$ano}";
        switch ($id_fase) {
          //fase
          case '5':
          if(!empty($usZona) && !empty($organismo)){

            /* Se define el titulo, márgenes izquierdo, derecho y
              * el color de relleno predeterminado
              */
              $pdf->SetTitle(utf8_decode("DICTAMEN USO DE SUELO"));
              //$pdf->SetFillColor(200,200,200);

              //$pdf->getY(15);
              $pdf->SetFont('Arial','B',13);
              $pdf->Ln('20');
              $pdf->setY(20);
              $pdf->setX(55);
              $pdf->Cell(100,5,utf8_decode('SECRETARÍA DE DESARROLLO URBANO'),0,1,'C');
              $pdf->setX(69);
              $pdf->Cell(70,5,utf8_decode('VIVIENDA Y OBRAS PÚBLICAS') ,0,1,'C');
              $pdf->Ln('10');
              $pdf->SetFont('Arial','B',12);
              $pdf->setX(100);
              $pdf->Cell(110,5,utf8_decode('DIRECCIÓN DE DESARROLLO URBANO Y VIVIENDA'),0,1,'L');
              $pdf->Ln('1');
              $pdf->setX(135);
              $pdf->Cell(70,5,utf8_decode($folio),0,1,'R');
              $pdf->Ln('5');
              $pdf->setX(186);
              $pdf->Cell(20,5,utf8_decode("ASUNTO: {$tramite->nombre}"),0,1,'R');
              //se pinta la fecha con el lugar donde se emite el documento
              $pdf->Ln('5');
              $pdf->SetFont('Arial','',12);
              $pdf->Cell(0,5,utf8_decode('Querétaro, Querétaro a '.$fecha.'.'),0,1,'L');
              $pdf->Ln('5');
              $pdf->SetFont('Arial','b',12);

              //podemos mandar id de form o nombre del formulario
              $presente ="";
                $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
               if($representante == 1){
                  $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }else if($representante == 59){
                  $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }
              $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
              $pdf->Cell(190,5,utf8_decode('P R E S E N T E.'),0,1,'l');
              $pdf->Ln('5');
              $pdf->SetFont('Arial','',12);

              $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");//frm_ubicacion_predio
              $this->json->valueFieldForm("txtCalle");
              $direccion = "<b>".$this->json->getFieldValue();
              $this->json->valueFieldForm("txtNumero");
              $direccion .= " " . $this->json->getFieldValue();
              $this->json->valueFieldForm("txtColonia");
              $direccion .= ", " . $this->json->getFieldValue();
              $this->json->valueFieldForm("txtDelegacion");
              $direccion .= ", Municipio  " . $this->json->getFieldValue() . ", Quéretaro.";

              $this->json->formData($data_informacion_solicitud,"frm_uso_de_suelo");//frm_uso_de_suelo
              $this->json->valueFieldForm("txtClaveCatastral");
              $clave_catastral = " Con clave catastral " . $this->json->getFieldValue() . ",</b>";
              $contenido_documento = "En atención a su solicitud, ingresada en esta Dirección a mi cargo el día ".$fecha.", se realizó el análisis correspondiente al predio ubicado en " . $direccion;
              $contenido_documento .= $clave_catastral;
              $contenido_documento .= " en la cual solicita un Dictamen para ubicar <b>" . $organismo ."</b> al respecto le informo que:";
              $pdf->WriteHtmlCell(190,utf8_decode($contenido_documento));
              $pdf->Ln('5');
              $pdf->SetFont('Arial','',12);
              $pdf->Ln('4');
              $contenido ='Habiéndose revisado el Plan de Desarrollo Urbano del Centro de Población de este Municipio, Técnico-Juridico cuya actualización fue aprobada por Acuerdo de Cabildo Municipal en la Vigésima Sesión Ordinaria celebrada el 11 de noviembre del 2004, publicado en el Periódico Oficial de Gobierno del Estado "La Sombra de Arteaga" el 1 de julio del año 2005, inscrito en el Registro Público de la Propiedad con fecha 25 de Octubre del 2005, bajo la partida 90 Libro Uno Sección Especial del Registro de los Planes de Desarrollo Urbanos Sección de los Municipios, del Registro Público de la Propiedad Oficina Central ante la fe de la Lic. Lorena Montes Hernández, Directora del Registro Público de la Propiedad y el Comercio en el Estado, encontrándose al <b>'.$usZona.'</b> por lo que revisando las Normatividades <b>'.$usZona.'</b> es FACTIBLE con el Uso pretendido.';
              ///////////////////////////////////////////////////////////aqui esta alv////////////////////////////////
              $pdf->WriteHtmlCell(190,utf8_decode($contenido));
              $pdf->Ln('25');
              $pdf->SetX(42);
              $pdf->SetFont('Arial','b',13);
              $pdf->Cell(120,5,utf8_decode('DICTAMEN DE USO DE SUELO'),0,1,'C');
              $pdf->Ln('12');
              $pdf->SetFont('Arial','',11);

              $this->json->formData($data_informacion_solicitud,"frm_uso_de_suelo");//frm_ubicacion_predio
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("optTipoDictamen");

              $pdf->Cell(55,5,utf8_decode('Clasificación:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);
              $pdf->Cell(120,5,utf8_decode('TIPO "'.$this->json->getFieldValue().'"'),0,1,'C');
              $pdf->Ln(1);
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(55,5,utf8_decode('Respuesta:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);
              $pdf->Cell(120,5,utf8_decode("FACTIBLE"),0,1,'C');
              $pdf->Ln(1);
              $pdf->SetFont('Arial','',11);
              $pdf->Ln(1);
              $pdf->Cell(55,5,utf8_decode('Uso analizado:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);
              $pdf->MultiCell(120,5,utf8_decode($organismo),0,'C',0);
              $pdf->Ln(1);
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(55,5,utf8_decode('Tipo de solicitud:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);
              $pdf->MultiCell(120,5,utf8_decode($Subt->nombreSubTramite),0,'C',0);
              $pdf->Ln(1);
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(55,5,utf8_decode('Uso de suelo de la zona:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);

              $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");//frm_ubicacion_predio
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $pdf->MultiCell(120,5,utf8_decode($usZona),0,'C',0);
              $pdf->Ln(1);
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(55,5,utf8_decode('Datos del predio:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);

              $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");//frm_ubicacion_predio
              $this->json->setFieldValue("");
              $this->json->valueForm();

              $pdf->MultiCell(120,5,utf8_decode($this->json->getFieldValue()),0,'C',0);
              $pdf->Ln(1);
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(55,5,utf8_decode('Clave catastral:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);

              $this->json->formData($data_informacion_solicitud,"frm_uso_de_suelo");//frm_ubicacion_predio
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtClaveCatastral");

              $pdf->MultiCell(120,5,utf8_decode($this->json->getFieldValue()),0,'C',0);
              $pdf->Ln(1);
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(55,5,utf8_decode('Superficie:'),0,0,'l');
              $pdf->SetFont('Arial','b',12);

              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtAreaTrabajo");

              $pdf->Cell(120,5,utf8_decode($this->json->getFieldValue().' m2'),0,0,'C');
              $pdf->Ln('20');
              //pagin 2 del documento
              $pdf->AddPage();
              $pdf->SetFont('Arial','BI',11);
              $pdf->Ln('20');
              $pdf->Cell(190,5,utf8_decode('El presente Dictamen queda sujeto al impacto que su actividad genere en la zona.'),0,1,'L');
              $pdf->Ln('5');
              $pdf->SetFont('Arial','',11);
              $pdf->MultiCell(190,5,utf8_decode('Por lo que se dictamina FACTIBLE el uso requerido, a dar cumplimiento a los lineamientos que marca, debiendo obtener el Visto Bueno que garantice el cumplimiento de las Normas de Seguridad que establece la Unidad Estatal de Protección Civil previo a su funcionamiento, presentando el acta correspondiente ante esta Autoridad Municipal, respetar el área de estacionamiento según lo dispuesto por el Artículo 374 del reglamento de Construcciones del Municipio, así como la normatividad y requerimientos indicados por el Código Urbano del Estado de Querétaro, en su Título Cuarto y condicionado a respetar la normatividad que a continuación se indica:'),0,'J',0);
              $pdf->Ln('5');
              $pdf->setx(15);
              $pdf->MultiCell(190,5,utf8_decode('Deberá presentar, visto bueno del H. Cuerpo de Bomberos de acuerdo al Art. 59 del Reglamento General de'),0,'J',0);
              $pdf->setx(20);
              $pdf->MultiCell(185,5,utf8_decode('-Construcciones para el Estado de Querétaro, Publicado en la Sombra de Arteaga el 8 de marzo de 1998.'),0,'J',0);
              $pdf->Ln('5');
              $pdf->setx(15);
              $pdf->MultiCell(190,5,utf8_decode('-Evitar en todo momento utilizar la vía pública como estacionamiento de usuarios del inmueble.'),0,'J',0);
              $pdf->Ln('5');
              $pdf->setx(15);
              $pdf->MultiCell(190,5,utf8_decode('-En caso de pretender colocar anuncios publicitarios en el inmueble, deberá contar previamente con los'),0,'J',0);
              $pdf->setx(20);
              $pdf->MultiCell(185,5,utf8_decode('permisos correspondientes.'),0,'L',0);
              $pdf->Ln('5');
              $pdf->MultiCell(190,5,utf8_decode('Se hace notar que la autorización del proyecto arquitectónico queda sujeta al cumplimiento de las Normas de Estacionamiento, Imagen Urbana y las requeridas por esta Dirección, recomendando se coordine con la misma, para la revisión de dicho proyecto y la tramitación de los permisos correspondientes.'),0,'J',0);
              $pdf->Ln('5');
              $pdf->MultiCell(190,5,utf8_decode('El presente Dictamen NO AUTORIZA el inicio de las obras ampliación y/o modificación en el predio, ni su funcionamiento, para tal fin, deberá coordinarse con la Autoridad correspondiente para la tramitación de los permisos que la normatividad requiera.'),0,'J',0);
              $pdf->SetFont('Arial','B',11);
              $pdf->Ln('5');
              $pdf->MultiCell(190,5,utf8_decode('El incumplimiento de cualquiera de las condicionantes indicadas, será motivo de CANCELACIÓN del presente documento, independientemente de hacerse  acreedor a las SANCIONES que correspondan.'),0,'J',0);
              $pdf->Ln('5');
              $pdf->MultiCell(190,5,utf8_decode('El presente Dictamen Uso de Suelo es un documento de carácter técnico administrativo que se fundamenta en los instrumentos de Planeación Urbana vigentes, así como en las Leyes y Reglamentos que intervienen en materia.'),0,'J',0);
              $pdf->Ln('5');
              $pdf->MultiCell(190,5,utf8_decode('Sin mas por el momento, quedo a sus distinguidas órdenes.'),0,'J',0);
              $pdf->Ln('5');
              $pdf->SetFont('Arial','B',12);
              $pdf->Cell(190,5,utf8_decode('A T E N T A M E N T E'),0,1,'C');
              $pdf->SetFont('Arial','BI',12);
              $pdf->Cell(190,5,utf8_decode('"QUERÉTARO"'),0,0,'C');
              $pdf->Ln('27');
              $pdf->SetFont('Arial','B',12);
              $pdf->Cell(190,5,utf8_decode('"Arq. Miguel Cabrera Lopéz"'),0,1,'C');
              $pdf->setx(66);
              $pdf->MultiCell(90,5,utf8_decode('SECRETARIO DE DESARROLLO URBANO, VIVIENDA, OBRAS PÚBLICAS'),0,'C',0);
              $pdf->SetFont('Arial','B',8);
              $pdf->setx(23);
               $pdf->SetFont('Arial','b',12);

                //se limpia y cierra para evitar errores
                //
                $pdf->close();
              /*
              * Se manda el pdf al navegador
              *
              * $this->pdf->Output(nombredelarchivo, destino);
              *
              * I = Muestra el pdf en el navegador
              * D = Envia el pdf para descarga
              * F = guarda el archivo dn una ruta
              */
              //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
              $name = "pdf/DictamenUsodeSuelo_{$id_tramite}_{$id_solicitud}.pdf";
              $pdf->Output(''.$name, 'f');
          }else{
            $name = null;
          }//.else
              //retorno la url
            break;
          case '8':
          $folio ='OP/0'.$id_tramite.'-'.$id_solicitud;
          $fetein = 30235;
          $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
            $cadena = $fechaCap.$fetein.$id_solicitud;
            $ca = (int)$cadena;
            
            $final= $this->generaCadena($ca);
          //orden de pago
          if(!empty($cantidad)){
            $pdf->SetTitle(utf8_decode("Pase a Caja"));
            $pdf->SetFillColor(200,200,200);
            $pdf->getX(105);
            $pdf->SetY(18);
            $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
            $pdf->Ln('8');
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(30);
            $pdf->Ln(20);

            //se pinta la fecha con el lugar donde se emite el documento
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
            $pdf->Ln(5);
            $pdf->SetFont('Arial','b',12);
            $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
            $pdf->Ln(21);
            $pdf->SetFont('Arial','B',12);
            $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',12);
            //podemos mandar id de form o nombre del formulario
            $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }
            $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',12);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',14);
            $pdf->Cell(190,4,utf8_decode($tramite->nombre.'.'),0,'J',false);
            $pdf->Ln(7);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(190,4,utf8_decode('('.$Subt->nombreSubTramite.').'),0,'J',false);
            $pdf->Ln(13);
            $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
            $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
            $pdf->Ln(8);
            $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
            //seccion cadena de pago
           
            $pdf->Ln(30);
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
            $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
            
            $pdf->Ln(15);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
            $pdf->Ln(6);
            $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'L,R',0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
             //se limpia y cierra para evitar errores

             $pdf->close();
           /*
           * Se manda el pdf al navegador
           *
           * $this->pdf->Output(nombredelarchivo, destino);
           *
           * I = Muestra el pdf en el navegador
           * D = Envia el pdf para descarga
           * F = guarda el archivo dn una ruta
           */
           //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
         $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";

             $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }
            break;

          default:
            # code...
            break;
        } //dictamen

          break;
        case '6':
        //proteccion civil
          $tramite = $this->TramitesConsulta->descTramite($id_tramite);

          switch ($id_fase) {
            case '4':
            $folio = 'OP/0'.$id_tramite.'-'.$id_solicitud;
              $fetein=31305;
              $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
            $cadena = $fechaCap.$fetein.$id_solicitud;
            $ca = (int)$cadena;
            
            $final= $this->generaCadena($ca);
              //orden de pago

              $this->load->model('TramitesConsulta');

              $tramite= $this->TramitesConsulta->descTramite($id_tramite);
              if(!empty($cantidad)){
                $pdf->SetTitle(utf8_decode("Pase a Caja"));
                $pdf->SetFillColor(200,200,200);
                $pdf->getX(105);
                $pdf->SetY(18);
                $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
                $pdf->Ln('8');
                $pdf->SetFont('Arial','B',15);
                $pdf->Cell(30);
                $pdf->Ln(20);

                //se pinta la fecha con el lugar donde se emite el documento
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
                $pdf->Ln(5);
                $pdf->SetFont('Arial','b',12);
                $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
                $pdf->Ln(21);
                $pdf->SetFont('Arial','B',12);
                $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
                $pdf->Ln(6);
                $pdf->SetFont('Arial','',12);
                //podemos mandar id de form o nombre del formulario
                $presente ="";
                $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
               if($representante == 1){
                  $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }else if($representante == 59){
                  $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }
                $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
                $pdf->Ln(6);
                $pdf->SetFont('Arial','B',12);
                $pdf->SetFont('Arial','B',12);
                $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
                $pdf->Ln(6);
                $pdf->SetFont('Arial','',14);
                $pdf->Cell(190,4,utf8_decode($tramite->nombre.'.'),0,'J',false);
                $pdf->Ln(7);
                $pdf->SetFont('Arial','',13);
                $pdf->Cell(190,4,utf8_decode('('.$Subt->nombreSubTramite.').'),0,'J',false);    
                $pdf->Ln(13);
                $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
                $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
                $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
                $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
                $pdf->Ln(8);
                $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
                $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
                $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
                $pdf->SetFont('Arial','B',13);
                $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
                //seccion cadena de pago
           
                $pdf->Ln(35);
                $pdf->SetFont('Arial','B',14);
                $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
                $pdf->Ln(10);
                $pdf->SetFont('Arial','',13);
                $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
                $pdf->Ln(5);
                $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
                $pdf->Ln(5);
                $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
                $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);

                $pdf->Ln(15);
                $pdf->SetFont('Arial','B',13);
                $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
                $pdf->Ln(6);
                $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
                $pdf->Ln(4);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,$final,'L,R',0,'L',0);
                $pdf->Ln(6);
                $pdf->SetFont('Arial','B',13);
                $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
                $pdf->Ln(6);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
                 //se limpia y cierra para evitar errores
                 //
                 $pdf->close();
               /*
               * Se manda el pdf al navegador
               *
               * $this->pdf->Output(nombredelarchivo, destino);
               *
               * I = Muestra el pdf en el navegador
               * D = Envia el pdf para descarga
               * F = guarda el archivo dn una ruta
               */
               //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
               $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
               $pdf->Output(''.$name, 'f');
              }else{
                $name = null;
              }
              break;
              case '6':
               /* Se define el titulo, márgenes izquierdo, derecho y
              * el color de relleno predeterminado
              */
              $folio = 'UMPC/D 06'.$id_solicitud.' /'.$ano;
              if(!empty($nombreNegocio) && !empty($giroNegocio)){
              $pdf->Ln('12');
              $pdf->SetTitle(utf8_decode("Visto Bueno de Potección Civil"));
              $pdf->SetFont('Arial','B',14);
              $pdf->Cell(190,5,utf8_decode('Visto Bueno de Protección Civil'),0,1,'C');
              $pdf->Cell(190,5,utf8_decode($folio),0,1,'C');
              $pdf->Ln('10');
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(190,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
              $pdf->SetFont('Arial','B',12);
            
                //frm_ubicacion_predio
                $presente ="";
                $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
               if($representante == 1){
                  $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }else if($representante == 59){
                  $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }
           
              $pdf->Cell(190,5,utf8_decode($presente),0,1,'L');
              $pdf->Cell(190,5,utf8_decode('P R E S E N T E'),0,1,'L');
              $pdf->Ln('10');
              $pdf->SetFont('Arial','',11);
              $pdf->MultiCell(190,5,utf8_decode('Por medio de esta misiva reciba un cordial saludo, de misma forma le informo que cumple con los requisitos solicitados por la Dirección de Protección Civil para el establecimiento que a continuación se describe:'),0,'J',0);
              $pdf->SetFont('Arial','',11);
              $pdf->Ln('5');
              $pdf->setx(45);
              $pdf->Cell(40,5,utf8_decode('Nombre del Negocio: '),0,0,'L');
              $pdf->SetFont('Arial','B',11);
              $pdf->Cell(110,5,utf8_decode($nombreNegocio),0,1,'L');
              $pdf->Ln('5');
              $pdf->SetFont('Arial','',11);
              $pdf->setx(45);
              $pdf->Cell(10,5,utf8_decode('Giro: '),0,0,'L');
              $pdf->SetFont('Arial','B',11);
              $pdf->MultiCell(140,5,utf8_decode($giroNegocio),0,'J',0);
              $pdf->Ln('5');
              $pdf->SetFont('Arial','',11);
              $pdf->setx(45);
              $pdf->Cell(23,5,utf8_decode('Ubicado en: '),0,0,'L');
              $pdf->SetFont('Arial','B',11);

              $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");//frm_ubicacion_predio
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtCalle");
              $calle = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtNumero");
              $calle .= " " . $this->json->getFieldValue();

              $this->json->valueFieldForm("txtColonia");
              $colonia = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtDelegacion");
              $colonia .= ", " . $this->json->getFieldValue();

              $pdf->MultiCell(130,5,utf8_decode($calle),0,'J',0);
              $pdf->Ln('5');
              $pdf->SetFont('Arial','',11);
              $pdf->setx(45);
              $pdf->Cell(20,5,utf8_decode('Localidad: '),0,0,'L');
              $pdf->SetFont('Arial','B',11);
              $pdf->MultiCell(130,5,utf8_decode($colonia." Querétaro, Qro."),0,'J',0);
              $pdf->SetFont('Arial','',11);
              $pdf->Ln('5');
              $pdf->MultiCell(190,5,utf8_decode('Por lo cual la Dirección Municipal de Protección Civil de ___________ otorga el Visto Bueno al inmueble del negocio antes mencionado de acuerdo al Art. 34 del reglamento de Protección Civil para el Municipio, ya que cumple  con la NOM-001-STPS-2008, NOM-002-STPS-2010, NOM-003-SEGOB-2002 y NOM-001-SEDEG-2005, por lo cual cubre con los requisitos básicos de seguridad para la operación que ahí se realizan. El Visto Bueno tiene validez desde la fecha de su expedición y tiene una vigencia hasta el 31 de Diciembre del presente año. El personal autorizado de la Unidad de Protección Civil podrá hacer  verificaciones en cualquier momento para dar cumplimiento a marco jurídico correspondiente. El interesado deberá subsanar las observaciones notificadas en el acta de inspección en un plazo no mayor a 30 días naturales.'),0,'J',0);
              $pdf->Ln('10');
              $pdf->MultiCell(190,5,utf8_decode('Se hace mención que el incumplimiento de cualquiera de las condiciones de seguridad que hace referencia el marco jurídico correspondiente, será motivo de CANCELACIÓN del presente documento, independientemente de hacerse acreedor de las sanciones adecuadas.'),0,'J',0);
              $pdf->Ln('10');
              $pdf->MultiCell(190,5,utf8_decode('Se debe en todo momento contar con los extintores requeridos vigentes, contar con botiquín de primeros auxilios, directorio de números de emergencia locales, acceso libre a la salida de emergencia y que se encuentre sin candados o algún  aditamento de sujeción, actualizar y notificar de forma inmediata a la Unidad de Protección Civil las modificaciones  que se le hagan al Programa Interno de Protección civil, y usar el equipo de protección personal adecuado para su actividad a realizar ya sea personal interno o externo.'),0,'J',0);
              $pdf->SetFont('Arial','',12);
              $pdf->Ln('5');
              $pdf->Cell(190,5,utf8_decode('"QUERÉTARO"'),0,1,'C');
              $pdf->SetFont('Arial','B',12);
              $pdf->Ln('20');
              $pdf->Cell(190,5,utf8_decode('C. Hugo Arena Jacome'),0,1,'C');
              $pdf->Cell(190,5,utf8_decode('Director de la Unidad Municipal de Protección Civil'),0,1,'C');

                //se limpia y cierra para evitar errores
                //
                $pdf->close();
              /*
              * Se manda el pdf al navegador
              *
              * $this->pdf->Output(nombredelarchivo, destino);
              *
              * I = Muestra el pdf en el navegador
              * D = Envia el pdf para descarga
              * F = guarda el archivo dn una ruta
              */
              //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
              $name = "pdf/VoBo_ProteccionCivil_{$id_tramite}_{$id_solicitud}.pdf";
              $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }
              //retorno la url
              break;

            default:
              // code...
              break;
          }
        break;//case 6

        case '4'://licencia funcionamiento
        switch ($id_fase) {
            case '4':
            $folio ='OP/0'.$id_tramite.'-'.$id_solicitud;
            $alcohol = $this->Solicitud_M->giroSolicitud($id_solicitud);
              $fetein=50002;
              $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
              $cadena = $fechaCap.$fetein.$id_solicitud;
              $ca = (int)$cadena;
              $final= $this->generaCadena($ca);
              //orden de pago

              $this->load->model('TramitesConsulta');

              $tramite= $this->TramitesConsulta->descTramite($id_tramite);

              if(!empty($cantidad)){
                $pdf->SetTitle(utf8_decode("Pase a Caja"));
                $pdf->SetFillColor(200,200,200);
                $pdf->getX(105);
                $pdf->SetY(18);
                $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
                $pdf->Ln('8');
                $pdf->SetFont('Arial','B',15);
                $pdf->Cell(30);
                $pdf->Ln(20);

                //se pinta la fecha con el lugar donde se emite el documento
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
                $pdf->Ln(5);
                $pdf->SetFont('Arial','b',12);
                $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
                $pdf->Ln(21);
                $pdf->SetFont('Arial','B',12);
                $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
                $pdf->Ln(6);
                $pdf->SetFont('Arial','',12);
                //podemos mandar id de form o nombre del formulario
               
                $presente ="";
                $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
               if($representante == 1){
                  $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                  $this->json->setFieldValue("");
                  $this->json->valueForm();
                  $presente=$this->json->getFieldValue();
                }else if($representante == 59){
                  $this->json->formData($data_informacion_solicitud,"frm_dts_contribuyente");// id = 9
                  $this->json->valueFieldForm("txtPrimerNom");
                  $presente = $this->json->getFieldValue();
                  $this->json->valueFieldForm("txtSegundoNom");
                  $presente .= " ". $this->json->getFieldValue();
                  $this->json->valueFieldForm("txtPrimerAp");
                  $presente .= " ". $this->json->getFieldValue();
                  $this->json->valueFieldForm("txtSegundoAp");
                  $presente .= " ". $this->json->getFieldValue();
                }

                $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
                $pdf->Ln(6);
                $pdf->SetFont('Arial','B',12);
                $pdf->SetFont('Arial','B',12);
                $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
                $pdf->Ln(6);
                $pdf->SetFont('Arial','',14);
                $pdf->Cell(63,4,utf8_decode($tramite->nombre.'.'),0,'J',false);
                if($alcohol->vta_alcohol == 1){
                  $pdf->SetFont('Arial','B',12);
                  $pdf->Cell(190,4,utf8_decode('(Con venta de alcohol)'),0,'J',false);
                }else{

                }
                $pdf->Ln(7);
                $pdf->SetFont('Arial','',13);
                $pdf->Cell(190,4,utf8_decode('('.$Subt->nombreSubTramite.').'),0,'J',false);    
                $pdf->Ln(13);
                $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
                $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
                $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
                $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
                $pdf->Ln(8);
                $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
                $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
                $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
                $pdf->SetFont('Arial','B',13);
                $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
                //seccion cadena de pago
                $pdf->Ln(30);
                $pdf->SetFont('Arial','B',14);
                $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
                $pdf->Ln(10);
                $pdf->SetFont('Arial','',13);
                $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
                $pdf->Ln(5);
                $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
                $pdf->Ln(5);
                $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
                $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
                
                $pdf->Ln(15);
                $pdf->SetFont('Arial','B',13);
                $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
                $pdf->Ln(6);
                $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
                $pdf->Ln(4);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,$final,'L,R',0,'L',0);
                $pdf->Ln(6);
                $pdf->SetFont('Arial','B',13);
                $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
                $pdf->Ln(6);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','B',11);
                $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
                $pdf->Ln(4);
                $pdf->SetFont('Arial','',11);
                $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
                 //se limpia y cierra para evitar errores
                 //
                 $pdf->close();
               /*
               * Se manda el pdf al navegador
               *
               * $this->pdf->Output(nombredelarchivo, destino);
               *
               * I = Muestra el pdf en el navegador
               * D = Envia el pdf para descarga
               * F = guarda el archivo dn una ruta
               */
               //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
               $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
               $pdf->Output(''.$name, 'f');

              }else{
                $name = null;
              }
              break;//fase caso

              case '6':
              $provicional = $this->M_Catalogo->provicional($id_solicitud);
              $apertura = $this->M_Catalogo->subTramiteSol($id_solicitud);
              $giro= $this->M_Catalogo->solGiro($id_solicitud);
              $folio = "LF-0{$id_tramite}{$id_solicitud}/{$ano}";
              //aqui empieza el documeto licencia de funcionamiento
              $pdf->SetTitle(utf8_decode("Licencia de funcionamiento"));
              $pdf->SetFont('Arial','B',14);
              $pdf->Cell(190,5,utf8_decode('"SECRETARÍA DE FINANZAS PÚBLICAS MUNICIPALES"'),0,1,'C');
              $pdf->SetFont('Arial','',14);
              $pdf->Cell(190,5,utf8_decode('LICENCIA MUNICIPAL DE FUNCIONAMIENTO 2018-2021'),0,1,'C');
              $pdf->SetFont('Arial','',11);
              $pdf->Cell(190,5,utf8_decode('Querétaro, Qro.'),0,1,'C');
              $pdf->Cell(190,5,utf8_decode('C.P.76750 Teléfonos: 01(414) 2732327-2730692'),0,1,'C');
              $pdf->Ln('5');
              $pdf->Cell(160,5,utf8_decode('TIPO DE LICENCIA'),0,0,'R');
              $pdf->SetFillColor(232,232,232);
              $pdf->Cell(30,5,utf8_decode($tipoLicencia),0,1,'C',1);
              $pdf->Ln('3');
              $pdf->setXY(5,43);
              $pdf->Cell(200,70,utf8_decode(''),1,1,'C');
              $pdf->setXY(10,50);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(48,5,utf8_decode('NOMBRE DEL PROPIETARIO:'),0,1,'L');
              $pdf->setXY(60,49.5);

              $presente ="";
              $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
             if($representante == 1){
                $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                $this->json->setFieldValue("");
                $this->json->valueForm();
                $presente=$this->json->getFieldValue();
              }else if($representante == 59){
                $this->json->formData($data_informacion_solicitud,"frm_dts_contribuyente");// id = 9
              $this->json->valueFieldForm("txtPrimerNom");
              $presente = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtSegundoNom");
              $presente .= " ". $this->json->getFieldValue();
              $this->json->valueFieldForm("txtPrimerAp");
              $presente .= " ". $this->json->getFieldValue();
              $this->json->valueFieldForm("txtSegundoAp");
              $presente .= " ".$this->json->getFieldValue();
              }
             

              $pdf->SetFont('Arial','B',8);
              $pdf->Cell(100,5,utf8_decode(mb_strtoupper($presente)),'B',1,'L');
              $pdf->setXY(160,50);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(10,5,utf8_decode('NO.'),0,1,'L');
              $pdf->setXY(170,50);
              $pdf->Cell(30,5,utf8_decode($folio),1,1,'R');
              $pdf->setXY(10,60);
              $pdf->Cell(51,5,utf8_decode('NOMBRE O RAZÓN SOCIAL:'),0,1,'L');
              $pdf->setXY(58,59);

              $this->json->formData($data_informacion_solicitud,"frm_dts_negocio");// id = 9
              $this->json->setFieldValue(" ");
              $this->json->valueFieldForm("txtRazon");

              $pdf->SetFont('Arial','B',8);
              $pdf->Cell(142,5,utf8_decode(mb_strtoupper($this->json->getFieldValue())),'B',1,'L');
              $pdf->SetFont('Arial','',9.5);
              $pdf->setXY(11,70);
              $pdf->Cell(51,5,utf8_decode('DOMICILIO (CALLE Y No.):'),0,1,'L');
              $pdf->setXY(55,69);

              $this->json->setFieldValue(" ");
              $this->json->valueFieldForm("txtCalle");
              $calle_numero = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtNumeroExt");
              $calle_numero .= "  " . $this->json->getFieldValue();

              $pdf->SetFont('Arial','B',8);
              $pdf->Cell(100,5,utf8_decode(mb_strtoupper($calle_numero)),'B',1,'L');
              $pdf->setXY(155,70);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(16,5,utf8_decode('RFC:'),0,1,'L');
              $pdf->setXY(165,69);

              $this->json->setFieldValue(" ");
              $this->json->valueFieldForm("txtRFC");
              $pdf->SetFont('Arial','B',8);

              $pdf->Cell(34,5,utf8_decode(mb_strtoupper($this->json->getFieldValue())),'B',1,'L');
              $pdf->SetFont('Arial','',9.5);
              $pdf->setXY(10,80);
              $pdf->Cell(19,5,utf8_decode('COLONIA:'),0,1,'L');
              $pdf->setXY(28,79);

              $this->json->valueFieldForm("txtColonia");
              $colonia = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtDelegacion");
              $colonia .= "," . $this->json->getFieldValue();
              $pdf->SetFont('Arial','B',8);

              $pdf->Cell(85,5,utf8_decode(mb_strtoupper($colonia)),'B',1,'L');
              $pdf->setXY(115,80);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(10,5,utf8_decode('C.P.:'),0,1,'L');
              $pdf->setXY(124.5,79);
              $pdf->SetFont('Arial','B',8);

              $this->json->valueFieldForm("txtCodigoPostal");

              $pdf->Cell(15,5,utf8_decode($this->json->getFieldValue()),'B',1,'L');
              $pdf->setXY(141,80);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(11,5,utf8_decode('TEL Y EXT.:'),0,1,'L');
              $pdf->setXY(162,80);
              $pdf->SetFont('Arial','B',8);

              $this->json->valueFieldForm("txtExtension");
              $extension = ($this->json->getFieldValue() != "") ? "(". $this->json->getFieldValue() .") " : "";
              $this->json->valueFieldForm("txtTelefono");
              $telefono = $this->json->getFieldValue();

              $pdf->Cell(38,5,utf8_decode($extension . $telefono),'B',1,'L');
              $pdf->setXY(10,90);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(68,5,utf8_decode('DICTAMEN DE USO DE SUELO NO.:'),0,1,'L');
              $pdf->setXY(70,89);

              $this->json->valueFieldForm("txtUsoSuelo");

              $pdf->SetFont('Arial','B',8);
              $pdf->Cell(80,5,utf8_decode($this->json->getFieldValue()),'B',1,'L');
              $pdf->setXY(151,90);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(17,5,utf8_decode('FECHA:'),0,1,'L');
              $pdf->setXY(166,89);
              $pdf->SetFont('Arial','B',8);

              $this->json->valueFieldForm("txtFechaDictamen");

              $pdf->Cell(34,5,utf8_decode($this->json->getFieldValue()),'B',1,'L');
              $pdf->setXY(10,100);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(75,5,utf8_decode('DICTAMEN DE PROTECCIÓN CIVIL NO.:'),0,1,'L');
              $pdf->setXY(76,99);

              $this->json->valueFieldForm("txtProtecciónCivil");

              $pdf->SetFont('Arial','B',8);
              $pdf->Cell(74,5,utf8_decode($this->json->getFieldValue()),'B',1,'L');
              $pdf->setXY(151,100);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(16,5,utf8_decode('FECHA:'),0,1,'L');
              $pdf->setXY(167,100);
              $pdf->SetFont('Arial','B',8);

              $this->json->valueFieldForm("txtFechaProteccionCivil");

              $pdf->Cell(32,5,utf8_decode($this->json->getFieldValue()),'B',1,'L');
              $pdf->SetFont('Arial','',9.5);
              $pdf->setXY(5,120);
              $pdf->Cell(200,150,utf8_decode(''),1,1,'C');
              //este es la imgen no toquen nada
              $pdf->setXY(10,155);
              $pdf->Image( base_url() . 'plantilla/images/img-tequis/TEQUIS-MARCA.png',60,125, 100 , 150,'PNG');
              $pdf->setXY(15,130);
              $pdf->Cell(42,5,utf8_decode('ACTIVIDADES CLAVE'),0,1,'L');
              $pdf->setXY(77,130);
              $pdf->Cell(47,5,utf8_decode('DESCRIPCIÓN DE LA ACTIVIDAD:'),0,1,'L');
              $pdf->setXY(76,139);
              $pdf->SetFont('Arial','B',9);
              $pdf->MultiCell(129,5,utf8_decode($giro->descripcion),0,'j',0);
              $pdf->setXY(39,150);
              $pdf->Cell(70,5,utf8_decode("VIGENCIA AÑO " . date("Y")),0,1,'R');
              $pdf->setXY(155,130);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(47,5,utf8_decode('FECHA DE EMISIÓN'),0,1,'L');
              $pdf->setXY(150,125);
              $pdf->SetFont('Arial','B',9);
              $pdf->Cell(50,5,utf8_decode("                ".date("Y/m/d")),'B',1,'L');
              //validamos si tendra la leyenda de condicionada (solo si es de alcohol y es apertura)
              $VtaAlcohol =$giro->vta_alcohol;
              if($VtaAlcohol == 1 && $provicional != null){
                $pdf->setXY(60,165);
                $pdf->SetFont('Arial','B',10);
                $pdf->MultiCell(145,5,utf8_decode('NOTA : ESTA LICENCIA QUEDA CONDICIONADA POR TREINTA DÍAS NATURALES A PARTIR DE SU FECHA DE EMISION, PARA QUE EL CONTRIBUYENTE REALICE SUS TRAMITES CORRESPONDIENTES ANTE GOBIERNO DEL ESTADO'),0,'j',0);
              }
              $pdf->setXY(13,140);
              $pdf->SetFont('Arial','',9.5);
              $pdf->Cell(45,50,utf8_decode($giro->sector ."-".$giro->grupo."- " . $giro->tipo."-".$giro->actividad),0,1,'C',1);
              $pdf->setXY(15,175);
              $pdf->Cell(45,50,utf8_decode('HORARIO NORMAL'),0,'j',0);
              $pdf->setXY(50,175);
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtHorarioApertura");
              $horarioApertura = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtHorarioCierre");
              $horarioCierre = $this->json->getFieldValue();

              $pdf->SetFont('Arial','B',8);
              $pdf->Cell(43,50,utf8_decode(date("h:i A",strtotime($horarioApertura)) . " A " .date("h:i A",strtotime($horarioCierre)) ),0,1,'R');
              $pdf->setXY(110,175);
              $pdf->SetFont('Arial','B',9.5);
              $pdf->Cell(45,50,utf8_decode('AMPLIACIÓN DE HORARIO:'),0,1,'L');
              $pdf->setXY(30,207);
              $pdf->Cell(15,5,utf8_decode('PRESIDENTE'),0,1,'L');
              $pdf->setXY(15,222);
              $pdf->Cell(55,3,utf8_decode('________________________________'),0,0,'L');
              $pdf->setXY(25,225);
              $pdf->Cell(65,5,utf8_decode("Lic. Antonio Mejía Lira"),0,1,'L');
              $pdf->setXY(150,207);
              $pdf->Cell(15,5,utf8_decode('SECRETARIO'),0,1,'L');
              $pdf->setXY(120,222);
              $pdf->Cell(55,3,utf8_decode('__________________________________________'),0,0,'L');
              $pdf->setXY(137,225);
              $pdf->Cell(60,5,utf8_decode("M. en A. Carlos Alberto Rentería Rivera"),0,'j',0);
              $pdf->setXY(30,233);

              $pdf->SetFont('Arial','B',15);
              $pdf->setXY(10,260);
              $pdf->Cell(200,5,utf8_decode('COLOCAR ESTA LICENCIA EN UN LUGAR VISIBLE'),0,1,'C');
              ///////////////////////////////////////////Inicia la otra pagina////////////////////////////////////////////////
              $pdf->AddPage();
              $pdf->setXY(10,155);
              $pdf->Image( base_url() . 'plantilla/images/img-tequis/TEQUIS-MARCA.png',30,40, 150 , 200,'PNG');
              $pdf->setXY(5,5);
              $pdf->Cell(200,120,utf8_decode(''),1,1,'C');
              $pdf->SetFont('Arial','',10);
              $pdf->setXY(5,20);
              $pdf->Cell(200,5,utf8_decode('A LOS CONTRIBUYENTES'),0,1,'C');
              $pdf->setXY(5,35);
              $pdf->Cell(200,5,utf8_decode('A FIN DE NO EXPONERSE AL PAGO DE MULTAS POR INFRACCIONES A LOS ORDENAMIENTOS MUNICIPALES,'),0,1,'C');
              $pdf->setXY(10,45);
              $pdf->Cell(150,5,utf8_decode('EL TITULAR DE ESTA LICENCIA DEBERÁ ABSTENERSE DE:'),0,1,'L');
              $pdf->setXY(30,60);
              $pdf->Cell(150,5,utf8_decode('-REALIZAR ACTIVIDAD(ES) DISTINTA(S) A LA AUTORIZADA.'),0,1,'L');
              $pdf->setXY(30,65);
              $pdf->Cell(150,5,utf8_decode('-ABRIR SU ESTABLECIMIENTO DESPUÉS DEL HORARIO PERMITIDO.'),0,1,'L');
              $pdf->setXY(30,70);
              $pdf->Cell(150,5,utf8_decode('-ABRIR LOS DÍAS DE CIERRE OBLIGATORIO.'),0,1,'L');
              $pdf->setXY(30,75);
              $pdf->Cell(150,5,utf8_decode('-MANTENER INSALUBRE SU ESTACIONAMIENTO ASÍ COMO EL FRENTE DEL MISMO.'),0,1,'L');
              $pdf->setXY(120,100);
              $pdf->Cell(80,5,utf8_decode('CON FUNDAMENTO EN EL REGLAMENTO DE'),0,1,'C');
              $pdf->setXY(120,105);
              $pdf->Cell(80,5,utf8_decode('POLICÍA Y GOBIERNO MUNICIPAL.'),0,1,'C');
              $pdf->setXY(10,160);
              $pdf->MultiCell(190,5,utf8_decode('LOS CONTRIBUYENTES TENDRÁN LA LICENCIA MUNICIPAL EN LUGAR VISIBLE, EL NO CUMPLIR CON ESTA DISPOSICIÓN ORIGINARA SANCIÓN ECONÓMICA. LA LICENCIA NO DEBERÁ TENER ENMENDADURAS.'),0,'L',0);
              $pdf->Ln('10');
              $pdf->MultiCell(190,5,utf8_decode('LOS CONTRIBUYENTES SON SUJETOS  DE LOS ORDENAMIENTOS QUE ESTABLECE LA LEY GENERAL DE HACIENDA DE LOS MUNICIPIOS DEL ESTADO DE QUERÉTARO Y LA LEY DE INGRESOS Y EGRESOS DE LOS MUNICIPIOS EN VIGOR.'),0,'L',0);
              $pdf->Ln('10');
              $pdf->MultiCell(190,5,utf8_decode('LOS CONTRIBUYENTES ESTÁN OBLIGADOS A OBSERVAR EL REGLAMENTO DE GOBIERNO MUNICIPAL EN TODO LO REFERENTE A LOS ORDENAMIENTOS  SOBRE LA REGLAMENTACIÓN AL COMERCIO, INDUSTRIA Y OFICIOS VARIOS.'),0,'L',0);
              $pdf->Ln('10');
              $pdf->MultiCell(190,5,utf8_decode('ESTA LICENCIA TENDRÁ VIGENCIA POR UN AÑO, DEBIÉNDO PRESENTAR EL CONTRIBUYENTE EN EL MES DE ENERO, FEBRERO O MARZO DE CADA AÑO PARA SU REVALIDACIÓN, PREVIO PAGO DE LOS DERECHOS CORRESPONDIENTES.'),0,'L',0);
              $pdf->Ln('10');
              $pdf->MultiCell(190,5,utf8_decode('EN CASO DE PERDIDA O EXTRAVÍO DE ESTA LICENCIA, EL CONTRIBUYENTE PODRÁ SOLICITAR A LA SECRETARÍA DE FINANZAS PÚBLICAS MUNICIPALES LA EXPEDICIÓN DE UN DUPLICADO PREVIO PAGO.'),0,'L',0);
              $pdf->setXY(5,140);
              $pdf->Cell(200,135,utf8_decode(''),1,1,'C');
              //justo aqui termina la elaboracion de docmuento licencia de funcionamiento

                //se limpia y cierra para evitar errores
                //
                $pdf->close();
              /*
              * Se manda el pdf al navegador
              *
              * $this->pdf->Output(nombredelarchivo, destino);
              *
              * I = Muestra el pdf en el navegador
              * D = Envia el pdf para descarga
              * F = guarda el archivo dn una ruta
              */
              //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
              $name = "pdf/licencia_funcionamiento_{$id_tramite}_{$id_solicitud}.pdf";
              $pdf->Output(''.$name, 'f');

              //retorno la url
              break;
                default:
                # code...
                break;
                 }
          break;//tramite caso 4
         case '3':
         //factibilidad de giro
         $tramite= $this->TramitesConsulta->descTramite($id_tramite);


        switch ($id_fase) {
          case '7':
          $folio = 'OP/0'.$id_tramite.'-'.$id_solicitud;
            $fetein = 30226;
            $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
            $cadena = $fechaCap.$fetein.$id_solicitud;
            $ca = (int)$cadena;
            
            $final= $this->generaCadena($ca);
          //orden de pago
          if(!empty($cantidad)){
            $pdf->SetTitle(utf8_decode("Pase a Caja"));
            $pdf->SetFillColor(200,200,200);
            $pdf->getX(105);
            $pdf->SetY(18);
            $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
            $pdf->Ln('8');
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(30);
            $pdf->Ln(20);

            //se pinta la fecha con el lugar donde se emite el documento
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
            $pdf->Ln(5);
            $pdf->SetFont('Arial','b',12);
            $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
            $pdf->Ln(21);
            $pdf->SetFont('Arial','B',12);
            $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',12);
            //podemos mandar id de form o nombre del formulario
            $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }
            $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',12);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',14);
            $pdf->Cell(190,4,utf8_decode($tramite->nombre.'.'),0,'J',false);
            $pdf->Ln(7);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(190,4,utf8_decode('('.$Subt->nombreSubTramite.').'),0,'J',false);
            $pdf->Ln(13);
            $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
            $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
            $pdf->Ln(8);
            $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
            //seccion cadena de pago
           
            $pdf->Ln(30);
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
            $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
            
            $pdf->Ln(15);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
            $pdf->Ln(6);
            $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'L,R',0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
             //se limpia y cierra para evitar errores

             $pdf->close();
           /*
           * Se manda el pdf al navegador
           *
           * $this->pdf->Output(nombredelarchivo, destino);
           *
           * I = Muestra el pdf en el navegador
           * D = Envia el pdf para descarga
           * F = guarda el archivo dn una ruta
           */
           //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
             $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
             $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }
            break;//pago
            case '5':
                  if(!empty($noDictamen) && !empty($nombreNegocio)){
                    //traer el giro del tramite seleccionado por el ciudadano
                  $giro= $this->M_Catalogo->solGiro($id_solicitud);
                  $solicitud  = $this->Solicitud_M->getSolicitud($this->session->userdata('id_usuario'),4,$id_solicitud);
                  $folio = 'N.º FG-0'.$id_tramite.$id_solicitud.'/'.$ano;
                  //aqui de aqui generare el documento de factibilidad de giro para despues cortar y pegar dnde realmente va
                  $pdf->SetTitle(utf8_decode("Factibilidad de giro"));
                  $pdf->Ln('20');
                  $pdf->SetFont('Arial','B',12);
                  $pdf->setY(20);
                  $pdf->setX(55);
                  $pdf->Cell(100,5,utf8_decode('SECRETARÍA DE DESARROLLO URBANO'),0,1,'C');
                  $pdf->setX(69);
                  $pdf->Cell(70,5,utf8_decode('VIVIENDA Y OBRAS PÚBLICAS'),0,1,'C');
                  $pdf->SetFont('Arial','B',11);
                  $pdf->Ln('8');
                  $pdf->Cell(190,5,utf8_decode('DIRECCIÓN DE DESARROLLO URBANO Y VIVIENDA'),0,1,'R');
                  $pdf->Cell(190,5,utf8_decode($folio),0,1,'R');
                  $pdf->Ln('5');
                  $pdf->Cell(190,5,'ASUNTO: '.$tramite->nombre,0,1,'R');
                  $pdf->SetFont('Arial','',11);
                  $pdf->Cell(0,5,utf8_decode('Querétaro, Querétaro a '.$fecha.'.'),0,1,'l');
                  $pdf->Ln('5');
                  $pdf->SetFont('Arial','b',12);
                  $pdf->Cell(190,5,utf8_decode($nombreNegocio),0,1,'l');
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(190,5,utf8_decode('REPRESENTADA POR'),0,1,'l');
                  $pdf->SetFont('Arial','b',12);

                  $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }

                  $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
                  $pdf->Cell(190,5,utf8_decode('P R E S E N T E.'),0,1,'l');
                  $pdf->Ln('5');
                  $pdf->SetFont('Arial','',11);
                  $mes_inicio = date("M",strtotime($solicitud->fecha_inicio));

                  if ($mes_inicio=="Jan") $mes_inicio="Enero";
                  if ($mes_inicio=="Feb") $mes_inicio="Febrero";
                  if ($mes_inicio=="Mar") $mes_inicio="Marzo";
                  if ($mes_inicio=="Apr") $mes_inicio="Abril";
                  if ($mes_inicio=="May") $mes_inicio="Mayo";
                  if ($mes_inicio=="Jun") $mes_inicio="Junio";
                  if ($mes_inicio=="Jul") $mes_inicio="Julio";
                  if ($mes_inicio=="Aug") $mes_inicio="Agosto";
                  if ($mes_inicio=="Sep") $mes_inicio="Septiembre";
                  if ($mes_inicio=="Oct") $mes_inicio="Octubre";
                  if ($mes_inicio=="Nov") $mes_inicio="Noviembre";
                  if ($mes_inicio=="Dec") $mes_inicio="Diciembre";

                  $fecha_inicio = date("d",strtotime($solicitud->fecha_inicio))." de ".$mes_inicio." de ".date("Y",strtotime($solicitud->fecha_inicio));;
                  $contenido_documento = 'En atención a la solicitud que ingreso el día '.$fecha_inicio.', en la cual solicita la <b>'.$tramite->nombre.'</b> del predio localizado en ';

                  $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");//frm_ubicacion_predio
                  $this->json->valueFieldForm("txtCalle");
                  $direccion = "<b>".$this->json->getFieldValue();
                  $this->json->valueFieldForm("txtNumero");
                  $direccion .= " " . $this->json->getFieldValue();
                  $this->json->valueFieldForm("txtColonia");
                  $direccion .= ", " . $this->json->getFieldValue();
                  $this->json->valueFieldForm("txtDelegacion");
                  $direccion .= ", Municipio " . $this->json->getFieldValue() . ", Quéretaro.</b> ";

                  $contenido_documento .= $direccion." El cual cuenta con <b>Dictamen de Uso de Suelo,</b> con <b>Oficio NO. " . $noDictamen ."</b> de fecha $fecha, del cual solicita <b>$tramite->nombre</b> para ubicar <b>$giro->descripcion.</b> ";

                  $pdf->WriteHtmlCell(190,utf8_decode($contenido_documento));
                //  $pdf->SetFont('Arial','b',11);
                  //$pdf->MultiCell(190,5,utf8_decode($this->json->getFieldValue()),0,'J',0);
                  //$pdf->SetFont('Arial','',11);
                  /*$pdf->Cell(33,5,utf8_decode('El cual cuenta con'),0,0,'l');
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(51,5,utf8_decode('Dictamen de Uso de Suelo,'),0,0,'l');
                  $pdf->SetFont('Arial','',11);
                  $pdf->Cell(8,5,utf8_decode('con'),0,0,'l');
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(10,5,utf8_decode('No.'.$noDictamen),0,1,'l');
                  $pdf->SetFont('Arial','',11);*/
                  /*$pdf->Cell(89,5,utf8_decode('de fecha '.$fecha.' del cual solicita'),0,0,'l');
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(37,5,utf8_decode($tramite->nombre),0,0,'l');
                  $pdf->SetFont('Arial','',11);
                  $pdf->Cell(10,5,utf8_decode('para ubicar'),0,1,'l');
                  $pdf->SetFont('Arial','b',11);
                  $pdf->MultiCell(189,5,utf8_decode($giro->descripcion),0,'j',0);*/
                  $pdf->Cell(190,5,utf8_decode(''),0,1,'l');
                  $pdf->Ln();
                  $pdf->WriteHTMLCell(190,utf8_decode('Le comunico que por lo que dada la superficie del predio, y revisando la tabla de normatividad de usos de suelo del Programa de Desarrollo Urbano del Centro de Población de este Municipio, se dictamina <b>FACTIBLE</b> el uso requerido, <b>la presente Factibilidad de Giro queda sujeto al impacto que su actividad genere en la Zona,</b> debiendo obtener el Visto Bueno que garantice el cumplimiento de las Normas de Seguridad que establece la Unidad Estatal de Protección Civil previo a su funcionamiento, presentado el acta correspondiente ante esta Autoridad Municipal, respetar el área de estacionamiento según lo dispuesto por los artículos 21 y 22 del Reglamento General de Construcción para el Estado de Querétaro, así como la normatividad y requerimientos  indicados por el Código Urbano del Estado de Querétaro, en su Titulo Cuarto y condicionado a respetar la normatividad que a continuación se indica:'));
                  $pdf->Ln('6');
                  $pdf->MultiCell(190,5,utf8_decode('El presente NO AUTORIZA el inicio de las obras ampliación y/o modificación en el predio, ni su funcionamiento, para tal fin, deberá coordinarse con la Autoridad correspondiente para la tramitación de los permisos que la normatividad requiera.'),0,'J',0);
                  $pdf->Ln('5');
                  $pdf->MultiCell(190,5,utf8_decode('En caso de pretender colocar anuncios publicitarios en el inmueble, deberá contar previamente con los permisos correspondientes.'),0,'J',0);
                  $pdf->SetFont('Arial','B',11);
                  $pdf->Ln('5');
                  $pdf->MultiCell(190,5,utf8_decode('El incumplimiento de cualquiera de las condicionantes indicadas, será motivo de CANCELACIÓN del presente documento, independientemente de hacerse  acreedor a las SANCIONES que correspondan.'),0,'J',0);
                  $pdf->Ln('5');
                  $pdf->SetFont('Arial','B',12);
                  $pdf->Cell(190,5,utf8_decode('A T E N T A M E N T E'),0,1,'C');
                  $pdf->SetFont('Arial','BI',12);
                  $pdf->Cell(190,5,utf8_decode('"QUERÉTARO"'),0,0,'C');
                  $pdf->Ln('18');
                  $pdf->SetFont('Arial','B',12);
                  //$pdf->Ln('27');
                  $pdf->Cell(189,5,utf8_decode('"Arq. Miguel Cabrera Lopéz"'),0,1,'C');
                  $pdf->setx(65);
                  $pdf->MultiCell(90,5,utf8_decode('SECRETARIO DE DESARROLLO URBANO, VIVIENDA Y OBRAS PÚBLICAS'),0,'C',0);
                  //fin de documento de Factibilad de giro

                  //se limpia y cierra para evitar errores

                  $pdf->close();
                /*
                * Se manda el pdf al navegador
                *
                * $this->pdf->Output(nombredelarchivo, destino);
                *
                * I = Muestra el pdf en el navegador
                * D = Envia el pdf para descarga
                * F = guarda el archivo dn una ruta
                */
                //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
                $name = "pdf/factibilidad_giro{$id_tramite}_{$id_solicitud}.pdf";
                $pdf->Output(''.$name, 'f');
              }else{
                $name = null;
              }
                //retorno la url
                break;
                  default:
                  # code...
                  break;
                    }
            break;//tramite caso 3
              case '7'://tramite licecnia de construccion
              $tramite= $this->TramitesConsulta->descTramite($id_tramite);
              switch ($id_fase) {
          case '7':
          //orden de pago
          $folio ='OP/0'.$id_tramite.'-'.$id_solicitud;
          $fetein=30216;
          $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
            $cadena = $fechaCap.$fetein.$id_solicitud;
            $ca = (int)$cadena;
            
            $final= $this->generaCadena($ca);
          if(!empty($cantidad)){
            $tipo_construccion = $this->M_Catalogo->solictud_tipoConstruccion($id_solicitud);
            $tipo = $tipo_construccion->tipo;
            $pdf->SetTitle(utf8_decode("Pase a Caja"));
            $pdf->SetFillColor(200,200,200);
            $pdf->getX(105);
            $pdf->SetY(18);
            $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
            $pdf->Ln('8');
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(30);
            $pdf->Ln(20);

            //se pinta la fecha con el lugar donde se emite el documento
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
            $pdf->Ln(5);
            $pdf->SetFont('Arial','b',12);
            $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
            $pdf->Ln(21);
            $pdf->SetFont('Arial','B',12);
            $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',12);
            //podemos mandar id de form o nombre del formulario
            $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }
            $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',12);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',14);
            $pdf->Cell(190,4,utf8_decode($tramite->nombre.'.'),0,'J',false);
            $pdf->Ln(7);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(190,4,utf8_decode('('.$Subt->nombreSubTramite.').'),0,'J',false);
            $pdf->Ln(13);
            $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
            $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
            $pdf->Ln(8);
            $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
            //seccion cadena de pago
            $pdf->Ln(30);
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
            $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
            
            $pdf->Ln(15);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
            $pdf->Ln(6);
            $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'L,R',0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
             //se limpia y cierra para evitar errores

             $pdf->close();
           /*
           * Se manda el pdf al navegador
           *
           * $this->pdf->Output(nombredelarchivo, destino);
           *
           * I = Muestra el pdf en el navegador
           * D = Envia el pdf para descarga
           * F = guarda el archivo dn una ruta
           */
           //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
            $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
            $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }

            break;//fase caso
            case '5':
              $folio = "N° LC-0{$id_tramite}{$id_solicitud}/{$ano}";
//
              $tipo_construccion = $this->M_Catalogo->solictud_tipoConstruccion($id_solicitud);
              if (!empty($this->Documentos->terminacion($id_solicitud))) {
                $pdf->SetTitle(utf8_decode("Terminacion de obra"));
                  $pdf->Ln('20');
                  $pdf->SetFont('Arial','B',11);
                  $pdf->setY(20);
                  $pdf->setX(55);
                  $pdf->Cell(100,5,utf8_decode('SECRETARÍA DE DESARROLLO URBANO, VIVIENDA Y OBRAS PUBLICAS'),0,1,'C');
                  $pdf->setX(69);
                  $pdf->Cell(70,5,utf8_decode('VENTANILLA ÚNICA DE GESTIÓN'),0,1,'C');
                  $pdf->SetFont('Arial','B',11);
                  $pdf->Ln('8');
                  $pdf->Cell(190,5,utf8_decode('Aviso de Terminación de Obra'),0,1,'R');
                  $pdf->Cell(190,5,utf8_decode('y Autorización de Uso y Ocupación'),0,1,'R');
                  $pdf->Cell(190,5,utf8_decode($folio),0,1,'R');
                  $pdf->SetFont('Arial','',11);
                  $pdf->Cell(0,5,utf8_decode('Querétaro a '.$fecha.'.'),0,1,'l');
                  $pdf->Ln('5');
                  $pdf->SetFont('Arial','',12);
                  $pdf->MultiCell(190,90,utf8_decode(''),1,'J',0);
                  $pdf->setY(66);
                  $pdf->setX(17);
                  $pdf->Cell(0,5,utf8_decode('PROPIETARIO'),0,1,'l');
                  $pdf->SetFont('Arial','b',9);

                  $presente ="";
                  $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
                 if($representante == 1){
                    $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                    $this->json->setFieldValue("");
                    $this->json->valueForm();
                    $presente=$this->json->getFieldValue();
                    $pdf->Cell(170,3,utf8_decode("".$presente),0,0,'R');
                  }else if($representante == 59){
                    $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");// id = 9
                    $this->json->setFieldValue("");
                    $this->json->valueFieldForm("txtPrimerApellido");
                    $primerAP = $this->json->getFieldValue();
                    $this->json->setFieldValue("");
                    $this->json->valueFieldForm("txtSegundoApellido");
                    $segundoAP = $this->json->getFieldValue();
                    $this->json->setFieldValue("");
                    $this->json->valueFieldForm("txtPrimerNombre");
                    $primername = $this->json->getFieldValue();
                    $this->json->setFieldValue("");
                    $this->json->valueFieldForm("txtSegundoNombre");
                    $segundoname = $this->json->getFieldValue();
                    $pdf->Cell(50,3,utf8_decode("".$primerAP),0,0,'C');
                    $pdf->Cell(50,3,utf8_decode("".$segundoAP),0,0,'C');
                    $pdf->Cell(84,3,utf8_decode("".$primername." ".$segundoname),0,0,'C');
                  }
                
                  $pdf->SetFont('Arial','',7);
                  $pdf->setX(17);
                  $pdf->Ln('2');
                  $pdf->Cell(185,5,utf8_decode(""),'B',1,'l');
                  $pdf->Cell(185,5,utf8_decode('APELLIDO PATERNO                               APELLIDO  MATERNO                                NOMBRE Y/O RAZÓN SOCIAL (REPRESENTANTE LEGAL)'),0,1,'C');
                  $pdf->SetFont('Arial','',12);
                  $pdf->setX(17);
                  $pdf->Cell(125,5,utf8_decode('DA AVISO DE TERMINACIÓN DE LA OBRA CONSISTENTE EN:'),0,1,'l');
                  $this->json->formData($data_informacion_solicitud,"frm_terminacion_obra");// id = 9
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtAvisoTerminacionObra");
                  $terminacionObra = $this->json->getFieldValue();
                  $pdf->SetFont('Arial','b',9);
                  $pdf->setX(17);
                  $pdf->Cell(185,5,utf8_decode(''.$terminacionObra),'B',1,'l');
                  $pdf->Ln('2');
                  $pdf->setX(17);
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(25,5,utf8_decode('UBICACIÓN:'),0,0,'l');
                  $pdf->SetFont('Arial','B',11);
                  $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");//frm_ubicacion_predio
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtCalle");
                  $calle = $this->json->getFieldValue();
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtNumero");
                  $numero = $this->json->getFieldValue();
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtColonia");
                  $colonia = $this->json->getFieldValue();
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtDelegacion");
                  $delegacion = $this->json->getFieldValue();
                  $pdf->Cell(1,5,utf8_decode("       ".$calle."                   ".$numero."                     ".$colonia."                             ".$delegacion),0,0,'r');
                  $pdf->Cell(160,5,utf8_decode(''),'B',1,'l');
                  $pdf->SetFont('Arial','',7);
                  $pdf->setX(42);
                  $pdf->Cell(160,5,utf8_decode('CALLE                          NÚMERO                          COLONIA O   FRACCIONAMIENTO                           DELEGACIÓN'),0,1,'C');
                  $pdf->SetFont('Arial','',12);
                  $pdf->setX(17);
                  $pdf->Cell(43,5,utf8_decode('CLAVE CATASTRAL:'),0,0,'l');
                  $this->json->formData($data_informacion_solicitud,"frm_terminacion_obra");//frm_ubicacion_predio
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtClaveCatastral");
                  $catastral = $this->json->getFieldValue();
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(142,5,utf8_decode(''.$catastral),'B',1,'l');
                  $pdf->Ln('2');
                  $pdf->setX(17);
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(25,5,utf8_decode('TELÉFONO:  '),0,0,'l');
                  $pdf->setX(42);
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtTelefono");
                  $telefono = $this->json->getFieldValue();
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(70,5,utf8_decode(''.$telefono),'B',0,'l');
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(10,5,utf8_decode('FAX:  '),0,0,'l');
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtFax");
                  $fax = $this->json->getFieldValue();
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(80,5,utf8_decode('  '.$fax),'B',1,'l');
                  $pdf->SetFont('Arial','',12);
                  $pdf->Ln('2');
                  $pdf->setX(17);
                  $pdf->Cell(112,5,utf8_decode('LA CUAL CUENTA CON DICTAMEN USO DE SUELO No.'),0,0,'l');
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtDictamen");
                  $dictamen = $this->json->getFieldValue();
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(40,5,utf8_decode(''.$dictamen),'B',1,'l');
                  $pdf->Ln('2');
                  $pdf->setX(17);
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(86,5,utf8_decode('Y CON LICENCIA DE CONSTRUCCIÓN No.'),0,0,'l');
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtLicenciaConstruccion");
                  $licencia = $this->json->getFieldValue();
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(66,5,utf8_decode(''.$licencia),'B',1,'l');
                  $pdf->Ln('2');
                  $pdf->setX(17);
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(30,5,utf8_decode('EXPEDIDA EL'),0,0,'l');
                  $pdf->SetFont('Arial','b',11);
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtExpedida");
                  $expedida = $this->json->getFieldValue();
                  $dateExpedida = date("d-m-Y", strtotime($expedida));
                  $pdf->Cell(35,5,utf8_decode(''.$dateExpedida),'B',0,'l');
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(43,5,utf8_decode('Y REVALIDACIÓN EL'),0,0,'l');
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtRevalidacion");
                  $revalidacion = $this->json->getFieldValue();
                  $dateRevalidacion = date("d-m-Y", strtotime($revalidacion));
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(42,5,utf8_decode('  '.$dateRevalidacion),'B',0,'l');
                  $pdf->Ln('10');
                  $pdf->setX(17);
                  $pdf->SetFont('Arial','',12);
                  $pdf->Cell(68,5,utf8_decode('CON UN VALOR ESTIMADO DE $'),0,0,'l');
                  $this->json->setFieldValue("");
                  $this->json->valueFieldForm("txtValorEstimado");
                  $valor = $this->json->getFieldValue();
                  $pdf->SetFont('Arial','b',11);
                  $pdf->Cell(80,5,utf8_decode(''.$valor),'B',1,'l');
                  $pdf->Ln('22');
                  $pdf->MultiCell(190,30,utf8_decode(''),1,'J',0);
                  $pdf->SetFont('Arial','',12);
                  //caja 2
                  $pdf->setX(17);
                  $pdf->sety(171);
                  $pdf->Cell(67,5,utf8_decode('REVISÓ'),0,0,'C');
                  $pdf->setX(120);
                  $pdf->Cell(65,5,utf8_decode('AUTORIZACIÓN'),0,1,'C');
                  $pdf->Ln('7');
                  $pdf->setX(17);
                  $pdf->Cell(65,5,utf8_decode(''),'B',0,'C');
                  $pdf->setX(120);
                  $pdf->Cell(65,5,utf8_decode(''),'B',1,'C');
                  $pdf->Ln('2');
                  $pdf->setX(17);
                  $pdf->Cell(65,5,utf8_decode('SUPERVISOR DE OBRA '),0,0,'C');
                  $pdf->setX(120);
                  $pdf->Cell(65,5,utf8_decode('DIR. DE DESARROLLO URBANO Y VIVIENDA'),0,1,'C');
                  $pdf->setY(-41);
                  $pdf->SetFont('Arial','',7);
                  $pdf->MultiCell(189.5,5,utf8_decode('HABIÉNDOSE REALIZADO LA INSPECCIÓN CORRESPONDIENTE A LA OBRA CITADA Y VERIFICÁNDOSE QUE SE ENCUENTRA TOTALMENTE TERMINADA Y DE ACUERDO A LA LICENCIA Y PLANOS APROBADOS POR ESTA DEPENDENCIA, SE OTORGA LA PRESENTE AUTORIZACIÓN CON FUNDAMENTO A LO DISPUESTO POR EL ARTÍCULO 268 DEL CÓDIGO URBANO PARA EL ESTADO DE QUERÉTARO.'),0,'J',0);

                  $pdf->close();

              }//if de terminacion de obra
              else {
              $pdf->SetTitle(utf8_decode("Licencia de construcción"));
              $pdf->SetFont('Arial','B',11);
              $pdf->setXY(5,5);
              $pdf->Cell(33,20,utf8_decode(''),'R',0,'C');
              $pdf->setXY(40,5);
              $pdf->Cell(130,5,utf8_decode('SECRETARÍA DE DESARROLLO URBANO, VIVIENDA Y OBRAS PUBLICAS'),0,1,'L');
              $pdf->SetFont('Arial','',13);
              $pdf->setXY(40,10);
              $pdf->Cell(130,5,utf8_decode('LICENCIA DE CONSTRUCCIÓN'),0,1,'L');
              $pdf->SetFont('Arial','',11);
              $pdf->setXY(40,15);
              $pdf->Cell(190,5,utf8_decode('Querétaro, Qro.'),0,1,'L');
              $pdf->setXY(40,20);
              $pdf->Cell(190,5,utf8_decode('C.P.76750 Teléfonos: 01(414) 2732327-2730692'),0,1,'L');
              $pdf->SetFont('Arial','',10);
              $pdf->setXY(145,30);
              $pdf->SetFillColor(232,232,232);
              $pdf->Cell(17,7,utf8_decode('FOLIO:'),'LBT',0,'L',1);
              $pdf->Cell(38,7,utf8_decode($folio),'TRB',1,'R');
              $pdf->Ln(3);

              $presente ="";
              $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
             if($representante == 1){
                $this->json->formData($data_informacion_solicitud,"frm_razon_social");
                $this->json->setFieldValue("");
                $this->json->valueForm();
                $presente=$this->json->getFieldValue();
              }else if($representante == 59){
                $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
                $this->json->setFieldValue("");
                $this->json->valueForm();
                $presente=$this->json->getFieldValue();
              }
              $pdf->setXY(10,40);
              $pdf->MultiCell(190,5,utf8_decode('EL MUNICIPIO  CONCEDE LICENCIA A: '. $presente),0,'L',0);
              $pdf->setXY(10,45);
              $pdf->Cell(190,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(112,40);
              $pdf->Cell(88,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(10,53);
              $pdf->MultiCell(190,5,utf8_decode('PARA LA OBRA QUE CONSISTE EN: '.$tipo_construccion->tipo),0,'L',0);
              $pdf->setXY(72,53);
              $pdf->Cell(128,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(10,58);
              $pdf->Cell(190,5,utf8_decode(''),'B',1,'L');
              $pdf->SetFont('Arial','',12);
              $pdf->setXY(10,67);
              $pdf->Cell(190,7,utf8_decode('DATOS DE LA OBRA'),1,1,'C',1);
              $pdf->setXY(10,79);
              $this->json->formData($data_informacion_solicitud,"frm_datos_obra");// id = 35
              $this->json->valueFieldForm("txtCalle");
              $direccion = " ".$this->json->getFieldValue();
              $this->json->valueFieldForm("txtNumeroOficial");
              $direccion .= " " . $this->json->getFieldValue();
              $pdf->MultiCell(190,5,utf8_decode('DIRECCIÓN: ' . $direccion),0,'L',0);
              $pdf->setXY(36,79);
              $pdf->Cell(164,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(10,89);
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtColonia");
              $pdf->MultiCell(190,5,utf8_decode('COLONIA: ' . $this->json->getFieldValue()),0,'L',0);
              $pdf->setXY(33,89);
              $pdf->Cell(167,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(10,99);
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtLocalidad");
              $pdf->MultiCell(67,5,utf8_decode('LOCALIDAD: ' . $this->json->getFieldValue()),0,'L',0);
              $pdf->setXY(36,99);
              $pdf->Cell(40,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(76,99);
              $pdf->MultiCell(70,5,utf8_decode('DELEGACIÓN: '),0,'L',0);
              $pdf->setXY(106,99);
              $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");// id = 35
              $this->json->valueFieldForm("txtDelegacion");
              $pdf->Cell(40,5,utf8_decode($this->json->getFieldValue()),'B',1,'L');
              $pdf->setXY(146,99);
              $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");// id = 20
              $pdf->MultiCell(64,5,utf8_decode('MUNICIPIO: _________'),0,'L',0);
              $pdf->setXY(171,99);
              $pdf->Cell(29,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(10,109);
              $pdf->Cell(190,7,utf8_decode('DATOS DEL PERITO RESPONSABLE'),1,1,'C',1);
              $pdf->setXY(10,119);
              $this->json->formData($data_informacion_solicitud,"frm_datos_perito");// id = 35
              $this->json->valueFieldForm("txtCedulaProfesional");
              $pdf->MultiCell(190,5,utf8_decode('CÉDULA PROFESIONAL: ' .  $this->json->getFieldValue()),0,'L',0);
              $pdf->setXY(62,119);
              $pdf->Cell(138,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(10,129);
              $this->json->valueFieldForm("txtNombreRazonSocial");
              $nombre = $this->json->getFieldValue();
              $this->json->valueFieldForm("txtApellidoPaterno");
              $nombre .= " " . $this->json->getFieldValue();
              $this->json->valueFieldForm("txtApellidoMaterno");
              $nombre .= " " . $this->json->getFieldValue();
              $pdf->MultiCell(190,5,utf8_decode('NOMBRE: ' . $nombre  ),0,'L',0);
              $pdf->setXY(32,129);
              $pdf->Cell(168,5,utf8_decode(''),'B',1,'L'); 
              $pdf->setXY(12,149);
              $pdf->MultiCell(90,5,utf8_decode('NÚM DE LICENCIA: '),0,'L',0);
              $pdf->setXY(52,149);
              $pdf->Cell(48,5,utf8_decode($noLicencia),'B',1,'L');
              $pdf->setXY(100,149);
              $pdf->MultiCell(90,5,utf8_decode('NÚM DE ALINEAMIENTO: '),0,'L',0);
              $pdf->setXY(153,149);
              $pdf->Cell(44,5,utf8_decode($noAlineamiento),'B',1,'L');
              $pdf->setXY(12,159);
              $this->json->formData($data_informacion_solicitud,"frm_perito_responsable");// id = 35
              $this->json->valueFieldForm("txtMConstruidos");
              $pdf->MultiCell(60,5,utf8_decode('METROS: ' . number_format($this->json->getFieldValue(),2)),0,'L',0);
              $pdf->setXY(32,159);
              $pdf->Cell(28,5,utf8_decode(''),'B',1,'L');
              $pdf->setXY(60,159);
              $pdf->MultiCell(31,5,utf8_decode('CUADRADOS.'),0,'L',0);
              $pdf->setXY(103,159);
              $pdf->MultiCell(96,5,utf8_decode('VIGENCIA: '),0,'L',0);
              $pdf->setXY(126,159);
              $pdf->Cell(71,5,utf8_decode($vigencia),'B',1,'L');
              $pdf->setXY(10,143);
              $pdf->Cell(190,28,utf8_decode(''),1,1,'L');
              $pdf->setXY(10,175);
              $pdf->Cell(190,7,utf8_decode('OBSERVACIONES'),1,1,'C',1);
              $pdf->setXY(10,185);
              $pdf->Cell(190,70,utf8_decode($observaciones),1,1,'L');
              $pdf->setXY(110,190);
              $pdf->Cell(45,5,utf8_decode('FECHA DE INGRESO:'),0,0,'L');
              //obtenemos la fecha de la solicitud
              $solicitud  = $this->Solicitud_M->getSolicitud($this->session->userdata('id_usuario'),4,$id_solicitud);

              $pdf->Cell(40,5,utf8_decode(date("Y/m/d",strtotime($solicitud->fecha_inicio))),'B',0,'L');
              $pdf->setXY(110,200);
              $pdf->Cell(45,5,utf8_decode('FECHA DE ENTREGA:'),0,0,'L');
              $pdf->Cell(40,5,utf8_decode('      /    /          '),'B',0,'L');
              $pdf->SetFont('Arial','',7);
              $pdf->setXY(10,260);
              $pdf->Cell(190,5,utf8_decode('DE ACUERDO  A LOS TÍTULOS I, II, IV, VII, IX CON SUS RESPECTIVOS CAPÍTULOS Y ARTÍCULOS DEL CÓDIGO URBANO DEL ESTADO DE QUERÉTARO,'),0,1,'C');
              $pdf->Cell(190,5,utf8_decode('ASÍ COMO AL REGLAMENTO DE CONSTRUCCIONES DEL MUNICIPIO.'),0,1,'C');
              $pdf->Cell(190,5,utf8_decode('AL TERMINO DE LA LICENCIA SE DEBERÁ DAR AVISO DE TERMINACIÓN DE OBRA, O EN SU DEFECTO, LA REVALIDACIÓN DE LA LICENCIA.'),0,1,'C');
              $pdf->setXY(10,258);
              $pdf->Cell(190,18,utf8_decode(''),1,1,'L');
              $pdf->SetFont('Arial','',12);
              $pdf->setXY(110,245);
              $pdf->Cell(16,5,utf8_decode('SELLO:'),0,0,'L');
              $pdf->Cell(69,5,utf8_decode(''),'B',0,'L');
              //aqui termina el documento de licencia de costruccion

              //se limpia y cierra para evitar errores

              $pdf->close();
            /*
            * Se manda el pdf al navegador
            *
            * $this->pdf->Output(nombredelarchivo, destino);
            *
            * I = Muestra el pdf en el navegador
            * D = Envia el pdf para descarga
            * F = guarda el archivo dn una ruta
            */
             }//else de terminacion de obra
            //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
            $name = "pdf/licencia_costruccion{$id_tramite}_{$id_solicitud}.pdf";
            $pdf->Output(''.$name, 'f');

            //retorno la url
                    break;
                      default:
                      # code...
                      break;
                        }
                break;//tramite caso 7
        case '8':
         //informe uso ude suelo
        switch ($id_fase) {
          case '7':
          $tramite = $this->TramitesConsulta->descTramite($id_tramite);
          //orden de pago
          $folio = 'OP/0'.$id_tramite.'-'.$id_solicitud;
          $fetein=30233;
          $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
          $cadena = $fechaCap.$fetein.$id_solicitud;
          $ca = (int)$cadena;
          $final= $this->generaCadena($ca); 

          if(!empty($cantidad)){
            $pdf->SetTitle(utf8_decode("Pase a Caja"));
            $pdf->SetFillColor(200,200,200);
            $pdf->getX(105);
            $pdf->SetY(18);
            $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
            $pdf->Ln('8');
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(30);
            $pdf->Ln(20);

            //se pinta la fecha con el lugar donde se emite el documento
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
            $pdf->Ln(5);
            $pdf->SetFont('Arial','b',12);
            $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
            $pdf->Ln(21);
            $pdf->SetFont('Arial','B',12);
            $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',12);
            //podemos mandar id de form o nombre del formulario
            $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }
            $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',12);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',14);
            $pdf->Cell(190,4,utf8_decode($tramite->nombre.'.'),0,'J',false);
            $pdf->Ln(7);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(190,4,utf8_decode('('.$Subt->nombreSubTramite.').'),0,'J',false);
            $pdf->Ln(13);
            $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
            $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
            $pdf->Ln(8);
            $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
            //seccion cadena de pago
            $pdf->Ln(30);
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
            $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
            
            $pdf->Ln(15);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
            $pdf->Ln(6);
            $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'L,R',0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
             //se limpia y cierra para evitar errores
             $pdf->close();
           /*
           * Se manda el pdf al navegador
           *
           * $this->pdf->Output(nombredelarchivo, destino);
           *
           * I = Muestra el pdf en el navegador
           * D = Envia el pdf para descarga
           * F = guarda el archivo dn una ruta
           */
           //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
             $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
             $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }
            break;//pago
            case '5':
                  $tramite = $this->TramitesConsulta->descTramite($id_tramite);
                  $folio = 'IUS-0'.$id_tramite.$id_solicitud.'/'.$ano;
                  if(!empty($predioZona) && !empty($clave)){
                    //traer el giro del tramite seleccionado por el ciudadano
                  //$giro= $this->M_Catalogo->solGiro($id_solicitud);
                  //aqui de aqui generare el documento de informe uso de suelo
                  $pdf->SetTitle(utf8_decode("Informe uso de suelo"));
                  $pdf->Ln('20');
                  $pdf->SetFont('Arial','B',12);
                  $pdf->setY(20);
                  $pdf->setX(55);
                  $pdf->Cell(100,5,utf8_decode('SECRETARÍA DE DESARROLLO URBANO'),0,1,'C');
                  $pdf->setX(69);
                  $pdf->Cell(70,5,utf8_decode('VIVIENDA Y OBRAS PÚBLICAS'),0,1,'C');
                  $pdf->SetFont('Arial','B',11);
                  $pdf->Ln('8');
                  $pdf->Cell(190,5,utf8_decode('DIRECCIÓN DE DESARROLLO URBANO Y VIVIENDA'),0,1,'R');
                  $pdf->Cell(190,5,utf8_decode($folio),0,1,'R');
                  $pdf->Ln('5');
                  $pdf->Cell(190,5,'ASUNTO: '.$tramite->nombre,0,1,'R');
                  $pdf->SetFont('Arial','',11);
                  $pdf->Cell(0,5,utf8_decode('Querétaro a '.$fecha.'.'),0,1,'l');
                  $pdf->Ln('5');
                  $pdf->SetFont('Arial','b',12);
                  //podemos mandar id de form o nombre del formulario
                  $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_datos_solicitante");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }
                  $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
                  $pdf->SetFont('Arial','b',12);
                  $pdf->Cell(190,5,utf8_decode('P R E S E N T E.'),0,1,'l');
                  $pdf->Ln('5');
                  $pdf->SetFont('Arial','',11);
                  $this->json->setFieldValue("");
                  $this->json->formData($data_informacion_solicitud,"frm_ubicacion_predio");// id = 9
                  $this->json->valueForm();
                  $contenido_documento='En atención a su solicitud de fecha '.$fecha.', en la cual solicita el <b>'.$tramite->nombre.'</b> del predio ubicado en <b>'.$this->json->getFieldValue().'</b>, Municipio, Querétaro. Con número catastral: <b>'.$clave.'</b>.';
                  $pdf->WriteHtmlCell(190,utf8_decode($contenido_documento));
                  $pdf->Ln('12');
                  $pdf->SetFont('Arial','B',11);
                  $pdf->MultiCell(190,5,utf8_decode('Habiéndose  revisado  el  Plan  de   Desarrollo  Urbano  del  Centro  de  Población  de este  Municipio,'),0,'J',0);
                  $pdf->SetFont('Arial','',11);
                  $contenido = 'Técnico Jurídico cuya actualización fue aprobada por Acuerdo de Cabildo Municipal en la Vigésima Sesión Ordinaria celebrada el 11 de Noviembre del 2004, publicado en el Periódico Oficial de Gobierno del Estado "La Sombra de Arteaga" el 1 de Julio del año 2005, inscrito en el Registro Público de la Propiedad con fecha 25 de Octubre del 2005, bajo la Partida 90  Libro Uno Sección Especial del Registro de los  Planes de Desarrollo Urbanos Sección de los Municipios, del Registro Publico de la Propiedad Oficina Central ante la fe de la Lic. Lorena Montes Hernández, Directora del Registro Publico de la Propiedad y del Comercio en el Estado, encontrándose considerado dentro del Plan de Desarrollo Urbano del centro de Población, el predio ubicado en Zona  '.$predioZona.'. <b>Por lo anterior con fundamento en los artículos 10, 13 Fracción ll y 323 del Código Urbano para el Estado de Querétaro.</b>';
                  $pdf->WriteHtmlCell(190,utf8_decode($contenido));
                  $pdf->Ln('10');
                  $pdf->MultiCell(190,5,utf8_decode('Sin más por el momento, quedamos a sus distinguidas órdenes.'),0,'J',0);
                  $pdf->Ln('40');
                  $pdf->SetFont('Arial','B',12);
                  $pdf->Cell(190,5,utf8_decode('A T E N T A M E N T E'),0,1,'C');
                  $pdf->SetFont('Arial','BI',12);
                  $pdf->Cell(190,5,utf8_decode('"QUERÉTARO"'),0,0,'C');
                  $pdf->Ln('40');
                  $pdf->SetFont('Arial','B',12);

                  $pdf->Cell(190,5,utf8_decode('Arq. Luis Eduardo Barajas Ortiz'),0,1,'C');
                  $pdf->Cell(190,5,utf8_decode('DIRECTOR DE DESARROLLO URBANO Y VIVIENDA'),0,1,'C');
                  //fin de documento de Factibilad de giro
                  //se limpia y cierra para evitar errores
                  $pdf->close();
                /*
                * Se manda el pdf al navegador
                *
                * $this->pdf->Output(nombredelarchivo, destino);
                *
                * I = Muestra el pdf en el navegador
                * D = Envia el pdf para descarga
                * F = guarda el archivo dn una ruta
                */
                //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
                $name = "pdf/informe_uso_suelo{$id_tramite}_{$id_solicitud}.pdf";
                $pdf->Output(''.$name, 'f');
              }else{
                $name = null;
              }
                //retorno la url
                break;
                  default:
                  # code...
                  break;
                    }
            break;//tramite caso 8
            case 5://tramite de alcoholes
            $folio = 'OP/0'.$id_tramite.'-'.$id_solicitud;
            $fetein=31110;
            $tramite = $this->TramitesConsulta->descTramite($id_tramite);
            $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
            $cadena = $fechaCap.$fetein.$id_solicitud;
            $ca = (int)$cadena;
            $final= $this->generaCadena($ca);
          //orden de pago
          if(!empty($cantidad)){
            $pdf->SetTitle(utf8_decode("Pase a Caja"));
            $pdf->SetFillColor(200,200,200);
            $pdf->getX(105);
            $pdf->SetY(18);
            $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
            $pdf->Ln('8');
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(30);
            $pdf->Ln(20);

            //se pinta la fecha con el lugar donde se emite el documento
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
            $pdf->Ln(5);
            $pdf->SetFont('Arial','b',12);
            $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
            $pdf->Ln(21);
            $pdf->SetFont('Arial','B',12);
            $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',12);
            //podemos mandar id de form o nombre del formulario
            $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_dts_contribuyente");// id = 9
            $this->json->valueForm();
            $this->json->valueFieldForm("txtPrimerNom");
            $primerNombre = $this->json->getFieldValue();
            $this->json->setFieldValue("");
            $this->json->valueFieldForm("txtSegundoNom");
            $segundoNombre = $this->json->getFieldValue();
            $this->json->setFieldValue("");
            $this->json->valueFieldForm("txtPrimerAp");
            $primerApellido = $this->json->getFieldValue();
            $this->json->setFieldValue("");
            $this->json->valueFieldForm("txtSegundoAp");
            $segundoApellido = $this->json->getFieldValue();
            $this->json->setFieldValue("");
            $presente = $segundoNombre != "" ? "$primerNombre $segundoNombre $primerApellido $segundoApellido" : "$primerNombre $primerApellido $segundoApellido";
            }
            
            $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',12);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
            $pdf->Ln(7);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(190,4,utf8_decode($Subt->nombreSubTramite.'.'),0,'J',false);
            $pdf->Ln(13);
            $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
            $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
            $pdf->Ln(8);
            $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
            //seccion cadena de pago
            $pdf->Ln(30);
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
            $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
            
            $pdf->Ln(15);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
            $pdf->Ln(6);
            $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'L,R',0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
             //se limpia y cierra para evitar errores
             $pdf->close();
           /*
           * Se manda el pdf al navegador
           *
           * $this->pdf->Output(nombredelarchivo, destino);
           *
           * I = Muestra el pdf en el navegador
           * D = Envia el pdf para descarga
           * F = guarda el archivo dn una ruta
           */
           //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
             $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
             $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }
            break;//pago de alcoholes
            case 9://tramite de alcoholes provicional
            $folio = 'OP/0'.$id_tramite.'-'.$id_solicitud;
            $fetein=31111;
            $tramite = $this->TramitesConsulta->descTramite($id_tramite);
            $fechaCap= date("y", strtotime($fechaTabla));//se convierte la fecha al formato de captura
            $cadena = $fechaCap.$fetein.$id_solicitud;
            $ca = (int)$cadena;
            $final= $this->generaCadena($ca);
          //orden de pago
          if(!empty($cantidad)){
            $pdf->SetTitle(utf8_decode("Pase a Caja"));
            $pdf->SetFillColor(200,200,200);
            $pdf->getX(105);
            $pdf->SetY(18);
            $pdf->Cell(180,10,'ORDEN DE PAGO',0,0,'C');
            $pdf->Ln('8');
            $pdf->SetFont('Arial','B',15);
            $pdf->Cell(30);
            $pdf->Ln(20);

            //se pinta la fecha con el lugar donde se emite el documento
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,5,utf8_decode('Querétaro, Qro., a '.$fecha),0,1,'R');
            $pdf->Ln(5);
            $pdf->SetFont('Arial','b',12);
            $pdf->cell(180,4,utf8_decode("FOLIO: ".$folio),0,0,'R');
            $pdf->Ln(21);
            $pdf->SetFont('Arial','B',12);
            $pdf->cell(40,2,utf8_decode('Nombre del Interesado'),0,0,'L');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',12);
            //podemos mandar id de form o nombre del formulario
            $presente ="";
            $representante =$this->Solicitud_M->consultarazonSocial($id_solicitud);
           if($representante == 1){
              $this->json->formData($data_informacion_solicitud,"frm_razon_social");
              $this->json->setFieldValue("");
              $this->json->valueForm();
              $presente=$this->json->getFieldValue();
            }else if($representante == 59){
              $this->json->formData($data_informacion_solicitud,"frm_dts_contribuyente");// id = 9
              $this->json->valueForm();
              $this->json->valueFieldForm("txtPrimerNom");
              $primerNombre = $this->json->getFieldValue();
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtSegundoNom");
              $segundoNombre = $this->json->getFieldValue();
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtPrimerAp");
              $primerApellido = $this->json->getFieldValue();
              $this->json->setFieldValue("");
              $this->json->valueFieldForm("txtSegundoAp");
              $segundoApellido = $this->json->getFieldValue();
              $this->json->setFieldValue("");
              $presente = $segundoNombre != "" ? "$primerNombre $segundoNombre $primerApellido $segundoApellido" : "$primerNombre $primerApellido $segundoApellido";
            }
           
            $pdf->Cell(190,5,utf8_decode($presente),0,1,'l');
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',12);
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(40,5,utf8_decode('Concepto'),0,0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(187,4,utf8_decode($Subt->nombreSubTramite.'.'),0,'J',false);
            $pdf->Ln(13);
            $pdf->Cell(40,5,utf8_decode('Solicitud'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha emisión'),1,0,'C',0);
            $pdf->Cell(50,5,utf8_decode('Fecha de vencimiento'),1,0,'C',0);
            $pdf->Cell(0,5,utf8_decode('Total'),1,0,'C',0);
            $pdf->Ln(8);
            $pdf->Cell(40,5,utf8_decode($id_solicitud),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fechaTabla),0,0,'C',0);
            $pdf->Cell(50,5,utf8_decode($fecha_vencimiento),0,0,'C',0);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(0,5,utf8_decode('$'.number_format($cantidad,2)),0,0,'C',0);
            //seccion cadena de pago
            $pdf->Ln(30);
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(40,5,utf8_decode('Formas de Pago:'),0,0,'L',0);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',13);
            $pdf->Cell(40,5,utf8_decode('1. Cajas de Municipio.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('2. Ventanilla BBVA Bancomer.'),0,0,'L',0);
            $pdf->Ln(5);
            $pdf->Cell(40,5,utf8_decode('3. Transferencia Bancaria.'),0,0,'L',0);
            $pdf->Image(base_url().'plantilla/images/1280px-BBVA_2019.png',120,175,40);
            
            $pdf->Ln(15);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'BBVA','L,R,T',0,'L',0);
            $pdf->Ln(6);
            $pdf->Cell(120,8,'CIE:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'1612700','L,R',0,'L',0);//linea de captura
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Referencia Bancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'L,R',0,'L',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',13);
            $pdf->Cell(120,8,'Pago Interbancario','L,R',0,'C',0);
            $pdf->Ln(6);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Clabe Interbancaria:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,'012914002016127004','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(120,8,'Concepto:','L,R',0,'L',0);
            $pdf->Ln(4);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(120,8,$final,'B,L,R',0,'L',0);
             //se limpia y cierra para evitar errores
             $pdf->close();
           /*
           * Se manda el pdf al navegador
           *
           * $this->pdf->Output(nombredelarchivo, destino);
           *
           * I = Muestra el pdf en el navegador
           * D = Envia el pdf para descarga
           * F = guarda el archivo dn una ruta
           */
           //se guarda en la carpeta pdf con el nombre del tramite,concatenando el id de tramite y de solicitud
             $name = "pdf/paseCaja_{$id_tramite}_{$id_solicitud}.pdf";
             $pdf->Output(''.$name, 'f');
            }else{
              $name = null;
            }
            break;//pago de alcoholes provicional

        default:
        # code...
        break;

  }

  if(!is_null($name)){
    $jsonResponse["response"] = 200;
    $jsonResponse["msg"] = "Documento Generado Exitosamente";
    $jsonResponse["url"] = $name;
    $jsonResponse["lineaCaptura"] = $final;
    $jsonResponse["folio"] = $folio;
    $jsonResponse["fecha_vencimiento"] = $fecha_vencimiento;

    }else{
      $jsonResponse["response"] = 522;
      $jsonResponse["msg"] = "Completa los campos marcados con un (*)";
    }//else

  echo json_encode($jsonResponse);

}

public function generaCadena($cadena_captura){
  $linea_captura = 0;
  $r = str_pad($cadena_captura,18,0,STR_PAD_RIGHT);
  
  $cadena1 = substr($r,0,1); 
  $cadena2 = substr($r,1,1); 
  $cadena3 = substr($r,2,1);
  $cadena4 = substr($r,3,1);
  $cadena5 = substr($r,4,1);
  $cadena6 = substr($r,5,1);
  $cadena7 = substr($r,6,1);
  $cadena8 = substr($r,7,1);
  $cadena9 = substr($r,8,1);
  $cadena10 = substr($r,9,1);
  $cadena11 = substr($r,10,1);
  $cadena12 = substr($r,11,1);
  $cadena13 = substr($r,12,1);
  $cadena14 = substr($r,13,1);
  $cadena15 = substr($r,14,1);
  $cadena16 = substr($r,15,1);
  $cadena17 = substr($r,16,1);
  $cadena18 = substr($r,17,1);
  $cadena19 = substr($r,18,1);
  
  $rcadena1 = $cadena1 * 1;
  if($rcadena1>9){
    $rscadena1 = substr($rcadena1,0,1) + substr($rcadena1,1,1);
  }
  else {
    $rscadena1 = $rcadena1;
  }
  
  $rcadena2 = $cadena2 * 2;
  if($rcadena2>9){
    $rscadena2 = substr($rcadena2,0,1) + substr($rcadena2,1,1);
  }
  else {
    $rscadena2 = $rcadena2;
  }
  
  $rcadena3 = $cadena3 * 1;
  if($rcadena3>9){
    $rscadena3 = substr($rcadena3,0,1) + substr($rcadena3,1,1);
  }
  else {
    $rscadena3 = $rcadena3;
  }
  
  $rcadena4 = $cadena4 * 2;
  if($rcadena4>9){
    $rscadena4 = substr($rcadena4,0,1) + substr($rcadena4,1,1);
  }
  else {
    $rscadena4 = $rcadena4;
  }
  
  $rcadena5 = $cadena5 * 1;
  if($rcadena5>9){
    $rscadena5 = substr($rcadena5,0,1) + substr($rcadena5,1,1);
  }
  else {
    $rscadena5 = $rcadena5;
  }
  
  $rcadena6 = $cadena6 * 2;
  if($rcadena6>9){
    $rscadena6 = substr($rcadena6,0,1) + substr($rcadena6,1,1);
  }
  else {
    $rscadena6 = $rcadena6;
  }
  
  $rcadena7 = $cadena7 * 1;
  if($rcadena7>9){
    $rscadena7 = substr($rcadena7,0,1) + substr($rcadena7,1,1);
  }
  else {
    $rscadena7 = $rcadena7;
  }
  
  $rcadena8 = $cadena8 * 2;
  if($rcadena8>9){
    $rscadena8 = substr($rcadena8,0,1) + substr($rcadena8,1,1);
  }
  
  else {
    $rscadena8 = $rcadena8;
  }
  
  $rcadena9 = $cadena9 * 1;
  if($rcadena9>9){
    $rscadena9 = substr($rcadena9,0,1) + substr($rcadena9,1,1);
  }
  else {
    $rscadena9 = $rcadena9;
  }
  
  $rcadena10 = $cadena10 * 2;
  if($rcadena10>9){
    $rscadena10 = substr($rcadena10,0,1) + substr($rcadena10,1,1);
  }
  else {
    $rscadena10 = $rcadena10;
  }
  
  $rcadena11 = $cadena11 * 1;
  if($rcadena11>9){
    $rscadena11 = substr($rcadena11,0,1) + substr($rcadena11,1,1);
  }
  else {
    $rscadena11 = $rcadena11;
  }
  
  $rcadena12 = $cadena12 * 2;
  if($rcadena12>9){
    $rscadena12 = substr($rcadena12,0,1) + substr($rcadena12,1,1);
  }
  else {
    $rscadena12 = $rcadena12;
  }
  
  $rcadena13 = $cadena13 * 1;
  if($rcadena13>9){
    $rscadena13 = substr($rcadena13,0,1) + substr($rcadena13,1,1);
  }
  else {
    $rscadena13 = $rcadena13;
  }
  
  $rcadena14 = $cadena14 * 2;
  if($rcadena14>9){
    $rscadena14 = substr($rcadena14,0,1) + substr($rcadena14,1,1);
  }
  else {
    $rscadena14 = $rcadena14;
  }
  
  $rcadena15 = $cadena15 * 1;
  if($rcadena15>9){
    $rscadena15 = substr($rcadena15,0,1) + substr($rcadena15,1,1);
  }
  else {
    $rscadena15 = $rcadena15;
  }
  
  $rcadena16 = $cadena16 * 2;
  if($rcadena16>9){
    $rscadena16 = substr($rcadena16,0,1) + substr($rcadena16,1,1);
  }
  else {
    $rscadena16 = $rcadena16;
  }
  
  $rcadena17 = $cadena17 * 1;
  if($rcadena17>9){
    $rscadena17 = substr($rcadena17,0,1) + substr($rcadena17,1,1);
  }
  else {
    $rscadena17 = $rcadena17;
  }
  
  $rcadena18 = $cadena18 * 2;
  if($rcadena18>9){
    $rscadena18 = substr($rcadena18,0,1) + substr($rcadena18,1,1);
  }
  else {
    $rscadena18 = $rcadena18;
  }
  
  
  $cadenaf = $rscadena1+$rscadena2+$rscadena3+$rscadena4+$rscadena5+$rscadena6+$rscadena7+$rscadena8+$rscadena9+$rscadena10+$rscadena11+$rscadena12+$rscadena13+$rscadena14+
        $rscadena15+$rscadena16+$rscadena17+$rscadena18;
  
  $cadenafinal = ceil($cadenaf/10)*10;
  $digito_verificador = $cadenafinal - $cadenaf;
  $linea_captura = $r.$digito_verificador;
  
  return $linea_captura;
  
    }
}

/* End of file GenerarDocs.php */
/* Location: .//C/Users/queho/AppData/Local/Temp/fz3temp-2/GenerarDocs.php */

?>
