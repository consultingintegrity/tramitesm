<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catalogo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Catalogo');
	}
	/**
	 * Obtenemos catalogo de direcciones par dependencias
	 * format data Json
	 */
	public function getDireccion(){
		$direccion = $this->M_Catalogo->getDireccion();
		$data["response"] = (!empty($direccion))  ? 200 : 500;
		$data["direccion"] = (!empty($direccion)) ? $direccion : null;
		echo json_encode($data);
	}//getDireccion

	public function getDependencia(){
		$id_direccion = (!empty($this->input->post('id_direccion'))) ? addslashes($this->input->post('id_direccion')) : 0;
		$dependencia = $this->M_Catalogo->getDependencia($id_direccion);
		$data["response"] = (!empty($dependencia))  ? 200 : 500;
		$data["dependencia"] = (!empty($dependencia)) ? $dependencia : null;
		echo json_encode($data);
	}//getDependencia

	public function getEstados(){

		$estados = $this->M_Catalogo->getEstados();
		if (!empty($estados)) {
			$data["response"] = 200;
			$data["estados"] = $estados;
		}//if
		else{
			$data["response"] = 400;
		}//else
		echo json_encode($data);


	}//getEstados

public function getMunicipios($id_estado){

		$municipios = $this->M_Catalogo->getMunicipios($id_estado);
		if (!empty($municipios)) {
			$data["response"] = 200;
			$data["municipios"] = $municipios;
		}//if
		else{
			$data["response"] = 400;
		}//else
		echo json_encode($data);


	}//getEstados
	public function getRol(){

		$rol = $this->M_Catalogo->getRol();
		if (!empty($rol)) {
			$data["response"] = 200;
			$data["rol"] = $rol;
		}//if
		else{
			$data["response"] = 400;
		}//else
		echo json_encode($data);


	}//getEstados

	public function getGiros(){
		$like = (!empty($this->input->post('like'))) ? addslashes($this->input->post('like')) : "";
		$giros = $this->M_Catalogo->getGiros($like);
		if (!empty($giros)) {
			$data["response"] = 200;
			$data["giro"] = $giros;
		}//if
		else{
			$data["response"] = 400;
		}//else
		echo json_encode($data);


	}//getGiros

	public function getSubTramite($id){
		$tramite = $this->M_Catalogo->tipo_subTramite($id);
		if (!empty($tramite)) {
			$data["response"] = 200;
			$data["sub_tramite"] = $tramite;
		}//if
		else{
			$data["response"] = 400;
		}//else
		echo json_encode($data);


	}//getSubTramite
}//class

/* End of file Catalogo.php */
/* Location: ./application/controllers/Catalogo.php */
