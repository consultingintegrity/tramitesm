<?php
defined('BASEPATH') or exit('No direct script access allowed');


/**
 *
 * Controller Licencia
 *
 * This controller for ...
 *
 * @package   CodeIgniter
 * @category  Controller CI
 * @author    Setiawan Jodi <jodisetiawan@fisip-untirta.ac.id>
 * @author    Raul Guerrero <r.g.c@me.com>
 * @link      https://github.com/setdjod/myci-extension/
 * @param     ...
 * @return    ...
 *
 */

class Licencia extends CI_Controller
{
    
  public function __construct()
  {
    parent::__construct();
    $this->load->library('license/License');//libreria para validar la conraseña del uso de la plataforma
  }

  public function index()
  { 
    $json_license = json_decode($this->license->getKeyLicense());
    if ($json_license->data[0]->values[0]->validate == "TRUE") {
      // mandar a página para solicitar contraseña
      redirect(base_url()."Auth");
    }//if
    else{
      $data["title"] = "clave de licencia";
      $this->load->view('head', $data);
      $this->load->view('auth/enterLicense');
      $this->load->view('scripts/js');
    }//else
  }//index

  public function registro(){
    $key_post = $this->input->post("txtKey");
    $license_data = json_decode($this->license->getKeyLicense());
    //validamos la contraseña que se ingresa en el formulario con la del archivo JSON
    if ($key_post == $license_data->data[0]->values[0]->key) {
        $license_data->data[0]->values[0]->validate = "TRUE";
        $this->license->setKeyLicense($license_data);
        $this->license->register();
        redirect(base_url()."Auth",'refresh');  
    }//
    else{
      $this->session->set_flashdata('message','Ingresa una clave de licencia valida');
      redirect(base_url()."Licencia",'refresh');  
    }
   // $license_data->data[0]->values[0]->validate = $key_post;

  }//registro

}

/* End of file Licencia.php */
/* Location: ./application/controllers/Licencia.php */