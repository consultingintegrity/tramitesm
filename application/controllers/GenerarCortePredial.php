<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GenerarCortePredial extends CI_Controller {

  public function __construct(){
    parent::__construct();
  }//.index

  public function generaPdfCortePredial(){
    $this->load->model('Predial_M');
    $this->load->library('Pdf');
    //fecha
    $dia = date("d");
    $mes = date("F");
    $ano = date("Y");

    if ($mes=="January") $mes="enero";
    if ($mes=="February") $mes="febrero";
    if ($mes=="March") $mes="marzo";
    if ($mes=="April") $mes="abril";
    if ($mes=="May") $mes="mayo";
    if ($mes=="June") $mes="junio";
    if ($mes=="July") $mes="julio";
    if ($mes=="August") $mes="agosto";
    if ($mes=="September") $mes="septiembre";
    if ($mes=="October") $mes="octubre";
    if ($mes=="November") $mes="noviembre";
    if ($mes=="December") $mes="diciembre";

    $fecha = $dia." de ".$mes." de ".$ano;
    $pdf = new Pdf();
    $pdf->AddPage('L','','A4');
    $pdf->SetMargins(15, 25 , 15, 20);
    $pdf->AliasNbPages();
    $sumCorte = ($this->Predial_M->sumMontoTotalP() == null)?0:$this->Predial_M->sumMontoTotalP();
    if($consulta_corte = $this->Predial_M->cortePredial()){

    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(0,0,utf8_decode($fecha),0,0,'C');
    $pdf->SetFont('Arial', 'B', 15);
    $pdf->SetTitle(utf8_decode("Corte de Caja"));
    $pdf->getX(105);
    $pdf->SetY(15);
    $pdf->Cell(180,10,"Corte de Caja",0,0,'C');
    $pdf->Ln(20);
    $pdf->SetFont('Arial','B',20);
    $pdf->Cell(20,10,'TOTAL',0,0,'L');
    $pdf->Cell(210,10,utf8_decode('$'.number_format($sumCorte,2)),0,0,'R');
    $pdf->SetDrawColor(0,0,0);
    $pdf->SetLineWidth(1);
    $pdf->Line(15,45,282,45);
    $pdf->Ln(15);
    $pdf->SetFont('Arial','',10);
    $pdf->SetLineWidth(0);
    $pdf->SetFillColor(247, 247, 247 );
    $pdf->Cell(15,5,'#','L',0,'C',True);
    $pdf->Cell(90,5,'Nombre Contribuyente','L',0,'C',True);
    $pdf->Cell(42,5,'Clave Catrastal','L',0,'C',True);
    $pdf->Cell(40,5,'Referencia Bancaria','L',0,'C',True);
    $pdf->Cell(25,5,'Folio','L',0,'C',True);
    $pdf->Cell(20,5,'Fecha','L',0,'C',True);
    $pdf->Cell(5,5,'','L',0,'C',True);
    $pdf->Cell(30,5,'Costo',0,0,'C',True);
    $pdf->Line(15,55,282,55);
    
    foreach ($consulta_corte as $corte) {
        $pdf->Ln(6);
        $pdf->SetFont('Arial','',8);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(15,6,utf8_decode($corte->id),'L',0,'C',True); 
        $pdf->Cell(90,6,utf8_decode($corte->nombreContribuyente),'L',0,'C',True);
        $pdf->Cell(42,6,utf8_decode($corte->ClaveCatrastal),'L',0,'C',True);
        $pdf->Cell(40,6,utf8_decode($corte->lineaCaptura),'L',0,'C',True);
        $pdf->Cell(25,6,utf8_decode($corte->folio),'L',0,'C',True);
        $pdf->Cell(20,6,utf8_decode($corte->fecha),'L',0,'C',True);
        $pdf->Cell(5,6,'$','L',0,'C',True);
        $pdf->Cell(30,6,utf8_decode(number_format($corte->total,2)),'R',0,'R',True);
    }
    $pdf->Output('corte.pdf','I');
    //mientras columna generar = 1 no  lo hagas
    //si inserta
    // $url = "Carpeta/cortePredio$id";
    //tipo=Predial
    //idpredial=0
    
      //insert(//idpredial,idcorte, url,monmto)
    }
    else{
       
        $pdf->SetFont('Arial', 'B', 15);
        $pdf->SetTitle(utf8_decode("Corte de Caja"));
        $pdf->getX(105);
        $pdf->SetY(15);
        $pdf->Cell(180,10,'CORTE DE CAJA',0,0,'C');
        $pdf->Ln(20);
        $pdf->SetFont('Arial','B',20);
        $pdf->Cell(20,10,'TOTAL',0,0,'L');
        $pdf->Cell(210,10,utf8_decode('$'.number_format($sumCorte,2)),0,0,'R');
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetLineWidth(1);
        $pdf->Line(15,45,282,45);
        $pdf->Ln(15);
        $pdf->SetFont('Arial','',10);
        $pdf->SetLineWidth(0);
        $pdf->SetFillColor(247, 247, 247 );
        $pdf->Cell(15,5,'#','L',0,'C',True);
        $pdf->Cell(90,5,'Nombre Contribuyente','L',0,'C',True);
        $pdf->Cell(42,5,'Clave Catrastal','L',0,'C',True);
        $pdf->Cell(40,5,'Referencia Bancaria','L',0,'C',True);
        $pdf->Cell(25,5,'Folio','L',0,'C',True);
        $pdf->Cell(20,5,'Fecha','L',0,'C',True);
        $pdf->Cell(5,5,'','L',0,'C',True);
        $pdf->Cell(30,5,'Costo',0,0,'C',True);
        $pdf->Line(15,55,282,55);
        $pdf->Ln(50);
        $pdf->SetFont('Arial','',20);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->Cell(180,10,'NO SE HAN REALIZADO CORTES',0,0,'C');
        $pdf->OutPut();
    }


  }//generaPdfCorte

}//class
?>
