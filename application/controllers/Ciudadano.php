<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ciudadano extends CI_Controller
{

    public function index()
    {
        if (!empty($this->session->userdata()) && $this->session->userdata('logged_in')
            && $this->session->userdata('rol')[0]->nombre == "Ciudadano") {
            //Cargamos el modelo de tramites para mostrarlos en la vista
            $this->load->model('tramitesConsulta');
            $data["tramites"] = $this->tramitesConsulta->consulta();
            $this->load->model('Notificacion_M');
            $data["notificaciones"] = $this->Notificacion_M->progresoSolictud($this->session->userdata('id_usuario'));
            //cargamos el modelo de catalogo
            $this->load->model("M_Catalogo");
            //Otenemos los  giros de un trámite
            $arr_giros = $this->M_Catalogo->getGiros();
            //Obtenemos los tipos de licencia de construcción
            $arr_tipo_construccion = $this->M_Catalogo->getTipoConstruccion();
            $this->load->model('Notificacion_M');
            $data["title"]    = "Bienvenido!";
            $data["giros"]    = $arr_giros;
            $data["tipo_construccion"]    = $arr_tipo_construccion;
            $this->load->view('head', $data);
            $this->load->view('ciudadano/header');
            $this->load->view('ciudadano/content');
            $this->load->view('footer');
            $this->load->view('scripts/js');
            $this->load->view('scripts/giro');
            $this->load->view('scripts/subTramites');
        } //if
        else {
            $this->session->sess_destroy();
            redirect(base_url() . "Auth", 'refresh');
        } //else
    } //index
    public function resuperaContrasenia()
    {

        $txtCorreoElectronico = (!empty($this->input->post("txtCorreoElectronico"))) ? addslashes($this->input->post("txtCorreoElectronico")) : "";
        $arr_validations      = [

            array(
                "field" => "txtCorreoElectronico",
                "label" => "Correo Electronico",
                "rules" => "required|max_length[65]|trim|valid_email",

            ),

        ];
        $arr_msg = [
            array(
                "required"       => "EL campo %s es requerido para continuar",
                "valid_email"    => "EL campo %s debe ser un correo electrónco valido",
                "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",

            ),
        ];
        //cargamos form_validation
        $this->load->library('validation');
        $form_validation = $this->validation->validForm($arr_validations, $arr_msg);
        if ($form_validation) {
            $this->load->library('email');

            $config['protocol'] = 'mail';

            $config["smtp_host"] = 'mail.plataformasdinamicas.com.mx';

            //Nuestro usuario
            $config["smtp_user"] = 'no-reply@plataformasdinamicas.com.mx';

            //Nuestra contraseña
            $config["smtp_pass"] = 'txMunicipio';

            $config['mailtype'] = 'html';

            //El puerto que utilizará el servidor smtp
            $config["smtp_port"] = '465';

            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8';

            if (!empty($txtCorreoElectronico)) {

                $this->email->initialize($config);

                $alfabeto = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
                $array    = array();
                $tamaño  = strlen($alfabeto) - 1;
                for ($i = 0; $i < 8; $i++) {
                    $n       = rand(0, $tamaño);
                    $array[] = $alfabeto[$n];
                }
                $contra = implode($array);

                $this->load->model('Usuario');
                $contra1     = hash('sha512', $contra);
                $id_usuario1 = $this->Usuario->getUsuarioId($txtCorreoElectronico);
                if ($id_usuario1==null) {
                    $this->session->set_flashdata('goodCC', " ");
                    redirect(base_url() . "Auth", 'refresh');
               }
               else {

               $id_usuario  = $id_usuario1->id;
                if ($this->Usuario->cambiarcontra($contra1, $id_usuario)) {
                    $header=base_url() . 'plantilla/images/correo/header.png';
                    $logos=base_url() . 'plantilla/images/correo/logos.png';
                    $this->email->from('no-reply@plataformasdinamicas.com.mx', 'Municipio');
                    $this->email->to($txtCorreoElectronico);
                    $this->email->subject('Recuperar contraseña plataforma Municipio');

                    $contenEmail = '<body style="font-family: Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;">
                                    <div style="margin-top: 0;margin-bottom: 0;margin-left: auto;margin-right: auto;background-color: #F2F2F2;width: 600px;min-height: 500px;position: relative">';
                    $contenEmail .=' <header> <img src="';
                    $contenEmail .=  $header;
                    $contenEmail .='" width="600" height="auto"></header>
                    <div style="padding:40px">
                    <h2>Cambio de contraseña</h2>
                    <p style="text-align:justify; line-height: 2em;">Tu contraseña ha sido actualizada, la próxima vez que inicies sesion utiliza la nueva contraseña que se te asigno.</p>
                    <p style="text-align:justify; line-height: 2em;"><b>Nueva contraseña: </b>';
                    $contenEmail .= $contra;
                    $contenEmail .= '</p><p style="text-align:justify; line-height: 2em;">Para cualquier duda y/o aclaración comunícate al siguiente número: (### 2732327)</p>
                        <div align="center"><img src="';
                    $contenEmail .=  $logos;
                    $contenEmail .= '" width="215" height="107"></div>
                              </div>

                          <div style="position:relative; left: 0; bottom: 0; width: 100%; background-color: black; color: white;text-align: center;font-size: 8px;">
                            <div style="padding:40px">
                              <table width="100%" border="0">
                                <tbody>
                                  <tr>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Atención Ciudadana</strong><br><br>Querétaro.</p></td>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Síguenos en redes sociales</strong><br><br></p></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>

                        </div>
                      </body>
                    ';
                    $this->email->message($contenEmail);

                    if ($this->email->send()) {
                        $this->session->set_flashdata('goodCC', " ");
                        redirect(base_url() . "Auth", 'refresh');
                    } //if de mensaje enviado
                } //if modificar contraseña
                else {
                    $this->session->set_flashdata('goodCC', " ");
                    redirect(base_url() . "Auth", 'refresh');
                }
                } //else correo existe
            } //if de validacion de correo
        } //if form validation

        else {
            $this->session->set_flashdata('errorCC', validation_errors());
            redirect(base_url() . "Auth", 'refresh');
        } //else validaciones de form_validation

    } //recuperar contra
    public function registro()
    {
        $this->load->library('email');
        $txtPrimerNom  = (!empty($this->input->post("txtPrimerNom"))) ? addslashes($this->input->post("txtPrimerNom")) : "";
        $txtSegundoNom = (!empty($this->input->post("txtSegundoNom"))) ? addslashes($this->input->post("txtSegundoNom")) : "";
        $txtPrimerAp   = (!empty($this->input->post("txtPrimerAp"))) ? addslashes($this->input->post("txtPrimerAp")) : "";
        $txtSegundoAp  = (!empty($this->input->post("txtSegundoAp"))) ? addslashes($this->input->post("txtSegundoAp")) : "";
        $txtCorreo     = (!empty($this->input->post("txtCorreo"))) ? addslashes($this->input->post("txtCorreo")) : "";
        $optGenero     = (!empty($this->input->post("optGenero"))) ? addslashes($this->input->post("optGenero")) : "";
        $txtpassword   = (!empty($this->input->post("txtContra"))) ? addslashes($this->input->post("txtContra")) : "";
        $txtTelefono   = (!empty($this->input->post("txtTelefono"))) ? addslashes($this->input->post("txtTelefono")) : "";

        if ($this->input->post("btnRegistrar")) {
            //validamos campos del formulario
            $arr_validations = [

                array(
                    "field" => "txtPrimerNom",
                    "label" => "Primer Nombre",
                    "rules" => "required|max_length[65]|trim",
                ),
                array(
                    "field" => "txtSegundoNom",
                    "label" => "Segundo Nombre",
                    "rules" => "max_length[65]|trim",
                ),
                array(
                    "field" => "txtTelefono",
                    "label" => "teléfono",
                    "rules" => "min_length[7]|trim|is_natural|max_length[14]",
                ),
                array(
                    "field" => "txtPrimerAp",
                    "label" => "Primer Apellido",
                    "rules" => "required|max_length[65]|trim",
                ),
                array(
                    "field" => "txtSegundoAp",
                    "label" => "Segundo Apellido",
                    "rules" => "max_length[65]|trim",
                ),
                array(
                    "field" => "txtCorreo",
                    "label" => "Correo Electronico",
                    "rules" => "required|max_length[65]|trim|valid_email|is_unique[usuario.correo_electronico]",

                ),
                array(
                    "field" => "txtConfirmPass",
                    "label" => "Confirmar Contraseña",
                    "rules" => "required|min_length[6]",
                ),
                array(
                    "field" => "txtContra",
                    "label" => "Contraseña",
                    "rules" => "required|min_length[6]|matches[txtConfirmPass]",
                ),
            ];
            $arr_msg = [
                array(
                    "required"       => "EL campo %s es requerido para continuar",
                    "valid_email"    => "EL campo %s debe ser un correo electrónco valido",
                    "min_length[2]"  => "El campo %s debe tener mas de 2 caracteres",
                    "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
                    "alpha"          => "El campo %s solo debe contener letras",
                    "is_unique"      => "Error ese %s ya se encuentra registrado",
                    "min_length[7]"  => "El campo %s debe tener mas de 7 números",
                    "max_length[14]" => "El campo %s debe tener menos de 14 números",

                ),
            ];

            //cargamos form_validation
            $this->load->library('validation');
            $form_validation = $this->validation->validForm($arr_validations, $arr_msg);

            if ($this->validation->acento($txtPrimerNom)) {
                $this->session->set_flashdata('error', "El campo primer nombre solo debe contener letras");
                redirect(base_url() . "Auth", 'refresh');
            } //if validacion de nombre1
            elseif ($this->validation->acento($txtSegundoNom) && $txtSegundoNom != null) {
                $this->session->set_flashdata('error', "El campo segundo nombre solo debe contener letras");
                redirect(base_url() . "Auth", 'refresh');
            } //elseif validacion de nombre 2

            elseif ($this->validation->acento($txtPrimerAp)) {
                $this->session->set_flashdata('error', "El campo primer apellido solo debe contener letras");
                redirect(base_url() . "Auth", 'refresh');
            } //elseif validacion ape1
            elseif ($this->validation->acento($txtSegundoAp) && $txtSegundoAp != null) {
                $this->session->set_flashdata('error', "El campo segundo apellido solo debe contener letras");
                redirect(base_url() . "Auth", 'refresh');
            } //elseif validacion ape2
            else {

                if ($form_validation) {

                    $this->load->model('Usuario');
                    //Llamamos la funcion de registro
                    if ($this->Usuario->registro($txtPrimerNom, $txtSegundoNom, $txtPrimerAp, $txtSegundoAp, $optGenero, 17, $txtCorreo, hash('sha512', $txtpassword), 2,$txtTelefono)) {

                        $config['protocol'] = 'mail';

                        //El servidor de correo que utilizaremos
                        $config["smtp_host"] = 'mail.plataformasdinamicas.com.mx';

                        //Nuestro usuario
                        $config["smtp_user"] = 'no-reply@plataformasdinamicas.com.mx';

                        //Nuestra contraseña
                        $config["smtp_pass"] = 'txMunicipio';

                        $config['mailtype'] = 'html';

                        //El puerto que utilizará el servidor smtp
                        $config["smtp_port"] = '465';

                        //El juego de caracteres a utilizar
                        $config['charset'] = 'utf-8';

                        $this->email->initialize($config);
                        $id_usuario1 = $this->Usuario->getUsuarioId($txtCorreo);

                        $id_usuario = $id_usuario1->id;
                        $header=base_url() . 'plantilla/images/correo/header.png';
                        $logos=base_url() . 'plantilla/images/correo/logos.png';

                        //---------------------------------------
                        $this->load->library('email');
                        $this->email->from('no-reply@plataformasdinamicas.com.mx', 'Municipio');
                        $this->email->to($txtCorreo);
                        $this->email->subject('Registro en plataforma Municipio');
                        $ddd = '<body style="font-family: Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;">
                        <div style="margin-top: 0;margin-bottom: 0;margin-left: auto;margin-right: auto;background-color: #F2F2F2;width: 600px;min-height: 500px;position: relative">';
                        $ddd .=' <header> <img src="';
                        $ddd .=  $header;
                        $ddd .='" width="600" height="auto"></header>
                                         <div style="padding:40px">
                                            <h2>Estimado usuario</h2>
                                            <p style="text-align:justify; line-height: 2em;">Agradecemos tu registro en la platarforma de trámites Municipales.
                                    <b>Da clic en el enlace  de abajo para verificar tu correo y poder accesar a la plataforma.</b>
                                    <p style="text-align:justify; line-height: 2em;">Para cualquier duda y/o aclaración comunícate al siguiente número: (### 2732327)</p><div align="center"><img src="';
                    $ddd .=  $logos;
                    $ddd .= '" width="215" height="107"></div>
                              </div>

                                            <center>
                                            <a href="
                                    ';
                        $dire = base_url() . "Usuarios/actualizaStatus/" . $id_usuario;
                        $ddd .= $dire;
                        $ddd .= '
                                            ">Da clic en este enlace para verificar tu correo electrónico</a>
                                            <br>
                                            </center>


                           <div style="position:relative; left: 0; bottom: 0; width: 100%; background-color: black; color: white;text-align: center;font-size: 8px;">
                            <div style="padding:40px">
                              <table width="100%" border="0">
                                <tbody>
                                  <tr>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Atención Ciudadana</strong><br><br>Querétaro.</p></td>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Síguenos en redes sociales</strong><br><br></p></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>

                        </div>
                      </body>
                                    ';

                        $this->email->message($ddd);

                        if ($this->email->send()) {
                            $this->session->set_flashdata('good', " ");
                            redirect(base_url() . "Auth", 'refresh');
                        } //if se envio email
                        else {
                            $this->session->set_flashdata('good', " ");
                            redirect(base_url() . "Auth", 'refresh');
                        }
                    } //if registro

                } //if    form

                else {
                    $this->session->set_flashdata('error', validation_errors());
                    redirect(base_url() . "Auth", 'refresh');
                } //else validaciones de form_validation

            } ///else de validacions de acento

        } //if boton registrar

    } //registro

    //consultar tramites pendientes y finalizados
    //valido mediante id_usuario y un val que me dira si es pendiente o finalizados
    //@Autor: EOG Krack

    public function misTramites(){
      if ($this->session->userdata('logged_in') && $this->session->userdata('rol')[0]->nombre == "Ciudadano") {
        $idUsuario = $this->session->userdata('id_usuario');
        //cargo modelo para mi informacion de trámites
        $this->load->model('tramitesConsulta');
        $dataTramite = $this->tramitesConsulta->tramitesPendientes($idUsuario);
        $title = 'Historial de Trámites';
        $desc = 'Aquí se muestran todos los trámites pendientes o en proceso actualmente.';
        $data = [
            "tramite" => $dataTramite,
            'title' => $title,
            'descripcion' => $desc,
        ];
        $this->load->model('Notificacion_M');
        $data["notificaciones"] = $this->Notificacion_M->progresoSolictud($this->session->userdata('id_usuario'));

        $this->load->view('head', $data);
        $this->load->view('ciudadano/header');
        $this->load->view('ciudadano/tramites',$data);
        $this->load->view('footer');
        $this->load->view('scripts/js');
      }//if
      else {
        redirect(base_url()."Auth");
      }//else
    }//.mistramites pendientes y concluidos

    public function comprobante($id_solicitud = "")
    {
      if ($this->session->userdata('logged_in') && $this->session->userdata('rol')[0]->nombre == "Ciudadano") {
        $idUsuario = $this->session->userdata('id_usuario');
        //cargo modelo para mi informacion de trámites
        $this->load->model('tramitesConsulta');
        $folio_pago = $this->tramitesConsulta->getFolioPago($id_solicitud,$idUsuario);
        if (!empty($folio_pago)) {
          $path = base_url().$folio_pago->url;
          header("Content-type: application/pdf");
          header('filename="' . basename($path) . '"');
          readfile($path);
        }//if
        else{
          echo "No se encontro folio de pago";
        }
      }//if
      else {
        redirect(base_url()."Auth");
      }//else
    }//comprobante


    public function correcciones(){
      if ($this->session->userdata('logged_in') && $this->session->userdata('rol')[0]->nombre == "Ciudadano") {
        $idUsuario = $this->session->userdata('id_usuario');
        //cargo modelo para mi informacion de trámites
        $this->load->model('TramitesConsulta');
  			$dataTramites = $this->TramitesConsulta->solicitudCorreccionCiudadano($idUsuario,6);
        $title = 'Solicitudes con corrección';
        $desc = 'Aquí se muestran las solicitudes que necesitan un archivo con las evidencias de las correcciones que solicitarón los inspectores.';
        $data = [
            "tramite" => $dataTramites,
            'title' => $title,
            'descripcion' => $desc,
        ];
        $this->load->model('Notificacion_M');
        $data["notificaciones"] = $this->Notificacion_M->progresoSolictud($this->session->userdata('id_usuario'));

        $this->load->view('head', $data);
        $this->load->view('ciudadano/header');
        $this->load->view('ciudadano/correcciones',$data);
        $this->load->view('footer');
        $this->load->view('scripts/js');
      }//if
      else {
        redirect(base_url()."Auth");
      }//else
    }//.mistramites pendientes y concluidos
} //class

/* End of file Ciudadano.php */
/* Location: ./application/controllers/ciudadano/Ciudadano.php */
