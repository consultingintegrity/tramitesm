<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 //use PhpOffice\PhpSpreadsheet\Spreadsheet;
 //use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
 use PhpOffice\PhpSpreadsheet\IOFactory;


class Excel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Predio');
	}

	public function index()
	{

		//cargamos la vista donde se subirá el archivo excel
		//$this->load->view('predial/cargarExcel');

		/*$spreadSheet = new Spreadsheet();
		$sheet = $spreadSheet->getActiveSheet();
		$sheet->setCellValue("A1","Hola maldita zorra! :V");
		$writer = new Xlsx($spreadSheet);
		$fileName = "test.xlsx";
		$writer->save($fileName);*/

	}

	public function read(){
		//$inputFileNamePATH  =  __DIR__.'/../../../db.xlsx';// escanear un archivo en una ruta espeficica

    if ($_FILES["excelPredial"]["type"] != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {//Validamos extensiones de archivos

        $this->session->set_flashdata('error', "Solamente se pueden cargar archivos en formato de Excel.");
        redirect(base_url() . "Funcionario", '');
    }
    //cargamos el modelo del predio para insertar los datos a la DB
		$file_excel = $_FILES["excelPredial"]["tmp_name"];
		$spreadsheet = IOFactory::load($file_excel);//cargamos el documento excel
		$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);//convertimos los datos del excel en un array

		$insert_data = array();//variable donde guardaremos los datos en la base de datos
		foreach ($sheetData as $key) {
			if ($key["A"] != NULL || $key["B"] != NULL || $key["C"] != NULL || $key["D"] != NULL || $key["E"] != NULL || $key["F"] != NULL || $key["G"] != NULL || $key["H"] != NULL || $key["I"] != NULL || $key["J"] != NULL || $key["K"] != NULL || $key["L"] != NULL || $key["M"] != NULL || $key["N"] != NULL || $key["O"] != NULL || $key["P"] != NULL || $key["Q"] != NULL || $key["R"] != NULL || $key["S"] != NULL || $key["T"] != NULL || $key["U"] != NULL || $key["V"] != NULL || $key["W"] != NULL || $key["X"] != NULL || $key["Y"] != NULL || $key["Z"] != NULL) {
				array_push($insert_data,
					array(
					 "lineaCaptura" => $key["A"],
					 "ClaveCatastral" => $key["B"],
					 "TipoContribucion" => $key["C"],
					 "nombreContribuyente" => $key["D"],
					 "ubicacionPredio" => $key["E"],
					 "numeroExterior" => $key["F"],
					 "letra" => $key["G"],
					 "numeroInterior" => $key["H"],
					 "coloniaPredio" => $key["I"],
					 "anoInicialAdeudo" => $key["J"],
					 "bimestreInicialAdeudo" => $key["K"],
					 "anofinalAdeudo" => $key["L"],
					 "bimestrefinalAdeudo" => $key["M"],
					 "rezagoAnosAnteriores" => $key["N"],
					 "rezagoAnoTranscurre" => $key["O"],
					 "impuestoAno" => $key["P"],
					 "adicional" => $key["Q"],
					 "actualización" => $key["R"],
					 "Recargo" => $key["S"],
					 "requerimientoGastoEjecucion" => $key["T"],
					 "embargoGastosEjecucion" => $key["U"],
					 "multa" => $key["V"],
					 "descuento" => $key["W"],
					 "total" => $key["X"],
					 "fechaGeneracion" => $key["Y"],
					 "vigenciaPago" => $key["Z"],)
				);
			}//if A != NULL
			else{
				$this->session->set_flashdata('error', "Ocurrio un error al cargar la información.");
				redirect(base_url() . "Funcionario", '');				
			}
		}//foreach

		if ($this->Predio->insertBatch($insert_data)) {
      $this->session->set_flashdata('valid',"Se ha cargado la información del archivo " . $_FILES["excelPredial"]["name"]);
      redirect(base_url() . "Funcionario/validPredial", '');
		}//if
		else{
      $this->session->set_flashdata('error', "Ocurrio un error al cargar la información.");
      redirect(base_url() . "Funcionario", '');
		}//else


	}//read

	//valid predial
	public function checkDB(){
		$val = (!empty($this->input->post('val'))) ? addslashes($this->input->post('val')) : 0;
		if($this->session->userdata('rol')[0]->id == 25){
			$resp=$val==3?"SI":"NO";
			//array
			$fecha=date("dmy");
			$data=[
			"id_funcionario" => $this->session->userdata('id_usuario'),
			"date" => $fecha,
			"val" => $resp
			];
			//insertamos validación
			if($this->Predio->check($data)){
				$jsonResponse["response"] = 200;
				$jsonResponse["msg"] = $val==3?"Información validada y Lista":"Información Limpiada y Lista Para Cargar registros" ;
			}else{
				$jsonResponse["estatus"] = 500;
				$jsonResponse["msg"] = "Ocurrio un problema Intentelo nuevamente";
			}
		}else{
			$jsonResponse["estatus"] = 500;
			$jsonResponse["msg"] = "Ocurrio un Error Intentelo nuevamente";
		}

		echo json_encode($jsonResponse);
	}
}//Class

/* End of file Excel.php */
/* Location: ./application/controllers/Excel.php */
