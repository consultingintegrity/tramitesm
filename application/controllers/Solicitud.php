<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitud extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('logged_in')) {
      $this->session->sess_destroy();
      redirect(base_url()."Auth","refresh");
    }//if
    $this->load->model('Solicitud_M');
    $this->load->model('Notificacion_M');
    $this->load->model('Fase_M');
    $this->load->library('LoadFile');
    $this->load->model('TramitesConsulta');
    $this->load->model('Funcionario');

    $this->load->library('CorreoElectronico');

    $this->load->model('Documentos');

  }

  public function index($estatus,$id_solicitud = "",$funcionario_opinion_tecnica = ""){
    //parsemos a int
    $id_solicitud = (int) $id_solicitud;
    //si un usuario con un rol de ciudadano entra a la url, lo vamos a redirigir a acceso
    if ($this->session->userdata('rol')[0]->nombre != "Ciudadano" && $this->session->userdata('rol')[0]->nombre !=  "Caja") {
      //guardamos el id del funcionario actual
      $id_funcionario = ($funcionario_opinion_tecnica == "") ? $this->session->userdata('id_usuario') : $funcionario_opinion_tecnica;
      //buscamos el id de la solicitud mas viejita
      // Za ZA ZA Yakuza Yakuza!
      $estatus = (int) $estatus;
      $solicitud  = ($this->Solicitud_M->getSolicitud($id_funcionario,$estatus,$id_solicitud) != null) ? $this->Solicitud_M->getSolicitud($id_funcionario,$estatus,$id_solicitud) :
      redirect(base_url()."Auth",'');
      //Obtenemos el giro de la solicitud
      $giro = $this->Solicitud_M->giroSolicitud($solicitud->id);
      //Obtenemos el tipo de licencia de construcción
      $tipo_licencia_construccion = $this->Solicitud_M->tipoConstruccionSoicitud($solicitud->id);
      $tramite = $this->TramitesConsulta->descTramite($solicitud->id_tramite);
      //obtenemos la solicitud
      $data_solicitud = $this->Solicitud_M->getDataSolicitud($solicitud->id);
      //Obtenemos la información de la solicitud
      $data_informacion_solicitud = json_decode($data_solicitud->informacion,1);
      //Guardamos los datos de la solicitud en diferentes arreglos para poderlos controlar de una mejor manera en la vista.
      /**
       * Empezemos por las pestañas :
       * Seguimos con el id de los formularios
       */
       //  var_dump($data_informacion_solicitud);
      $pestanias_solicitud = array();
      $form_solicitud = array();
      $id_pestania_no_repeat = 0;
      for ($i=0; $i < count($data_informacion_solicitud); $i++) {
        if (isset($data_informacion_solicitud[$i]["form_pestanias"])) {
          for ($k=0; $k <  count($data_informacion_solicitud[$i]["form_pestanias"]); $k++) {
            if (!empty( $data_informacion_solicitud[$i]["form_pestanias"][$k]["id_formulario"])) {
              array_push($form_solicitud, $data_informacion_solicitud[$i]["form_pestanias"][$k]["id_formulario"]);
              if (!in_array($data_informacion_solicitud[$i]["form_pestanias"][$k]["id_pestania"], $pestanias_solicitud)) {
                $id_pestania_no_repeat =  $data_informacion_solicitud[$i]["form_pestanias"][$k]["id_pestania"];
                array_push($pestanias_solicitud, $id_pestania_no_repeat);
              }//if
            }//if
          }
        }//if
      }//for
      //obtenemos los requisitos de la Solicitud
      $data_requisitos_solicitud = $this->Solicitud_M->getRequisitosSolicitud($solicitud->id);

      //obtenemos el N° de fase del id de la fase
      $no_fase = ($funcionario_opinion_tecnica == "") ? $this->Fase_M->getNoFase($solicitud->id_fase)->noFase : $this->Fase_M->getNoFase(72)->noFase;
      //$no_fase = $this->Fase_M->getNoFase(72)->noFase;
      //obtenemos la información del tramite
      $id_tramite = $solicitud->id_tramite;
      $tramite = $this->TramitesConsulta->descTramite($solicitud->id_tramite);

      //Cargamos el modelo de las pestañas para obtener las relaciondas con la fase correspondiente del tramite
      $this->load->model('Pestania');
      $id_usuario = $this->session->userdata('rol')[0]->id;
      //cargamos el modelo de fases
      $this->load->model('Fase_M');
      $fase =  $this->Fase_M->getFase($no_fase,$id_tramite)->id;//
      $pestanias = $this->Pestania->tramite_fase_pestania($id_tramite,$fase,$id_usuario);
      //obtenemos el id de las pestañas
      $id_pestanias = $this->obtenerPestania($pestanias);
      //no tiene perfilado
      $perfilado_form = "";
      $perfilado_requisitos = "";

      //Cargamos el modelo para obtener los formularios
      $this->load->model('Formulario');
      //obtenemos los formularios de cada pestaña
      $form_pestanias = $this->Formulario->getFormPestania($id_pestanias,$perfilado_form,$solicitud->id_tramite);
      //Obtenemos el id de los formularios de las pestañas
      $id_form = $this->obtenerForm($form_pestanias);
      //obtenemos los formularios de las pestañas
      $this->Formulario->setIdFormulario($id_form);
      $formularios = $this->Formulario->getForm();
      $atributos_formularios = $this->Formulario->getAributoFormulario();
      $campos_formularios = $this->Formulario->getCamposFormulario();

      //Fase 4 JEfe de inspeccion
      //Obtenemos el documento de inspección
      $documentoInspeccion = $this->Solicitud_M->getDocInspeccion($solicitud->id);
      //Fase 6 Firma de SubDirector
      $licencia = $this->Solicitud_M->getLicencia($solicitud->id);

      //fase 8 de ventanilla
      //obtengo la licencia
      $doclicencia = $this->Solicitud_M->docLicencia($solicitud->id);
      //obtenemos el link del pase de CAJA
      $reciboPago = $this->Solicitud_M->getPago($solicitud->id);
      //optenemos las opiniones tecnicas para rol
      $opinionTecnicaFuncionario = !empty($this->Solicitud_M->getOpinionTecFuncionario($solicitud->id,$id_funcionario))?$this->Solicitud_M->getOpinionTecFuncionario($solicitud->id,$id_funcionario):0;
      //fecha de funcionario para subir opinion
      $fechaopinionTecnica = ($funcionario_opinion_tecnica == "")? 0 : $this->Solicitud_M->fechaFuncionariosOpinion($solicitud->id,$this->session->userdata('id_usuario'));


      //optenemos las opiniones tecnicas para comision
      $opinionesTecnicas = $this->Solicitud_M->getOpinionTec($solicitud->id);
      //optenemos el dictamen si hay uno
      $dictamen_Comision = $this->Solicitud_M->getDocDictamen($solicitud->id);

      $dataTitle = $tramite->nombre . " -- Solicitud #" . $solicitud->id;
      //Asignamos la información al array asociativo para ocupar en las vistas
      $data["title"] = $dataTitle;
      //datos de la solicitud
      $data["petanias_solicitud"] = $pestanias_solicitud;
      $data["form_solicitud"] = $form_solicitud;
      $data["info_solicitud"] = $data_informacion_solicitud;
      $data["data_requisitos_solicitud"] = $data_requisitos_solicitud;
      $data["no_fase"] = $no_fase;
      $data["giro"] = $giro;
      $data["tipo_licencia_construccion"] = $tipo_licencia_construccion;
      //elementos del Trámite
      $data["solicitud"] = $solicitud;
      $data["form_pestanias"] = $form_pestanias;
      $data["pestanias"] = $pestanias;
      $data["formularios"] = $formularios;
      $data["campos_formularios"] = $campos_formularios;
      //Fase 4
      $data["inspeccion"] = $documentoInspeccion;

      //Fase 5
      $data["licencia"] = $licencia;
      //fase 8
      $data["docLicencia"] = $doclicencia;
      //trámite de protecicón civil
      $data["reciboPago"] = $reciboPago;
      //para alcohol funcionarios
      $data["opinionTecFuncionario"] = $opinionTecnicaFuncionario;
      //para alcohol comision
      $data["opinionesTec"] = $opinionesTecnicas;
      //dictamenes de alcohol
      $data["dictamenAlcohol"] = !empty($dictamen_Comision[0])?$dictamen_Comision[0]:"";
      //dictamenes de alcohol
      $data["informeCabildo"] = !empty($dictamen_Comision[1])?$dictamen_Comision[1]:"";
      //fecha para subir opinion
      $data["fechaOpinion"] = !empty($fechaopinionTecnica)?$fechaopinionTecnica:"";

      $this->load->view('head',$data);
      $this->load->view('funcionario/header');
      $this->load->view('pestanias/pestaniasRevision',$data);
      $this->load->view('footer');
      $this->load->view('scripts/js');
      $this->load->view('scripts/utils');
      $this->load->view('scripts/ws-funcionario');
      $this->load->view('scripts/evaluacion');
      $this->load->view('scripts/dashboard');
    }//if
    //quizas esto no sea necesario
    else if ($this->session->userdata('rol')[0]->nombre == "Caja") {
      $id_funcionario = $this->session->userdata('id_usuario');
      $pases_caja = $this->Solicitud_M->pasesCaja($id_funcionario);

      $data["pases_caja"] = $pases_caja;
      $data["title"] = "Pases a caja";

      $this->load->view('head',$data);
      $this->load->view('funcionario/header');
      $this->load->view('funcionario/content',$data);
      $this->load->view('footer');
      $this->load->view('scripts/js');
      $this->load->view('scripts/utils');
      $this->load->view('scripts/pago');
      $this->load->view('scripts/ws-funcionario');
      $this->load->view('scripts/dashboard');
    }//else if

    else{
      redirect(base_url()."Auth",'');
    }//else
  }//index

  /*
  *return json data
  */
  public function insert(){
    $data = array();
    $id_ciudadano = $this->session->userdata('id_usuario');
    //validamos que tipo de inserción haremos
    if ($this->input->post('infoSolicitud') == "text") {
      //Obtenemos el id del funcionario el cual le dra seguiemiento al trámite
      $id_dependencia = (!empty($this->input->post('id_dependencia')) ? addslashes($this->input->post('id_dependencia')) : 0);
      $id_tramite = (!empty($this->input->post('id_tramite')) ? addslashes($this->input->post('id_tramite')) : 0);
      $id_giro = (!empty($this->input->post('id_giro')) ? addslashes($this->input->post('id_giro')) : 0);
      $id_tipo_construccion = (!empty($this->input->post('id_tipo_construccion')) ? addslashes($this->input->post('id_tipo_construccion')) : 0);//tipo de licencia de construcción
      $id_fase =  $this->Fase_M->getFase(2,$id_tramite)->id;//siempre la fase sera la  NO°2
      $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario($id_fase,$id_dependencia)) ? $this->Solicitud_M->getIdFuncionario(2,$id_dependencia)->id : 0);
      $id_sub_tramite = ($this->input->post('id_sub_tramite'));
      $id_tipoSolicitud = ($this->input->post('id_tipo_solicitud'));
      $razonSocial = ($this->input->post('razonSocial'));
      $provicional = ($this->input->post('tipo_provicional')>0)? addslashes($this->input->post('tipo_provicional')) : 0;
      $dataSolicitud =  json_encode($this->input->post('datasolicitud'));
      $dataSolicitud = str_replace('\"','"',$dataSolicitud);
      $dataSolicitud = str_replace('"[','[',$dataSolicitud);
      $dataSolicitud = str_replace(']"',']',$dataSolicitud);
      //Validamos que exista un funcionario asugnado
      if ($id_funcionario > 0) {
        //insertamos una nueva soliciud y obtenemos el id de la misma
        $id_solicitud = $this->Solicitud_M->insertSolicitud($id_tramite,$id_ciudadano,$id_funcionario,$id_fase,$dataSolicitud);
        if ($id_solicitud > 0) {
          //verificamos si la solicitud trae un giro
          if ($id_giro > 0 ) {
            $this->Solicitud_M->insertSolicitudGiro($id_solicitud,$id_giro);
          }//if
          if ($id_tipo_construccion > 0 ) {
            $this->Solicitud_M->insertSolicitudConstruccion($id_solicitud,$id_tipo_construccion);
          }//if
          if ($id_sub_tramite > 0){
            $this->Solicitud_M->insertSubTramite($id_solicitud,$id_sub_tramite);
          }
          if ($id_tipoSolicitud > 0){
            $this->Solicitud_M->insertaTipoSolicitud($id_solicitud, $id_tipoSolicitud);
          }
          if ($provicional > 0){
            $this->Solicitud_M->insertaTipoSolicitud($id_solicitud, $provicional);
          }
          if ($razonSocial > 0){
            $this->Solicitud_M->insertarazonSocial($id_solicitud, $razonSocial);
          }
          //insertamos el seguimiento de la solicitud con el funcionario
          $this->Solicitud_M->funcionarioSolicitud($id_fase, $id_solicitud, $id_funcionario);
          $tramite = $this->TramitesConsulta->descTramite($id_tramite);
          //insertamos la notificación
          //Obtenemos el giro o tipo de licencia de construcción si el trámite lo tiene.
          $tipoTramite = $this->TramitesConsulta->tipoLicenciaSolicitud($id_solicitud);
          $tipoLicencia = ($tipoTramite->giro != null) ? " ($tipoTramite->giro)" : "" ;
          $tipoLicencia = ($tipoTramite->tipoConstruccion != null) ? " ($tipoTramite->tipoConstruccion)" : $tipoLicencia ;
          $nombreTramite = $tipoTramite->nombre  . $tipoLicencia;
          $contenido = "Tu Trámite ".$nombreTramite." se encuentra en la fase de ventanilla. Espera el seguimiento del funcionario para estar al pendiente del progreso del mismo";
          $this->Notificacion_M->insert($id_solicitud,$id_ciudadano,$contenido);

          //Obtenemos html del correo
          $subTitulo = "Tu solicitud para el  trámite " . $nombreTramite ." se encuentra en evaluación";
          $texto = "¡Hola " . $this->session->userdata('primer_nombre') . "! " . " Este correo electrónico es para avisarte que los requisitos para la solicitud de tu trámite " . $tramite->nombre ." ya se encuentra en evaluación.";
          $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
          $direccion = $this->session->userdata('correo_electronico');
          //mandamos correo eléctronico
          $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
          //la solicitud se inserto correctamente
          $data["response"] = 200;
          $data["id_solicitud"] = $id_solicitud;
          $data["msg"] = "En un plazo de 15 a 20 días tendrás respuesta a tu trámite";
        }//if
        else{
        $data["response"] = 522;
        $data["msg"] = "No Pudimos registrar tu solicitud. Vuelve a iniciar una nueva.";
      }//else
      }//if funcionario > 0
      else{
        $data["response"] = 522;
        $data["msg"] = "Por el momento no existe un funcionario que pueda atender tu solicitud.";
      }//else
    }//if
    else if ($this->input->post('infoSolicitud') == "files") {
        // var_dump($_FILES);
      //var POST
      $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? $this->input->post('id_solicitud') : "";
      $requisitos = (!empty($this->input->post('listaRequisitos'))) ? json_decode($this->input->post('listaRequisitos')) : "";
    	$urlDocs = "docsSolicitud/"; //Url para guardar la ruta de los archivos
    	//Verificamos que el directorio exista
      // var_dump($requisitos);
      $this->loadfile->dir_exist($urlDocs);
      //Controlamos el número de requisitos subidos
      $requisitosSubidos = 0;
      //Subimos los requisitos al server :v-
      for ($i=0; $i < count($requisitos) ; $i++) {
        //Agregamos el nombre del requisito a la variable de control
        //nomencaltura de los requisitos
        $nombreRequisito = "usr" . $id_ciudadano ."Sol" . $id_solicitud . "Req" . $requisitos[$i]->id_requisito . $requisitos[$i]->nombre . date('Y-m-d');
        $inputFile = 'txtRequisito' . $requisitos[$i]->id_requisito ;
        //Subimos los requisitos al servidor
        if (!empty($_FILES[$inputFile]["name"])) {
          $extensiones_permitidas =  array('application/pdf','pdf','application/octet-stream','dwg');
          //validamos la extensión de los archivos PDF || AUTOCAD
          if (in_array($_FILES[$inputFile]["type"],$extensiones_permitidas)) {
            $config["upload_path"]    = $urlDocs;//url donde se subira el archivo
            $config ['max_size' ]     =  5242880;//tamaño maximo del archivo
            $config['file_name'] = $nombreRequisito;//nombre del archivo
            $config ['allowed_types'] =  '*';//extensiones permitidas
            $respond = $this->loadfile->upload_files($config,$inputFile);
            //obtenemos el nombre del arcihvo una vez subido en el servidor
            $nameRequisitoOnServer = ($respond["valid"]) ? $urlDocs . $respond["uploadSuccess"]["file_name"] : "error";
            $this->Solicitud_M->insertaRequisitoSolicitud($nameRequisitoOnServer,$id_ciudadano,$requisitos[$i]->id_requisito,$id_solicitud);
            $requisitosSubidos++;
          }//if
          else{
            $nameRequisitoOnServer = "No se Subio el archivo";
            $this->Solicitud_M->insertaRequisitoSolicitud($nameRequisitoOnServer,$id_ciudadano,$requisitos[$i]->id_requisito,$id_solicitud);
            $requisitosSubidos--;
          }//else

        }//if
        else{
          $requisitosSubidos--;
        }
      }//for
      $data["response"] = ($requisitosSubidos > 0) ? 200 : 522;
      $data["msg"] = ($requisitosSubidos ==  count($requisitos)) ? "Te notificaremos el seguimiento de tu solicitud vía correo electrónico y podrás ver el seguimiento en la pestaña de notificaciones de esta plataforma" : "Algunos de tus requisitos no se pudierón subir a la plataforma. El funcionario correspondiente de dar seguimiento a tu solicitud te hará saber que requisitos necesitas volver a subir";
      //  $data["response"] = ($requisitosSubidos > 0) ? 200 : 522;
      $data["error"] = ($requisitosSubidos ==  count($requisitos)) ? "Te notificaremos el seguimiento de tu solicitud vía correo electrónico y podrás ver el seguimiento en la pestaña de notificaciones de esta plataforma" : "Algunos de tus requisitos no se pudierón subir a la plataforma. El funcionario correspondiente de dar seguimiento a tu solicitud te hará saber que requisitos necesitas volver a subir";
      //echo json_encode($data);
    }//else if

    echo json_encode($data);
  }//insert

  /**
   * return json data
   */
  public function actualiza()
  {
    //variables que ocuparemos para la actalizción de la fase del trámite
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;
    $id_fase = (!empty($this->input->post('id_fase'))) ? addslashes($this->input->post('id_fase')) : 0;
    $id_ciudadano = (!empty($this->input->post('id_ciudadano'))) ? addslashes($this->input->post('id_ciudadano')) : 0;
    $id_tramite = (!empty($this->input->post('id_tramite'))) ? addslashes($this->input->post('id_tramite')) : 0;
    $archivo_inspeccion = (!empty($_FILES)) ? $_FILES : null;
    $id_usuario = $this->session->userdata('id_usuario'); //id de la persona logueada
    $url = (!empty($this->input->post('pdf'))) ? addslashes($this->input->post('pdf')) : 0;
    $impacto = (!empty($this->input->post('impacto'))) ? addslashes($this->input->post('impacto')) : null;
    $opinionTecnica = (!empty($this->input->post('opinionTecnica'))) ? addslashes($this->input->post('opinionTecnica')) : null;

    $urlPago = (!empty($this->input->post('pago'))) ? addslashes($this->input->post('pago')) : 0;
    $costoTramite = (!empty($this->input->post('costoTramite'))) ? addslashes($this->input->post('costoTramite')) : $this->Solicitud_M->getSolicitudCosto($id_solicitud)->costo;//Costo de tramite emitido por venanilla
    $costoTramite = str_replace(',','',$costoTramite);
    $fecha_vencimiento = (!empty($this->input->post('fecha_vencimiento'))) ? addslashes(date("Y-m-d",strtotime($this->input->post('fecha_vencimiento')))) : null;
    $DU = $this->input->post('DU') == "true" ? 2 : 0;
    $PC = $this->input->post('PC') == "true" ? 3 : 0;
    $SF = $this->input->post('SF') == "true" ? 5 : 0;
    $fechaDictamen = !empty($this->input->post('fechaDictamen')) ? addslashes(date("Y-m-d",strtotime($this->input->post('fechaDictamen')))): 0;
    //referencias de tramite
    $referenciaBanco = (!empty($this->input->post('referencia'))) ? addslashes($this->input->post('referencia')) : 0;
    $folioSol = (!empty($this->input->post('folio'))) ? addslashes($this->input->post('folio')) : 0;
    //obtenemos información del Trámite
    $tramite = $this->TramitesConsulta->descTramite($id_tramite);
    //obtenemos la siguinte fase
    $info_fase = $this->Fase_M->fase($id_fase);
    //obtenemos el id de la siguiente fase
    $siguiente_fase = $info_fase->siguiente;
    //obtenemos información de la siguinte fases
    $info_siguiente_fase = $this->Fase_M->fase($siguiente_fase);
    $finTramite = false;
    //Validamos que si es ventnailla, este se le asignara a la cual hizo la revisión de los requisito_solicitud
    if ($info_fase->id_rol == 7 || $info_fase->id_rol == 2 || $info_fase->id_rol == 3) {
      //si es venta de alcohol se envia a ventanilla de PC
      $rol_funcionario = $this->Fase_M->fase($siguiente_fase)->id_rol;
      $id_funcionario  =  $this->Solicitud_M->getFuncionarioSeguimientoSolicitud($rol_funcionario,$id_solicitud)->id_usuario;

    }
    else if ($info_fase->siguiente == 0) {
      if($id_tramite != 5){
        $id_funcionario  =  $this->Solicitud_M->getFuncionarioSeguimientoSolicitud($info_fase->id_rol,$id_solicitud)->id_usuario;
        $finTramite = true;
        $siguiente_fase = $info_fase->id;
      }else{
        $id_funcionario  =  $this->Solicitud_M->getFuncionarioSeguimientoSolicitudAlcohol($info_fase->id_rol,$id_solicitud)->id_usuario;
        $finTramite = true;
        $siguiente_fase = $info_fase->id;
      }
    } else if($id_tramite == 5 && $info_fase->id_rol == 15){//si es comision
      if ($info_fase->siguiente == 67) {
        //inserto los funcionarios
        /*$funcionario1 = $this->Solicitud_M->insertarFuncionarioOpinion($id_solicitud,$PC);
        $funcionario2 = $this->Solicitud_M->insertarFuncionarioOpinion($id_solicitud,$SF);
        $funcionario3 = $this->Solicitud_M->insertarFuncionarioOpinion($id_solicitud,$DU);*/
        $this->insertUsuariosOpinion($DU,$PC,$SF,$id_solicitud,$fechaDictamen);

        $registro = $this->Solicitud_M->idFuncionarioOpinion($id_solicitud);
        if($registro != null){
          $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario($id_fase,$tramite->id_dependencia)) ? $this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia)->id : 0);
        }else{
          $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario($id_fase,$tramite->id_dependencia)) ? $this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia)->id : 0);
        }

        //de comision lo mando a desarrollo urbano
       // $id_funcionario = $this->Solicitud_M->getIdFuncionario($siguiente_fase,2)->id;
      }//if
      //aqui se van a cambiar el estatus de los roles que falaten de subir la opinion
      //para que no les aparesca si el funcionario ya lo paso de fase
      /**
       * En esta parte alteramos un poco el orden, ya que debemos actualizar  en otra tabla :v
       * Debemos cambiar el status en la tabla usuarios opinion y cambair el valor a 0
       */
      else if ($opinionTecnica) {
        $rol_funcionario = $this->Fase_M->fase($siguiente_fase)->id_rol;
        $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario(66,$tramite->id_dependencia)) ? $this->Solicitud_M->getIdFuncionario(66,$tramite->id_dependencia)->id : 0);
        $siguiente_fase = 67;
        //Subimos el archivo de opinión tecnica al servidor
        $urlDocOpinionTecnica = $this->addOpinionTecnica($id_solicitud,$id_usuario,$tramite->nombre,"txtDocOpinionTecnica");
        //insertamos la ruta del archivo que se subio en la tabla opinion_tecnica
        $infoDoc = $this->Solicitud_M->insertOpinionTecnica($id_solicitud,$id_usuario,$urlDocOpinionTecnica);
        //Actualizamos el estatus del usuario en la tabla usuariosopinion
        $infoDoc = $this->Solicitud_M->updateUsuarioOpinionTecnica($id_solicitud,$id_usuario);
        $jsonResponse["adversiment"] = ($infoDoc == "error") ? "Tu archivo de opinión técnica se subió correctamente" : "NO se subió tu archivo de opinión técnica" . $urlDocInspeccion;

      }//else if

      else if($info_siguiente_fase->id_rol == 19){
        //obtengo los registros de funcionarios opinion y los actualizo todos a 0 esten en 0 o no
        $UsrOpinion =$this->Solicitud_M->FuncionariosOpinion($id_solicitud);
        if(is_array($UsrOpinion)){
        foreach($UsrOpinion as $usrOpinion){
          $result = $this->Solicitud_M->updateUsuarioOpinionTecnica($id_solicitud,$usrOpinion->id_funcionario);
        }//foreach
      }//if
        //si esta en la fase de dictamen, se debera subir un archivo de dictamen usamos la libreria de inspeccion para el archivo
        $urlDocAlcohol = $this->addInspeccion($id_solicitud,$id_usuario,$tramite->id,"txtDocDictamen");
        //insertamos en la tabla de dictamen
        $infoDoc = $this->Solicitud_M->insertDictamenAlcohol($id_solicitud,$id_usuario,$urlDocAlcohol);
        $jsonResponse["adversiment"] = ($infoDoc == "error") ? "Tu archivo se subió correctamente" : "NO se subió tu archivo" . $urlDocAlcohol;
        $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario($id_fase,$tramite->id_dependencia)) ? $this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia)->id : 0);

      }
    }else if($id_tramite == 5 && $info_fase->id_rol == 4){
       if($info_siguiente_fase->id_rol == 15){
        $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario($id_fase,$tramite->id_dependencia)) ? $this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia)->id : 0);
      }else if($info_siguiente_fase->id_rol == 3){
        //Si es caja, Siempre nos traera el usuario correspondiente de caja
        $id_funcionario = $this->Solicitud_M->getIdFuncionario($siguiente_fase,5)->id;
      }
    } else if($id_tramite == 5 && $info_fase->id_rol == 19){
       //si esta en la fase de dictamen, se debera subir un archivo de dictamen usamos la libreria de inspeccion para el archivo
      $urlDocAlcohol = $this->addInspeccion($id_solicitud,$id_usuario,$tramite->id,"txtDocDictamen");
      //insertamos en la tabla de dictamen
      if(!empty($urlDocAlcohol)){
      $ano=date("Y");
      $folioSol="VA-0{$id_tramite}{$id_solicitud}/{$ano}";
      $infoDoc = $this->Documentos->insertaDocs($id_solicitud,$id_tramite,$urlDocAlcohol,$folioSol);
      $jsonResponse["adversiment"] = ($infoDoc != "error") ? "Tu archivo se subió correctamente" : "NO se subió tu archivo" . $urlDocAlcohol;
      $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario($id_fase,$tramite->id_dependencia)) ? $this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia)->id : 0);
      }
    }
    else if($id_tramite == 9 && $info_fase->id_rol == 22){
      //si esta en la fase de dictamen, se debera subir un archivo de dictamen usamos la libreria de inspeccion para el archivo
     $urlDocAlcohol = $this->addInspeccion($id_solicitud,$id_usuario,$tramite->id,"txtDocDictamen");
     //insertamos en la tabla de dictamen
     if(!empty($urlDocAlcohol)){
      $ano=date("Y");
      $folioSol="PA-0{$id_tramite}{$id_solicitud}/{$ano}";
      $res = $this->Documentos->insertaDocs($id_solicitud,$id_tramite,$urlDocAlcohol,$folioSol);
      $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario($id_fase,$tramite->id_dependencia)) ? $this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia)->id : 0);
      $jsonResponse["adversiment"] = ($res == true) ? "Tu archivo se subió correctamente" : "NO se subió tu archivo" . $urlDocAlcohol;
    }
   }
    else{
      //obtenemos el id del funcionario al cual se le asignará el trámite
      if ($info_siguiente_fase->id_rol == 3 ) {
        //Si es caja, Siempre nos traera el usuario correspondiente de caja
        $id_funcionario = $this->Solicitud_M->getIdFuncionario($siguiente_fase,5)->id;

      }//if
      else{
        //Obtenemos todos los funcionrios que no sean caja
        $id_funcionario = ($this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia) != NULL ? $this->Solicitud_M->getIdFuncionario($siguiente_fase,$tramite->id_dependencia)->id : 0);
      }//else
    }//else
      //obtenemos la información del rol
    if ($id_funcionario == 0) {
        $jsonResponse["response"] = 404;
        $jsonResponse["msg"] = "No hay funcionarios disponibles para atender la solicitud.";
    }//if
    else{

      $info_fase_siguiente = $this->Fase_M->fase($siguiente_fase);
      $rol = $this->Funcionario->rol($info_fase_siguiente->id_rol);
      //Validamos el estatus del tramite en base a su rol y no de fase
      $estatus = $this->faseSolicitud($info_fase->id_rol,$info_fase->noFase,$info_fase->siguiente,$id_tramite);
      //insertamos el id del funcionario en la tabla de seguimiento del Trámite
        if($this->Solicitud_M->update($id_solicitud, $id_funcionario,$siguiente_fase,$estatus,$costoTramite)){
          //Cargamos modelo de usuario para obtener su informaciónote
          $this->load->model('Usuario');
          switch ($info_fase->id_rol) {
            /*case 3: //Se realizo el pago y se notifica que puede psar a recojer su trámite
                //obtenemos información de usuario
                $usuario = $this->Usuario->getUsuario($id_ciudadano);
                //Mandamos correo CorreoElectronico
                $tramite = $this->TramitesConsulta->descTramite($id_tramite);
                //Obtenemos html del correo
                $subTitulo = "Tu licencia  para el trámite " . $tramite->nombre ." Se encuentra en Ventanilla!";
                $texto = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que tu puedes pasar a recoger la licencia para tu trámite " . $tramite->nombre ." en las oficinas de ventanilla del Municipio de Tequisquiapan.";
                $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
                $direccion = $usuario->correo_electronico;
                //mandamos correo eléctronico
                $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
            break;*/
            case 4: //Ventanilla Revisión, pase a caja y entrega
              //Mandamos correo si pasa a fase de inspección
            /*  if ($siguiente_fase  == 3) {
                //obtenemos información de usuario
                $usuario = $this->Usuario->getUsuario($id_ciudadano);
                //Mandamos correo CorreoElectronico
                $tramite = $this->TramitesConsulta->descTramite($id_tramite);
                //Obtenemos html del correo
                $subTitulo = "Tu solicitud para el  trámite " . $tramite->nombre ." Se encuentra en inspección";
                $texto = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que tu trámite " . $tramite->nombre ." ya se encuentra en inspección.";
                $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
                $direccion = $usuario->correo_electronico;
                //mandamos correo eléctronico
                $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
              }//if
              else if ($siguiente_fase == 9) {
                //obtenemos información de usuario
                $usuario = $this->Usuario->getUsuario($id_ciudadano);
                //Mandamos correo CorreoElectronico
                $tramite = $this->TramitesConsulta->descTramite($id_tramite);
                //Obtenemos html del correo
                $subTitulo = "Tu solicitud para el  trámite " . $tramite->nombre ." Se encuentra en Caja";
                $texto = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que tu trámite " . $tramite->nombre ." ya se encuentra en Caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de tu historial de trámites.";
                $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
                $direccion = $usuario->correo_electronico;
                //mandamos correo eléctronico
                $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
              }
              else if ($finTramite) {//fin de oslicitud
                //obtenemos información de usuario
                $usuario = $this->Usuario->getUsuario($id_ciudadano);
                //Mandamos correo CorreoElectronico
                $tramite = $this->TramitesConsulta->descTramite($id_tramite);
                //Obtenemos html del correo
                $subTitulo = "Ahora puedes pasar a recoger tu trámite " . $tramite->nombre ;
                $texto = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que tu la licencia para tu trámite " . $tramite->nombre ." ya se encuentra lista para ser recogida en las oficinas de ventanilla.";
                $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
                $direccion = $usuario->correo_electronico;
                //mandamos correo eléctronico
                $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
              }//if*/
              if(!empty($urlPago)){
                //inserto el documento si se genero correctamente
                $res = $this->Documentos->insertaPago($id_solicitud,$id_tramite,$urlPago,$fecha_vencimiento,$folioSol,$referenciaBanco);
              }
              if(!empty($url)){
                $res = $this->Documentos->insertaDocs($id_solicitud,$id_tramite,$url,$folioSol);
              }
            break;
            case 8: //fase de inspección
              //buscamos que ya exista un documento de inspeccion, significa que el jede de inspección regreso el trámite para que volviera a subir el Documentos
              $inspecicon_antigua = $this->Solicitud_M->existeDocumentoInspeccion($id_solicitud);
              if (!empty($inspecicon_antigua)) {
                //borramos el archivo de inspeccion
                unlink($inspecicon_antigua->url);
                //si esta en la fase de inspección, se debera subr un archivo de inspeccion
                $urlDocInspeccion = $this->addInspeccion($id_solicitud,$id_usuario,$tramite->nombre,"txtDocInspeccion");
                //actualizamos en la tabla inspeccion
                $infoDoc = $this->Solicitud_M->updateInspeccion($id_solicitud,$id_usuario,$urlDocInspeccion);
                $jsonResponse["adversiment"] = ($infoDoc == "error") ? "Tu archivo de inspección se subió correctamente" : "NO se subió tu archivo de inspección" . $urlDocInspeccion;
              }//if
              else{
                //actualizamos el impacto de la solictud
                $this->Solicitud_M->updateGiroSolicitud($id_solicitud,$impacto);
                //si esta en la fase de inspección, se debera subr un archivo de inspeccion
                $urlDocInspeccion = $this->addInspeccion($id_solicitud,$id_usuario,$tramite->nombre,"txtDocInspeccion");
                //insetamos en la tabla inspeccion
                $infoDoc = $this->Solicitud_M->insertInspeccion($id_solicitud,$id_usuario,$urlDocInspeccion);
                $jsonResponse["adversiment"] = ($infoDoc == "error") ? "Tu archivo de inspección se subió correctamente" : "NO se subió tu archivo de inspección" . $urlDocInspeccion;
              }
              break;
            case 5:

              if(!empty($url)){
                $res = $this->Documentos->insertaDocs($id_solicitud,$id_tramite,$url,$folioSol);
              }
            break;
              case 11:

              if(!empty($url)){
                $res = $this->Documentos->insertaDocs($id_solicitud,$id_tramite,$url,$folioSol);
              }
            break;
            case 21:
              //inspeccion pero de sec de gobierno para finanzas
               //buscamos que ya exista un documento de inspeccion, significa que el jede de inspección regreso el trámite para que volviera a subir el Documentos
               $inspecicon_antigua = $this->Solicitud_M->existeDocumentoInspeccion($id_solicitud);
               if (!empty($inspecicon_antigua)) {
                 //borramos el archivo de inspeccion
                 unlink($inspecicon_antigua->url);
                 //si esta en la fase de inspección, se debera subr un archivo de inspeccion
                 $urlDocInspeccion = $this->addInspeccion($id_solicitud,$id_usuario,$tramite->nombre,"txtDocInspeccion");
                 //actualizamos en la tabla inspeccion
                 $infoDoc = $this->Solicitud_M->updateInspeccion($id_solicitud,$id_usuario,$urlDocInspeccion);
                 $jsonResponse["adversiment"] = ($infoDoc == "error") ? "Tu archivo de inspección se subió correctamente" : "NO se subió tu archivo de inspección" . $urlDocInspeccion;
               }//if
               else{
                 //actualizamos el impacto de la solictud
                 $this->Solicitud_M->updateGiroSolicitud($id_solicitud,$impacto);
                 //si esta en la fase de inspección, se debera subr un archivo de inspeccion
                 $urlDocInspeccion = $this->addInspeccion($id_solicitud,$id_usuario,$tramite->nombre,"txtDocInspeccion");
                 //insetamos en la tabla inspeccion
                 $infoDoc = $this->Solicitud_M->insertInspeccion($id_solicitud,$id_usuario,$urlDocInspeccion);
                 $jsonResponse["adversiment"] = ($infoDoc == "error") ? "Tu archivo de inspección se subió correctamente" : "NO se subió tu archivo de inspección" . $urlDocInspeccion;
               }
              break;
              case 24:
              if(!empty($url)){
                $res = $this->Documentos->insertaDocs($id_solicitud,$id_tramite,$url,$folioSol);
              }
            break;
            default:
              // code...
              break;
          }
          //obtenemos el giro o tipoo de licencia de construcción
          $tipoTramite = $this->TramitesConsulta->tipoLicenciaSolicitud($id_solicitud);
          $tipoLicencia = ($tipoTramite->giro != null) ? " ($tipoTramite->giro)" : "" ;
          $tipoLicencia = ($tipoTramite->tipoConstruccion != null) ? " ($tipoTramite->tipoConstruccion)" : $tipoLicencia ;
          $nombreTramite = $tipoTramite->nombre  . $tipoLicencia;
          $contenido = ($finTramite) ? "Ha finalizado el seguimiento de este trámite."  : "Tu Trámite ".$nombreTramite." paso a la siguiente fase.";
          $this->Notificacion_M->insert($id_solicitud,$id_ciudadano,$contenido);
          $this->correoSeguimientoSolicitud($info_fase->id_rol,$id_tramite,$info_fase_siguiente->noFase,$id_ciudadano,$rol,$finTramite,$nombreTramite);
          $jsonResponse["response"] = 200;
          $jsonResponse["msg"] = $contenido;
        }//if
        else{
          $jsonResponse["response"] = 522;
          $jsonResponse["msg"] = "Algo salio terriblemente mal";
        }//else

      }//else funcionario > 0
      echo json_encode($jsonResponse);
  }//actualiza

  /**
   * @return json data
   */
  public function cancela()
  {
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;
    $id_ciudadano = (!empty($this->input->post('id_ciudadano'))) ? addslashes($this->input->post('id_ciudadano')) : 0;
    $elimina = $this->Solicitud_M->delete($id_solicitud,$id_ciudadano,0);
    $id_tramite = (!empty($this->input->post('id_tramite'))) ? addslashes($this->input->post('id_tramite')) : 0;
    $texto = (!empty($this->input->post('texto'))) ? addslashes($this->input->post('texto')) :"";

    //la Solicitud paso a etatus 0
    if ($elimina) {

      //cargamos modelo de usuario para obtener el correo del ciudadano
      $this->load->model('Usuario');
      //obtenemos información de usuario
      $usuario = $this->Usuario->getUsuario($id_ciudadano);
      //obtenemos información del Trámite
      $tramite = $this->TramitesConsulta->descTramite($id_tramite);
      //se manda correo al ciudadano
      //Obtenemos html del correo
      $subTitulo = "¡Tu solicitud  para el trámite " . $tramite->nombre ." fue cancelada!";
      $texto = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que tu solicitud fue cancelada por el siguiente motivo: " . $texto .".";
      $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
      $direccion = $usuario->correo_electronico;
      //mandamos correo eléctronico
      $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
      $json["response"] = 200;
      $json["msg"] = "Solicitud Cancelada";
    }//if
    else{
      $json["response"] = 500;
      $json["msg"] = "La Solicitud no Pudo Ser Cancelada";
    }//else
    echo json_encode($json);

  }//cancela

  /**
   * @return json data
   */
  public function correccion()
  {
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;
    $id_ciudadano = (!empty($this->input->post('id_ciudadano'))) ? addslashes($this->input->post('id_ciudadano')) : 0;
    $elimina = $this->Solicitud_M->delete($id_solicitud,$id_ciudadano,5);
    $id_tramite = (!empty($this->input->post('id_tramite'))) ? addslashes($this->input->post('id_tramite')) : 0;
    $texto = (!empty($this->input->post('texto'))) ? addslashes($this->input->post('texto')) :"";
    $id_usuario = $this->session->userdata('id_usuario'); //id de la persona logueada

    //la Solicitud paso a etatus 0
    if ($elimina) {
      //cargamos modelo de usuario para obtener el correo del ciudadano
      $this->load->model('Usuario');
      //obtenemos información de usuario
      $usuario = $this->Usuario->getUsuario($id_ciudadano);
      //obtenemos información del Trámite
      $tramite = $this->TramitesConsulta->descTramite($id_tramite);
      //se manda correo al ciudadano
      //Obtenemos html del correo
      $subTitulo = "¡Tu solicitud N° ".$id_solicitud." para el trámite " . $tramite->nombre ." necesita correcciones!";
      $contenido = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que debido a la inspección realizada necesitas hacer algunas modificaciones. El funcionario que realizó la inspección redactó lo siguiente: " . $texto .".";
      $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$contenido);
      $direccion = $usuario->correo_electronico;
      //mandamos correo eléctronico
      $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
      $this->Notificacion_M->insert($id_solicitud,$id_ciudadano,"En espera de correcciones. Verifica los detalles en tu correo electrónico");
      $this->Solicitud_M->correccion($id_solicitud,$id_usuario,$texto);
      $json["response"] = 200;
      $json["msg"] = "Solicitud Cancelada";
    }//if
    else{
      $json["response"] = 500;
      $json["msg"] = "La Solicitud no Pudo Ser Cancelada";
    }//else
    echo json_encode($json);

  }//cancela

  /**
   * @return json data
   */
  public function correccionInspeccion()
  {
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;
    $id_fase = (!empty($this->input->post('id_fase'))) ? addslashes($this->input->post('id_fase')) : 0;
    $id_tramite = (!empty($this->input->post('id_tramite'))) ? addslashes($this->input->post('id_tramite')) : 0;
    $texto = (!empty($this->input->post('texto'))) ? addslashes($this->input->post('texto')) :"";
    //obtenemos información del Trámite
    $tramite = $this->TramitesConsulta->descTramite($id_tramite);
    //obtenemos la siguinte fase
    $info_fase = $this->Fase_M->fase($id_fase);
    $fase_anterior = $info_fase->anterior;

    //Buscamos al usuaro de inspección del trámites
    $rol_funcionario = $this->Fase_M->fase($fase_anterior)->id_rol;
    $id_funcionario  =  $this->Solicitud_M->getFuncionarioSeguimientoSolicitud($rol_funcionario,$id_solicitud)->id_usuario;
    //la Solicitud paso a etatus 0
    if ($id_funcionario > 0) {
      if($this->Solicitud_M->update($id_solicitud, $id_funcionario,$fase_anterior,4," ")){
        //Cargamos modelo de usuario para obtener su información
        $this->load->model('Usuario');
        //obtenemos información de usuario
        $usuario = $this->Usuario->getUsuario($id_funcionario);
        //obtenemos información del Trámite
        $tramite = $this->TramitesConsulta->descTramite($id_tramite);
        //se manda correo al ciudadano
        //Obtenemos html del correo
        $solicitud  = ($this->Solicitud_M->getSolicitud($id_funcionario,4,$id_solicitud)->id > 0  ) ? $this->Solicitud_M->getSolicitud($id_funcionario,4,$id_solicitud) : "";
        $subTitulo = "Tu inspeccion del trámite " . $tramite->nombre ." tiene correcciones!";

        $texto = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que necesitas actualizar tu documento de inspección de la solicitud ".$solicitud->id.". Jefe de inspección redacto lo siguiente: " . $texto .".";

        $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
        $direccion = $usuario->correo_electronico;
        //mandamos correo eléctronico
        $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
        $json["response"] = 200;
        $json["msg"] = "Solicitud Actualizada";
      }//if
      else{
        $json["response"] = 500;
        $json["msg"] = "No se encontro Funcionario de seguimiento";
      }//else
    }//if
    else{
      $json["response"] = 500;
      $json["msg"] = "La Solicitud no Pudo Ser Actualizada";
    }//else
    echo json_encode($json);

  }//cancela


  /**
   * @return json data
   * fUNCIÓN QUE PONE EL ESTATUS EN ESTADO DE CORRECCIONES PARA QUE EL CIUDADANO SUBA UN PDF QUE VISUALIZARÁ EL INSPECTOR
   */
  public function correccionInspeccionCiudadano()
  {
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;
    $id_fase = (!empty($this->input->post('id_fase'))) ? addslashes($this->input->post('id_fase')) : 0;
    $id_tramite = (!empty($this->input->post('id_tramite'))) ? addslashes($this->input->post('id_tramite')) : 0;
    $obs_inspector = (!empty($this->input->post('texto'))) ? addslashes($this->input->post('texto')) :"";
    //obtenemos información del Trámite
    $tramite = $this->TramitesConsulta->descTramite($id_tramite);
    //OBTENEMOS LA FASE ACTUAL, YA QUE LA SOLICITUD SE QUEDARÁ EN INSPECCIÓN
    $solicitud = $this->Solicitud_M->getSolicitudCosto($id_solicitud);//función que nos trae el row de una solicitud

    $fase_actual = $solicitud->id_fase;//OBTENEMOS LA FASE ACTUAL DE LA SOLICITUD
    $id_funcionario = $solicitud->id_funcionario;//OBTENEMOS EL ID EL FUNCIONARIO QUE TIENE EL INSPECTOR

    //ACTUALIZAMOS EL STATUS DE LA SOLICITUD A ESTADO DE CORRECCIONES POR PARTE DEL CIUDADANO 6
    if($this->Solicitud_M->update($id_solicitud, $id_funcionario,$fase_actual,6," ")){
      //Cargamos modelo de usuario para obtener su informaciónjaja
      $this->load->model('Usuario');
      //obtenemos información del ciudadano
      $usuario = $this->Usuario->getUsuario($solicitud->id_ciudadano);
      //CREAMOS CONTENIDO DEL CORREO
      $subTitulo = "Tu inspeccion del trámite " . $tramite->nombre ." tiene correcciones.";
      $texto = "¡Hola " . $usuario->primer_nombre . "! " . " Este correo electrónico es para avisarte que necesitas actualizar tu documento de inspección de la solicitud ".$solicitud->id.". El inspector redactó lo siguiente: " . $obs_inspector .". Puede subir su archivo de evidencia en la plataforma de trámites en la sección de correcciones.";

      //INSERTAMOS EN LA TABLA DONDE EL CIUDADANO SUBIRA SU DOCUMENTO DE CORRECCIÓN DE INSPECCIÓN
      $this->Solicitud_M->insertCorreccionCiudadano($id_solicitud,$solicitud->id_ciudadano, $solicitud->id_funcionario," ",$obs_inspector);
      //INSERTAMOS EN LA TABLA notificaciones
      $this->Notificacion_M->insert($id_solicitud,$solicitud->id_ciudadano,"SUBIR DOCUMENTO DE CORRECCIONES QUE SOLICITÓ EL INSPECTOR");

      $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
      $direccion = $usuario->correo_electronico;
      //mandamos correo eléctronico
      $this->correoelectronico->mandarCorreo($direccion, $tramite->nombre, $contenido_mail);
      $json["response"] = 200;
      $json["msg"] = "Solicitud Actualizada.";
    }//if update solicitud
    else{
        $json["response"] = 500;
        $json["msg"] = "La Solicitud no Pudo Ser Actualizada";
    }//else
    echo json_encode($json);

  }//correccionInspeccionCiudadano

  /**
   * Guardamos la inspección del funcionario y la asociamos a una soliciud
   */
  public function addInspeccion($id_solicitud,$id_usuario,$nombreTramite,$inputFile)
  {
    $nameDocInspeccionOnServer = "No existe archivo";
    if (!empty($_FILES[$inputFile]["name"])) {
      $urlDocs = "docsInspeccion/"; //Url para guardar la ruta de los archivos
      //validamos que exista un directorio donde se subiran los archivos de inspeccion
      $this->loadfile->dir_exist($urlDocs);
      $nombreRequisito = "usr" . $id_usuario ."Sol" . $id_solicitud . "Ins" . $nombreTramite. date('Y-m-d');
      $config["upload_path"]    = $urlDocs;//url donde se subira el archivo
      $config ['allowed_types'] =  'application/pdf|pdf|.pdf';//extensiones permitidas
      $config ['max_size' ]     =  5242880;//tamaño maximo del archivo
      $config['file_name'] = $nombreRequisito;//nombre del archivo

      $respond = $this->loadfile->upload_files($config,$inputFile);
      $nameDocInspeccionOnServer = ($respond["valid"]) ? $urlDocs . $respond["uploadSuccess"]["file_name"] : "error";

    }//if
    else{
      $nameDocInspeccionOnServer = "No se subió documento  servidor";
    }
    return $nameDocInspeccionOnServer;


  }//addInspeccion


  /**
   *@param INT $id_Soicitud
   *@param INT $id_usuario
   *@param INT $nombreTramite
   *@param INT $inputFile
   * Guardamos la opinion Tecnica en la carpeta de opinionTecnica
   */
  public function addOpinionTecnica($id_solicitud,$id_usuario,$nombreTramite,$inputFile)
  {
    $nameDocInspeccionOnServer = "No existe archivo";
    if (!empty($_FILES[$inputFile]["name"])) {
      $urlDocs = "docsOpinionTecnica/"; //Url para guardar la ruta de los archivos
      //validamos que exista un directorio donde se subiran los archivos de inspeccion
      $this->loadfile->dir_exist($urlDocs);
      sleep(1);
      $nombreRequisito = "usr" . $id_usuario ."Sol" . $id_solicitud . "OpinionTecnica" . $nombreTramite. date('Y-m-d');
      $config["upload_path"]    = $urlDocs;//url donde se subira el archivo
      $config ['allowed_types'] =  'application/pdf|pdf|.pdf';//extensiones permitidas
      $config ['max_size' ]     =  5242880;//tamaño maximo del archivo
      $config['file_name'] = $nombreRequisito;//nombre del archivo

      $respond = $this->loadfile->upload_files($config,$inputFile);
      $nameDocInspeccionOnServer = ($respond["valid"]) ? $urlDocs . $respond["uploadSuccess"]["file_name"] : "error";
    }//if
    else{
      $nameDocInspeccionOnServer = "No se subió documento  servidor";
    }
    return $nameDocInspeccionOnServer;
  }//addOpinionTecnica

  /**
   * Muestra solo a rol VENTANILLA
   */
  public function entrega()
  {
    if ($this->session->userdata('rol')[0]->nombre == "Ventanilla") {
      //guardamos el id del funcionario actual
      $id_funcionario = $this->session->userdata('id_usuario');
      //Obtenemos los trámites  a Entregar
      $tramites_finalizados = $this->Solicitud_M->entregaTramite($id_funcionario);

      $data = array("title"=>"Entregas de Licencias","tramites_finalizados"=>$tramites_finalizados);

      $this->load->view('head',$data);
      $this->load->view('funcionario/header');
      $this->load->view('funcionario/entrega',$data);
      $this->load->view('footer');
      $this->load->view('scripts/js');
      $this->load->view('scripts/utils');
      $this->load->view('scripts/finEvaluacion');
      $this->load->view('scripts/ws-funcionario');
      $this->load->view('scripts/dashboard');

    }
    else{
      redirect(base_url()."Auth",'');
    }//else

  }//entrega

  /**
   * @param array $pestanias_form
   */
  public function obtenerPestania($arr_pestania)
  {
    $pestanias = array();
    foreach ($arr_pestania as $pestania) {
      array_push($pestanias, $pestania->id_pestania);
    }//foreach
      return $pestanias;
  }//obtener_pestania
  /**
   * @param array $arr_pestania
   */
  public function obtenerForm($arr_form)
  {
    $id_form = array();
    foreach ($arr_form as $form) {
      array_push($id_form, $form->id_formulario);
    }//foreach
      return $id_form;
  }//obtenerForm

  /**
   * @param INT id_rol
   * @return INT estatus
   */
  public function faseSolicitud($id_rol,$noFase,$siguiente_fase,$id_tramite)
  {
    $estatus = 4;
    /**
     *  VENTANILLA, INSPECCION, JEFE DE INSPECCION, ENCARGADO DE NUMEROS OFICIALES, SUB DIRECTOR = 4
     *  DIRECTOR = 3
     *  CAJA = 2
     *  VENTANILLA = 1
     */
     switch ($id_tramite) {
       case 1://Número Oficial
         if ($id_rol == 4 || $id_rol == 8  || $id_rol == 9 || $id_rol == 5 || $id_rol ==6 || $id_rol == 3) {
          if ($noFase == 2) {
              $estatus = 4;
          }//if
          else if ($noFase == 9) {
            $estatus = 2;
          }
            else if($noFase == 7) {
              $estatus = 3;
          }//else
          else if($noFase == 8) {
              $estatus = 3;
          }//else
          else if($siguiente_fase == 0) {
              $estatus = 1;
          }//else
        }//if
        else if ($id_rol == 7) {
          $estatus = 3;
        }//else if
        break;
       case 2://Dictamen uso de suelo
          if ($id_rol == 4 || $id_rol == 8  || $id_rol == 9 || $id_rol == 5 || $id_rol ==6 || $id_rol == 3) {
           if ($noFase == 2) {
               $estatus = 4;
           }//if
           else if ($noFase == 9) {
             $estatus = 2;
           }
             else if($noFase == 7) {
               $estatus = 3;
           }//else
           else if($noFase == 8) {
               $estatus = 3;
           }//else
           else if($siguiente_fase == 0) {
               $estatus = 1;
           }//else
         }//if
         else if ($id_rol == 7) {
           $estatus = 3;
         }//else if
        break;
       case 3:
        if ($noFase == 7) {//ventanilla emite pase  caja
          $estatus = 3;
        }//if
        else if ($id_rol == 3) {//caja cobr a ciudadano y pasa trámite a entrega
          $estatus = 2;
        }//else if
        else if($siguiente_fase == 0) {//ventanilla termina seguimiento de trámite
            $estatus = 1;
        }//else
        break;
      case 4://Licencia de funcionamiento
        if ($id_rol == 3) {//fase de caja
          $estatus = 4;
        }//if
        else if ($noFase == 8) {//Presidente pasa a entrega
           $estatus = 2;
        }//if
        else if ($noFase == 4) {//Ventanilla Genera pase a caja
          $estatus = 3;
        }//if
        else if ($noFase == 5) {//Caja cobra a ciudadano
          $estatus = 3;
        }//if
        else if ($noFase == 6 || $noFase == 7) {//Ventanilla Genera documento y pasa a tesorero
          $estatus = 4;
        }//if
        else if ($noFase == 9 && $siguiente_fase == 0) {
          $estatus = 1;//Ventanilla entrega trámite
        }//else if
        //Fase de ventanilla en trámite de inspección
        else if($noFase == 4) {
            $estatus = 3;
        }//else
        break;
      case 5://permiso de alcohol
        if ($id_rol == 4 || $id_rol == 7 || $id_rol == 3 || $id_rol == 15 || $id_rol == 19) {

            if ($noFase == 5) {
              $estatus = 3;
            }
            else if($noFase == 6) {
                $estatus = 3;
            }//else
            else if($noFase == 7) {
                $estatus = 2;
            }//else
          else if($noFase == 8 && $siguiente_fase == 0) {
                $estatus = 1;
            }//else
              }
        break;
      case 6://protección Civil
        if ($id_rol == 3) {//fase de caja
          $estatus = 4;
        }//if
        else if ($noFase == 4) {//Ventanilla Genera pase a caja
          $estatus = 3;
        }//if
        else if ($noFase == 5) {//Caja cobra a ciudadano
          $estatus = 3;
        }//if
        else if ($noFase == 6) {//Ventanilla Genera documento y despues entrega el documento
          $estatus = 2;
        }//if
        else if ($noFase == 7 && $siguiente_fase == 0) {
          $estatus = 1;//Ventanilla entrega trámite
        }//else if
        //Fase de ventanilla en trámite de inspección
        else if($noFase == 4) {
            $estatus = 3;
        }//else
        break;
      case 7://licencia de construcción
        if ($id_rol == 3) {//fase de caja
            $estatus = 2;
        }//if
        else if ($noFase == 6 || $noFase == 7) {//director pasa a ventanilla para emitir pase de caja
          $estatus = 3;
        }//if
        else if ($noFase == 9 && $siguiente_fase == 0) {
          $estatus = 1;//Ventanilla entrega trámite
        }//else if
        break;
        case 8://informe uso de suelo
        if ($id_rol == 4 || $id_rol == 7 || $id_rol == 3 ) {

           if ($noFase == 6) {
             $estatus = 3;
           }
             else if($noFase == 7) {
               $estatus = 3;
           }//else
           else if($noFase == 8) {
               $estatus = 2;
           }//else
           else if($noFase == 9 && $siguiente_fase == 0) {
               $estatus = 1;
           }//else
        }
        break;
        case 9://alcohol provicional
        if ($id_rol == 4 || $id_rol == 19 || $id_rol == 3 || $id_rol == 22) {

           if ($noFase == 3) {
             $estatus = 3;
           }
             else if($noFase == 4) {
               $estatus = 4;
           }//else
           else if($noFase == 6) {
            $estatus = 2;
        }//else
           else if($noFase == 7 && $siguiente_fase == 0) {
               $estatus = 1;
           }//else
        }
        break;
       default:
         // code...
         break;
     }



    return $estatus;
  }//faseSoliitud

  /**
   * Muestra la información del pago de una solicitud
   */
  public function infoSolicitud()
  {
    $id_solicitud = (!empty($this->input->post('id_solicitud')))  ? addslashes($this->input->post('id_solicitud')) : null;
    $estatus = (!empty($this->input->post('estatus')))  ? addslashes($this->input->post('estatus')) : null;
    $pases_caja = $this->Solicitud_M->infoSolicitud($id_solicitud,$estatus);
    echo json_encode($pases_caja);
  }//infoSolicitud

  public function correoSeguimientoSolicitud($id_rol,$id_tramite,$no_fase,$id_ciudadano,$rol,$finTramite,$nombreTramite){
    //obtenemos información de usuario
    $usuario = $this->Usuario->getUsuario($id_ciudadano);
    $dependencia=$this->Funcionario->dependencia($id_tramite);
    //Mandamos correo CorreoElectronico
    //$tramite = $this->TramitesConsulta->descTramite($id_tramite);

    $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." se encuentra en ".$rol->nombre;
    $texto = "";
    //Obtenemos html del correo
    switch ($id_tramite) {
      case 1://Alineamiento y Número Oficial
      if ($id_rol == 4 || $id_rol == 3) {
        if ($no_fase == 3) {
          $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.".";
        }
        else if ($no_fase == 9) {
          $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
        }
        else if ($no_fase == 10 && !$finTramite) {
          $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
        }
        else if ($finTramite) {
          $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
          $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
        }
        //mandamos correo eléctronico
        if ($texto != "") {
          $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
          $direccion = $usuario->correo_electronico;
          $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
        }
      }//if
      break;
      case 2://Dictamen Uso de Suelo
        if ($id_rol == 4 || $id_rol == 3) {
          if ($no_fase == 3) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.".";
          }
          else if ($no_fase == 9) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
          }
          else if ($no_fase == 10  && !$finTramite) {
            $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
          }
          else if ($finTramite) {
            $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
            $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
          }
          //mandamos correo eléctronico
          if ($texto != "") {
            $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
            $direccion = $usuario->correo_electronico;
            $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
          }
        }//if
      break;
      case 3://Factibilidad de Giro
        if ($id_rol == 4 || $id_rol == 3) {
          if ($no_fase == 3) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.".";
          }
          else if ($no_fase == 8) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
          }
          else if ($no_fase == 9  && !$finTramite) {
            $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
          }
          else if ($finTramite) {
            $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
            $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
          }
          //mandamos correo eléctronico
          if ($texto != "") {
            $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
            $direccion = $usuario->correo_electronico;
            $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
          }
        }//if
      break;
      case 4://Licencia de Funcionamiento
        if ($id_rol == 4 || $id_rol == 3 ) {
          if ($no_fase == 3) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.".";
          }
          else if ($no_fase == 5) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
          }
          else if ($no_fase == 9  && !$finTramite) {
            $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
          }
          else if ($finTramite) {
            $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
            $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
          }
          //mandamos correo eléctronico
          if ($texto != "") {
            $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
            $direccion = $usuario->correo_electronico;
            $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
          }
        }//if
      break;
      case 5://Bebidas Alcoholicas
      // code...
      if ($id_rol == 4 || $id_rol == 3 || $id_rol == 15 || $id_rol = 19) {
        if ($no_fase == 3 || $no_fase == 5) {
          $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre.".";
        }
        else if ($no_fase == 7) {
          $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
        }
        else if ($no_fase == 8 && !$finTramite) {
          $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
        }
        else if ($finTramite) {
          $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
          $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
        }
        //mandamos correo eléctronico
        if ($texto != "") {
          $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
          $direccion = $usuario->correo_electronico;
          $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
        }
      }//if
      break;
      case 6://Protección Civil
        if ($id_rol == 4 || $id_rol == 3 || $id_rol == 8) {
          if ($no_fase == 3) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.". (Las visitas de los inmuebes estan sujetas a dispocisión de los recursos del departamento)";
          }
          else if ($no_fase == 5) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
          }
          else if ($no_fase == 7 && !$finTramite) {
            $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
          }
          else if ($finTramite) {
            $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
            $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
          }
          //mandamos correo eléctronico
          if ($texto != "") {
            $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
            $direccion = $usuario->correo_electronico;
            $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
          }
        }//if
      break;
      case 7://Licencia de Construcción
        if ($id_rol == 4 || $id_rol == 3 || $id_rol == 8) {
          if ($no_fase == 2) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.".";
          }
          else if ($no_fase == 3) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.".";
          }
          else if ($no_fase == 8) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
          }
          else if ($no_fase == 9 && !$finTramite) {
            $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
          }
          else if ($finTramite) {
            $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
            $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
          }
          //mandamos correo eléctronico
          if ($texto != "") {
            $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
            $direccion = $usuario->correo_electronico;
            $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
          }
        }//if
      break;
      case 8://Informe uso de suelo
        if ($id_rol == 4  || $id_rol == 3 || $id_rol == 8) {
          if ($no_fase == 3) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre." de ".$dependencia->nombre_dependencia.".";
          }
          else if ($no_fase == 8) {
            $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
          }
          else if ($no_fase == 9 && !$finTramite) {
            $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
          }
          else if ($finTramite) {
            $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
            $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
          }
          //mandamos correo eléctronico
          if ($texto != "") {
            $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
            $direccion = $usuario->correo_electronico;
            $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
          }
        }//if
      break;
      case 9://Bebidas Alcoholicas(Provicional)
      // code...
      if ($id_rol == 4 || $id_rol == 3) {
        if ($no_fase == 2) {
          $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite ." se encuentra en la fase de ".$rol->nombre.".";
        }
        else if ($no_fase == 4) {
          $texto = "¡Hola " . $usuario->primer_nombre . "! ".  " Este correo electrónico es para avisarte que tu solicitud para el trámite " . $nombreTramite . " ya se encuentra en caja. Puedes imprimir tu orden de pago en la plataforma de trámites en la sección de historial de trámites.";
        }
        else if ($no_fase == 7 && !$finTramite) {
          $texto =  "¡Hola " . $usuario->primer_nombre . "! ".   " Este correo electrónico es para avisarte que  puedes pasar a las oficinas de ventanilla del municipio para finalizar el seguimiento de tu solicitud correspondiente al trámite " . $nombreTramite;
        }
        else if ($finTramite) {
          $subTitulo = "Tu solicitud para el trámite " . $nombreTramite ." finalizó";
          $texto =  "Finalizó el seguimiento de tu Solicitud para el trámite " . $nombreTramite;
        }
        //mandamos correo eléctronico
        if ($texto != "") {
          $contenido_mail = $this->correoelectronico->inicioTramite($subTitulo,$texto);
          $direccion = $usuario->correo_electronico;
          $this->correoelectronico->mandarCorreo($direccion, $nombreTramite, $contenido_mail);
        }
      }//if
      break;
      default:
        // code...
        break;
    }//switch


  }//correoSeguimientoSolicitud

  /**
   * @param INT $id_solicitud
   * Cambiar estatus a lasolicitud
   */
  public function update($id_solicitud = ""){
    if ($id_solicitud != "") {
      $id_funcionario = $this->session->userdata('id_usuario');
      $solicitud  = ($this->Solicitud_M->getSolicitud($id_funcionario,5,$id_solicitud) != null) ? $this->Solicitud_M->getSolicitud($id_funcionario,5,$id_solicitud) : 0;
      if ($this->Solicitud_M->delete($solicitud->id,$solicitud->id_ciudadano,4)) {
        $this->session->set_flashdata('good', " La Solicitud cambio a la tabla de Trámites");
      }//if
      else {
        $this->session->set_flashdata('error', " Ocurrio un error al actualiazar el estatus de la solicitud");
      }
      redirect(base_url() . "Funcionario/correcciones", '');
    }//if
    else {
      redirect(base_url()."Auth","");
    }//else
  }//update

  public function opinionTecnica(){
   $opinion = (!empty($this->input->post('opinion'))) ? addslashes($this->input->post('opinion')) : null;
   $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;

   if($opinion != null){

    $opinionT=$this->Solicitud_M->insertOpinion($id_solicitud,$opinion);

    $jsonResponse["response"] = 200;
    $jsonResponse["msg"] = "Información guardada correctamente";

   }else{

    $jsonResponse["response"] = 522;
    $jsonResponse["msg"] = "Completa el campo de opinión técnica (*)";
   }

  echo json_encode($jsonResponse);
  }//opinion

  /**
   * @param INT $opDesarrolloUrbano
   * @param INT $opProteccionCivil
   * @param INT $opFinanzas
   * Esta funcion registrará en la base de datos los usuarios encargadados de subir la opinión técnica
   */

  public function insertUsuariosOpinion($opDesarrolloUrbano, $opProteccionCivil, $opFinanzas, $id_solicitud,$fechaDictamen){
    $arr_usuarios_opinion = array($opDesarrolloUrbano, $opProteccionCivil, $opFinanzas);
    //Recorremos el arreglo para encontrar a los usuarios para aregistrar en la tabla usuariosopinion
    for ($i=0; $i < count($arr_usuarios_opinion); $i++) {
      if ($arr_usuarios_opinion[$i] != 0) {
          $id_funcionario = (!empty($this->Solicitud_M->getIdFuncionario(72,$arr_usuarios_opinion[$i])) ? $this->Solicitud_M->getIdFuncionario(72,$arr_usuarios_opinion[$i])->id : 0);
          //validamos que encontro un funcionarios
          if ($id_funcionario != 0) {
            $this->Solicitud_M->insertarFuncionarioOpinion($id_solicitud,$id_funcionario,$fechaDictamen);
          }// if $id_funcionario != 0
      }// if != 0
    }//for

  }//insertUsuariosOpinion



  public function insertTipoPago(){
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;
    $fecha = (!empty($this->input->post('fecha'))) ? addslashes(date("Y-m-d",strtotime($this->input->post('fecha')))) : null;
    $corte = (!empty($this->input->post('en_corte'))) ? addslashes($this->input->post('en_corte')) : 0; //corte = 0 : no se ha enviado a corte / corte = 1 : se realizo corte
    $tipo_pago = (!empty($this->input->post('tipo_pago'))) ? addslashes($this->input->post('tipo_pago')) : 0;

    $folio = $this->Solicitud_M->getFolio($id_solicitud);
    $lineaCap = $this->Solicitud_M->getlinea($id_solicitud);

      if($id_solicitud != 0){
        if($tipo_pago == "C"){
        $tipoCaja = $this->Solicitud_M->insertaTipoPago($id_solicitud, $folio, $fecha, $corte, $tipo_pago);
        $this->session->set_flashdata('good', "Se ingreso correctamente el folio de banco");
        }
        elseif($tipo_pago == "B"){
          $tipoBanco = $this->Solicitud_M->insertaTipoPago($id_solicitud, $lineaCap, $fecha, $corte, $tipo_pago);
        $this->session->set_flashdata('good', "Se ingreso correctamente el folio de banco");
        }
        }
    else{
      $error=$this->session->set_flashdata('error', "la linea de captura no existe");
    }
  }//insertTipoPagoBanco

  public function enCorte(){
      $update_corte = ($this->input->post('corte') == 0) ? addslashes($this->input->post('corte')) : 1;//se actualiza el estatus de corte
     if($this->Solicitud_M->generaCorte($update_corte))
     {
      $this->session->set_flashdata('good', " Se actualizo el estatus del corte de pago");

      if($this->Solicitud_M->insertaCorte())
      {

        $jsonResponse["response"] = 200;
        $jsonResponse["msg"] = "Información guardada correctamente";

      }//if insertaCorte
      else{
        $this->session->set_flashdata('error', "Error al insertar en corte");
      }

      $jsonResponse["response"] = 200;
      $jsonResponse["msg"] = "Información guardada correctamente";

    }
    else{
      $jsonResponse["response"] = 522;
    $jsonResponse["msg"] = "Completa el campo de opinión técnica (*)";

    }
    echo json_encode($jsonResponse);

  }//en Corte

  /**
   * return json data
   */
  public function evidenciaInspeccion()
  {
    //variables que ocuparemos para la actalizción de la fase del trámite
    $id_solicitud = (!empty($this->input->post('id_solicitud'))) ? addslashes($this->input->post('id_solicitud')) : 0;

    $urlDocs = "docsCiudadanoCorrecciones/"; //Url para guardar la ruta de los archivos
    //Verificamos que el directorio exista
    // var_dump($requisitos);
    $this->loadfile->dir_exist($urlDocs);
    //nomencaltura de los requisitos
    $nombreRequisito = "SOL" . $id_solicitud . date('Y-m-d');
    $inputFile = 'importData';
  //  echo $_FILES[$inputFile]["importData"];
    //Subimos los requisitos al servidor
    if (!empty($_FILES[$inputFile]["name"])) {
      $extensiones_permitidas =  array('application/pdf','pdf');
      //validamos la extensión de los archivos PDF || AUTOCAD
      if (in_array($_FILES[$inputFile]["type"],$extensiones_permitidas)) {
        $config["upload_path"]    = $urlDocs;//url donde se subira el archivo
        $config ['max_size' ]     =  5242880;//tamaño maximo del archivo
        $config['file_name'] = $nombreRequisito;//nombre del archivo
        $config ['allowed_types'] =  '*';//extensiones permitidas
        $respond = $this->loadfile->upload_files($config,$inputFile);
      }
    }
    //insertasmo en DB
    $insert = $this->Solicitud_M->updateCorreccionCiudadano($id_solicitud,$urlDocs . $respond["uploadSuccess"]["file_name"]);

    if ($insert) {
      $jsonResponse["estatus"] = 200;
      $jsonResponse["data"] = $respond;
      $jsonResponse["msg"] = "Se subió el documento de correcciones. Espera las indicaciones del inspector";
    }
    else{

        $jsonResponse["estatus"] = 200;
        $jsonResponse["data"] = $respond;
        $jsonResponse["msg"] = "Ocurrio un error inesperado";
    }
    echo json_encode($jsonResponse);

  }//actualiza
}//class
