<?php
/**
 * @author Guadalupe Valerio
 * @version 1.0
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * En esta clse vamos a authenticar a todos los  usuarios de la plataforma
 */
class Auth extends CI_Controller
{

    public function index()
    {
        // $this->load->library('license/license');//libreria para validar la conraseña del uso de la plataforma
       // $this->license->authenticate();
        
      //validamos que exista una session
      if (!$this->session->userdata('logged_in')) {
        //Obtenemos los atos del formulario de login
        $this->load->model('Formulario');
        $this->Formulario->setIdFormulario(1);
        $formulario = $this->Formulario->getForm(); //obtenemos los datos del formulario
        //Obtenemos los aributos del formulario
        $atributos_formulario = $this->Formulario->getAributoFormulario();
        //obtenemos los campos del formulrio
        $campos_formulario = $this->Formulario->getCamposFormulario();
        //mandamos los datos a la vista

        $this->Formulario->setIdFormulario(2);
        $formAlCiu        = $this->Formulario->getForm();
        $atributosAlCiu   = $this->Formulario->getAributoFormulario();
        $campos_formAlCiu = $this->Formulario->getCamposFormulario();
        $this->Formulario->setIdFormulario(4);
        $for = $this->Formulario->getForm();
        $atr = $this->Formulario->getAributoFormulario();
        $cam = $this->Formulario->getCamposFormulario();

    		$this->load->model('TramitesConsulta');
    		$dataTramites = $this->TramitesConsulta->tramitesConculuidos();

        $data = [
            "formAlCiu"            => $formAlCiu,
            "atributosAlCiu"       => $atributosAlCiu,
            "campos_formAlCiu"     => $campos_formAlCiu,
            "formulario"           => $formulario,
            "atributos_formulario" => $atributos_formulario,
            "campos_formulario"    => $campos_formulario,
            "for"                  => $for,
            "atr"                  => $atr,
            "cam"                  => $cam,
            "tramitesConcluidos"   => $dataTramites,
        ];
        //Cargamos las vistas correspondientes a este controlador
        $data["title"] = "Acceso a la Plataforma";
        $this->load->view('head', $data);
        $this->load->view('auth/content', $data);
        $this->load->view('footer');
        $this->load->view('scripts/js');
      }//if
      else{
        $this->load->library('validation');
        $this->validation->redirect();
      }//else

    } //index

    public function login()
    {
        $email = ($this->input->post("txtEmail")) ? addslashes($this->input->post("txtEmail")) : "";
        $pwd   = ($this->input->post("txtPwd")) ? addslashes($this->input->post("txtPwd")) : "";
        //validamos que la información venga desde el boton del formulrio
        if ($this->input->post("btnEntrar")) {
            //hacemos las validaciones de los campos del formulario
            $arr_validations = [
                array(
                    "field" => "txtEmail",
                    "label" => "Correo Electrónico",
                    "rules" => "required|max_length[65]|min_length[10]|valid_email|trim",
                ),
                array(
                    "field" => "txtPwd",
                    "label" => "Contraseña",
                    "rules" => "required|min_length[2]",
                ),
            ];
            $arr_msg = [
                array(
                    "valid_email"     => "EL campo %s debe ser un correo electrónco correcto",
                    "min_length[10]"  => "El campo %s debe tener mas de 10 caracteres",
                    "max_length[65]"  => "El campo %s solo debe tener 65 caracteres",
                    "min_length[6]"   => "El campo %s debe tener mas de 6 caracteres",
                    "max_length[110]" => "El campo %s supera el maximo de caracteres",
                ),
            ];
            //cargamos la libreria de form_validation
            $this->load->library('validation');
            $form_validation = $this->validation->validForm($arr_validations, $arr_msg);
            //Si la validación es correcta
            if ($form_validation) {
                //cargamos el modelo de usuario
                $this->load->model('usuario');
                //Mandamos llamar la funcion del moedlo para validar que existe el usuario
                $usuario = $this->usuario->exist($email, hash('sha512', $pwd));
                //validamos que la base de datos nos devuelva información
                if (!empty($usuario)) {
                    //valdamos la informacuón
                    $valid = $this->validation->existe($email, $usuario->correo_electronico, hash('sha512', $pwd), $usuario->contrasenia);
                    //si los dtos ingresados coinciden con la info en DB
                    if ($valid) {
                        $this->usuario->setId($usuario->id);
                        $this->redirect();
                    } //if
                    //la información ingresada no coincide con el registro de la base de datos
                    else {
                        //mandamos los datos a la vista
                        $this->session->set_flashdata('error', "La información proporcionada no es la correcta.");
                        redirect(base_url() . 'Auth', '');
                    } //else
                } //if
                //la base de datos no devolvio información
                else {
                    //mandamos los datos a la vista
                    $this->session->set_flashdata('error', " Usuario no encontrado");
                    redirect(base_url() . 'Auth', '');
                } //else
            } //if
            else {
                //mandamos los datos a la vista
                $this->session->set_flashdata('error', validation_errors());
                redirect(base_url() . 'Auth', '');
            } //else
        } //if
        else {
            redirect(base_url() . 'Auth', '');
        } //else
    } //login

    public function redirect()
    {
        //cargamos el modelo de usuario
        $this->load->model('usuario');
        $id =   $this->usuario->getId();
        $data_usuario = $this->usuario->getUsuario($id);
        if ($data_usuario->status == 1) {
            $rol_usr   = $this->usuario->getRol($id);
            $img       = (!empty($data_usuario->imagen)) ? base_url() . $data_usuario->imagen : (($data_usuario->genero == "M") ? base_url() . "plantilla/images/icons/user-man.png" : base_url() . "plantilla/images/icons/user-women.png");
            $sess_data = array(
                "logged_in"          => true,
                "id_usuario"         => $data_usuario->id,
                "id_persona"         => $data_usuario->id_persona,
                "primer_nombre"      => $data_usuario->primer_nombre,
                "segundo_nombre"     => $data_usuario->segundo_nombre,
                "primer_apellido"    => $data_usuario->primer_apellido,
                "id_municipio"       => $data_usuario->id_municipio,
                "id_estado"          => $data_usuario->id_estado,
                "segundo_apellido"   => $data_usuario->segundo_apellido,
                "genero"             => $data_usuario->genero,
                "fecha_nacimiento"   => $data_usuario->fecha_nacimiento,
                "telefono"           => $data_usuario->telefono,
                "correo_electronico" => $data_usuario->correo_electronico,
                "img"                => $img,
                "rol"                => $rol_usr,
                "id_dependencia"     => $data_usuario->idDependencia,
                "rol_actual"         => $rol_usr[0],
            );
            $this->session->set_userdata($sess_data);
            $this->load->library('validation');
            $this->validation->redirect();
        } //if
        else if ($data_usuario->status == 0) {
            $this->session->set_flashdata('error', " Tu usuario ha sido bloqueado. Contacta al  administrador de la plataforma para poder desbloquear tu cuenta.");
            redirect(base_url() . 'Auth', '');
        } //else if
        else if ($data_usuario->status == 2) {
            $this->session->set_flashdata('error', " Necesitas confirmar tu cuenta. Revisa tu correo electrónico con el cual te registraste y sigue las instrucciones.");
            redirect(base_url() . 'Auth', '');
        } //else if

    } //redirect

    public function log_out()
    {
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
} //class

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */
