<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @author Pepe-droid
 * Muestra las vista de politica de datos de la plataforma
 * 14/01/2019
 */
class Politica extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function datos()
  {
      $data["title"]="Política de datos";
      $data["val"]=1;
      $this->load->view('terminosYcondiciones/head',$data);
      $this->load->view('terminosYcondiciones/datos',$data);
  }//index

  function privacidad()
  {
      $data["title"]="Política de Privacidad";
      $data["val"]=2;
      $this->load->view('terminosYcondiciones/head',$data);
      $this->load->view('terminosYcondiciones/datos',$data);
  }//index

}//Class
