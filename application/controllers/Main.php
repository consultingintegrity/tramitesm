<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$data["title"]="Plataforma de Trámites";
		$this->load->model('TramitesConsulta');
		$dataTramites['tramites'] = $this->TramitesConsulta->consulta();
		$dataTramites["tramitesConcluidos"] = $this->TramitesConsulta->tramitesConculuidos();
		$this->load->view('head',$data);
		$this->load->view('header');
		$this->load->view('slider');
		$this->load->view('content',$dataTramites);
		$this->load->view('footer');
		$this->load->view('scripts/js');
	}
}
