<section id="page-title" class="page-title-mini">
    <div class="container clearfix">
        <div class="col-md-4">
            <img src="<?= base_url(); ?>plantilla/images/img-tequis/logos.png" alt="">
        </div>
        <?php if($valid == 0):?>
        <div class="col-md-8">
            <h3 style="transform: translateY(20px);">Ingresa la Información para proceder con el pago.</h3>
        </div>
        <?php endif;?>
    </div>
</section><!-- #page-title end -->
<section>
    <div class="col-12 text-center">
        <span style="font-size: 4.5em; opacity: 0.35;"><i class="fas fa-laptop-house"></i></span>
        <h2 style="font-size: 2em;
            letter-spacing: 0.1em; color: inherit; font-weight: 300;
            line-height: 1.75em;
            text-transform: uppercase; 
            margin-bottom: auto; 
            color: #7c8081;">Pago de <strong style="font-weight: 400;">predial</strong></h2>
    </div>
    <?php if($valid == 0):?>
    <center><p>La información solicitada a continuación es requerida para proceder con el pago</p></center>
    <center><p><strong>*Nota: Esta Información no se almacenara*</strong></p></center>
    <?php elseif($valid == 1):?>
        <center><p>SE MOSTRARA LA RESPUESTA DEL PAGO</p></center>
        <center><h2><strong><?=$status?></strong></h2></center>
    <?php endif;?>
</section>
<?php if($valid == 0):?>
<section id="content">
    <div class="container">
        <div class="content-wrap" style="padding: 20px 0;">
            <div class="row">
                <div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-lg-6">
                        <form method="#" action="#">
                        <input type="hidden" class="form-control" id="txtclave" name="txtclave" value="<?=$clave?>" required>                        
                            <div class="form-group">
                                <label for="txtnombre">Nombre</label>
                                <input type="text" class="form-control" id="txtnombre" name="txtnombre" required placeholder="Ingresa tu nombre">
                            </div>
                            <div class="form-group">
                                <label for="txtApellido">Apellidos</label>
                                <input type="text" class="form-control" id="txtApellido" name="txtApellido" required placeholder="Ingresa tus Apellidos">
                            </div>
                            <div class="form-group">
                                <label for="txtcorreo">Correo Electrónico</label>
                                <input type="email" class="form-control" id="txtcorreo" name="txtcorreo" required placeholder="Ingresa tu Correo">
                            </div>
                            <div class="form-group">
                                <label for="txtnumber">Celular</label>
                                <input type="text" class="form-control" id="txtnumber" name="txtnumber" required placeholder="Ingresa tu Numero">
                            </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                <label for="txtmoneda">MSI</label>
                                </div>
                                <div class="col-md-3" >
                                    <input class="form-control cbxMSI" type="checkbox" id="cbxMSI" name="cbxMSI">
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <div class="col-md-12">
                                <center><input type="button" class="button button-large" name="btnpagobanco" value="PAGAR" onClick="generarCobro()"></input></center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php elseif($valid == 1):?>
    <section id="content">
    <div class="container">
        <div class="content-wrap" style="padding: 20px 0;">
            <div class="row">
            <?php if($frm==1):?>
                <center><h4><?=$status?></h4>
                    <B>CLICK EN EL BOTON E IMPRIME TU RECIBO</B>
                    <br>
                    <a name="doc" id="doc" value="" class="btn btn-primary" onclick="descargar(<?=$id?>)">
                    <i class="fa fa-download" aria-hidden="true"></i> Imprimir</a>
                </center>
                <?php elseif($frm==0):?>
                <center><a class="btn btn-primary" href="<?=base_url()?>">Volver</a></center>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>
<?php endif;?>