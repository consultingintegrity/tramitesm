
<style>
 .alert {
     padding: 20px;
     background-color: #f44336;
     color: white;
 }
 .yes {
     padding: 20px;
     background-color: #008000;
     color: white;
 }
 .closebtn {
     margin-left: 15px;
     color: white;
     font-weight: bold;
     float: right;
     font-size: 22px;
     line-height: 20px;
     cursor: pointer;
     transition: 0.3s;
 }

 .closebtn:hover {
     color: black;
 }
</style>
<?php if (!empty($this->session->flashdata('error'))): ?>
 <div class="alert">
   <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
   <strong><?=$this->session->flashdata('error')?></strong>
 </div>
 <?php endif?>
<?php if (!empty($this->session->flashdata('valid'))): ?>
 <div class="yes">
   <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
   <strong><?=$this->session->flashdata('valid')?></strong>
 </div>
 <?php endif?>

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">
  <div class="container clearfix">
    <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
  </div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
  <div class="content-wrap">
    <div class="container clearfix">
      <div class="row">
          <div class="col-lg-12">
              <div class="col-md-12">
                <h3>Verifica que la información cargada de la <b>Base de datos</b> sea la correcta.</h3>
                <a onClick="check(3)" class="btn btn-primary"><i class="fa fa-check-circle-o"></i> Correcto</a>
                <a onClick="check(2)" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Cargar Nuevamente</a>
              </div>
              <p><b>Se mostraran 30 registros cargados con toda la inforción para validar que todos los campos esten correctos y la información no se haya dañado al momento de cargarla.</b></p>
              <div class="table-responsive table--no-card m-b-40">
                <table class="tablePredial table-borderless table-striped table-earning" id="tablePredial">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>LineaCaptura</th>
                      <th>Clave Catastral</th>
                      <th>Tipo Contribución</th>
                      <th>Contribuyente</th>
                      <th>Ubicación</th>
                      <th>N°Ext</th>
                      <th>letra</th>
                      <th>N° Int</th>
                      <th>Colonia</th>
                      <th>Año iniAdeudo</th>
                      <th>Bimestre iniAdeudo</th>
                      <th>Año finAdeudo</th>
                      <th>Bimestre finAdeudo</th>
                      <th>Rezago AñosAnteriores</th>
                      <th>Rezago Añotranscurre</th>
                      <th>Impuesto Año</th>
                      <th>Adicional</th>
                      <th>Actualización</th>
                      <th>Recargo</th>
                      <th>ReqGastoEjecución</th>
                      <th>embargoGastoEjecución</th>
                      <th>Multa</th>
                      <th>Descuento</th>
                      <th>Total</th>
                      <th>FechaGeneración</th>
                      <th>VigenciaPago</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php foreach ($prediales as $predial):?>
                      <tr>
                        <td><?=$predial->id ?></td>
                        <td><?=$predial->lineaCaptura ?></td>
                        <td><?=$predial->ClaveCatastral ?></td>
                        <td><?=$predial->TipoContribucion ?></td>
                        <td><?=$predial->nombreContribuyente ?></td>
                        <td><?=$predial->ubicacionPredio ?></td>
                        <td><?=$predial->numeroExterior ?></td> 
                        <td><?=$predial->letra ?></td>
                        <td><?=$predial->numeroInterior ?></td>
                        <td><?=$predial->coloniaPredio ?></td>
                        <td><?=$predial->anoInicialAdeudo ?></td>
                        <td><?=$predial->bimestreInicialAdeudo ?></td>
                        <td><?=$predial->anofinalAdeudo ?></td>
                        <td><?=$predial->bimestrefinalAdeudo ?></td>
                        <td><?="$".number_format ($predial->rezagoAnosAnteriores,2) ?></td>
                        <td><?="$".number_format ($predial->rezagoAnoTranscurre,2) ?></td>
                        <td><?="$".number_format ($predial->impuestoAno,2) ?></td>
                        <td><?="$".number_format ($predial->adicional,2) ?></td>
                        <td><?="$".number_format ($predial->actualización,2) ?></td>
                        <td><?="$".number_format ($predial->Recargo,2) ?></td>
                        <td><?="$".number_format ($predial->requerimientoGastoEjecucion,2) ?></td>
                        <td><?="$".number_format ($predial->embargoGastosEjecucion,2) ?></td>
                        <td><?="$".number_format ($predial->multa,2) ?></td>
                        <td><?="$".number_format ($predial->descuento,2) ?></td>
                        <td><?="$".number_format ($predial->total,2) ?></td>
                        <td><?=$predial->fechaGeneracion ?></td>
                        <td><?=$predial->vigenciaPago ?></td>
                    <?php endforeach; ?>
                  </tbody>
                  </table>
              </div>
          </div>      
          <div class="line"></div>
    </div>
  </div>
</section><!-- #content end -->

<style>
td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}
</style>
