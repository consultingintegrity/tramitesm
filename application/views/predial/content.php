<!-- Content
============================================= -->
<section id="page-title" class="page-title-mini">
    <div class="container clearfix">
        <div class="col-md-4">
            <img src="<?= base_url(); ?>plantilla/images/img-tequis/logos.png" alt="">
        </div>
        <div class="col-md-8">
            <h3 style="transform: translateY(20px);">Consulta la información de tu impuesto predial.</h3>
        </div>
    </div>
</section><!-- #page-title end -->
<section>
    <div class="col-12 text-center">
        <span style="font-size: 4.5em; opacity: 0.35;"><i class="fas fa-laptop-house"></i></span>
        <h2 style="font-size: 2em;
            letter-spacing: 0.1em; color: inherit; font-weight: 300;
            line-height: 1.75em;
            text-transform: uppercase; 
            margin-bottom: auto; 
            color: #7c8081;">Pago de <strong style="font-weight: 400;">predial</strong></h2>
    </div>
</section>
<section id="content">
    <div class="container">
        <div class="content-wrap" style="padding: 20px 0;">
            <div class="row">
                <div>
                    <div class="col-md-3">
                    </div>
                    <div class="col-lg-6">
                        <form method="POST" action="<?= base_url() ?>Predial">
                            <div class="form-group">
                                <label for="txtClaveCatastral">CLAVE CATASTRAL</label>
                                <input type="text" class="form-control" id="txtClaveCatastral" Maxsize="15" onkeyup="maskClaveCtastral(this.id);" name="txtClaveCatastral" required placeholder="Ingresa la clave Catastral">
                            </div>
                            <center><input type="button" class="button button-mini" name="btnConsulta" value="CONSULTAR" onClick="obtenerInformacionPredio()"></input></center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <!--Siguente pantalla-->
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-warning col-md-12" role="alert">
                    <h5><strong>Clave Catastral:<input class="form-control" id="lblClaveC" name="lblClaveC" disabled /></strong></h5>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-12 col-sm-12" style="overflow: auto;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">NOMBRE DEL PROPIETARIO</th>
                                <th scope="col">DIRECCIÓN DEL PREDIO</th>
                                <th scope="col">FECHA DE VIGENCIA</th>
                                <th scope="col">DESGLOSE</th>
                                <th scope="col">IMPORTE TOTAL A PAGAR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td id="nombrePropietario"></td>
                                <td id="direccionPago"></td>
                                <td id="fechaV"></td>
                                <td id="desglose"></td>
                                <td class="col-md-4"><strong id="importe" style="color: black;"></strong></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 text-center">
                <h4>Metodo de pago</h4>
                    <input type="button" class="button button-mini" name="btnGenerar" id="btnGenerar" value="Linea de Captura" onClick="generarPago()"></input>
                    <input type="button" class="button button-mini" name="btnPagar" id="btnPagar" value="PAGO EN LINEA" onclick="botonPagar()"></input>
                    <button class="btn btn-secondary"><a type="button" href="<?= base_url() ?>">Regresar</a></button>
                </div>
                <div class="col-md-12"><br>
                    <div class="alert alert-warning" role="alert">
                        <center><strong>¡AVISO IMPORTANTE!</strong> Favor de conservar el talón de pago que emite el banco para
                            cualquier aclaración.</center>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
</section><!-- #content end -->
<div class="line"></div>
<!-- Modal -->
<div class="modal fade" id="modalPagar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Pagar en linea</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="transform: translateY(-20px);">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
            <div class="col-12 text-center">
                     <p>Selecciona proveedor de pago</p>
                     <form action="<?=base_url()?>Paybanco" method="POST">
                     <input type="hidden" value="" name="clavemodal" id="clavemodal"></input>
                    <button type="submit" class="btn btn-secondary">
                        <img src="<?= base_url(); ?>plantilla/images/1280px-BBVA_2019.png" width="100">
                    </button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>