
<body>
<div class="contenido">
	<header> <img src="<?=base_url()?>plantilla/images/header.png" width="800" height="auto"></header>
	<div class="texto">
	<?php if($val == 1):?>
		<h3><strong>POLITICA DE SERVICIOS, TÉRMINOS Y CONDICIONES</strong></h3>
	  <p>
			Estimado usuario, esta página establece los &quot;Términos y Condiciones&quot; que regulan el uso del
			Portal denominado Plataforma de trámites del Municipio con dirección
			electrónica <a href="<?=base_url()?>"><?=base_url()?></a> (en adelante, el &quot;Portal&quot;). Le
			pedimos que lea cuidadosamente los siguientes términos y condiciones de uso, ya que por el
			simple uso o acceso a cualquiera de las páginas que integran el Portal se entenderá que
			acepta y acuerda obligarse en los términos y condiciones aquí descritos. Si no acepta estos
			Términos y Condiciones, no podrá tener acceso al mismo. La información ingresada a este
			Portal es para uso exclusivo del Municipio, a fin de facilitar los trámites de
			carácter municipal para con los ciudadanos.<br>
		</p>
		<h3><strong>Uso de los Servicios</strong></h3>
		<p>
		A).	A fin de poder acceder a determinados Servicios y Trámites por Internet, es posible que se le solicite información personal, por ejemplo, datos identificativos e información de contacto, como parte del proceso de registro  y pago en el Servicio o Trámite o como parte del uso continuado de los Servicios o Trámites. Usted se compromete a que cualquier información de registro que facilite al Municipio de Jalpan de Serra Querétaro será precisa, correcta y actual.
			<br>
		B).	Usted acepta utilizar los Servicios y Trámites por Internet exclusivamente con los fines estipulados en las Condiciones y cualquier ley o regulación aplicable o cualquiera de las prácticas o directrices aceptadas en las jurisdicciones pertinentes, incluida cualquier ley relativa a la exportación de datos desde cualquier país pertinente.7
		<br>
		C).	Usted se compromete a no acceder ni a intentar acceder a ninguno de los Servicios o Trámites por Internet por ningún otro medio que no sea la interfaz provista por el Municipio. Se compromete en concreto a no acceder, o intentar acceder, a ninguno de los Servicios o Trámites por Internet utilizando medios automatizados, incluidos secuencias de comandos o rastreadores web.
		<br>
		D).	Asimismo, se compromete a no involucrarse en ninguna actividad que interfiera o que interrumpa los Servicios o Trámites por Internet, ni a los servidores y redes conectados a aquellos. 
		<br>
		E).	Usted se compromete a no reproducir, duplicar, copiar, vender, comercializar ni revender los, Archivos, información y Servicios o Trámites por Internet para ningún fin.
		<br>
		F).	Usted acepta que es el único responsable, y que el Muncipio de Jalpan de Serra, Querétaro renuncia a toda responsabilidad hacia usted o hacia cualquier tercero, del incumplimiento de cualquiera de sus obligaciones en virtud de las Condiciones, así como de las consecuencias, incluido cualquier pérdida o daño que pueda ocasionar al Municipio, derivado de dicho incumplimiento.
		</p>
		<h3><strong>Contraseñas y seguridad de la cuenta</strong></h3>
		<p>
		A).	Por l presente, reconoce y acepta que es usted el único responsable de mantener la confidencialidad de sus contraseñas asociadas a cualquiera de las cuentas que utiliza para acceder a los Servicios y Trámites por Internet del Municipio.
		<br>
		B).	En consecuencia, acepta que usted será el único responsable ante el Municipio de todas y cada una de las actividades que se desarrollen en su cuenta.
		<br>
		C).	4.3 Acepta notificar de inmediato a Municipio de Jalpan de Serra, Querétaro cualquier uso no autorizado de su contraseña o cuenta de que tenga conocimiento.
		</p>
		<?php elseif($val==2):?>
			<h3><strong>POLÍTICA DE PRIVACIDAD</strong></h3>
			<p>
			La presente Política de Privacidad establece los términos en que el Municipio de Jalpan de Serra usa y protege la información que es proporcionada por sus usuarios al momento de utilizar la aplicación web, portales o medios digitales y electrónicos.
			<br>
			El Municipio está comprometido con la seguridad de los datos de sus usuarios. 
			<br>
			Cuando se le solicite llenar o validar los campos de información personal con la cual usted pueda ser identificado, lo llevamos a cabo asegurando que sólo se utilizará de acuerdo con los términos de este documento. Sin embargo, esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos revisar constantemente esta página para asegurarse que está de acuerdo con dichos cambios.
			<br>
			</p>
			<h3><strong>Información que es recogida</strong></h3>
			<p>
			Nuestros canales digitales y electrónicos podrán recoger información personal, como son: Nombre completo, información de contacto e información demográfica. De igual forma cuando sea necesario podrá ser requerida información específica para procesar algún pago o servicios que se agreguen.
			<br>
			</p>
			<h3><strong>Uso de la información recogida</strong></h3>
			<p>
			Nuestraq canales digitales y electrónicos emplean la información con el fin de proporcionar el mejor servicio de cara al ciudadano, particularmente para mantener un registro de usuarios y mejorar nuestros servicios. 
			<br>
			El Municipio está comprometido con el compromiso de mantener su información segura. Usamos sistemas avanzados y los actualizamos constantemente para asegurarnos que no existan accesos no autorizados.
			<br>
			</p>
			<h3><strong>Enlaces a Terceros</strong></h3>
			<p>
			La aplicación pudiera contener enlaces a otros sitios que pudieran ser de su interés. Una vez que usted de clic en estos enlaces y abandone nuestra página, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los términos o privacidad ni de la protección de sus datos en esos otros sitios terceros. Dichos sitios están sujetos a sus propias políticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted está de acuerdo con estas.
			<br>
			</p>
			<h3><strong> Fundamento para el tratamiento de datos personales.</strong></h3>
			<p>
			“El Municipio trata los datos personales antes señalados con fundamento en los artículos 89, fracción XVI de la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados, 81, fracción XVI de la Ley de Protección de Datos Personales en Posesión de Sujetos Obligados del Estado de Querétaro, así como con los artículos 111 y 115 de la Ley de Transparencia y Acceso a la Información Pública del Estado de Querétaro
			<br>
			</p>
		<?php endif;?>
  </div>
	<br><br><br>
	<footer class="pie"><div align="center"><img src="<?=base_url()?>plantilla/images/img-tequis/logos.png" width="215" height="107"></div></footer>
</div>
</body>
</html>
