
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="promo promo-full promo-border header-stick bottommargin-lg">
                    <div class="container clearfix">
                        <h3>BIENVENIDO</h3>
                        <span>Revisa la descripción y requisitos de cada trámite y da clic en el siguiente botón para iniciar tu sesión.</span>
                        <!--lleva al Auth, poner el nombre del controlador cuando se requiera el inicio-->
                        <!--<a href="<?= base_url() ?>" class="button button-teal button-reveal button-xlarge button-rounded tright"><i class="icon-angle-right"></i><span>Entrar</span></a>-->
                    </div>
                </div>
                 <div class="container">
                   <!--acciones para sistema-->
                   <div class="col-md-offset">
                        <a class="button button-teal button-reveal button-rounded tright" href="<?=base_url()?>Predial"><i class="fa fa-leanpub"></i>Pago de Predial</a>
                    </div>
                    <br>
                   <div class="row">
                <!-- foreach para mostrar todos los tramites-->
            <?php foreach($tramites as $T){ ?>
                <div class="col-md-12">
                        <div class="feature-box fbox-light fbox-effect">
                            <div class="fbox-icon">
                                <i><img src="<?= base_url().$T->logo?>"></i>
                            </div>
                            <h3><?=$T->nombre?></h3>
                            <p><?=$T->descripcion?></p>
    <!--redirecciono a el desgloce espesifico del tramite-->
                            <a onclick="location.href ='<?= base_url()."DesgloceTramite/filtroTramite/".$T->id?>'" class="button button-teal button-mini">mas información</a>
                        </div>
                        <div class="line"></div>
                    </div>
                <?php } ?>
                    <script type="text/javascript">

                        jQuery(document).ready(function($) {

                            var ocClients = $("#oc-clients-full");

                            ocClients.owlCarousel({
                                margin: 30,
                                loop: true,
                                nav: false,
                                autoplay: true,
                                dots: false,
                                autoplayHoverPause: true,
                                responsive:{
                                    0:{ items:2 },
                                    480:{ items:3 },
                                    768:{ items:4 },
                                    992:{ items:5 },
                                    1200:{ items:6 }
                                }
                            });

                        });

                    </script>

                </div>
                <!--termina tramites-->

            </div>

        </div>

        </section><!-- #content end -->
