<!DOCTYPE html>
<html dir="ltr" lang="es">
<head>
 <meta charset="UTF-8">


    <!-- Stylesheets
    ============================================= -->
    <link rel="shortcut icon" href="<?= base_url()?>plantilla/images/img-tequis/logo.png" />
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>plantilla/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>plantilla/style.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>plantilla/css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>plantilla/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>plantilla/css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>plantilla/css/select2.min.css" type="text/css" />
    <!-- select 2 by juery -->
    <link rel="stylesheet" href="<?=base_url()?>plantilla/css/tequis.css" type="text/css" />
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>plantilla/js/DataTables/datatables.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <script src="<?=base_url()?>plantilla/js/sweetalert2.all.min.js"></script>
    <!--[if lt IE 9]>
    	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
      <script type="text/javascript" src="<?=base_url()?>plantilla/js/jquery-3.3.1.js"></script>

    <!-- External JavaScripts
    ============================================= -->
	<script type="text/javascript" src="<?=base_url()?>plantilla/js/jquery.js"></script>
	<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins.js"></script>


    <!-- Document Title
    ============================================= -->
	<title><?= $title ?></title>

</head>
