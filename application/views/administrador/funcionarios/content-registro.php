<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                  <strong><?=(isset($funcionario)) ? "Modificar" : "Registrar" ?></strong> Funcionario
              </div>
              <div class="card-body card-block">
                <form action="" method="post" name="<?=(isset($funcionario)) ? 'frmModificaFuncionario' : 'frmNuevoFuncionario' ?>" id="<?=(isset($funcionario)) ? 'frmModificaFuncionario' : 'frmNuevoFuncionario' ?>" class="form-horizontal">
                  <div class="card-title">
                      <h3 class="text-center title-2">Información Personal</h3>
                  </div>
                  <hr>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Nombre (s)</label>
                      </div>
                      <div class="col-12 col-md-4">
                        <input type="text" id="txtPrimerNom" name="txtPrimerNom" placeholder="Primer Nombre" class="form-control" value="<?=(isset($funcionario)) ?$funcionario->primer_nombre : ""?>" required>
                        <small class="form-text text-muted">Primer Nombre</small>
                      </div>
                      <div class="col-12 col-md-4">
                        <input type="text" id="txtSegundoNom" name="txtSegundoNom" placeholder="Segundo Nombre" class="form-control" value="<?=(isset($funcionario)) ?$funcionario->segundo_nombre :""?>">
                        <small class="form-text text-muted">Segundo Nombre</small>
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Apellido (s)</label>
                      </div>
                      <div class="col-12 col-md-4">
                        <input type="text" id="txtPrimerAp" name="txtPrimerAp" placeholder="Primer Apellido" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->primer_apellido : ""?>" required>
                        <small class="form-text text-muted">Primer Apellido</small>
                      </div>
                      <div class="col-12 col-md-4">
                        <input type="text" id="txtSegundoAp" name="txtSegundoAp" placeholder="Segundo Apellido" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->segundo_apellido : ""?>">
                        <small class="form-text text-muted">Segundo Apellido</small>
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label for="optGenero" class=" form-control-label">Género</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <select name="optGenero" id="optGenero" class="form-control" required>
                            <option value="0">Seleccione una opción</option>
                            <option value="M" <?=(isset($funcionario) && $funcionario->genero == "M") ? "selected" :""?>>Masculino</option>
                            <option value="F" <?=(isset($funcionario) && $funcionario->genero == "F") ? "selected" :""?>>Femenino</option>
                        </select>
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Fecha de Nacimiento</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="date" id="txtFecha" name="txtFecha" placeholder="Fecha de Nacimiento" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->fecha_nacimiento : ""?>" required>
                      </div>
                  </div>
                  <hr>
                  <div class="card-title">
                    <h3 class="text-center title-2">Información de Contacto</h3>
                  </div>
                  <hr>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Móvil (Personal)</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" id="txtMovil" name="txtMovil" placeholder="Móvil (Personal)" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->telefono : ""?>" >
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Teléfono de Oficina</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" id="txtTelOficina" name="txtTelOficina" placeholder="Telefono de Oficina" class="form-control" value="<?=(isset($funcionario))  ? $funcionario->telefono_oficina  : " "?>">
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Extensión</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" id="txtExtension" name="txtExtension" placeholder="Extensión" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->extension : ""?>">
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label for="email-input" class=" form-control-label">Correo Electrónico</label>
                      </div>
                      <div class="col-12 col-md-9">
                          <input type="email" id="txtCorreo" name="txtCorreo" placeholder="Correo Electrónico" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->correo_electronico :""?>" required>
                          <input type="hidden" id="txtCorreoActual" name="txtCorreoActual" placeholder="" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->correo_electronico :""?>" required>
                          <small class="help-block form-text">El correo electrónico será con el cual el funcionario ingrese a la plataforma</small>
                      </div>
                  </div>
                  <hr>
                  <div class="card-title">
                    <h3 class="text-center title-2">Información de ubicación del funcionario</h3>
                  </div>
                  <hr>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label for="optEstado" class=" form-control-label">Estado</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <select name="optMunicipio" id="optEstado" class="form-control">
                      <?php foreach ($estados as $dir): ?>
                          <?php if ($dir->id != 1): ?>
                            <option value="<?=$dir->id ?>" <?=(isset($funcionario) && $dir->id == $funcionario->id_direccion) ? "selected" : "" ?>><?=$dir->nombre ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </select>
                      </div>
                  </div>
                  <div class="row form-group">
                    <div class="col col-md-3">
                      <label for="optMunicipio" class=" form-control-label">Municipio</label>
                    </div>
                    <div class="col-12 col-md-9">
                    <select name="optMunicipio" id="optMunicipio" class="form-control">
                    <option value=""></option>

                    </select>
                    </div>
                    </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Calle</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" id="txtCalle" name="txtCalle" placeholder="Calle" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->calle : ""?>" required>
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Colonia</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" id="txtColonia" name="txtColonia" placeholder="Colonia" class="form-control" value="<?=(isset($funcionario))? $funcionario->colonia  : ""?>" required>
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Número Exterior</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" id="txtNumeroE" name="txtNumeroE" placeholder="Número Exterior" class="form-control"  value="<?=(isset($funcionario)) ? $funcionario->numero_exterior : ""?>"required>
                      </div>
                  </div>
                  <div class="row form-group">
                      <div class="col col-md-3">
                          <label class=" form-control-label">Número Interior</label>
                      </div>
                      <div class="col-12 col-md-9">
                        <input type="text" id="txtNumeroI" name="txtNumeroI" placeholder="Número Interior" class="form-control" value="<?=(isset($funcionario)) ? $funcionario->numero_interior : ""?>">
                      </div>
                  </div>
                  <hr>
                  <div class="card-title">
                    <h3 class="text-center title-2">Información de Acceso a La Plataforma</h3>
                  </div>
                  <hr>
                  <div class="row form-group">
                    <div class="col col-md-3">
                      <label for="optDireccion" class=" form-control-label">Dependencia</label>
                    </div>
                    <div class="col-12 col-md-9">
                      <select name="optDireccion" id="optDireccion" class="form-control">
                        <?php foreach ($direcciones as $dir): ?>
                          <?php if ($dir->id != 1): ?>
                            <option value="<?=$dir->id ?>" <?=(isset($funcionario) && $dir->id == $funcionario->id_direccion) ? "selected" : "" ?>><?=$dir->nombre ?></option>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col col-md-3">
                      <label for="optDependencia" class=" form-control-label">Dirección</label>
                    </div>
                    <div class="col-12 col-md-9">
                      <select name="optDependencia" id="optDependencia" class="form-control">
                      </select>
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col col-md-3">
                      <label for="optRol" class=" form-control-label">Rol</label>
                    </div>
                    <div class="col-12 col-md-9">
                      <select name="optRol" id="optRol" class="form-control">
                        <?php /*foreach ($rol as $r): */?>
                          <!--<option value="<?=$r->id ?>" <?=(isset($funcionario) && $r->id == $funcionario->id_rol) ? "selected"  : "" ?>><?=$r->nombre?></option>-->
                        <?php/* endforeach;*/ ?>
                      </select>
                    </div>
                  </div>
                  <?php if (!isset($funcionario)): ?>
                  <div class="row form-group">
                    <div class="col col-md-3">
                      <label for="password-input" class=" form-control-label">Contraseña</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="password" id="txtContra" name="txtContra" placeholder="Contraseña" class="form-control" required>
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col col-md-3">
                      <label for="password-input" class=" form-control-label">Confirmar Contraseña</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="password" id="txtConfirmarContra" name="txtConfirmarContra" placeholder="Confirmar Contraseña" class="form-control" required>
                        <small class="help-block form-text">Confirme la contraseña</small>
                    </div>
                  </div>
                  <?php else: ?>
                    <input type="hidden" id="funcionario" name="funcionario" value="<?=$funcionario->id_usuario ?>">
                    <input type="hidden" id="persona" name="persona" value="<?=$funcionario->id ?>">
                  <?php endif; ?>
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm" name="btnRegistrar" value="Registrar">
                        <i class="fa fa-dot-circle-o"></i> <?=(isset($funcionario)) ? "MODIFICAR" : "REGISTRAR" ?>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div>
    </div>
  </div>
</div>
