<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row m-t-30">
        <div class="col-md-12">
          <!-- DATA TABLE-->
          <div class="table-responsive m-b-40">
            <table class="table table-borderless table-data3">
                <thead>
                  <tr>
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th>Dependencia</th>
                    <th>Correo</th>
                    <th>Estatus</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                  <tbody>
                    <?php foreach ($funcionarios as $funcionario): ?>
                    <tr>
                      <?php $funcionario->imagen = ($funcionario->imagen != null) ? base_url() . $funcionario->imagen : (($funcionario->genero) == "M" ? base_url() . "plantilla/images/icons/user-man.png" : base_url() . "plantilla/images/icons/user-women.png")?>

                      <td><img src="<?=$funcionario->imagen;?>" width="40" height="40" class="rounded mx-auto d-block" alt="imagen de usuario"></td>
                      <td><?=$funcionario->primer_nombre?> <?=$funcionario->segundo_nombre?> <?=$funcionario->primer_apellido?> <?=$funcionario->segundo_apellido?></td>
                      <td><?=$funcionario->dependencia?></td>
                      <td><?=$funcionario->correo_electronico?></td>
                      <?php
                      $estatus = ($funcionario->status  == 1) ? "<label class='switch switch-text switch-success switch-pill'>
                                                                    <input type='checkbox' class='switch-input' checked='true'>
                                                                    <span data-on='Activo' data-off='Inactivo' class='switch-label'></span>
                                                                    <span class='switch-handle'></span>
                                                                    </label>"
                                                                    : "<label class='switch switch-text switch-danger switch-pill'>
                                                                    <input type='checkbox' class='switch-input' checked='true'>
                                                                    <span data-on='Inactivo' data-off='Activo' class='switch-label'></span>
                                                                    <span class='switch-handle'></span>
                                                                  </label>" ;
                        ?>
                      <td class="process"  onclick="location.href ='<?=base_url()?>Administrador/actualizaStatus/<?=$funcionario->id_usuario . "/" . $funcionario->status ?>'" ><?= $estatus ?> </td>
                      <td>
                        <div class="table-data-feature">
                          <?php if ($funcionario->status == 1): ?>
                            <button class="item" data-toggle="tooltip" data-placement="top" title="Editar" onclick="location.href ='<?=base_url()?>Administrador/index/editar/<?=$funcionario->id_usuario ?>'">
                                <i class="zmdi zmdi-edit"></i>
                            </button>
                            <button class="btnPwd" onclick="modalPwdFuncionario(<?=$funcionario->id_usuario?>);" title="Cambiar Contraseña">
                                <i class="fa fa-key fa-1x"></i>
                            </button>
                            &nbsp;&nbsp;&nbsp;
                            <button data-toggle="modal" class="btnPwd" title="Eliminar Funcionario" onclick="location.href ='<?=base_url()?>Administrador/eliminaFuncionario/<?=$funcionario->id_usuario ?>'" >
                                <i class="fa fa-trash fa-1x"></i>
                            </button>
                          <?php else: ?>
                           no disponible
                          <?php endif; ?>
                        </div>
                      </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
              </table>
          </div>
          <!-- END DATA TABLE-->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modal medium -->
<div class="modal fade" id="modalCambiarPwd" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="mediumModalLabel">Modificar Conraseña del Funcionario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body card-block">
            <form action="" method="post" class="form-horizontal" id="frmCambiaPwd" name="frmCambiaPwd">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="txtPwd" class=" form-control-label">Contraseña</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="password" id="txtPwd" name="txtPwd" placeholder="Nueva Contraseña" class="form-control" required>
                        <span class="help-block">Ingrese la nueva contraseña</span>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="txtConfirmPwd" class=" form-control-label">Confirmar Contraseña</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <input type="password" id="txtConfirmPwd" name="txtConfirmPwd" placeholder="Confirmar Contraseña" class="form-control" required>
                        <span class="help-block">Confirme la contraseña</span>
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Modificar</button>
  </form>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
<!-- end modal medium -->
