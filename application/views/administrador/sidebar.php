<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="<?=base_url()?>plantilla/images/img-tequis/logos.png" alt="Tequisquiapan" width="80%" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
            <li class="<?=($navBar != null && $navBar == 'Dashboard') ? 'active' : '' ?> has-sub">
                <a href="<?=base_url()?>Administrador">
                    <i class="fas fa-tachometer-alt"></i>Inicio</a>
            </li>
            <li class="<?=($navBar != null && $navBar == 'registro' || $navBar == 'consultar' || $navBar == 'editar') ? 'active' : '' ?> has-sub">
              <a class="js-arrow" href="#">
                <i class="fa fa-users"></i>Funcionarios</a>
              <ul class="list-unstyled navbar__sub-list js-sub-list">
                <li>
                  <a href="<?=base_url()?>Administrador/index/registro">Registrar</a>
                </li>
                <li>
                  <a href="<?=base_url()?>Administrador/index/consultar">Consultar</a>
                </li>
              </ul>
            </li>
            <!--ACTIVAR CUANDO HALLA TRAMITES-->
<!--            <li class="<?=($navBar != null && $navBar == 'Historial') ? 'active' : '' ?> has-sub">
              <a class="js-arrow" href="#">
                <i class="far fa-calendar-minus"></i>Historial</a>
              <ul class="list-unstyled navbar__sub-list js-sub-list">
                <li>
                  <a href="<?=base_url()?>Administrador/index/historial-solicitudes">Solicitudes</a>
                </li>
                <li>
                  <a href="<?=base_url()?>Administrador/index/historial-tramites">Estadisticas Generales de Trámites Realizados</a>
                </li>
                <li>
                  <a href="<?=base_url()?>Administrador/index/historial-tramites-licencia-construccion">Estadisticas de Trámites Licencia de Construcción</a>
                </li>
              </li>
                <a href="<?=base_url()?>Administrador/index/historial-tramites-licencia-dictamen-uso-suelo">Estadisticas de Trámites Dictamen Uso de Suelo</a>
              </li>
              </ul>
            </li>
            <li class="<?=($navBar != null && $navBar == 'seguimiento-soilcitud') ? 'active' : '' ?> has-sub">
              <a href="<?=base_url()?>Administrador/index/seguimiento-soilcitud"><i class="fas fa-eye"></i>Seguimiento Solicitudes</a>
            </li>
            -->
            <li class="has-sub">
            <?php if($navBar == 'Dashboard'): ?>
              <i class="fa fa-area-chart"></i> Total de Visitas <br><center><?=$Visitas_total?></center>
            <?php endif; ?>
            </li>
            <!--se agrega el boton para historial de predial-->
            <li class="<?=($navBar != null && $navBar == 'historial-predial') ? 'active' : '' ?> has-sub">
                <a href="<?=base_url()?>Administrador/index/historial-predial"><i class="fa fa-map" aria-hidden="true"></i>Historial de Predial</a>
            </li>
            </ul>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR-->
