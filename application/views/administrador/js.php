<!-- Jquery JS-->
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/wow/wow.min.js"></script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url()?>plantilla/adminStyles/vendor/select2/select2.min.js">
    </script>

    <!-- Select 2 by jQuery -->
    <script type="text/javascript" src="<?=base_url()?>plantilla/js/select2.min.js"></script>
    <!-- jQuery MASK -->
    <script type="text/javascript" src="<?=base_url()?>plantilla/js/jquery.mask.js"></script>
    <!-- TimePicker -->
    <script type="text/javascript" src="<?=base_url()?>plantilla/js/jquery.timepicker.min.js"></script>
    <link type="text/css" href="<?=base_url()?>plantilla/js/jquery.timepicker.min.css" rel="stylesheet"/>

    <!-- Main JS-->
    <script src="<?=base_url()?>plantilla/adminStyles/js/main.js"></script>
    <!--DataTables-->
    <script type="text/javascript" src="<?=base_url()?>plantilla/js/DataTables/datatables.min.js"></script>
    <!--buttons DataTables-->
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
</body>

</html>
<!-- end document-->
