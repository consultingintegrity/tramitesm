<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <h2 class="title-1">Historial de Licencias de Construcción</h2>
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
              <div class="table-responsive table--no-card m-b-40">
                <table class="table table-borderless table-striped table-earning">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Licencia</th>
                      <th>Tipo</th>
                      <th>Total de Metros Construidos</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 0; foreach ($infoSolicitud as $solicitud):?>
                      <tr>
                        <td><?=$solicitud->id ?></td>
                        <td><?=$solicitud->nombre ?></td>
                        <td><?=$solicitud->tipo ?></td>

                        <?php foreach (json_decode($solicitud->informacion,1) as $key): ?>
                          <?php if (!empty($key)): ?>
                            <?php if (isset($key["id_form"]) && $key["id_form"] == 35): ?>
                              <?php foreach ($key["content"] as $val): ?>
                                <?php if ($val["field"] == "txtMConstruidos"): ?>
                                  <td><?= number_format($val["value"]); ?></td>
                                <?php endif; ?>
                              <?php endforeach; ?>
                            <?php endif; ?>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </tr>
                    <?php endforeach; ; ?>
                  </tbody>
                  </table>
              </div>
          </div>

            <div class="col-lg-12">
                <div class="au-card m-b-30">
                    <div class="au-card-inner">
                        <h3 class="title-2 m-b-40">Estadística Adicional</h3>
                        <p>La siguiente gráfica describe cuantas solicitudes del total que se muestra en la tabla superior cuentan con cada descripción de estadistica adicional que el ciudadano llenó al momento de generar la solicitud del trámite de licencia de construcción.</p>
                        <br>
                        <canvas id="estadisticasConstruccionChar"</canvas>
                    </div>
                </div>
        </div>
      </div>
    </div>
  </div>
