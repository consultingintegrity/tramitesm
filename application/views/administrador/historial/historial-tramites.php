<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <h2 class="title-1">Trámites Solicitados</h2>
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
              <div class="au-card m-b-30">
                  <div class="au-card-inner">
                      <h3 class="title-2 m-b-40">Trámites Realizados</h3>
                      <p>Al colocar el cursor sobre un trámite en la gráfica de barras, mostrará el nombre y la cantidad de veces realizado dicho trámite.</p>
                        <canvas id="tramitesSolicitdosChar"</canvas>
                  </div>
              </div>
          </div>
            <div class="col-lg-12">
                <div class="au-card m-b-30">
                    <div class="au-card-inner">
                        <h3 class="title-2 m-b-40">Ingresos</h3>
                        <p>Al colocar el cursor sobre un trámite en la gráfica de pasteles, mostrara el nombre y la cantidad generada por dicho trámite.</p>
                          <canvas id="ingresosChar"</canvas>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
