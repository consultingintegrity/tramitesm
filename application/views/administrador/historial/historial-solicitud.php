<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <h2 class="title-1">Historial de Todas las solicitudes</h2>
            <br>
          </div>
          <div class="col-md-3 col-lg-3">
            <div class="statistic__item">
              <h2 class="number"><?=number_format($solicitudes_proceso) ?></h2>
              <span class="desc">Solicitudes En Revisión</span>
              <div class="icon">
                  <i class="zmdi zmdi-eye"></i>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-lg-3">
            <div class="statistic__item">
              <h2 class="number"><?=number_format($solicitudes_terminadas) ?></h2>
              <span class="desc">Solicitudes Terminadas</span>
              <div class="icon">
                  <i class="zmdi zmdi-eye-off"></i>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-lg-3">
            <div class="statistic__item">
                    <h2 class="number"><?=$solicitudes_canceladas ?></h2>
                    <span class="desc">Solicitudes Canceladas</span>
                    <div class="icon">
                        <i class="zmdi zmdi-close"></i>
                    </div>
                </div>
          </div>
          <div class="col-md-3 col-lg-3">
            <div class="statistic__item">
                    <h2 class="number"><?=$solicitudes_caja ?></h2>
                    <span class="desc">Solicitudes En Caja</span>
                    <div class="icon">
                        <i class="zmdi zmdi-money-box"></i>
                    </div>
                </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
              <div class="table-responsive table--no-card m-b-40">
                <table class="table table-borderless table-striped table-earning">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Trámite</th>
                      <th>Ciudadano</th>
                      <th>Dependencia</th>
                      <th>Estatus</th>
                      <th>Fecha Termino</th>
                      <th>Seguimiento</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($solicitudes as $solicitud): ?>
                      <?php if ($solicitud->estatus != 0 || $solicitud->estatus != 1): ?>
                      <tr>
                        <td><?=$solicitud->id ?></td>
                        <td><?=$solicitud->tramite; ?> <?=($solicitud->giro != null) ? "($solicitud->giro)" : "" ?> <?=($solicitud->tipoConstruccion != null) ? "($solicitud->tipoConstruccion)" :"" ?></td>
                        <td><?=$solicitud->primer_nombreC  .  " " . $solicitud->primer_apellidoC ?></td>
                        <td><?=$solicitud->dependencia ?></td>

                        <?php switch ($solicitud->estatus) {//validamos el estatus del trámite
                          case '4':
                            $solicitud->estatus = "Revisión";
                            break;
                          case '3':
                            $solicitud->estatus = "Pago";
                            break;
                          case '2':
                            $solicitud->estatus = "Entrega en Ventanilla";
                            break;
                          case '5':
                            $solicitud->estatus = "Correcciones";
                            break;
                          case '1':
                            $solicitud->estatus = "<span class='status--process'>Terminado</span>";
                            break;
                          case '0':
                            $solicitud->estatus = "<span class='status--denied' >Cancelado</span>";
                            break;
                          default:
                            // code...
                            break;
                        } ?>
                        <td><?=$solicitud->estatus ?></td>
                        <?php $fecha_fin = ($solicitud->estatus != "<span class='status--process'>Terminado</span>")?"" :$solicitud->fecha_fin;?>
                        <td><?=$fecha_fin?></td>
                        <td><button  title="Ver Seguimiento De La Solicitud" name="btnSeguimientoSolicitud" id="btnSeguimientoSolicitud" class="btn btn-primary" onclick="location.href='<?=base_url()?>Administrador/index/solicitud-expediente/<?=$solicitud->id?>'"><i class="fas fa-external-link-square-alt"></i></button></td>
                      </tr>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </tbody>
                  </table>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>
