        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1><?= $title ?></h1>
            </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                	<div class="promo promo-full promo-border header-stick bottommargin-lg">
                   		<span><?= $descripcion ?></span>
	               </div>
                 	<div class="continer">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Nombre del Trámite</th>
                                <th>Fecha</th>
                                <th>Estatus</th>
                                <th>Imprime tu orden de pago</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($tramite)){
                                    foreach ($tramite as $tramites) { ?>
                                      <?php
                                        switch ( $tramites->estatus ) {
                                          case 0:
                                            $tramites->estatus = "Cancelado";
                                            break;
                                          case 1:
                                            $tramites->estatus = "Terminado";
                                            break;
                                          case 2:
                                            $tramites->estatus = "Recoger Trámite";
                                            break;
                                          case 4:
                                            $tramites->estatus = "Revisión";
                                            break;
                                          case 5:
                                            $tramites->estatus = "Correcciones";
                                            break;
                                          case 3:
                                            $tramites->estatus = "Pagar";
                                            break;
                                          default:
                                            break;
                                        }
                                      ?>
                                    <tr>
                                        <td><?= $tramites->nombre; ?> <?=($tramites->giro != null) ? "($tramites->giro)" : "" ?> <?=($tramites->tipoConstruccion != null) ? "($tramites->tipoConstruccion)" :"" ?></td>
                                        <td><?= $tramites->fecha_inicio; ?></td>
                                        <?=$td1=''; ?>

                                        <?php switch ($tramites->estatus ) {
                                          case "Cancelado":
                                            $td = 'Cancelado';
                                            $tramites->estatus = "Cancelado";
                                            break;
                                          case "Terminado":
                                            $td='Terminado';
                                            break;
                                          case "Recoger Trámite":
                                            $td='Recoge tu trámite';
                                            break;
                                          case "Revisión":
                                            $td='En revisión';
                                            break;
                                          case "Correcciones":
                                            $tramites->estatus = "Correcciones";
                                            break;
                                          case "Pagar":
                                          if ($tramites->costo <= 0) {
                                            $td='Aún no se genera tu recibo de pago';
                                          }
                                          else {
                                            $td='En espera de pago';
                                            $td1='<a class="btn btn-default" href="'.base_url().'Ciudadano/comprobante/'.$tramites->id.'" target="_blank" title="Imprimir"><i class="fa fa-print" aria-hidden="true"></i></a>';
                                          }
                                            break;
                                          default:
                                            break;
                                        }
                                      ?>
                                        <td><?=$td;?></td>
                                        <td><?=$td1;?></td>

                                    </tr>
                                <?php }?>
                        <?php
                        }else{?>
                            <td>
                                         <div class="line"></div>
                                    <td>No hay trámites para mostrar</td>
                            </td>
                        <?php }?>
                            </tbody>
                        </table>
                    </div>
						<div class="line"></div>
                   </div>
                </div>
        </section><!-- #content end -->
