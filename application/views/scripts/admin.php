<script type="text/javascript">
  var id_usuario = 0;
  var correo_funcionario = "";

    //WS Services
    var Broadcast = {
          BROADCAST_URL : "127.0.0.1",
          BROADCAST_PORT : 2000,
          };
    //var conn = new Connection2(Broadcast.BROADCAST_URL+":"+Broadcast.BROADCAST_PORT);

  //$(document).ready( function () {

    $('.table').DataTable({
      "language":espaniol()
      });
  //  });//ready document

  $( "form" ).submit(function( event ) {
    event.preventDefault();
    frm = $(this).attr("id");
    switch (frm){
      case "frmNuevoFuncionario":
        registrarFuncionario(frm);
      break;
      case "frmModificaFuncionario":
        modificarFuncionario(frm);
      break;
      case "frmCambiaPwd":
        cambiarContrasenia(id_usuario,frm);
      break;
      case "frmCorreoFuncionario":
        correoFuncionrio(correo_funcionario,frm);
      break;
      case "frmPerfilAdmin":
        modificarAdmin(frm);
        break;
      case "frmPwdAdmin":
          id_usuario = <?=$this->session->userdata("id_usuario") ?>;
        cambiarContrasenia(id_usuario,frm);
        break;
      case "frmImagenPerfil":
        cambiarImagen();
        break;
    }//switch
  });

  $("button").on("click", function(event) {
    btnClass = $(this).attr("class");
    switch(btnClass){
      case "btn btn-primary contacto":
      correo_funcionario = $(this).val();
      $('#modalCorreoFuncionario').modal("show");
      break;
    }//switch
  });//click


  function registrarFuncionario(id_form){
    $.ajax({
      url: '<?= base_url() ?>Administrador/registrar_fun',
      type: "POST",
      dataType: "json",
      data: $("#"+id_form).serialize(),
      success:function(data) {
        if (data.status == 200) {
          successAlert(data.title,data.msg);
          $('#'+id_form).trigger("reset");
        }//if
        else if (data.status == 400) {
          errorAlert(data.title,data.msg);
        }
      }//success
    });//ajax
  }//registrarFuncionario

  function modificarFuncionario(id_form){
    $.ajax({
      url: '<?= base_url() ?>Administrador/modificar_fun',
      type: "POST",
      dataType: "json",
      data: $("#"+id_form).serialize(),
      success:function(data) {
        if (data.status == 200) {
          $("#txtCorreoActual").val($("#txtCorreo").val());
          successAlert(data.title,data.msg);
        }//if
        else if (data.status == 400) {
          errorAlert(data.title,data.msg);
        }
      }//success
    });//ajax
  }//registrarFuncionario

  function espaniol(){
    var cajaEspaniol = {
      "sProcessing":     "Procesando...",
      "sLengthMenu":     "Mostrar _MENU_ registros",
      "sZeroRecords":    "No se encontraron resultados",
      "sEmptyTable":     "Ningún dato disponible en esta tabla",
      "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
      "sInfoPostFix":    "",
      "sSearch":         "Buscar:",
      "sUrl":            "",
      "sInfoThousands":  ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst":    "Primero",
          "sLast":     "Último",
          "sNext":     "Siguiente",
          "sPrevious": "Anterior"
      },
      "oAria": {
          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }
    };//cajaEspaniol

    return cajaEspaniol;
  }//espaniol

  function cambiarContrasenia(id_usuario,id_form){
    $.ajax({
      url: '<?= base_url() ?>Administrador/cambiarContrasenia',
      type: "POST",
      dataType: "json",
      data: { "txtPwd" : $("#txtPwd").val(),
              "txtConfirmPwd" : $("#txtConfirmPwd").val(),
              "id_usuario" : id_usuario
            },
      success:function(data) {
        if (data.status == 200) {
          $('#modalCambiarPwd').modal("hide");
          $('#'+id_form).trigger("reset");
          successAlert(data.title,data.msg);
        }//if
        else if (data.status == 400) {
          errorAlert(data.title,data.msg);
        }
      }//success
    });//ajax
  }//cambiarcontrasenia

  function correoFuncionrio(correoFuncionario,id_form){
      $.ajax({
        url: '<?= base_url() ?>Administrador/enviarCorreo',
        type: "POST",
        dataType: "json",
        data: { "CorreoFuncionario" : correoFuncionario,
                "txtmsj" : $("#txtMensaje").val()
              },
        success:function(data) {
          if (data.status == 200) {
            $('#modalCorreoFuncionario').modal("hide");
            $('#'+id_form).trigger("reset");
            successAlert(data.title,data.msg);
          }//if
          else if (data.status == 400) {
            errorAlert(data.title,data.msg);
          }
        }//success
      });//ajax
    }//correoFuncionrio

  function modificarAdmin(id_form){
    $.ajax({
      url: '<?= base_url() ?>Administrador/updateAdministrador',
      type: "POST",
      dataType: "json",
      data: $("#"+id_form).serialize(),
      success:function(data) {
        if (data.status == 200) {
          $("#txtCorreoActual").val($("#txtCorreo").val());
          $(".js-acc-btn").empty();
          $(".js-acc-btn").append($("#txtPrimerNom").val() + " " + $("#txtPrimerAp").val());
          $(".email").empty();
          $(".email").append($("#txtCorreo").val());
          successAlert(data.title,data.msg);
        }//if
        else if (data.status == 400) {
          errorAlert(data.title,data.msg);
        }
      }//success
    });//ajax
  }//registrarFuncionario

  function cambiarImagen(){
    var formData = new FormData($("#frmImagenPerfil")[0]);
    formData.append('txtImg', $('input[type=file]')[0].files[0]);
    jQuery.ajax({
          url: "<?=base_url()?>Administrador/imagen",
          enctype: 'multipart/form-data',
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          type: 'POST',
          dataType: 'json',
          success: function(jsonResponse){
            if (jsonResponse.status == 200) {
              $(".img-user").attr("src","");
              $(".img-user").attr("src",jsonResponse.ruta);
              successAlert(jsonResponse.title,jsonResponse.msg);
            }//if
            else{
              errorAlert(jsonResponse.title,jsonResponse.msg);
            }//else
          }//success
      });
  }//cambiarImagen

  function modalPwdFuncionario(id){
    $('#modalCambiarPwd').modal("show");
    id_usuario =  id;
  }//modalCambiarPwd

  $('.tablaPredio').DataTable({
      "language":espaniol(),
            dom : "Bftip",
            buttons : [
                'copyHtml5',
                 {
                    extend:    'excelHtml5',
                    text:      '<i class="fa fa-file-excel-o"></i>',
                    titleAttr: 'Excel'
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="fa fa-file-pdf-o"></i>',
                    titleAttr: 'PDF',
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                }
            ]
      });

</script>
