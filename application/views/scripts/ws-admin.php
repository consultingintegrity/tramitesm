<script type="text/javascript">

var Connection2 = (function(){

  function Connection2(url) {

      this.open = false;

      this.socket = new WebSocket("ws://" + url);
      this.setupConnectionEvents();
    }

  Connection2.prototype = {
    setupConnectionEvents : function () {
          var self = this;

          self.socket.onopen = function(evt) { self.connectionOpen(evt); };
          self.socket.onmessage = function(evt) { self.connectionMessage(evt); };
          self.socket.onclose = function(evt) { self.connectionClose(evt); };
      },

      connectionOpen : function(evt){
          this.open = true;
          this.addSystemMessage("Connected");
    },
      connectionMessage : function(evt){
        var data = JSON.parse(evt.data);
        $("#solicitudes_proceso").text(data.msg.msg.solicitudes_proceso);
        $("#soilcitudes_terminadas").text(data.msg.msg.solicitudes_terminadas);
        $("#solicitudes_canceladas").text(data.msg.msg.solicitudes_canceladas);
        $('.table').DataTable().destroy();
        $("#tablaGralSolicitudes").empty();
        $("#tablaGralFuncionariosSolicitudes").empty();
        //$('.table').DataTable().clear();
        //each Tabla
        $.each(data.msg.msg.solicitudes, function(i, item) {


          var fechaInicio = new Date(item.fecha_inicio).getTime();
          var fechaFin    = new Date(item.fecha_fin).getTime();
          var diff = fechaFin - fechaInicio;
          var tramite = (item.giro != null) ? item.tramite + " ("+item.giro+")" : (item.tipoConstruccion != null) ? item.tramite + " ("+item.tipoConstruccion+") " : item.tramite;

          estatus = "";
          switch (item.estatus){
            case '4':
            estatus = "Revisión";
            break;
          case '3':
            estatus = "Pago";
            break;
          case '2':
            estatus = "Entrega en Ventanilla";
            break;
          case '5':
            estatus = "Correcciones";
            break;
          case '1':
            estatus = "<span class='status--process'>Terminado</span>";
            break;
          case '0':
            estatus = "<span class='status--denied' >Cancelado</span>";
            break;
          }
          if (estatus != "<span class='status--process'>Terminado</span>" && estatus !="<span class='status--denied' >Cancelado</span>") {


            $("#tablaGralFuncionariosSolicitudes").append('<tr>'
                +'<td>'+item.id+'</td>'
                +'<td>'+item.primer_nombreF +" "+item.primer_apellidoF+'</td>'
                +'<td>'+item.dependencia+'</td>'
                +'<td>'+tramite+'</td>'
                +'</tr>');
          }
          $("#tablaGralSolicitudes").append('<tr>'
            +'<td>'+item.id+'</td>'
            +'<td>'+item.fecha_inicio+'</td>'
            +'<td>'+item.fecha_fin+'</td>'
            +'<td>'+tramite+'</td>'
            +'<td>'+estatus+'</td>'
            +'<td>+'+parseInt(diff/(1000*60*60*24))+' día(s)</td>'
            +'</tr>');

        });//each

        $(".au-skill-container").empty();

        $.each(data.msg.msg.progreso_solicitudes, function(i, item) {
          var tramite = (item.giro != null) ?  " ("+item.giro+")" : (item.tipoConstruccion != null) ? " ("+item.tipoConstruccion+")" : "";

          if (item.estatus != 0 && item.estatus != 1) {
            $(".au-skill-container").append('<div class="au-progress">'
                +'<td>#'+item.id + " " + item.nombre +'</td>'
                +'<span class="au-progress__title">'+tramite+'</span>'
                +'<div class="progress mb-2">'
                  +'<div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: '+item.progreso+'%" aria-valuenow="'+item.progreso+'"'
                  +'aria-valuemin="0" aria-valuemax="100">'+parseInt(item.progreso)+'%</div>'
                +'</div>'
              +'</div>');
          }
        });//each

        $("#documentos").empty();
        $.each(data.msg.msg.documentos_generados, function(i, item) {
          $("#documentos").append('<div class="col-md-6 col-lg-3">'
          +'<div class="statistic__item">'
              +'<h2 class="number">'+item.documentos_generados+'</h2>'
              +'<span class="desc">'+item.tramite+'</span>'
              +'<div class="icon">'
                +'<i class="zmdi zmdi-file"></i>'
              +'</div>'
            +'</div>'
        +'</div>');
        });//each
        $('.table').DataTable().draw();
        //console.log(data);
        //  this.addChatMessage(data.msg);
      },
      connectionClose : function(evt){
          this.open = false;
          //this.addSystemMessage("Disconnected");
      },

      sendMsg : function(message){

          this.socket.send(JSON.stringify({
              msg : message
          }));
      },

      addChatMessage : function(data){
        alert(data.msg);
        console.log(data);
        /*switch(data.broadType){
          case Broadcast.POST : this.addNewPost(data); break;
          default : console.log("nothing to do");
        }*/
      },

      addNewPost : function(data){

        var newPost = data.data;

        newHtml = "<div>"+
                "<span> "+ newPost.postText + "</span>" +
                "</div>";

        $("#messages").prepend(newHtml);
      },

      addSystemMessage : function(msg){
          // this.chatwindow.innerHTML += "<p>" + msg + "</p>";
      }
    };

    return Connection2;

})();


</script>
