<script type="text/javascript">

  $(document).ready(function() {
    $('#txtInversion').mask("#,##0.00", {reverse: true});
    $('#txtValorEstimado').mask("#,##0.00", {reverse: true});
    $('.metros').mask("#,##0.00", {reverse: true});
    $('#txtcobro').mask("#,##0.00", {reverse: true});
    $('#txtnumber').mask("000-000-0000", {reverse: true});
  });


  function validaFormulario(idForm){
    var frm = document.getElementById(idForm);
    var _valid = 1;
    if (frm != null){
      for (var i = (frm.elements.length-1); i >= 0; i = i - 1) {
        frm.elements[i].style.backgroundColor = "#FFF";

        /*if(frm.elements[i].type == 'number' && frm.elements[i].required == false)
            if(!(parseFloat(frm.elements[i].value) > parseFloat(frm.elements[i].min) && parseFloat(frm.elements[i].value) < parseFloat(frm.elements[i].max))){
                _valid = 0;
                frm.elements[i].focus();
                frm.elements[i].style.backgroundColor = "#FEF";
                warningAlert("Valor fuera de rango","");
            }*/

          if(frm.elements[i].required == true ){
            switch(frm.elements[i].type){
                  case 'text':
                      if(frm.elements[i].value == ""){
                          _valid = 0;
                          frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                          break;
                      }
                      break;
                  case 'email':
                    var tmpVal = frm.elements[i].value;
                    var cntArroba = 0;
                    var cntPoint = 0;
                    var cntPostChar = 0;
                    var existePunto = 0;
                    for(var j = 0; j < tmpVal.length; j++){
                      if(tmpVal[j] == '@')
                          cntArroba++;
                      if(tmpVal[j] == '.')
                          cntPoint++;
                    }
                    if(cntArroba == 0){
                      _valid = 0;
                      frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                          break;
                    }
                    if(cntPoint == 0){
                      _valid = 0;
                      frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                          break;
                    }
                    if(cntArroba > 1){
                      _valid = 0;
                      frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                          break;
                    }
                    if(cntArroba > 1){
                      _valid = 0;
                      frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                          break;
                    }
                    break;
                  case 'password':
                      if(frm.elements[i].id.length >= 7)
                          if(frm.elements[i].id.substr(-7,7) != "Confirm")
                              if(frm.elements[i].value != $("#" + frm.elements[i].id + "Confirm").val()){
                                  _valid = 0;
                                  frm.elements[i].focus();
                                  frm.elements[i].style.backgroundColor = "#EEF";
                                  document.getElementById(frm.elements[i].id + "Confirm").style.backgroundColor = "#EEF";
                                  errorAlert("Las contraseñas no coinciden","");
                              }

                      if(frm.elements[i].value == ""){
                          _valid = 0;
                          frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                      }
                      break;
                  case 'number':
                      if(frm.elements[i].value == ""){
                           _valid = 0;
                           frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                       }
                      break;
                  case 'file':
                    if(frm.elements[i].value == ""){
                         _valid = 0;
                         frm.elements[i].focus();
                        frm.elements[i].style.backgroundColor = "#FDD";
                     }
                    break;

                      /*if(document.getElementById(frm.elements[i].id+'Label').textContent == 'Documento cargado correctamente' |
                      document.getElementById(frm.elements[i].id+'Label').textContent == 'Validado por Funcionario'  |
                      document.getElementById(frm.elements[i].id+'Label').textContent == 'Elegir Archivo' ){
                          $("#" + frm.elements[i].id).removeClass('btn-danger');
                          $("#" + frm.elements[i].id).addClass('btn-primary');
                      }
                      else{
                        _valid = 0;
                        frm.elements[i].focus();
                        $("#" + frm.elements[i].id).removeClass('btn-primary');
                        $("#" + frm.elements[i].id).addClass('btn-danger');
                      /*}
                    //}
                    break;*/
                  case 'checkbox':
                      if(frm.elements[i].checked == false){
                           _valid = 0;
                          frm.elements[i].focus();
                      }
                      break;
                  case 'select-one':
                      if(frm.elements[i].selectedIndex == -1){
                          _valid = 0;
                          frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                      }
                      break;
                  case 'textarea':
                      if(frm.elements[i].value == ""){
                          _valid = 0;
                          frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                      }
                      break;
                  case 'date':
                    var fecha = new Date();
                    var anio = fecha.getFullYear();
                    console.log(anio);
                    //alert(frm.elements[i].value.substr(5,4));
                      if(frm.elements[i].value == "" || frm.elements[i].value.length > 10 || parseInt(frm.elements[i].value.substr(0,4)) > anio || parseInt(frm.elements[i].value.substr(0,4)) < 1930){
                          _valid = 0;
                          frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                      }
                      break;
                  case 'time':
                      if(frm.elements[i].value == ""){
                          _valid = 0;
                          frm.elements[i].focus();
                          frm.elements[i].style.backgroundColor = "#FDD";
                      }
                      break;
              }
          }//if
      }//for
    }//if
    return _valid;
  }//validarFormulario

  function successAlert(title,text){
  	swal({
      closeOnClickOutside: false,
  		title,
  		html:"<h5>"+text+"</html>",
  	 	type:"success",
      confirmButtonText: 'Cerrar',
      width: 600,
      padding: '3em'
  	});

  }//successAlert|

  function errorAlert(title, text){
  	swal({
          allowOutsideClick:false,
  			  title: title,
  			  html: "<h5>"+text+"</h5>",
  			  type: "error",
  			  dangerMode: true,
          confirmButtonText: 'Cerrar',
          width: 600,
          padding: '3em'
  			});
  }//warningalert


  function confirmarPago(costo,id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite){
   const inputOptions = new Promise((resolve) => {
        resolve({
          '1': 'Banco',
          '2': 'Caja'
        })
      });
      f = new Date();
      var date = f.getFullYear() + "/" + (f.getMonth() +1) + "/" + f.getDate();
    swal({//forma de pago
      allowOutsideClick:false,
      title: "Seleccione una forma de pago",
      type:"warning",
      buttons: true,
      showCancelButton: true,
      input: 'radio',
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
      inputOptions: inputOptions,
      dangerMode: true
    }).then((result) =>{
          if(result.value == 1){
            swal({
              allowOutsideClick:false,
              title: "Banco",
              html: "<h5>Verifique la referencia del pago sea la misma que la solicitud</h5>",
              type:"warning",
              buttons: true,
              showCancelButton: true,
              confirmButtonText: "Registrar",
              cancelButtonText: "Cancelar",
              dangerMode: true
            }).then((result) =>{//swal banco

                if(result.value){
                  var tipo_pagoB = 'B';
                 swal({
                    allowOutsideClick:false,
                    title: "¿Confirmar pago por la cantidad de $"+costo+"?",
                    html: "<h5>¡Una vez confirmado el pago, no serás capaz de revertir esta acción!</h5>",
                    type:"warning",
                    buttons: true,
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    dangerMode: true
                    }).then((result) =>{
                      if (result.value) {
                        //insertar tipo banco
                        tipoPago(id_solicitud, date, 0, tipo_pagoB);
                          
                          siguienteFase(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite);
                      }//if metodo
                      else{
                        swal("No se registro el pago");
                      }
                    });
                }//if banco registra folio
                else{
                  swal("Se ha cancelado el pago");
                }
            });//then banco



          }//if si eligio banco
          else if(result.value == 2) {
            swal({
              allowOutsideClick:false,
              title: "Caja",
              html: "<h5>Verifique la referencia del pago sea la misma que la solicitud</h5>",
              type:"warning",
              buttons: true,
              showCancelButton: true,
              confirmButtonText: "Registrar",
              cancelButtonText: "Cancelar",
              dangerMode: true
            }).then((result) =>{//swal caja
                if(result.value){
                  var tipo_pagoB = 'C';
                  swal({
                    allowOutsideClick:false,
                    title: "¿Confirmar pago por la cantidad de $"+costo+"?",
                    html: "<h5>¡Una vez confirmado el pago, no serás capaz de revertir esta acción!</h5>",
                    type:"warning",
                    buttons: true,
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    dangerMode: true
                    }).then((result) =>{
                      if (result.value) {
                        tipoPago(id_solicitud, date, 0, tipo_pagoB);
                          siguienteFase(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite);
                      }//if metodo
                      else{
                        swal("No se registro el pago");
                      }
                    });
                }//if caja registra folio
                else{
                  swal("Se ha cancelo el pago");
                }
            });
          }//else if si eligio caja
          else{
            swal("Debe elegir una opcion");
          }
    });//then forma de pago
    
  }//confirmarPago

  function confirmarEntrega(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite){
    swal({
      allowOutsideClick:false,
      title: "¿Entregar el documento con la solicitud "+id_solicitud+"?",
      html: "<h5>¡Una vez entregado el trámite, no serás capaz de revertir esta acción!</h5>",
      type:"warning",
      buttons: true,
      showCancelButton: true,
      confirmButtonText: "Aceptar",
      cancelButtonText: "Cancelar",
      closeOnCancel: true,
      dangerMode: true,
    })
    .then((result) => {
      if (result.value) {
        siguienteFase(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite);
      }
      else {
        swal("¡No se reflejarón cambios en el sistema!");
      }
    });
  }//confirmarPago

  function mail(){
    if (id_tramite == 6 && no_fase == 3 ) {
      swal({
        allowOutsideClick:false,
        title: "Escriba el mensaje indicando las correcciones que hará el ciudadano para terminar con el proceso de inspección.",
        html: "<h5>¡Una vez realizada la acción el trámite pasará a estatus de corrección!<h5>",
        type: "warning",
        input: 'textarea', // can be also 'email', 'password', 'select', 'radio', 'checkbox', 'textarea', 'file'
        confirmButtonText: 'Rechazar Solicitud',
        showCancelButton: true,
        cancelButtonText: "No Rechazar",
        closeOnCancel: true,
        width: 600,
        padding: '3em'
        }).then(function(result) {
          // result.value will containt the input value
          if (result.value) {
            correccionCiudadano(result.value);
          }//
          else{
            swal({
              title:"Es necesario que escriba un motivo para avisar al ciudadano las correcciones que debe realizar.",
              confirmButtonText: 'Aceptar'
            });
          }//else
        });
    }//if
    else{
      swal({
        allowOutsideClick:false,
        title: "Escriba el mensaje por el cual la solicitud es rechazada",
        html: "<h5>¡Una vez rechazado el trámite, no habrá más seguimiento de mismo!<h5>",
        type: "warning",
        input: 'textarea', // can be also 'email', 'password', 'select', 'radio', 'checkbox', 'textarea', 'file'
        confirmButtonText: 'Rechazar Solicitud',
        showCancelButton: true,
        cancelButtonText: "No Rechazar",
        closeOnCancel: true,
        width: 600,
        padding: '3em'
        }).then(function(result) {
          // result.value will containt the input value
          if (result.value) {
            cancelaSolicitud(result.value);
          }//
          else{
            swal({
              title:"Es necesario que escriba un motivo para avisar al ciudadano de la cancelación de su solicitud.",
              confirmButtonText: 'Aceptar'
            });
          }//else
        });

    }//else
  }//mail

  function mailCiudadanoCorrecciones(){
      swal({
        allowOutsideClick:false,
        title: "Escriba el mensaje indicando las correcciones que debe hacer el ciudadano para terminar con el proceso de inspección.",
        html: "<h5>¡Una vez realizada la acción, queda en espera de las correcciones del ciudadano para continuar con el proceso!<h5>",
        type: "warning",
        input: 'textarea', // can be also 'email', 'password', 'select', 'radio', 'checkbox', 'textarea', 'file'
        confirmButtonText: 'MANDAR CORRECCIONES',
        showCancelButton: true,
        cancelButtonText: "CANCELAR",
        closeOnCancel: true,
        width: 600,
        padding: '3em'
        }).then(function(result) {
          // result.value will containt the input value
          if (result.value) {
            correccionCiudadano(result.value);
          }//
          else{
            swal({
              title:"Es necesario que escriba un motivo para avisar al ciudadano las correcciones que debe realizar.",
              confirmButtonText: 'Aceptar'
            });
          }//else
        });
  }//mail
  function correccion(){
    const inputOptions = new Promise((resolve) => {
        resolve({
          '1': 'Corrección Inspección.',
          '2': 'Cancelar Solicitud'
        })
      });
    swal({
      allowOutsideClick:false,
      title: "¿Seleccione una opción?",
      type: "warning",
      input: 'radio', // can be also 'email', 'password', 'select', 'radio', 'checkbox', 'textarea', 'file'
      confirmButtonText: 'Aceptar',
      showCancelButton: true,
      cancelButtonText: "No Rechazar",
      closeOnCancel: true,
      inputOptions: inputOptions,
      width: 600,
      padding: '3em'
      }).then(function(result) {
        // result.value will containt the input value
        if (result.value == 1) {
          swal({
            allowOutsideClick:false,
            title: "Escriba el mensaje por el cual la solicitud es rechazada",
            text: "¡Una vez rechazada la solicitud, el inspector subirá un nuevo documento de inspección!",
            type: "warning",
            input: 'textarea', // can be also 'email', 'password', 'select', 'radio', 'checkbox', 'textarea', 'file'
            confirmButtonText: 'Aceptar',
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            closeOnCancel: true,
            width: 600,
            padding: '3em'
            }).then(function(result) {
              // result.value will containt the input value
              if (result.value) {
                correccionInspeccion(result.value);
              }//
              else{
                swal({
                  title:"Es necesario que escriba un motivo para avisar el funcionario de inspección porque se regreso el trámite",
                  confirmButtonText: 'Aceptar'
                });
              }//else
            });
        }//if
        else if (result.value == 2) {
          mail()
        }//else if
        else{
          swal({
            title:"Es necesario seleccionar una opción",
            confirmButtonText: 'Aceptar'
          });
        }//else
      });
  }//mail


  function tipoImpacto(){
      const inputOptions = new Promise((resolve) => {
          resolve({
            '1': 'Alto Impacto.',
            '2': 'Bajo Impacto'
          })
        });
      swal({
        allowOutsideClick:false,
        title: "Seleccione el tipo de impacto del establecimiento",
        type: "warning",
        input: 'radio', // can be also 'email', 'password', 'select', 'radio', 'checkbox', 'textarea', 'file'
        confirmButtonText: 'Aceptar',
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        closeOnCancel: true,
        inputOptions: inputOptions,
        width: 600,
        padding: '3em'
        }).then(function(result) {
          // result.value will containt the input value
          if (result.value == 1 || result.value == 2) {
            impacto = (result.value == 1) ? "Alto Impacto" : (result.value == 2) ? "Bajo Impacto" : null;
            inspeccion();
          }//if
          else{
            swal({
              title:"Es necesario seleccionar una acción",
              confirmButtonText: 'Aceptar'
            });
          }//else
        });
    }//mail

  function confirmarSiguienteFase(no_fase){
    //Lanzamos ventana de corroboración de precio
    if (id_fase == 8 || id_fase == 18 || id_fase == 24  || id_fase == 31 || id_fase == 43 || id_fase == 52 || id_fase == 61 || id_fase == 69 || id_fase == 75) {
      swal({
          allowOutsideClick:false,
          title: "¿Seguro qué el precio es correcto?",
          html: "<h5>¡Corroborar costo con la Ley de Ingresos!</h5>",
          type:"warning",
          buttons: true,
          showCancelButton: true,
          confirmButtonText: "Aceptar",
          cancelButtonText: "Cancelar",
          closeOnCancel: true,
          dangerMode: true,
          width: 800,
          padding: '4em'
      })
      .then((result) => {
          if (result.value) {
            swal({
              allowOutsideClick:false,
              title: "¿La información es correcta?",
              html: "<h5>¡Una vez finalizada la revisión de la documentación, no se podrán realizar modificaciones!</h5>",
              type:"warning",
              buttons: true,
              showCancelButton: true,
              confirmButtonText: "Aceptar",
              cancelButtonText: "Cancelar",
              closeOnCancel: true,
              dangerMode: true,
              width: 800,
              padding: '4em'
            })
            .then((result) => {
              if (result.value) {
                if (no_fase != 3) {
                  if (id_tramite == 1 || id_tramite == 2) {//Validamos la generación del documento Alineamiento y Número Oficial
                    if (no_fase == 5 && pdf == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el documento para proceder con el seguimiento de la solicitud");
                    }//if
                    else if (no_fase == 8 && pago == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                      }//else if
                    else{
                      siguienteFase();//El documento se genero correctamente
                    }//else
                  }//if
                  //Trámite 4
                  else if (id_tramite == 3) {//Validamos la generación del documento Alineamiento y Número Oficial
                    if (no_fase == 5 && pdf == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el documento para proceder con el seguimiento de la solicitud");
                    }//if
                    else if (no_fase == 7 && pago == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                      }//else if
                    else{
                      siguienteFase();//El documento se genero correctamente
                    }//else
                  }//else if
                  //Trámite 6
                  else if (id_tramite == 3) {//Validamos la generación del documento Alineamiento y Número Oficial
                    if (no_fase == 6 && pdf == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el documento para proceder con el seguimiento de la solicitud");
                    }//if
                    else if (no_fase == 4 && pago == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                      }//else if
                    else{
                      siguienteFase();//El documento se genero correctamente
                    }//else
                  }//if
                  //Validación para trámite de licencias de funcionamiento
                  else if(id_tramite == 4) {
                    //Validamos las fases donde se generan documentos
                    if (no_fase  == 4 && pago == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                    }//if
                    else if (no_fase == 6 && pdf == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                    }//else if
                    else{
                      siguienteFase();
                    }//else
                  }//else if
                  else if(id_tramite == 5) {
                    //Validamos las fases donde se generan documentos
                    if (no_fase  == 6 && pago == null) {
                      errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                    }else{//if
                      siguienteFase();
                    }
                  }
                  else{
                    siguienteFase();
                  }//else
                }//if
                else{
                  if (id_tramite == 4 ) {
                    tipoImpacto();
                  }//if
                  else if(id_tramite == 9){
                    siguienteFase();
                  }//if provicional
                  else {
                    inspeccion();
                  }//else
                }//else
              }
              else {
                swal("¡No se registrarón cambios en la solicitud!");
              }
              });
          }
          else {
            swal("¡No se registrarón cambios en la solicitud!");
          }
        });
    }//if
    else{
      swal({
        allowOutsideClick:false,
        title: "¿La información de la solicitud es correcta?",
        html: "<h5>¡Una vez finalizada la revisión de la documentación, no se podrán realizar modificaciones!</h5>",
        type:"warning",
        buttons: true,
        showCancelButton: true,
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnCancel: true,
        dangerMode: true,
        width: 800,
        padding: '4em'
      })
      .then((result) => {
        if (result.value) {
          if (no_fase != 3) {
            if (id_tramite == 1 || id_tramite == 2) {//Validamos la generación del documento Alineamiento y Número Oficial
              if (no_fase == 5 && pdf == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el documento para proceder con el seguimiento de la solicitud");
              }//if
              else if (no_fase == 8 && pago == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                }//else if
              else{
                siguienteFase();//El documento se genero correctamente
              }//else
            }//if
            //Trámite 4
            else if (id_tramite == 3) {//Validamos la generación del documento Alineamiento y Número Oficial
              if (no_fase == 5 && pdf == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el documento para proceder con el seguimiento de la solicitud");
              }//if
              else if (no_fase == 7 && pago == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                }//else if
              else{
                siguienteFase();//El documento se genero correctamente
              }//else
            }//else if
            //Trámite 6
            else if (id_tramite == 3) {//Validamos la generación del documento Alineamiento y Número Oficial
              if (no_fase == 6 && pdf == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el documento para proceder con el seguimiento de la solicitud");
              }//if
              else if (no_fase == 4 && pago == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
                }//else if
              else{
                siguienteFase();//El documento se genero correctamente
              }//else
            }//if
            //Validación para trámite de licencias de funcionamiento
            else if(id_tramite == 4) {
              //Validamos las fases donde se generan documentos
              if (no_fase  == 4 && pago == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
              }//if
              else if (no_fase == 6 && pdf == null) {
                errorAlert("¡Upps! Algo Esta Mal", "Es necesario generar el pase a caja para proceder con el seguimiento de la solicitud");
              }//else if
              else{
                siguienteFase();
              }//else
            }//else if
            else if(id_tramite == 5){
                  if(no_fase == 2 || no_fase == 7){
                    siguienteFase();
                  }else if(no_fase == 4 || no_fase == 5){
                    cargaDictamen();
                  }
                  //Subir documento de opinión técnica
                  else if(no_fase == 9){
                    opinionTecnica();
                  }//else if
                }//tramite 5 o de alcoholes
                else if(id_tramite == 9){
                  if(no_fase == 2 || no_fase == 4 || no_fase == 5){
                    siguienteFase();
                  }else if(no_fase == 6){
                    cargaDictamen();
                  }
                }//tramite 9 o provicional del alcohol
            else{
              siguienteFase();
            }//else
          }//if //termina si es diferente a fase 3
          else{
            if (id_tramite == 4 ) {
              tipoImpacto();
            }//if
              else if(id_tramite == 5){
                  if(no_fase == 3){///
                    solicitarOpinion();
                  }
                }//tramite 5 o de alcoholes
            else {
              inspeccion();
            }//else
          }//else
        }
        else {
          swal("¡No se registrarón cambios en la solicitud!");
        }
        });
    }//else
  }//confirmarPago

  function calculaMetros(){
    $("#txtMConstruidos").val("");
    var totalMetros = 0;
      $('#frm_perito_responsable input').each(
        function(index){
          var input = $(this).val();
          input = input.replace(",","");
          //no se deen sumar los campos superficie del terreno y Äreas verdes
          input = (!($(this).attr('id').trim() == "txtSupTerreno")) ? input : 0;
          input = (!($(this).attr('id').trim() == "txtAreaVerde")) ? input : 0;
          //Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
          input = (input == null || input == undefined || input == "") ? 0 : input;
          // sumamos las cantiades
          totalMetros += parseFloat(input);
        }//index
      );//each function
      $("#txtMConstruidos").val(totalMetros.toString());//asugnamos el valor al campo
  }//calculaMetros

  function checkbox(input){
    if(input.checked)
        $('#'+input.id).val('TRUE');
   else
        $('#'+input.id).val('FALSE');

  }//checkbox

  function maskClaveCtastral(campo){
    $("#" + campo).mask("999 999 999 999 999", {
        // Generamos un evento en el momento que se rellena
        completed:function(){
            $("#" + campo).addClass("ok")
        }
    });
  }//maskClaveCatastral

  function onlyNumbers(input){
    $(input).on('input', function (e) {
      $(this).val($(this).val().replace(/[^0-9,.]/g, ''));
    });
  }//onlyNumbers

</script>
