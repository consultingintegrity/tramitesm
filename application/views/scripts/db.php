<script>
//valid predial
function check(val){

  var title = val==3?"¿Estas seguro que la información es correcta?":"¿Seguro que quiere Cargar Nuevamente la información?";
  var html = val==3?"<h5>¡Una vez confirmando quedara registrada la confirmación !</h5>":"<h5>¡Una vez confirmando se limpiaran todos los registros del Predial y tendra que cargar la información nuevamente!</h5>";
swal({
          allowOutsideClick:false,
          title: title,
          html: html,
          type:"warning",
          buttons: true,
          showCancelButton: true,
          confirmButtonText: "Aceptar",
          cancelButtonText: "Cancelar",
          dangerMode: true
          }).then((result) =>{
            if (result.value){
              $.ajax({
                  url: '<?=base_url()?>phpspreadsheet/Excel/checkDB',
                  type: 'POST',
                  dataType: 'json',
                  data: {val: val},
                  success:function(jsonResponse){
                    if(jsonResponse.response == 200){
                    successAlert(jsonResponse.msg,"");
                    setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 3000);
                    }else{
                      errorAlert(jsonResponse.msg,"");
                    }
                  },
                  error:function(){ 
                    errorAlert("Error, intente Nuevamente","");
                  }
              });//ajax
              
            }
            else{
                swal('Acción cancelada');
            }
          });//then
}//check registros correctos
</script>