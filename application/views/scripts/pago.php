<script type="text/javascript">
  $(document).ready( function () {
    //Globals vars
    var id_solicitud = 0//solicitud en revisión
    var id_fase = 0;//funcionario que revisa
    var no_fase = 0;//funcionario que revisa
    var id_ciudadano =0;//ciudadano que realizó la solicitud
    var id_tramite = 0;//ciudadano que realizó la solicitud

    var cajaEspaniol = {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    };//cajaEspaniol
    $('#tablePago').DataTable({
        "language":cajaEspaniol
    });
    /* predial */
    $('#tablePredial').DataTable({
        "language":cajaEspaniol
    });

    $(".btnPagar").on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      obtenerInformacionPago(this.value);
    });

    $(".btnPagarPredial").on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      obtenerInformacionPagoPredial(this.value);
    });

    $(".btnCorte").on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      realizaCorte(this.value);
    });

    $(".btnCortePredial").on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      realizaCortePredial();
    });

    $(".btnCancelar").on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      $.ajax({
        url: '<?=base_url()?>Solicitud/infoSolicitud',
        type: 'POST',
        dataType: 'json',
        data: {id_solicitud: this.value, estatus:3},
        success:function(json){
          id_solicitud = json.id;
          id_fase = json.id_fase;
          id_funcionario = json.id_funcionario;
          id_ciudadano = json.id_ciudadano;
          id_tramite = json.id_tramite;
          mail(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite);
        },//success
        error:function(){
          //doSomething
        }//error
      });
    });
    function obtenerInformacionPago(id_solicitud){
      $.ajax({
        url: '<?=base_url()?>Solicitud/infoSolicitud',
        type: 'POST',
        dataType: 'json',
        data: {id_solicitud: id_solicitud, estatus:3},
        success:function(json){
          id_solicitud = json.id;
          id_fase = json.id_fase;
          id_funcionario = json.id_funcionario;
          id_ciudadano = json.id_ciudadano;
          id_tramite = json.id_tramite;
          confirmarPago(json.costo,id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite);
        },//success
        error:function(){
          //doSomething
        }//error
      });
    }//obtenerInformacionPago

    function mail(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite,){
      swal({
        allowOutsideClick:false,
        title: "Escriba el mensaje por el cual la solicitud es rechazada",
        html: "<h5>¡Una vez rechazado el trámite, no habrá mas seguimiento de mismo!<h5>",
        type: "warning",
        input: 'textarea', // can be also 'email', 'password', 'select', 'radio', 'checkbox', 'textarea', 'file'
        confirmButtonText: 'Rechazar Solicitud',
        showCancelButton: true,
        cancelButtonText: "No Rechazar",
        closeOnCancel: true,
        width: 600,
        padding: '3em'
        }).then(function(result) {
          // result.value will containt the input value
          if (result.value) {
            cancelaSolicitud(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite,result.value);
          }//
          else{
            swal({
              title:"Es necesario que escriba un motivo para avisar al ciudadano de la cancelación de su solicitud.",
              confirmButtonText: 'Aceptar'
            });
          }//else
        });
    }//mail
  });//readyDocument
  function siguienteFase(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite){
    $.ajax({
      url: '<?=base_url()?>Solicitud/actualiza',
      type: 'POST',//Tipo de datos a mandar
      dataType:'json',//Tipo de respuesta que esperamos
      data: {
        id_solicitud: id_solicitud,
        id_fase : id_fase,
        id_ciudadano : id_ciudadano,
        id_tramite : id_tramite
      },//data
      success:function(jsonResponse){
        if (jsonResponse.response == 200) {
          successAlert("Revisión Exitosa ",jsonResponse.msg);
           $("#fila" + id_solicitud).remove();
           onSiguienteFase();
        }//if
        else{
          errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
        }//else

      }//sucess
    });//ajax
  }//siguiente_fase

  function cancelaSolicitud(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite,texto){
    $.ajax({
      url: '<?=base_url()?>Solicitud/cancela',
      type: 'POST',
      dataType: 'json',
      data: {id_solicitud: id_solicitud, id_ciudadano:id_ciudadano, texto:texto,id_tramite:id_tramite},
      beforeSend: function() {
        swal({ title: "Cancelando Solicitud", allowOutsideClick:false });
        swal.showLoading();
      },
      success:function(response){
        if (response.response == 200) {
          $("#btnFinalizar").attr('disabled','disabled');
          $("#btnRechazar").attr('disabled','disabled');
            swal.close();
            successAlert(response.msg," ");
             $("#fila" + id_solicitud).remove();
             onSiguienteFase();
        }//if
        else{
          swal.close();
          errorAlert("Algo salió mal",response.msg);
        }//else

      }//success
    })
      .done(function() {
      })
      .fail(function() {
      })
      .always(function() {
    });
  }//cancelaSolicitud

  function tipoPago(id_solicitud, fecha, corte, tipo_pago){
    $.ajax({
      url: '<?=base_url()?>Solicitud/insertTipoPago',
      type: 'POST',//Tipo de datos a mandar
      dataType:'json',//Tipo de respuesta que esperamos
      data: {
        id_solicitud: id_solicitud,
        fecha : fecha,
        en_corte : corte,
        tipo_pago : tipo_pago
      },//data
      success:function(jsonResponse){
        if (jsonResponse.response == 200) {
          successAlert("Todo bien en insertar tipo pago");
        }//if
        else{
          errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
        }//else

      }//sucess
    });

  }//PagoBanco

  

  function realizaCorte(){
    $.ajax({
      url:'<?=base_url()?>Solicitud/enCorte',
      type: 'POST',
      dataType: 'json',
      data: { corte : 0 },
      success:function(jsonResponse){
        if(jsonResponse.response==200){
        successAlert("Se genero correctamente el corte", "Todo bien.");
        setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 1000);
        }//if
        else{
          errorAlert('Algo salio mal al modificar el estatus de corte');
        }
      }//success
    });//ajax

  }//realizaCorte
  
  function obtenerInformacionPagoPredial(id){

      $.ajax({
        url: '<?=base_url()?>Predial/consultaTotal',
        type: 'POST',
        dataType: 'json',
        data: {id: id},
        success:function(jsonResponse){
          swal({
                    allowOutsideClick:false,
                    title: "¿Confirmar pago por la cantidad de $"+jsonResponse.total.total+"?",
                    html: "<h5>¡Una vez confirmado el pago, no serás capaz de revertir esta acción!</h5>",
                    type:"warning",
                    buttons: true,
                    showCancelButton: true,
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                    dangerMode: true
                    }).then((result) =>{
                      if (result.value){
                        $.ajax({
                            url: '<?=base_url()?>Predial/realizaPagoPredio',
                            type: 'POST',
                            dataType: 'json',
                            data: {id: id},
                            success:function(jsonResponse){
                              if(jsonResponse.response == 200){
                              successAlert("Se realizo correctamente el pago del Predial",'');
                              setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario/predial"); }, 1000);
                              }else{
                                errorAlert("Error 500 al realizar el pago");
                              }
                            },
                            error:function(){ 
                              errorAlert("Error al realizar el pago");
                            }
                        });//ajax
                        
                      }
                      else{
                          swal('Pago Cancelado');
                      }
                    });//then
        },//success
        error:function(){
          //doSomething
        }//error
      });
    }//obtenerInformacionPago
  
    function realizaCortePredial(){
    $.ajax({
      url:'<?=base_url()?>Predial/realizaCortePredial',
      type: 'POST',
      dataType: 'json',
      success:function(jsonResponse){
        if(jsonResponse.response==200){
        successAlert("Se genero correctamente el corte de predial", "Todo bien.");
        setTimeout(function(){window.location.replace("<?=base_url()?>Funcionario/predial");}, 2000);
        }//if
        else if(jsonResponse.response==500){
          errorAlert('No se han realizado pagos de predial', '');
        }
      }//success
    });//ajax

  }//realizaCorte

</script>
