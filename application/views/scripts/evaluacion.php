<script type="text/javascript">
  //globals vars to use on javascript
  var id_solicitud = <?= $solicitud->id ?>;//solicitud en revisión
  var id_fase = <?= $solicitud->id_fase ?>;//funcionario que revisa
  var no_fase = <?= $no_fase ?>;//funcionario que revisa
  var id_ciudadano = <?= $solicitud->id_ciudadano ?>;//ciudadano que realizó la solicitud
  var id_tramite = <?= $solicitud->id_tramite ?>;//ciudadano que realizó la solicitud
  var pdf = null;
  var pago = null;
  var fecha_vencimiento = null;
  var costoTramite = null;
  var id_giro = <?=(!empty($giro)) ? $giro->id : 0 ?>;
  var impacto = null;
  var referencia = null;
  var folio = null;

  $(document).ready(function() {
    $("#btnFinalizar").on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
        confirmarSiguienteFase(no_fase);
    });//button

    //cancelar la solicitud de un ciudadano
    $("#btnRechazar").on('click',function(event) {
      if (no_fase == 2) {
        mail();
      }
      else if (no_fase == 4) {
        correccion();
      }//else if
    });//btnRechazar .click

    //Mandar Solicitud a Correccione s
    $("#btnCorrecciones").on('click',function(event) {
      if (id_tramite == 6) {
        mail();
      }//if
    });//click function btnCorrecciones

    //Mandar aviso a ciudadano que supa documento de correcciones
    $("#btnCorreccionesCiudadano").on('click',function(event) {
      mailCiudadanoCorrecciones();
    });//click function btnCorrecciones

  });//redyDocument

  function siguienteFase(){
    pdf = null ? '' : pdf;
    pago = null ? '' : pago;
    fecha_vencimiento = null ? '' : fecha_vencimiento;
    folio = null ? '' : folio;
    referencia = null ? '' : referencia;
    $.ajax({
      url: '<?=base_url()?>Solicitud/actualiza',
      type: 'POST',//Tipo de datos a mandar
      dataType:'json',//Tipo de respuesta que esperamos
      data: {
        id_solicitud: id_solicitud,
        id_fase : id_fase,
        id_ciudadano : id_ciudadano,
        id_tramite : id_tramite,
        pdf : pdf,
        pago : pago,
        fecha_vencimiento : fecha_vencimiento,
        costoTramite : costoTramite,
        id_giro : id_giro,
        folio : folio,
        referencia : referencia
      },//data
      success:function(jsonResponse){
        if (jsonResponse.response == 200) {
          $("#btnFinalizar").attr('disabled','disabled');
          onSiguienteFase();
          successAlert("Revisión Exitosa ",jsonResponse.msg);
          setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 18000);
        }//if
        else if (jsonResponse.response == 404) {
          errorAlert("Algo salio Terriblemente Mal ",jsonResponse.msg);
        }//else if
        else{
          $("#btnFinalizar").attr('disabled','disabled');
          errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
          setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 18000);
        }//else

      }//sucess
    });

  }//siguienteFase

  function  inspeccion(){
    if ($("#txtDocInspeccion").val() != "") {
      var formData = new FormData($("#frm_inspeccion_subir")[0]);//creaos un formData pra subir archivos
      //AGREGAMOS EL ARCHIVO
      formData.append('importData', $('input[type=file]')[0].files[0]);
      //PARAMETROS TIPO post
      formData.append('id_solicitud', id_solicitud);
      formData.append('id_fase',id_fase);
      formData.append('no_fase', no_fase);
      formData.append('id_tramite',id_tramite);
      formData.append('id_ciudadano',id_ciudadano);
      formData.append('impacto',impacto);


      $.ajax({
        url: '<?=base_url()?>Solicitud/actualiza',
        enctype: 'multipart/form-data',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
          swal({ title: "Actualizando Solicitud", allowOutsideClick:false });
          swal.showLoading();
        },
        success:function(jsonResponse){
          if (jsonResponse.response == 200) {
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            successAlert("Revisión Exitosa ",jsonResponse.msg + " " + jsonResponse.adversiment);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//if
          else{
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//else

        }//sucess
      });
    }//if
    else{
      errorAlert("Parece que algo esta mal.","Nececitas subir el documento de inspección para poder seguir con el seguimiento del trámite");

    }//else

  }//inspeccion

//generar pdf
  function generar(){
    //obtengo mis datos

    switch (id_tramite) {
      case 1:
      //numero oficial
        var descripcion = document.getElementById("txtdescripcion").value;
        var asignoNO = document.getElementById("txtasignoNO").value;
        var comprobanteNO = document.getElementById("txtcomprobanteNO").value;

        break;
        case 2:
      //uso de suelo
        var usZona = document.getElementById("txtusZona").value;
        var organismo = document.getElementById("txtorganismo").value;

        break;
      case 6:
      //proteccion civil
        var nombreNegocio = document.getElementById("txtnegocio").value;
        var giroNegocio = document.getElementById("txtgiro").value;
        break;
      case 3:
         //giro
        var nombreNegocio = document.getElementById("txtnegocio").value;
        var noDictamen = document.getElementById("txtdictamen").value;
        break;
      case 4:
      //Licencia de funcionamiento
        var tipoLicencia = document.getElementById("txtlicencia").value;
        break;
        case 7:
      //Licencia de construccion

        var noLicencia = document.getElementById("txtnoLicencia").value;
        var noAlineamiento = document.getElementById("txtnoAlinemiento").value;
        var vigencia = document.getElementById("txtVigencia").value;
        var observaciones = document.getElementById("txtobservaciones").value;
        break;
      //----------------------------------------------------------------
      case 8:
      //informe uso de suelo
        var predioZona = document.getElementById("txtzona").value;
        var clave = document.getElementById("txtclave").value;

        break;
      //---------------------------------------------------------------
      default:
      var asunto = document.getElementById("txtasunto").value;
      var interesado = document.getElementById("txtinteresado").value;
        break;
    }
    //    var interesado = document.getElementById("txtinteresado").value;
    $.ajax({
        url: '<?=base_url()?>GenerarDocs/generarPdf',
        type: 'POST',//Tipo de datos a mandar
        dataType:'json',//Tipo de respuesta que esperamos
        data: {
          id_solicitud: id_solicitud,
          no_fase : no_fase,
          id_ciudadano : id_ciudadano,
          id_tramite : id_tramite,
          descripcion : descripcion,
          asignoNO : asignoNO,
          comprobanteNO , comprobanteNO,
          asunto : asunto,
          clave : clave,
          interesado : interesado,
          organismo : organismo,
          usZona : usZona,
          nombreNegocio : nombreNegocio,
          giroNegocio : giroNegocio,
          noDictamen : noDictamen,
          predioZona : predioZona,
          tipoLicencia : tipoLicencia,
          observaciones : observaciones,
          noLicencia : noLicencia,
          noAlineamiento : noAlineamiento,
          vigencia : vigencia


        },//data
        success:function(jsonResponse){
          if (jsonResponse.response == 200) {
            successAlert("Excelente",jsonResponse.msg);

            pdf = jsonResponse.url;
            var base = "<?= base_url()?>";
            var url = base+pdf;
            folio = jsonResponse.folio;
            var iframe = document.getElementById("iframe").setAttribute("src",url);

          }//if
          else if(jsonResponse.response == 522){
            errorAlert("Algo salió mal",jsonResponse.msg);
          }//else

        }//sucess
      });
  }//./generar

  //generar pago
  function generarPago(){
    //obtengo mis datos
    var cantidad = document.getElementById("txtcobro").value;
    costoTramite = (cantidad != 0) ? cantidad : 0;
    $.ajax({
        url: '<?=base_url()?>GenerarDocs/generarPdf',
        type: 'POST',//Tipo de datos a mandar
        dataType:'json',//Tipo de respuesta que esperamos
        data: {
          id_solicitud: id_solicitud,
          no_fase : no_fase,
          id_ciudadano : id_ciudadano,
          id_tramite : id_tramite,
          costoTramite : costoTramite
        },//data
        success:function(jsonResponse){
          console.log(jsonResponse);
          if (jsonResponse.response == 200) {
            successAlert("Excelente",jsonResponse.msg);

            pago = jsonResponse.url;
            fecha_vencimiento = jsonResponse.fecha_vencimiento;
            var base = "<?= base_url()?>";
            var url = base+pago;
            folio = jsonResponse.folio;
            referencia = jsonResponse.lineaCaptura;
            var iframe = document.getElementById("iframe1").setAttribute("src",url);

          }else if(jsonResponse.response == 522){
            errorAlert("Algo salió mal",jsonResponse.msg);

          }//else

        }//sucess
      });

  }//./pago

  function cancelaSolicitud(texto){
      $.ajax({
        url: '<?=base_url()?>Solicitud/cancela',
        type: 'POST',
        dataType: 'json',
        data: {id_solicitud: id_solicitud, id_ciudadano:id_ciudadano, texto:texto,id_tramite:id_tramite},
        beforeSend: function() {
          swal({ title: "Cancelando Solicitud", allowOutsideClick:false });
          swal.showLoading();
        },
        success:function(response){
          if (response.response == 200) {
            $("#btnFinalizar").attr('disabled','disabled');
            $("#btnRechazar").attr('disabled','disabled');
              swal.close();
              successAlert(response.msg,"En unos momentos serás redirigido a tu lista de trámites");
              onSiguienteFase();
              setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//if
          else{
            swal.close();
            errorAlert("Algo salió mal",response.msg);
          }//else

        }//success
      })
        .done(function() {
          console.log("success");
        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
      });
  }//cancelaSolicitud

  function correccionInspeccion(texto){
    $.ajax({
      url: '<?=base_url()?>Solicitud/correccionInspeccion',
      type: 'POST',
      dataType: 'json',
      data: {id_solicitud: id_solicitud, id_fase:id_fase, texto:texto,id_tramite:id_tramite},
      beforeSend: function() {
        swal({ title: "Actualizando Solicitud", allowOutsideClick:false });
        swal.showLoading();
      },
      success:function(response){
        if (response.response == 200) {
          $("#btnFinalizar").attr('disabled','disabled');
          $("#btnRechazar").attr('disabled','disabled');
            swal.close();
            successAlert(response.msg,"En unos momentos serás redirigido a tu lista de trámites");
            onSiguienteFase();
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
        }//if
        else{
          swal.close();
          errorAlert("Algo salió mal",response.msg);
        }//else

      }//success
    })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
    });
  }//correccionInspeccion

  function correccionCiudadano(texto){
    $.ajax({
      url: '<?=base_url()?>Solicitud/correccionInspeccionCiudadano',
      type: 'POST',
      dataType: 'json',
      data: {id_solicitud: id_solicitud, id_ciudadano:id_ciudadano, texto:texto,id_tramite:id_tramite},
      beforeSend: function() {
        swal({ title: "Cancelando Solicitud", allowOutsideClick:false });
        swal.showLoading();
      },
      success:function(response){
        if (response.response == 200) {
          $("#btnFinalizar").attr('disabled','disabled');
          $("#btnCorrecciones").attr('disabled','disabled');
            swal.close();
            successAlert(response.msg,"En unos momentos serás redirigido a tu lista de trámites");
            onSiguienteFase();
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
        }//if
        else{
          swal.close();
          errorAlert("Algo salió mal",response.msg);
        }//else

      }//success
    })
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
    });

  }//correccionCiudadano

   //se guarda la opinion tecnica de alcoholes
  function cargaOpinion(){
    if ($("#txtOpinionTec").val() != "") {

      var formData = new FormData($("#frm_opinion_tecnica")[0]);//creaos un formData pra subir archivos

      //AGREGAMOS EL ARCHIVO
      formData.append('importData', $('input[type=file]')[0].files[0]);
      //PARAMETROS TIPO post
      formData.append('id_solicitud', id_solicitud);
      formData.append('id_fase',id_fase);
      formData.append('no_fase', no_fase);
      formData.append('id_tramite',id_tramite);
      formData.append('id_ciudadano',id_ciudadano);

      $.ajax({
        url: '<?=base_url()?>Solicitud/actualiza',
        enctype: 'multipart/form-data',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
          swal({ title: "Actualizando Solicitud", allowOutsideClick:false });
          swal.showLoading();
        },
        success:function(jsonResponse){
          if (jsonResponse.response == 200) {
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            successAlert("Revisión Exitosa ",jsonResponse.msg + " " + jsonResponse.adversiment);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//if
          else{
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//else

        }//sucess
      });
    }//if
    else{
      errorAlert("Parece que algo esta mal.","Nececitas subir el documento para poder seguir con el seguimiento del trámite");

    }//else
}//opinion tecnica

function  cargaDictamen(){
    if ($("#txtDocDictamen").val() != "") {
      if(no_fase == 4){
      var formData = new FormData($("#frm_sube_dictamen")[0]);//creaos un formData pra subir archivos
      }else if(no_fase == 5){
        var formData = new FormData($("#frm_sube_informe")[0]);//creaos un formData pra subir archivos
      }else if(no_fase == 6){
        var formData = new FormData($("#frm_carga_permiso")[0]);//creaos un formData pra subir archivos
      }
      //AGREGAMOS EL ARCHIVO
      formData.append('importData', $('input[type=file]')[0].files[0]);
      //PARAMETROS TIPO post
      formData.append('id_solicitud', id_solicitud);
      formData.append('id_fase',id_fase);
      formData.append('no_fase', no_fase);
      formData.append('id_tramite',id_tramite);
      formData.append('id_ciudadano',id_ciudadano);

      $.ajax({
        url: '<?=base_url()?>Solicitud/actualiza',
        enctype: 'multipart/form-data',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
          swal({ title: "Actualizando Solicitud", allowOutsideClick:false });
          swal.showLoading();
        },
        success:function(jsonResponse){
          if (jsonResponse.response == 200) {
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            successAlert("Revisión Exitosa ",jsonResponse.msg + " " + jsonResponse.adversiment);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//if
          else{
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//else

        }//sucess
      });
    }//if
    else{
      errorAlert("Parece que algo esta mal.","Nececitas subir el documento para poder seguir con el seguimiento del trámite");

    }//else

  }//dictamen

    //solicitar opinion
    function solicitarOpinion(){
    //obtengo mis datos
    $.ajax({
        url: '<?=base_url()?>Solicitud/actualiza',
        type: 'POST',//Tipo de datos a mandar
        dataType:'json',//Tipo de respuesta que esperamos
        data: {
          id_solicitud: id_solicitud,
          id_fase : id_fase,
          id_ciudadano : id_ciudadano,
          id_tramite : id_tramite,
          SF : document.getElementById("cboxSF").checked,
          PC : document.getElementById("cboxPC").checked,
          DU : document.getElementById("cboxDU").checked,
          fechaDictamen : document.getElementById("txtFechaDictamen").value
        },//data
        success:function(jsonResponse){
          console.log(jsonResponse);
          if (jsonResponse.response == 200) {
            $("#btnFinalizar").attr('disabled','disabled');
            onSiguienteFase();
            successAlert("Revisión Exitosa ",jsonResponse.msg);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 18000);
          }//if
          else{
            $("#btnFinalizar").attr('disabled','disabled');
            errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 18000);
          }//else

        }//sucess
      });

  }//./pago



  function  opinionTecnica(){
    if ($("#txtDocOpinionTecnica").val() != "") {
      var formData = new FormData($("#frm_doc_opinion_tecnica")[0]);//creaos un formData pra subir archivos
      //AGREGAMOS EL ARCHIVO
      formData.append('importData', $('input[type=file]')[0].files[0]);
      //PARAMETROS TIPO post
      formData.append('id_solicitud', id_solicitud);
      formData.append('id_fase',id_fase);
      formData.append('no_fase', no_fase);
      formData.append('id_tramite',id_tramite);
      formData.append('id_ciudadano',id_ciudadano);
      formData.append('opinionTecnica',true);
      $.ajax({
        url: '<?=base_url()?>Solicitud/actualiza',
        enctype: 'multipart/form-data',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
          swal({ title: "Actualizando Solicitud", allowOutsideClick:false });
          swal.showLoading();
        },
        success:function(jsonResponse){
          if (jsonResponse.response == 200) {
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            successAlert("Revisión Exitosa ",jsonResponse.msg + " " + jsonResponse.adversiment);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//if
          else{
            swal.close();
            $("#btnFinalizar").attr('disabled','disabled');
            errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
            setTimeout(function(){ window.location.replace("<?=base_url()?>Funcionario"); }, 8000);
          }//else

        }//sucess
      });
    }//if
    else{
      errorAlert("Parece que algo esta mal.","Nececitas subir el documento de opinión técnica para poder seguir con el seguimiento del trámite");

    }//else

  }//opinionTecnica
</script>
