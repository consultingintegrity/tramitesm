<script type="text/javascript">
    function obtenerInformacionPredio() {
        var clave = document.getElementById("txtClaveCatastral").value;
        var monto = '';
        if (clave != "") {
            $.ajax({
                url: '<?= base_url() ?>predial/consultaPredial',
                type: 'POST',
                asyn: false,
                dataType: 'json',
                data: {
                    clave: clave
                },
                success: function(jsonResponse) {
                    if (jsonResponse.estatus == 200) {
                        successAlert("Éxito", jsonResponse.msg);
                        var col = jsonResponse.predial.coloniaPredio == null ? "" : jsonResponse.predial.coloniaPredio;
                        var direccion = jsonResponse.predial.ubicacionPredio + ' ' + jsonResponse.predial.coloniaPredio != "null" ? jsonResponse.predial.numeroExterior + ' ' + jsonResponse.predial.ubicacionPredio + ' ' + col : "Sin Dirección";
                        monto = jsonResponse.predial.total;
                        document.getElementById('nombrePropietario').innerText = jsonResponse.predial.nombreContribuyente;
                        var Catastral = document.getElementById("lblClaveC").setAttribute("value", clave);
                        var direccionPredio = document.getElementById("direccionPago").textContent = '#' + direccion;
                        var totalPago = document.getElementById("importe").textContent = "$" + monto + " MXN";
                        fechaVigencia = document.getElementById("fechaV").textContent = jsonResponse.predial.vigenciaPago;
                        var desglose = "<ul style='columns: 2; list-style-type: none;'>" +
                            '<li>Impuesto:</li>' +
                            '<li>Adicional:</li>' +
                            '<li>Actualización</li>' +
                            '<li>Recargos:</li>' +
                            '<li>Requerimientos:</li>' +
                            '<li>Embargo:</li>' +
                            '<li>Multa:</li>' +
                            '<li>Rezago:</li>' +
                            '<li>Descuento:</li>' +
                            '<li>' + jsonResponse.predial.impuestoAno + '</li>' +
                            '<li>' + jsonResponse.predial.adicional + '</li>' +
                            '<li>' + jsonResponse.predial.actualización + '</li>' +
                            '<li>' + jsonResponse.predial.Recargo + '</li>' +
                            '<li>' + jsonResponse.predial.requerimientoGastoEjecucion + '</li>' +
                            '<li>' + jsonResponse.predial.embargoGastosEjecucion + '</li>' +
                            '<li>' + jsonResponse.predial.multa + '</li>' +
                            '<li>' + jsonResponse.predial.rezagototal + '</li>' +
                            '<li>' + jsonResponse.predial.descuento + '</li>' +
                            '</ul>';
                        document.getElementById("desglose").innerHTML = desglose;
                    } else if (jsonResponse.estatus == 500) {

                        errorAlert("Consulta NO DISPONIBLE \n No se puede Atender la Solicitud acude al municipio para realizar pago", "Error: " + jsonResponse.msg);

                    } //else

                }, //success
            });
        } else {
            errorAlert("Parece que algo esta mal.", "Por favor ingresa tu clave Catastral");
        }
    } //obtenerInformacionPago

    function generarPago() {
        var claveCatastral = document.getElementById("lblClaveC").value;

        if (claveCatastral != "") {

            swal({
                title: "Desea generar el pago",
                html: "<h5>¡Una vez generado el tiempo para el pago comenzará a correr!<h5>",
                type: "warning",
                confirmButtonText: 'Generar',
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                closeOnCancel: true,
                width: 600,
                padding: '3em'
            }).then(function(result) {
                // result.value will containt the input value
                if (result.value) {

                    $.ajax({
                        url: '<?= base_url() ?>GenerarPredial/generaPdfCorte',
                        type: 'POST',
                        asyn: false,
                        dataType: 'json',
                        data: {
                            clave: claveCatastral
                        },
                        success: function(jsonResponse) {

                            if (jsonResponse.response == 200) {
                                var pdf = jsonResponse.url;
                                successAlert("Exito", jsonResponse.msg);
                                registrarPagoPredio(jsonResponse.nombre, jsonResponse.claveCatastral, jsonResponse.folio, jsonResponse.lineaCaptura, jsonResponse.Total, jsonResponse.url, jsonResponse.fecha_emision, jsonResponse.fecha_vencimiento);
                                window.open("<?= base_url() ?>" + pdf, '_blank');

                            } else if (jsonResponse.response == 500) {

                                errorAlert("Algo salio mal", "Error: " + jsonResponse.msg);
                                setTimeout(function() {
                                    window.location.replace("<?= base_url() ?>");
                                }, 4000);
                            } //else
                            else if (jsonResponse.response == 600) {
                                var pdf = jsonResponse.url;
                                successAlert("Exito", jsonResponse.msg);
                                window.open("<?= base_url() ?>" + pdf, '_blank');
                                setTimeout(function() {
                                    window.location.replace("<?= base_url() ?>");
                                }, 4000);
                            }

                        }, //success
                    });
                } //
                else {
                    swal({
                        title: "No se realizo ninguna acción.",
                        confirmButtonText: 'Aceptar'
                    });
                } //else
            });

        } else {
            errorAlert("Parece que algo esta mal.", "Por favor primero consulte su ClaveCatastral");
        }
    }

    function registrarPagoPredio(nombre, cCatastral, folio, lCaptura, total, url, fechaEmision, fechaVencimiento) {
        $.ajax({
            url: '<?= base_url() ?>predial/insertarPredial',
            type: 'POST',
            asyn: false,
            dataType: 'json',
            data: {
                nombre: nombre,
                cCatastral: cCatastral,
                folio: folio,
                lCaptura: lCaptura,
                total: total,
                url: url,
                fechaEmision: fechaEmision,
                fechaVencimiento: fechaVencimiento
            },
            success: function(jsonResponse) {
                if (jsonResponse.estatus == 200) {
                    setTimeout(function() {
                        window.location.replace("<?= base_url() ?>");
                    }, 4000);

                } else if (jsonResponse.estatus == 500) {

                    errorAlert("Algo Salio mal", jsonResponse.msg);

                } //else

            }, //success
        });
    }

    function botonPagar() {
        var claveCatastral = document.getElementById("lblClaveC").value;
        if (claveCatastral != "") {
            $( "#clavemodal" ).attr("value",claveCatastral);
            $('#modalPagar').modal('show');
        } else {
            errorAlert("Parece que algo esta mal.", "Por favor primero consulte su ClaveCatastral");
        }
    }

    function generarCobro(){
        var claveCatastral = document.getElementById("txtclave").value;
        var nombre = document.getElementById("txtnombre").value;
        var apellido = document.getElementById("txtApellido").value;
        var correo = document.getElementById("txtcorreo").value;
        var numero = document.getElementById("txtnumber").value;
        var msi = $("#cbxMSI").is(':checked');
        if(nombre != "" || apellido != "" || correo != "" || numero != ""){
        $.ajax({
            url: '<?= base_url() ?>Paybanco/generarPago',
            type: 'POST',
            dataType: 'json',
            data: {
                nombre: nombre,
                claveCatastral: claveCatastral,
                apellido: apellido,
                correo: correo,
                numero: numero,
                msi: msi,
            },
            success: function(jsonResponse) {
                if (jsonResponse.estatus == 200) {
                    successAlert("Todo Correcto", jsonResponse.msg);
                    setTimeout(function() {
                         window.location.replace(jsonResponse.url, '_blank');
                    }, 2000);
                } else if (jsonResponse.estatus == 500) {

                    errorAlert("Algo Salio mal", jsonResponse.msg);
                    setTimeout(function() {
                        window.location.replace("<?= base_url()?>");
                     }, 4000);
                } //else

            }, //success
        });
        }
        else{
            swal({
                        title: "Completa los campos.",
                        confirmButtonText: 'Aceptar'
                    });
        }

    }

    function descargar(id){
        $.ajax({
            url: '<?= base_url() ?>Paybanco/descargar',
            type: 'POST',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(jsonResponse) {
                if (jsonResponse.estatus == 200) {
                    successAlert("Todo Correcto", jsonResponse.msg);
                    setTimeout(function() {
                         window.location.replace("<?= base_url()?>"+jsonResponse.url, '_blank');
                    }, 2000);
                } else if (jsonResponse.estatus == 500) {

                    errorAlert("Algo Salio mal", jsonResponse.msg);
                    setTimeout(function() {
                        window.location.replace("<?= base_url()?>");
                     }, 4000);
                } //else

            }, //success
        });
    }
</script>