<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="<?= base_url() ?>plantilla/js/functions.js"></script>
<link href="<?= base_url() ?>plantilla/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">
<!--DataTables-->
<script type="text/javascript" src="<?=base_url()?>plantilla/js/DataTables/datatables.min.js"></script>
<!--buttons DataTables-->
<script type="text/javascript" src="<?=base_url()?>plantilla/js/dataTables.buttons.min.js"></script>
<script src="<?=base_url()?>plantilla/js/jszip.min.js"></script>
<script src="<?=base_url()?>plantilla/js/pdfmake.min.js"></script>
<script src="<?=base_url()?>plantilla/js/vfs_fonts.js"></script>
<script src="<?=base_url()?>plantilla/js/buttons.html5.min.js"></script>
<!-- Select 2 by jQuery -->
<script type="text/javascript" src="<?=base_url()?>plantilla/js/select2.min.js"></script>
<!-- jQuery MASK -->
<script type="text/javascript" src="<?=base_url()?>plantilla/js/jquery.mask.js"></script>
<!-- TimePicker -->
<script type="text/javascript" src="<?=base_url()?>plantilla/js/jquery.timepicker.min.js"></script>
<link type="text/css" href="<?=base_url()?>plantilla/js/jquery.timepicker.min.css" rel="stylesheet"/>
<script src="https://kit.fontawesome.com/241efd7edf.js" crossorigin="anonymous"></script>
