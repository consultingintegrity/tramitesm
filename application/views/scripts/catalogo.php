
  <script type="text/javascript">
    var roles = <?=json_encode($rol)?>;
    var id_rol =  <?=(isset($funcionario)) ? $funcionario->id_rol  : 0 ?>;

    $(document).ready(function() {
      municipios($("#optEstado").val());
      llenarDependencia($("#optDireccion").val());
      llenarRoles($("#optDireccion").val());

      $('#optEstado').on('change', function() {
        var id_estado = $(this).val();
        municipios(id_estado);
      });

      $('#optDireccion').on('change', function() {
        var id_dependencia = $(this).val();
        llenarDependencia(id_dependencia);
        llenarRoles(id_dependencia);
      });
    });//readyDocument

    function municipios(id_estado){
      var id_municipio_usuario = <?=(isset($funcionario)) ? $funcionario->id_municipio : 0 ?>;
      $.ajax({
        url: '<?= base_url() ?>Catalogo/getMunicipios/'+id_estado,
        type: "POST",
        dataType: "json",
        success:function(data) {
          $('#optMunicipio').empty();
          $.each(data["municipios"], function(key, value) {
            if (id_municipio_usuario == value.id) {
              $('#optMunicipio').append('<option value="'+ value.id +'" selected>'+ value.nombre +'</option>');
            }//if
            else{
              $('#optMunicipio').append('<option value="'+ value.id +'">'+ value.nombre +'</option>');
            }
          });//each
        }//success
      });//ajax
    }//muincipios

    function llenarDependencia(id_direccion){
      $.ajax({
        url: '<?=base_url()?>Catalogo/getDependencia',
        type: 'POST',
        dataType: 'json',
        data: {id_direccion: id_direccion},
        success:function(response){
          if (response.response == 200) {
            $("#optDependencia").empty();
            $.each(response["dependencia"], function(index, val) {
               /* iterate through array or object */
               if (val.id != 1) {
                 $("#optDependencia").append('<option value='+val.id+' title='+val.descripcion+'>'+val.nombre+'</option>');
               }//if
            });
          }//if
          else{
            $("#optDependencia").append('<option value="0" title="No hay dependencias">No hay dependencias asociadas a esta dirección</option>');
          }//else
        },//success
        error:function(data){
          alert("error de conexión con el servidor");

        }//error
      })
      .done(function() {
      })
      .fail(function() {
      })
      .always(function() {
      });
    }//llenarDependencia

    function llenarRoles(id_direccion){
      $("#optRol").empty();//limpiamos el select de roles
      //Recorremos los roles para seleccionar solo los correspondientes a la dirección
      for(i = 0; i < roles.length; i++){
        if (id_direccion == 2) {//desarrollo urbano
          if (roles[i].id != 7 && roles[i].id != 10 && roles[i].id > 3 && roles[i].id < 12 || roles[i].id == 20 || roles[i].id == 23){
            //Validamos que la vista sea de editar no de registrar nuevo funcioanrio
            if (id_rol == roles[i].id) {
              $('#optRol').append('<option value="'+ roles[i].id + '" selected>' + roles[i].nombre +'</option>');
            }//if
            else{
              $('#optRol').append('<option value="'+ roles[i].id + '">' + roles[i].nombre +'</option>');
            }//else
          }//if
        }//if id 2
        else if (id_direccion == 3) {//protección civil
          if (roles[i].id == 4 || roles[i].id == 8 || roles[i].id == 20){
            //Validamos que la vista sea de editar no de registrar nuevo funcioanrio
            if (id_rol == roles[i].id) {
              $('#optRol').append('<option value="'+ roles[i].id + '" selected>' + roles[i].nombre +'</option>');
            }//if
            else{
              $('#optRol').append('<option value="'+ roles[i].id + '">' + roles[i].nombre +'</option>');
            }//else
          }//if
        }//else if dir 3
        else if (id_direccion == 4) {//ayuntamiento
          if (roles[i].id == 4 || roles[i].id == 15 || roles[i].id == 19 || roles[i].id == 20 || roles[i].id == 22 ){
            //Validamos que la vista sea de editar no de registrar nuevo funcioanrio
            if (id_rol == roles[i].id) {
              $('#optRol').append('<option value="'+ roles[i].id + '" selected>' + roles[i].nombre +'</option>');
            }//if
            else{
              $('#optRol').append('<option value="'+ roles[i].id + '">' + roles[i].nombre +'</option>');
            }//else
          }//if
        }//else if 4
        else if (id_direccion == 5) {
          if (roles[i].id >= 3 && roles[i].id <= 4  || roles[i].id == 21 || roles[i].id == 12 || roles[i].id == 13 || roles[i].id == 25 ){
            //Validamos que la vista sea de editar no de registrar nuevo funcioanrio
            if (id_rol == roles[i].id) {
              $('#optRol').append('<option value="'+ roles[i].id + '" selected>' + roles[i].nombre +'</option>');
            }//if
            else{
              $('#optRol').append('<option value="'+ roles[i].id + '">' + roles[i].nombre +'</option>');
            }//else
          }
        }//else if dir 5
      }//for

    }//llenarRoles


</script>
