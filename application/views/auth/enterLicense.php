<body class="stretched">
    <div class="container">
    <form class="form-signin" method="POST" action="<?=base_url()?>Licencia/registro">
        <h2 class="form-signin-heading">Ingresa la clave de la licencia par poder usar la plataforma</h2>
        <p>No puedes hacer uso de la pataforma si no cuentas con la licencia de operación.</p>
        <label for="txtKey" >Clave de Licencia</label>
        <?php if (!empty($this->session->flashdata('message'))): ?>
            <span class="badge badge-danger"><?=$this->session->flashdata('message')?></span>
        <?php endif ?>
        <input type="text" name="txtKey" id="txtKey" class="form-control" placeholder="Clave de licencia" required autofocus>
        <button class=" button button-mini btn btn-lg btn-primary btn-block" type="submit">INGRESAR</button>
    </form>
    </div> <!-- /container -->
</body>