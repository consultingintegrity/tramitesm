<?php if (!$this->session->userdata('logged_in')): ?>
	<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_two_third">
						<div class="widget clearfix">
							<img src="<?= base_url() ?>plantilla/images/img-tequis/logos.png" alt="" class="alignleft" style="margin-top: 1px; padding-right: 18px; border-right: 1px solid #4A4A4A;">
							<p><strong>Municipio de Jalpan de Serra</strong><br>Presidencia Municipal de JALPAN - Administración 2018-2021 </p>
							<p><strong>Dirección:</strong><br> Independencia 12	Col. Centro
							Jalpan de Serra, Querétaro
							C.P. 76340</p>
							
						</div>
					</div>
					<div class="col_one_third col_last">

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">
								<div class="col-md-6 clearfix bottommargin-sm">
								<strong>CONTÁCTO<strong>
									<br>Email: ingresos.jalpan@gmail.com
									<br>Tel: 441-296-02-43 <br>ext 111 y 112</strong><br>
									<strong>Dirección de Finanzas Publicas Municipales<br><br>
									<a class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
										<i class="icon-facebook"></i>
										<i class="icon-facebook"></i>
									</a>
									<a href="https://www.facebook.com/JalpandeSerraMagico/" target="_blank"><small style="display: block; margin-top: 3px;"><strong>Síguenos</strong><br>en Facebook</small></a>
								</div>
								<div class="col-md-6 clearfix bottommargin-sm">
									<a href="<?=base_url()?>Politica/datos" class="">Términos</a><br>
									<a href="<?=base_url()?>Politica/privacidad" class="">política de privacidad</a>
								</div>
						  </div>

					  </div>

					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

				</div>

			</div><!-- #copyrights end -->

	</footer><!-- #footer end -->

	</div><!-- #wrapper end 'images/footer-bg.jpg'-->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

<?php else: ?>
	 	<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						<div class="copyrights-menu copyright-links clearfix">
						<img width="15%" src="<?= base_url() ?>plantilla/images/img-tequis/logos.png">
						</div>
					</div>


				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->
	</div><!-- #wrapper end 'images/footer-bg.jpg'-->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

<?php endif ?>
