

<body class="stretched">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="<?= base_url() ?>" class="retina-logo" ><img src="<?=base_url()?>plantilla/images/img-tequis/logos.png" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->
                </div>
