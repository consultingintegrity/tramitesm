<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">

    <div class="container clearfix">
        <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
          <div class="continer">
            <?php if ($this->session->userdata('rol')[0]->nombre == "Ventanilla"): ?>
              <?php if (!empty($tramites_finalizados)): ?>
                <h3>Trámites pendientes.</h3>
                <label>Para realizar los trámites ingresa a la pestaña de Trámites</label>
                <table id="tableEntrega" name="tableEntrega" cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                  <thead>
                    <tr>
                      <th>Solicitud</th>
                      <th>Trámite</th>
                      <th>Fecha Inicio</th>
                      <th>Ciudadano</th>
                      <th>Correo Electrónico</th>
                      <th>Documento</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($tramites_finalizados as $t) :?>
                    
                    <tr height="20"  id ="fila<?= $t->id ?>">
                      <td height="20" ><?= $t->id?></td>
                      <td height="20" width="279"><img src="<?=base_url().$t->logo ?>" alt="" class="col-sm-5 rounded float-left"> <?= $t->nombre?></td>
                      <td height="20" width="279"><?= $t->fecha_inicio?></td>
                      <td height="20" width="279"><?= $t->primer_nombre." " . $t->segundo_nombre . " " . $t->primer_apellido . " " . $t->segundo_apellido ?></td>
                      <td ><?= $t->correo_electronico?></td>
                      <?php if($t->id_tramite != 5):?>               
                      <td height="20" width="279"><a href="<?= base_url().$t->doc ?>" target='_blank'><i class='fa fa-eye'> Ver Documento</i></a></td>
                      <?php elseif($t->id_tramite == 5):?>
                      <td height="20" width="279">Se publicará en la gaceta municipal</td>
                      <?php endif;?>
                      <td><button class="btn btn-success btnEntrega" type="button" name="btnEntrega<?=$t->id ?>" id="btnEntrega<?=$t->id ?>" value="<?=$t->id ?>" title="Entregar">ENTREGAR</button> </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php else: ?>
                <h3>No tienes trámites pendientes.</h3>
                <label>Los Trámites pendientes se listan en una tabla</label>
              <?php endif; ?>
      <?php endif; ?>
         </div>
  <div class="line"></div>
           </div>
        </div>
</section><!-- #content end -->

<style>
td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}
</style>
