 <style>
  .alert {
      padding: 20px;
      background-color: #f44336;
      color: white;
  }
  .yes {
      padding: 20px;
      background-color: #008000;
      color: white;
  }
  .closebtn {
      margin-left: 15px;
      color: white;
      font-weight: bold;
      float: right;
      font-size: 22px;
      line-height: 20px;
      cursor: pointer;
      transition: 0.3s;
  }

  .closebtn:hover {
      color: black;
  }
</style>


<body class="stretched">

<?php if (!empty($this->session->flashdata('error'))): ?>
  <div class="alert">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>Upps!</strong><?=$this->session->flashdata('error')?>
  </div>

  <?php endif?>
  <?php if (!empty($this->session->flashdata('errorr'))): ?>
  <div class="alert">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>Upps!</strong><?=$this->session->flashdata('errorr')?>
  </div>
  <?php endif?>
<?php if (!empty($this->session->flashdata('good'))): ?>
  <div class="yes">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>Tus datos se han modificado correctamente</strong><?=$this->session->flashdata('good')?>
  </div>

  <?php endif?>

  <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1 align="center">Modifica tus datos <?=$this->session->userdata('primer_nombre');?></h1>

            </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                  <div class="continer">
                    <form name="Funcionario" id="Funcionario" action= "<?=base_url()?>modificarFuncionario/cambiarDatosFuncionario/" method="POST" enctype="multipart/form-data">
                      <div class="form-group col-md-6">
                        <label for="txtImagenPerfil">Selecciona imagen de perfil:</label> <input value="" name="txtImagenPerfil" id="txtImagenPerfil"class="form-control" type="file" required/>
                      </div>
                      <div class="form-group col-md-6">
                        <label for="txtTelefono">Teléfono:</label> <input type="text" name="txtTelefono" id="txtTelefono"class="form-control" required/>
                      </div>
                      <div  align="center">
                             <input type="submit" name="btnModificar" value="Modificar" class="btn btn-lg btn-primary" />
                      </div>
                      
                    </form>
                  </div>
                  <div class="line"></div>
                </div>
            </div>
     <script type="text/javascript">
      
      $("#txtTelefono").val("<?=$this->session->userdata('telefono');?>");
      
     
      
     </script>
  </section><!-- #content end -->
