        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
            </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                 	<div class="continer" id="DataTables_Table_0_wrapper">

                   <button class="btn btn-success btnCortePredial" type="button" name="btnCortePredial" id="btnCortePredial"  title="" >Corte Predial</button>
                   <a class= "btn btn-primary" href="<?=base_url()?>GenerarCortePredial/generaPdfCortePredial" target="_blank">Imprimir Corte de Predial</a>
                    <?php if ($this->session->userdata('rol')[0]->nombre == "Caja"): ?>
                      
                        <?php if (!empty($datospredial)): ?>
                          <h3>Prediales Pendientes</h3>
                          <label>Busca el predial que el ciudadano va a pagar con su clave catastral. Puedes buscar por contenido de la tabla.</label>
                          
                          <div class="table-responsive table--no-card m-b-40">
                          
                          <table class ="tablaPredio table-borderless table-striped table-earning"  id="tablePredial" name="tablePredial" cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                            <thead>
                              <tr>
                                <th>Nombre del Contribuyente</th>
                                <th>Clave Catrastal</th>
                                <th>Folio</th>
                                <th>Linea de Captura</th>
                                <th>Monto a Pagar</th>
                                <th>Fecha Emision</th>
                                <th>Fecha Vencimiento</th>
                                <th>Estatus Vigente</th>
                                <th>Pagado</th>
                                <th>Opciones</th>
                              </tr>
                            </thead>
                            <tbody>
                        <?php foreach($datospredial as $predial) :?>
                              <tr>
                                <td><?=$predial->nombreContribuyente?></td>
                                <td><?=$predial->claveCatrastal?></td>
                                <td><?=$predial->FOLIO?></td>
                                <td><?=$predial->lineaCaptura?></td>
                                <td><?='$ '.$predial->total?></td>
                                <td><?=$predial->fecha_emision?></td>
                                <td><?=$predial->fecha_vencimiento?></td>
                                <td><?=$predial->estatus_vigente?></td>
                                <td style="color:red;"><?=$predial->estatus_pago?></td>
                                <td> 
                                <a class="btn btn-primary"href="<?= base_url().$predial->url?>" target="_blank">ver</a>
                                <?php if($predial->estatus_pago == "No Pagado"):?>
                                <button class="btn btn-success btnPagarPredial" type="button" name="btnPagarPredial" id="btnPagarPredial" value="<?=$predial->id?>" title="Pagar"><i class="fa fa-money fa-2x" aria-hidden="true"></i></button>
                                <?php elseif($predial->estatus_pago=="Pagado"):?>
                                <?php endif;?>
                                </td>                                
                              </tr>
                             <?php endforeach; ?> 
                            </tbody>
                          </table>

                          </div><!-- div table-responsive end -->
                          
                        <?php else: ?>
                          <h3>No se han realizado pagos de predial.</h3>
                          <label>Las prediales pendientes se listan en una tabla</label>

                    <?php endif; ?>
              <?php endif; ?>
					       </div>
					<div class="line"></div>
                   </div>
                </div>
        </section><!-- #content end -->

        <style>
        td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
        </style>
