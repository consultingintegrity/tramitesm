<body class="stretched">
    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">
        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">
            <div id="header-wrap">
                <div class="container clearfix">
                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href=<?=base_url()?>."Funcionario" class="retina-logo" ><img src="<?= base_url() ?>plantilla/images/img-tequis/logos.png" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->
                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">
                      <ul><!--verifico el rol para validar que funcion puede hace cada rol-->
                        <li><a href=<?=base_url()."Funcionario"?>>INICIO</a></li>
                          <?php if($this->session->userdata('rol')[0]->id != 3 && $this->session->userdata('rol')[0]->id != 25): ?>
                          <li><a href="<?=base_url()?>Solicitud/index/4">Trámites</a>
                          <?php if ($this->session->userdata('id_dependencia') == 3 && $this->session->userdata('rol')[0]->id == 8): ?>
                          <li><a href="<?=base_url()?>Funcionario/correcciones">Correcciones</a>
                          <?php endif; ?>
                          <?php endif; ?>
                          <?php if($this->session->userdata('rol')[0]->id == 4 && $this->session->userdata("id_dependencia") == 3): ?>
                            <?php endif; ?>
                          <?php if($this->session->userdata('rol')[0]->id == 3): ?>
                          <li><a href="<?=base_url()?>Funcionario/predial">Pago de Predial</a></li>
                          <!--activar cuando haya tramites-->
<!--                      <li><a href="<?=base_url()?>Funcionario/verCorte">Corte de Caja</a></li>
                          <li><a href="<?=base_url()?>GenerarCorte/generaPdfCorte" target="_blank" rel="noopener noreferrer">Imprimir Corte</a></li>  
-->
                          <?php elseif($this->session->userdata('rol')[0]->id == 4): ?>
                            <li><a href="<?=base_url()?>Solicitud/entrega">Entrega de Trámite</a></li>

                            <li><a href="<?=base_url()?>Solicitud/index/3">Orden de pago</a></li>

                            
                            <?php elseif($this->session->userdata('rol')[0]->id == 7 || 12 ): ?>
                            
                          <?php endif;?>
                            <li><a href="#">
                              <?= $this->session->userdata('primer_nombre')." "; ?> <img src="<?= $this->session->userdata('img'); ?>" width="40" height="40" class="rounded mx-auto d-block" alt="imagen de usuario"></a>
                              <ul>
                                <li><a href="<?=base_url()?>ModificarFuncionario">Mis datos</a></li>
                                <?php if (count($this->session->userdata('rol')) > 1): ?>
                                <li><a href="">Cambiar de ROl</a>
                                  <ul>
                                    <?php foreach ($this->session->userdata('rol') as $rol): ?>
                                      <?php if ($this->session->userdata('rol')[0]->nombre == $rol->nombre): ?>
                                        <li><a href="#"><?=$rol->nombre?> (En Uso) </a></li>
                                        <?php else: ?>
                                        <li><a href="<?=base_url().'Funcionario/switchRole/'.$rol->id?>"><?=$rol->nombre?></a></li>
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                                  </ul>
                                </li>

                                <?php endif; ?>
                                <li><a href="<?= base_url() ?>Auth/log_out">Salir</a></li>
                              </ul>
                            </li>
                        </ul>
                    </nav><!-- #primary-menu end -->
                </div>
            </div>
        </header><!-- #header end -->
