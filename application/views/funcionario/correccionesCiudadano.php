<style>
  .alert {
      padding: 20px;
      background-color: #f44336;
      color: white;
  }
  .yes {
      padding: 20px;
      background-color: #008000;
      color: white;
  }
  .closebtn {
      margin-left: 15px;
      color: white;
      font-weight: bold;
      float: right;
      font-size: 22px;
      line-height: 20px;
      cursor: pointer;
      transition: 0.3s;
  }

  .closebtn:hover {
      color: black;
  }
</style>
<?php if (!empty($this->session->flashdata('error'))): ?>
  <div class="alert">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>Upps!</strong><?=$this->session->flashdata('error')?>
  </div>

  <?php endif?>
<?php if (!empty($this->session->flashdata('good'))): ?>
  <div class="yes">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong><?=$this->session->flashdata('good')?></strong>
  </div>

  <?php endif?>

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">

    <div class="container clearfix">
        <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
          <div class="continer">
            <?php if ($this->session->userdata('rol')[0]->id > 3): ?>
              <?php if (!empty($correcciones)):?>
                <h3>Solicitudes con corecciones.</h3>
                <label>A continuación se muestran las solicitudes con correcciones que tienen que realizar los ciudadanos.</label>
                <p>Selecciona la solicitud que quieres pasar a evaluación.</p>
                <p>
                   <b>A un costado del botón azul, aparece el documento con las correcciones que subió el ciudadano, si no aparece, es porque aún el ciudadano no sube las correcciones de inspección.</b>
                 </p>
                <table id="tableEntrega" name="tableEntrega" cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                  <thead>
                    <tr>
                      <th>Solicitud</th>
                      <th>Trámite</th>
                      <th>Fecha Inicio</th>
                      <th>Correo Electrónico</th>
                      <th>Teléfono</th>
                      <th>Fecha de Corrección</th>
                      <th>Observaciones</th>
                      <th>Tiempo Transcurrido</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($correcciones as $t) :?>

                    <tr height="20"  id ="fila<?= $t->id ?>">
                      <td height="20" ><?= $t->id?></td>
                      <td height="20" width="279"><?= $t->nombre?></td>
                      <td height="20" width="279"><?= date("Y-m-d H:i", strtotime($t->fecha_inicio))?></td>
                      <td ><?= $t->correo_electronico?></td>
                      <td height="20" width="279"><?=$t->telefono ?></td>
                      <td height="20" width="279"><?=$t->fechaCorreccion ?></td>
                      <td height="20" width="279"><?=$t->observaciones ?></td>
                      <?php
                        $fecha_hoy= date_create(date("Y-m-d"));
                        $t->fechaCorreccion=date_create($t->fechaCorreccion);
                        $dias_transcurridos=date_diff($t->fechaCorreccion,$fecha_hoy);
                      ?>
                      <td height="20" width="279"><?=$dias_transcurridos->format("%R%a días")?></td>
                      <td>
                        <a href='<?=base_url().$t->docCorreccionCiudadano?>' class="btn btn-primary" type="button" name="button" title="Correcciones Ciudadano" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                        <?php if (!empty($t->docCorreccionCiudadano)): ?>
                          <button class="btn btn-primary" type="button" name="button" title="Evaluar" onclick="location.href='<?=base_url().'Solicitud/index/6/'.$t->id ?>'"><i class="fa fa-external-link-square" aria-hidden="true"></i></button>
                        <?php endif; ?>
                      </td>

                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php else: ?>
                <h3>No tienes solicitudes con correcciones.</h3>
                <label>Los Trámites pendientes se listan en una tabla</label>
              <?php endif; ?>
      <?php endif; ?>
         </div>
  <div class="line"></div>
           </div>
        </div>
</section><!-- #content end -->

<style>
td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}
</style>
