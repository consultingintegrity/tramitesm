<body class="stretched">

    <!-- Document Wrapper
    ============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="index.html" class="retina-logo" ><img src="<?= base_url() ?>plantilla/images/img-tequis/logos.png" alt="Canvas Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul>
                            <li><a href='<?= base_url()."Usuarios"?>'>Datos personales</a></li>
                            <li><a href='<?= base_url()."Usuarios/index/contacto"?>'>Datos de contacto</a>
                            <li><a href='<?= base_url()."Usuarios/index/imagen"?>'>Imagen</a> </li> 
                          
                      
                          
                            <li><a href='<?= base_url()."Usuarios/index/contra"?>'>Contraseña</a> </li>
                            
                         
                            <li><a href="#"> 
                             <?= $this->session->userdata('primer_nombre')." "; ?> <img src="<?= $this->session->userdata('img'); ?>" width="40" height="40" class="rounded mx-auto d-block" alt="imagen de usuario"></a>
                                <ul>
                                    
                                    <li><a href="<?= base_url() ?>Ciudadano">Regresar a mis tramites</a></li>
                                </ul>
                            </li>
                          
                          
                        </ul>
                    </nav><!-- #primary-menu end -->
                </div>
            </div>
        </header><!-- #header end -->