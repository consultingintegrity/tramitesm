<style>
	.alert {
	    padding: 20px;
	    background-color: #f44336;
	    color: white;
	}
	.yes {
	    padding: 20px;
	    background-color: #008000;
	    color: white;
	}
	.closebtn {
	    margin-left: 15px;
	    color: white;
	    font-weight: bold;
	    float: right;
	    font-size: 22px;
	    line-height: 20px;
	    cursor: pointer;
	    transition: 0.3s;
	}

	.closebtn:hover {
	    color: black;
	}
</style>


<body class="stretched">

<?php if (!empty($this->session->flashdata('error'))): ?>
	<div class="alert">
	  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
	  <strong>Upps!</strong><?=$this->session->flashdata('error')?>
	</div>

	<?php endif?>
<?php if (!empty($this->session->flashdata('good'))): ?>
	<div class="yes">
	  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
	  <strong>Tus datos se han modificado correctamente</strong><?=$this->session->flashdata('good')?>
	</div>

	<?php endif?>

  <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1 align="center">Modifica tus datos <?=$this->session->userdata('primer_nombre');?></h1>

            </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                 	<div class="continer">
                 		<form name="<?=$formulario[0]->name?>" id="<?=$formulario[0]->name?>" action="<?=base_url() . $formulario[0]->action?>" method="<?=$formulario[0]->method?>" action="<?=base_url() . $formulario[0]->action?>"
											<?php foreach ($atributos_formulario as $atributo): ?>
												<?=$atributo->nombre . "=" . $atributo->value?>
											<?php endforeach?>
										>
										<?php foreach ($campos_formulario as $campo): ?>
										<?php if ($campo->atributo == "label"): ?>
											<div class="col-md-6">
												<div class="form-group">
													<label for="login-form-username"><?=$campo->etiqueta?>:</label>
													<?php elseif ($campo->atributo == "input" && $campo->type != "checkbox"): ?>
													<input type="<?=$campo->type?>" id="<?=$campo->name?>" name="<?=$campo->name?>" placeholder="<?=$campo->placeholder?>" class="form-control" <?=$campo->required?>/>
												</div>
											</div>

											<?php elseif ($campo->atributo == "date"): ?>
												<input type="<?=$campo->type?>" id="<?=$campo->name?>" name="<?=$campo->name?>" placeholder="<?=$campo->placeholder?>" class="form-control" <?=$campo->required?>/>
											</div>

										<?php elseif ($campo->atributo == "input" && $campo->type == "checkbox"): ?>
												<input type="<?=$campo->type?>" id="<?=$campo->name?>" name="<?=$campo->name?>" value="<?=$campo->placeholder?>" class="form-check-input" <?=$campo->required?>/>
											</div>
										<?php elseif ($campo->atributo == "select" && $campo->name == "optGenero"): ?>
												<select name="<?=$campo->name?>" id="<?=$campo->name?>" class="form-control">
													<?php if ($this->session->userdata('genero') == "M"): ?>¿
														<option value="M" selected>Masculino</option>
														<option value="F" >Femenino</option>
														<?php elseif ($this->session->userdata('genero') == "F"): ?>
														<option value="M">Masculino</option>
														<option value="F" selected >Femenino</option>
													<?php endif?>
												</select>
											</div>
										</div>
										<?php elseif ($campo->atributo == "select" && $campo->name == "optEstado"): ?>
											<select id="<?=$campo->name?>" name="<?=$campo->name?>" class="form-control">
											</select>
										</div>
									</div>
											<?php elseif ($campo->atributo == "select" && $campo->name == "optMunicipio"): ?>
											<select id="<?=$campo->name?>" name="<?=$campo->name?>" class="form-control">
											</select>
										</div>
									</div>
										<?php elseif ($campo->atributo == "button"): ?>
											<div class="col_full nobottommargin">
												<input type="<?=$campo->type?>" class="button button-3d nomargin" name="<?=$campo->name?>" id="<?=$campo->name?>" value="<?=$campo->etiqueta?>"></input>
											</div>
										<?php endif?>
									<?php endforeach?>	-
									</form>
								</div>
									<div class="line"></div>
                </div>
            </div>
	   <script type="text/javascript">
	   	$(document).ready(function() {
	   	$("#txtPrimerNom").val("<?=$this->session->userdata('primer_nombre');?>");
	   	$("#txtSegundoNom").val("<?=$this->session->userdata('segundo_nombre');?>");
		$("#txtPrimerAp").val("<?=$this->session->userdata('primer_apellido');?>");
   		$("#txtSegundoAp").val("<?=$this->session->userdata('segundo_apellido');?>");
   		$("#txtFecha").val("<?=$this->session->userdata('fecha_nacimiento');?>");
   		$("#txtEmail").val("<?=$this->session->userdata('correo_electronico');?>");
   		$("#txtTelefono").val("<?=$this->session->userdata('telefono');?>");
			llenaEstados();

	   	function llenaMunicipios(id_estado){
  			$.ajax({
	          	url: '<?=base_url()?>Catalogo/getMunicipios/'+id_estado,
	            type: "GET",
	            dataType: "json",
	            success:function(municipios) {
	            	$('#optMunicipio').empty();
								if (municipios.response == 200) {
									$.each(municipios["municipios"], function(index, val) {
									 if (val.id == <?=$this->session->userdata('id_municipio')?>) {
										 $("#optMunicipio").append('<option value='+ val.id +' selected>'+ val.nombre +'</option>');
									 }//if
									 else{
										 $("#optMunicipio").append('<option value='+ val.id +'>'+ val.nombre +'</option>');
									 }//else
								 });//each
								}
								else{
									$("#optMunicipio").append('<option value=0>No Hay Municipios Disponibles</option>');


								}

	            }
	          });

	   	}//llenaMunicipios

	   	function llenaEstados(){
	   			//Obtenemos los estados y llenamos en en el select
	  			$.ajax({
	  			async:true,
	  			url:  '<?=base_url()?>Catalogo/getEstados',
	  			type:  'POST',
	  			dataType: 'json',
	  			success:function(estados){
						$('#optMunicipio').empty();
						if (estados.response  == 200) {
							$.each(estados["estados"], function(index, val) {
								/* iterate through array or object */
								//Si el id de la session es igual al id de los array de estados, selecciono el option
								if (val.id == <?=$this->session->userdata('id_estado')?>) {
									$("#optEstado").append('<option value='+val.id+' selected>'+val.nombre+'</option>');
								}//if
								else{
									$("#optEstado").append('<option value='+val.id+'>'+val.nombre+'</option>');
								}
							});//Each
							llenaMunicipios($("#optEstado").val());
						}
						else{
							$("#optEstado").append('<option value=0>No Hay Estados Disponibles</option>');
						}

	  			}//Success
		  		})
		  		.done(function() {
		  			console.log("success");
		  		})
		  		.fail(function() {
		  			console.log("error");
		  		})
		  		.always(function() {
		  			console.log("complete");
		  		});//ajax
	   		}//llenaEstados
				$( "#optEstado" ).change(function() {
					var id_estado = $("#optEstado").val();
					llenaMunicipios(id_estado);
				});
		});


  	</script>


  </section><!-- #content end -->
