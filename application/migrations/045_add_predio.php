<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_predio extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 17,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'lineaCaptura' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> FALSE,
				),
				
		        'ClaveCatastral' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> FALSE,
		        ),
		        'TipoContribucion' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> FALSE,
		        ),
		        'nombreContribuyente' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> FALSE,
		        ),
		        'ubicacionPredio' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> FALSE,
		        ),
		        'numeroExterior' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> FALSE,
		        ),
		        'letra' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> TRUE,
		        ),
		        'numeroInterior' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> TRUE,
		        ),
		        'coloniaPredio' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'null'	=> TRUE,
		        ),
		        'anoInicialAdeudo' => array(
		                'type' => 'text',
		                'null'	=> FALSE,
		        ),
		        'bimestreInicialAdeudo' => array(
		                'type' => 'text',
		                'null'	=> FALSE,
		        ),
		        'anofinalAdeudo' => array(
		                'type' => 'text',
		                'null'	=> FALSE,
		        ),
		        'bimestrefinalAdeudo' => array(
		                'type' => 'text',
		                'null'	=> FALSE,
		        ),
		        'rezagoAnosAnteriores' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => true,
		                'null'	=> FALSE,
		        ),
		        'rezagoAnoTranscurre' => array(
		                'type' => 'DOUBLE',
				'null'	=> FALSE,
				'unsigned' => true,
		        ),
		        'impuestoAno' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'adicional' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'actualización' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'Recargo' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'requerimientoGastoEjecucion' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'embargoGastosEjecucion' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'multa' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'descuento' => array(
		                'type' => 'DOUBLE',
		                'null'	=> FALSE,
		        ),
		        'total' => array(
		                'type' => 'DOUBLE',
		                'unsigned' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'fechaGeneracion' => array(
		                'type' => 'DATE',
		                'null'	=> FALSE,
		        ),
		        'vigenciaPago' => array(
		                'type' => 'DATE',
		                'null'	=> FALSE,
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		//creamos la tabla
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('predio');

		 //creamos un array con los datos de los paìses
		 //ingresamos el registro en la base de dato
	}//up

	public function down() {
		$this->dbforge->drop_table("predio");		
	}//down

}

/* End of file 045_add_predio.php */
/* Location: ./application/migrations/045_add_predio.php */