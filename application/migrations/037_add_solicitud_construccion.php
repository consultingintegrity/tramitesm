<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Solicitud_Construccion extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'id_solicitud' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_tipoConstruccion' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('solicitud_construccion');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `solicitud_construccion` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `solicitud_construccion` ADD FOREIGN KEY (`id_tipoConstruccion`) REFERENCES `tipo_construccion`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
	}//up

	public function down() {
		$this->dbforge->drop_table("solicitud_construccion");
	}//down

}//class

/* End of file 037_add_solicitud_construccion.php */
/* Location: ./application/migrations/037_add_solicitud_construccion.php */
