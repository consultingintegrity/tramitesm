<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tramite extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'nombre' => array(
	                'type' => 'CHAR',
	                'constraint' => '100',
	                'unique' => FALSE,
	                "null" => FALSE,
	        ),
	        'descripcion' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '190',
	        ),
	        'logo' => array(
	                'type' => 'CHAR',
	                'constraint' => '128',
	        ),
	        'dirigido' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '255',
		),
		'documentoObtenido' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '600',
		),
		'vigencia' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '255',
		),
		'fundamentoJurudico' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '355',
		),
		'status' => array(
	                'type' => 'int',
	                'constraint' => '2',
		),
		'presencial' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '1555',
	    ),
	    'id_dependencia' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	    ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('tramite');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `tramite` ADD FOREIGN KEY (`id_dependencia`) REFERENCES `dependencia`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");

		//creamos un array con los datos del usuario
		$data_tramite = array(
													array("nombre"=>"Alineamiento y número oficial",
													"descripcion" => "Asignación de un número oficial exterior y números oficiales interiores habitacionales, comerciales, de servicios e industriales otorgados a cada predio urbano que cuente con frente a la vía pública señalado en los planes.",
													"logo" => "plantilla/images/icons/tramites/6.png",
													"dirigido" => "Propietario/a de predios o representante legal.",
													"documentoObtenido" => "-Asignación de número oficial
													-Asignación de número oficial en casas.
													-Asignación de número oficial en fraccionamientos.
													-Asignación de número oficial a comercio/empresas.",
													"vigencia" => "Permanente.",
													"fundamentoJurudico" => "Código Urbano del Estado de Querétaro Art. 321, 357, y 358.
																	Código Municipal de Tequisquiapan Art. 431.
																		Reglamento de Construcción para el Municipio de Tequisquiapan Art. 22, 38, 39, 40, y 43.",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.</br> 
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tú trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 2),//1
													array("nombre"=>"Dictamen uso de suelo",
													"descripcion" => "Expedir la autorización del uso o destino que pretenda destinarse a los predios (exceptuando la autorizacion para uso habitacional unifamiliar)",
													"logo" => "plantilla/images/icons/tramites/1.png",
													"dirigido" => "Propietarios/as de predios",
													"documentoObtenido" => "-Obra Nueva
													-Ratificación
													-Regularización
													-Ampliación
													-Modificación
													-Revalidación
													-Reconsideración",
													"vigencia" => "A la actualizacion de los planes parciales de desarrollo urbano",
													"fundamentoJurudico" => "Constitución Política de los Estados Mexicanos Art. 115 fracción V. Ley General de Asentamientos Humanos Art. 9, 15, 28 y 32. Código Urbano del Estado de Querétaro Art. 16 fracción III, 318, 324, 325, 326, 327, 328 y 351.
													Código Municipal de Querétaro Art. 73 fracción IX y último párrafo y 431.
													Reglamento de Construcción para el Municipio de Querétaro Art. 5, 22, 43 y 186.
													Planes y Programas de Desarrollo Urbano.",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.</br> 
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tú trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 2),//2
													array("nombre"=>"Factibilidad de giro",
													"descripcion" => "Determinar si el negocio es viable en el lugar solicitado por el usuario",
													"logo" => "plantilla/images/icons/tramites/5.png",
													"dirigido" => "Ciudadanía en General",
													"documentoObtenido" => "-Apertura de Factibilidad de Giro
													-Modificación de Factibilidad de Giro/alcohol",
													"vigencia" => "1 año calendario",
													"fundamentoJurudico" => "Constitución Política de los Estados Unidos Mexicanos, Art. 115 Fracción V.
													Ley General de Asentamiento Humanos, Art. 9, 15, 28, y 32 .
													Código Urbano del Estado de Querétaro Art. 16 Fracción III, 318, 324, 325, 326, 327, 328 y 351.
													Código Municipal de Querétaro Art. 73 Fracción IX y último párrafo.
													Reglamento de Construcción para el Municipio de Querétaro Art. 5, 22 y 430.
													Planes y Programas de Desarrollo Urbano.
													",
													"status" => 0,													"presencial" => "-Acude al centro de atención especificado.</br>
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tu trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 2),//3
													array("nombre"=>"Licencia de funcionamiento",
													"descripcion" => "Tiene por objeto constatar las instalaciones y documentación para otorgar a establecimientos comerciales una licencia para su correcto funcionamiento.",
													"logo" => "plantilla/images/icons/tramites/4.png",
													"dirigido" => "Ciudadanía en General",
													"documentoObtenido" => "-Apertura de Licencia de Funcionamiento.</br>
													-Renovación de Licencia de Funcionamiento.</br>
													-Modificación a la Licencia de Funcionamiento.</br>
													-Licencia de Funcionamiento (Alcoholes).</br>
													-Apertura de Licencia de Funcionamiento.</br>
													-Renovación de Licencia de Funcionamiento.</br>
													-Modificación a la Licencia de Funcionamiento.",
													"vigencia" => "1 año calendario",
													"fundamentoJurudico" => "Artículos  1, 4, 6, 25 y 26 de la Ley sobre Bebidas Alcohólicas del Estado de Querétaro.
													Artículo 23 fracciones II y III del Reglamento Interior de la Secretaría de Gobierno
													Artículo 53 de la Ley de Hacienda del Estado.
													",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.</br>
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tu trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 5),//4
													array("nombre"=>"Permiso para almacenaje, venta, porteo y consumo de bebidas alcohólicas",
													"descripcion" => "Tiene por objeto constatar las instalaciones y documentación para otorgar a establecimientos un permiso para almacenaje, venta, porteo y consumo de bebidas alcohólicas",
													"logo" => "plantilla/images/icons/tramites/3.png",
													"dirigido" => "El titular de la Licencia Municipal de Funcionamiento",
													"documentoObtenido" => "-Solicitud de permiso provisional para almacenaje, venta y consumo de bebidas alcohólicas
													-Permiso para almacenaje, venta, porteo y consumo de bebidas alcohólicas",
													"vigencia" => "a) Solicitud vigengia determinada.</br>
													b) Permiso 30 dias naturales.",
													"fundamentoJurudico" => "- Constitución Política de los Estados Unidos Mexicanos.
													- Ley Orgánica Municipal del Estado de Querétaro.
													- Ley de Hacienda de los Municipios del Estado de Querétaro.
													- Ley de Ingresos del Municipio de Querétaro.
													- Ley sobre Bebidas Alcohólicas del Estado de Querétaro.
													- Código Municipal de Querétaro.
													- Reglamento para el Almacenaje, Venta, Porteo y Consumo de Bebidas Alcohólicas en el Municipio de Querétaro.
													- Reglamento Interior de la Secretaría de Servicios Públicos Municipales de Querétaro.
													",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.</br>
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tu trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 4),//5
													array("nombre"=>"VoBo de protección civil",
													"descripcion" => "Tiene por objeto constatar las instalaciones y
																							documentación para otorgar a establecimiento el
																							dictamen de que se encuentra en condiciones
																							óptimas para funcionar",
													"logo" => "plantilla/images/icons/tramites/7.png",
													"dirigido" => "Ciudadanía en General",
													"documentoObtenido" => "-Vo.Bo. de Protección Civil
													-Vo.Bo. para la Licencia de Funcionamiento
													-Vo.Bo para Establecimientos",
													"vigencia" => "validez desde la fecha de su expedición y tiene una vigencia hasta el 31 de Diciembre del presente año.",
													"fundamentoJurudico" => " 	",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.</br>
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tu trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 3),//6
													array("nombre"=>"Licencia de construcción",
													"descripcion" => "Autorización oficial por escrito para que se inicien las obras de un proyecto de edificación,
																						según los planos y especificaciones, después de pagar las tasas y derechos que correspondan.
																						También llamada licencia para construcción, permiso de edificación, permiso de obras, permiso para construcción.",
													"logo" => "plantilla/images/icons/tramites/2.png",
													"dirigido" => "Ciudadanía en General",
													"documentoObtenido" => "-Acabados
													-Aviso de Terminación de Obra
													-Bardeo
													-Casa Habitación
													-Modificación de Fachada
													-Obra Menor
													-Regularización de Obra
													-Obra nueva
													-Ampliación, modificación, remodelación
													-Revalidación",
													"vigencia" => "Determinada.",
													"fundamentoJurudico" => " 	",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.</br>
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tu trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 2),//7

													array("nombre"=>"Informe de Uso de Suelo",
													"descripcion" => "Documento administrativo de carácter informativo en el que se hace constar la normatividad aplicable en materia de usos de suelo, normas generales y particulares determinadas para un predio, lote o edificación en función de la zonificación secundaria correspondiente. Por su naturaleza carece de validez y vigencia jurídica sin causar derechos adquiridos.",
													"logo" => "plantilla/images/icons/tramites/8.png",
													"dirigido" => "Ciudadanía en General",
													"documentoObtenido" => "-Acabados
													-Aviso de Terminación de Obra
													-Bardeo
													-Casa Habitación
													-Modificación de Fachada
													-Obra Menor
													-Regularización de Obra
													-Obra nueva
													-Ampliación, modificación, remodelación
													-Revalidación",
													"vigencia" => "",
													"fundamentoJurudico" => " 	",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.</br>
													-Preséntate en la Ventanilla de Atención.</br>
													-Entrega al encargado de la ventanilla los requisitos indicados.</br>
													-Realiza el pago de tu trámite.</br>
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 2),//8

													array("nombre"=>"Permiso provicional para almacenaje, venta, porteo y consumo de bebidas alcohólicas",
													"descripcion" => "Tiene por objeto constatar las instalaciones y documentación para otorgar a establecimientos un permiso para almacenaje, venta, porteo y consumo de bebidas alcohólicas",
													"logo" => "plantilla/images/icons/tramites/3.png",
													"dirigido" => "El titular de la Licencia Municipal de Funcionamiento",
													"documentoObtenido" => "-Solicitud de permiso provisional para almacenaje, venta y consumo de bebidas alcohólicas
													-Permiso para almacenaje, venta, porteo y consumo de bebidas alcohólicas",
													"vigencia" => "La que corresponda al pago de derechos, según la última expedición",
													"fundamentoJurudico" => "- Constitución Política de los Estados Unidos Mexicanos.
													- Ley Orgánica Municipal del Estado de Querétaro.
													- Ley de Hacienda de los Municipios del Estado de Querétaro.
													- Ley de Ingresos del Municipio de Querétaro.
													- Ley sobre Bebidas Alcohólicas del Estado de Querétaro.
													- Código Municipal de Querétaro.
													- Reglamento para el Almacenaje, Venta, Porteo y Consumo de Bebidas Alcohólicas en el Municipio de Querétaro.
													- Reglamento Interior de la Secretaría de Servicios Públicos Municipales de Querétaro.
													",
													"status" => 0,
													"presencial" => "-Acude al centro de atención especificado.
													-Preséntate en la Ventanilla de Atención.
													-Entrega al encargado de la ventanilla los requisitos indicados.
													-Realiza el pago de tu trámite.
													-Espera la resolución y acude por tu respuesta en la fecha indicada.",
													"id_dependencia" => 4),//9

		);
		//ingresamos el registro en la base de datos
		$this->db->insert_batch("tramite", $data_tramite);
	}//up

	public function down() {
		$this->dbforge->drop_table("tramite");
	}//down

}//class

/* End of file 016_add_tramite.php */
/* Location: ./application/migrations/016_add_tramite.php */
