<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_etiqueta extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		    'id' => array(
		            'type' => 'INT',
		            'constraint' => 11,
		            'unsigned' => TRUE,
		            'auto_increment' => TRUE,
		    ),
		    'nombre' => array(
		            'type' => 'VARCHAR',
		            'unique' => TRUE,
		            'constraint' => '65',
		            'null'	=> FALSE,
		    ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('etiqueta');
		//creamos lo registros
		$data_label = array(
			array("nombre"=>"Correo electrónico"),//1
			array("nombre"=>"Contraseña"),//2
			array("nombre"=>"Entrar"),//3
			array("nombre"=>"¿Olvidaste tu Contraseña?"),//4
			array("nombre"=>"Recordarme"),//5
			array("nombre"=>"Primer Nombre"),//6
			array("nombre"=>"Segundo Nombre"),//7
			array("nombre"=>"Primer Apellido"),//8
			array("nombre"=>"Segundo Apellido"),//9
			array("nombre"=>"Confirma tu Contraseña"),//10
			array("nombre"=>"Registrar"),//11
			array("nombre"=>"Género"),//12
			array("nombre"=>"Contraseña (actual)"),//13
			array("nombre"=>"Nueva Contraseña"),//14
			array("nombre"=>"Verificar Contraseña"),//15
			array("nombre"=>"Modificar Contraseña"),//16
			array("nombre"=>"ACEPTAR"),//17
			array("nombre"=>"Estado"),//18
			array("nombre"=>"Municipio"),//19
			array("nombre"=>"Número Telefónico"),//20
			array("nombre"=>"Seleccione una Imagen de Perfil"),//21
			array("nombre"=>"Rol"),//22
			array("nombre"=>"Dependencia"),//23
			array("nombre"=>"Dirección"),//24
			array("nombre"=>"Lote"),//25
			array("nombre"=>"Manzana"),//26
			array("nombre"=>"Calle"),//27
			array("nombre"=>"Colonia"),//28
			array("nombre"=>"Nombre del Propietario"),//29
			array("nombre"=>"El Propietario del Predio con Domicilio En"),//30
			array("nombre"=>"Clave Catastral"),//31
			array("nombre"=>"Fecha de nacimiento"),//32
			array("nombre"=>"Inspección"),//33
			array("nombre"=>"Clave catastral de inmueble"),//34
			array("nombre"=>"Área de trabajo"),//35
			array("nombre"=>"Tipo de dictamen"),//36
			array("nombre"=>"fax"),//37
			array("nombre"=>"Tipo de solicitud"),//38
			array("nombre"=>"Obra nueva"),//39
			array("nombre"=>"Ratificación"),//40
			array("nombre"=>"Regularización"),//41
			array("nombre"=>"Ampliación"),//42
			array("nombre"=>"Modificación"),//43
			array("nombre"=>"Revalidación"),//44
			array("nombre"=>"Reconcideración"),//45
			array("nombre"=>"Habitacional"),//46
			array("nombre"=>"No habitacional"),//47
			array("nombre"=>"Comercial"),//48
			array("nombre"=>"Servicios"),//49
			array("nombre"=>"Cantidad"),//50
			array("nombre"=>"Tipo de funcionamiento"),//51
			array("nombre"=>"Descripción"),//52
			array("nombre"=>"Número"),//53
			array("nombre"=>"Delegación"),//54
			array("nombre"=>"Teléfono de oficina"),//55
			array("nombre"=>"Extensión"),//56
			array("nombre"=>"Nº y letra ext"),//57
			array("nombre"=>"Nº o letra interior"),//58
			array("nombre"=>"Código postal"),//59
			array("nombre"=>"RFC"),//60
			array("nombre"=>"Nombre o razón social"),//61
			array("nombre"=>"Entre las calles de"),//62
			array("nombre"=>"Capital de inversión"),//63
			array("nombre"=>"Horario de trabajo (Apertura)"),//64
			array("nombre"=>"Dictamen uso de suelo Nº"),//65
			array("nombre"=>"Fecha dictamen"),//66
			array("nombre"=>"Nº de acta constitutiva"),//67
			array("nombre"=>"Fecha de alta"),//68
			array("nombre"=>"Número notaria"),//69
			array("nombre"=>"Dictamen protección civil"),//70
			array("nombre"=>"Licencia de alcoholes Nº"),//71
			array("nombre"=>"Fecha de expedición"),//72
			array("nombre"=>"Descripción de las actividades"),//73
			array("nombre"=>"Empleos generados"),//74
			//Etiquetas para formulario de construcciónote
			array("nombre"=>"Destino De La Obra"),//75
			array("nombre"=>"Domicilio"),//76
			array("nombre"=>"Número Oficial"),//77
			array("nombre"=>"Localidad"),//78
			array("nombre"=>"Número De Contrato De Agua"),//79
			array("nombre"=>"Dictamen De Uso De Suelo No."),//80
			array("nombre"=>"Revisión De Proyecto No."),//81
			//PERITO RESPONABLE
			array("nombre"=>"PERITO RESPONSABLE"),//82
			array("nombre"=>"SUPERFICIE M2"),//83
			array("nombre"=>"Superficie del terreno"),//84
			array("nombre"=>"Sótano"),//85
			array("nombre"=>"Planta Baja"),//86
			array("nombre"=>"1er. Nivel"),//87
			array("nombre"=>"2do. Nivel"),//88
			array("nombre"=>"Mezzanine"),//89
			array("nombre"=>"Área Verde"),//90
			array("nombre"=>"Volados"),//91
			array("nombre"=>"Estacionamiento"),//92
			array("nombre"=>"Otros"),//93
			array("nombre"=>"Total de M2 Construidos"),//94
			array("nombre"=>"TIPO DE DESCARGA"),//95
			array("nombre"=>"Aguas Residuales y Pluviales"),//96
			array("nombre"=>"-Drenaje Municipal"),//97
			array("nombre"=>"FOSA SÉPTICA"),//98
			array("nombre"=>"TIPO DE SUMINISTRO DE GAS"),//99
			array("nombre"=>"Domiciliario"),//100
			array("nombre"=>"Entubado"),//101
			array("nombre"=>"Estacionario"),//102
			array("nombre"=>"-Otros"),//103
			array("nombre"=>"TIPO DE GUARNICIÓN Y BANQUETA"),//104
			array("nombre"=>"-Concreto"),//105
			array("nombre"=>"-Empedrado"),//106
			array("nombre"=>"-Adoquín"),//107
			array("nombre"=>"-Adocreto"),//108
			array("nombre"=>"Otros."),//109
			array("nombre"=>"PAVIMENTOS Y ARROYOS"),//110
			array("nombre"=>"Cédula Profesional"),//111
			array("nombre"=>"Clave Del Colegio Respectivo"),//112
			array("nombre"=>"PROPIETARIO"),//113
			array("nombre"=>"Da Aviso De Terminación De La Obra Consistente En"),//114
			array("nombre"=>"Ubicación"),//115
			array("nombre"=>"Colonia o Fraccionamiento"),//116
			array("nombre"=>"Dictamen Uso De Suelo No."),//117
			array("nombre"=>"Licencia De Construcción No."),//118
			array("nombre"=>"Expedida El"),//119
			array("nombre"=>"Y Revalidación El"),//120
			array("nombre"=>"Con un Valor Estimado De"),//121
			array("nombre"=>"REVISO"),//122
			array("nombre"=>"SUPERIOR DE OBRA"),//123
			array("nombre"=>"AUTORIZACIÓN"),//124
			array("nombre"=>"DIR. DE DESARROLLO URBANO Y VIVIENDA"),//125
			array("nombre"=>"Fecha"),//126
			array("nombre"=>"CONCEPTO"),//127
			array("nombre"=>"3er. Nivel"),//128
			array("nombre"=>"INFORMACIÓN ADICIONAL ESTADÍSTICA"),//129
			array("nombre"=>"(Capacidad)"),//130
			array("nombre"=>"Asfalto"),//131
			array("nombre"=>"Número De Licencia Anterior"),//132
			array("nombre"=>"Horario de trabajo (Cierre)"),//133
			array("nombre"=>"Dictamen"),//134
			array("nombre"=>"Informe"),//135
			array("nombre"=>"Nombre de Negocio"),//136
			array("nombre"=>"Nombre"),//137
			//etiqueta para razón social
			array("nombre"=>"Razón Social"),//138
		);
		//insertamos lo datos
		$this->db->insert_batch("etiqueta",$data_label);

	}//up

	public function down() {
		$this->dbforge->drop_table("etiqueta");
	}//down

}//class

/* End of file 013_add_etiqueta.php */
/* Location: ./application/migrations/013_add_etiqueta.php */
