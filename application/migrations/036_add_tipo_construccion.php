<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Tipo_Construccion extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'tipo' => array(
	                'type' => 'CHAR',
	                'constraint' => '100',
                  'unique'=>TRUE,
	                "null" => FALSE),
        'descripcion' => array(
                'type' => 'CHAR',
                'constraint' => '100',
                "null" => TRUE)
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('tipo_construccion');
		//Agregamos registros a la tabla
		$data_giro = array(
			array("tipo"=> "Obra Menor","descripcion"=>""),
  		array("tipo"=> "Bardeo","descripcion"=>""),
  		array("tipo"=> "Acabados","descripcion"=>""),
  		array("tipo"=> "Modificación de Fachada","descripcion"=>""),
  		array("tipo"=> "Casa Habitación","descripcion"=>""),
  		array("tipo"=> "Ampliación, Modificación, Remodelación y Revalidación","descripcion"=>""),
  		array("tipo"=> "Aviso de Terminación de Obra","descripcion"=>""),
  		array("tipo"=> "Regularización de Obra","descripcion"=>""),
		);//data_giro
		//Insertamos los datos a la base de datos
		$this->db->insert_batch("tipo_construccion",$data_giro);
	}//up

	public function down() {
		$this->dbforge->drop_table("tipo_construccion");
	}//down

}//class

/* End of file 036_add_fase.php */
/* Location: ./application/migrations/021_add_tipo_construccion.php */
