<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_documento_corte extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),

            'id_corte' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),

            'id_cortepredial' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),

  	        'monto_total' => array(
                'type' => 'CHAR',
                'constraint' => 11,
                'null' => false,
              ),
            
            'monto_total' => array(
                'type'     => 'DOUBLE',
                'unsigned' => true,
                'null'     => false,
              ),
            'tipo_corte' => array(
                'type' => 'CHAR',
                'constraint' => 11,
                'null' => false,
            ),
            'url' => array(
                'type' => 'CHAR',
	            'constraint' => '180',
            ),
            'documento_generado' => array(
                'type' => 'INT',
                'constraint' => 2,
                'null' => false,
              ),        


        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('documento_corte');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `documento_corte` ADD FOREIGN KEY (`id_corte`) REFERENCES `corte`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `documento_corte` ADD FOREIGN KEY (`id_cortepredial`) REFERENCES `cortepredial`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");   
        //	$this->db->query("ALTER TABLE `pago` CHANGE `fecha_vencimiento` `fecha_vencimiento` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("documento_corte");
    } //down

} //class

/* End of file 033_add_pago.php */
/* Location: ./application/controllers/033_add_pago.php */
