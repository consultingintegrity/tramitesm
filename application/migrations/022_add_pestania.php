<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_pestania extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'nombre' => array(
	                'type' => 'CHAR',
	                'constraint' => 100,
	        ),
	        'descripcion' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '200',
	        ),
	        'id_documento' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('pestania');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `pestania` ADD FOREIGN KEY (`id_documento`) REFERENCES `documento`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//Creamos los registros para la tabla
		$data_pestania = array(
				array("Nombre"=>"SOLICITUD","descripcion"=>"En esta sección se subirán todos los datos correspondientes a la solicitud del trámite seleccionado","id_documento"=>1),//1
				array("Nombre"=>"REQUISITOS","descripcion"=>"En esta sección se subirán todos los datos correspondientes a la solicitud del trámite seleccionado","id_documento"=>1),//2
				array("Nombre"=>"INSPECCIÓN","descripcion"=>"En esta sección se suben los archivos de la inspección realizada","id_documento"=>1),//3
				array("Nombre"=>"REVISIÓN DE INSPECCIÓN","descripcion"=>"En esta sección se revisan los archivos de la inspección realizada","id_documento"=>1),//4
				array("Nombre"=>"ASIGNACIÓN DE NÚMERO OFICIAL","descripcion"=>"En esta sección otorga el número oficial","id_documento"=>1),//5
				array("Nombre"=>"FIRMA DIRECTOR ","descripcion"=>"En esta sección el secretario firma la licencia","id_documento"=>1),//6
				array("Nombre"=>"RECEPCIÓN DE DOCUMENTO FIRMADO","descripcion"=>"En esta sección se recibe la licencia ya firmada","id_documento"=>1),//7
				array("Nombre"=>"GENERACIÓN DE ORDEN DE PAGO","descripcion"=>"En esta sección se genera la orden de pago de la licencia ","id_documento"=>1),//8
				array("Nombre"=>"FIRMA SECRETARIO ","descripcion"=>"En esta sección el director firma la licencia","id_documento"=>1),//9
				array("Nombre"=>"ELABORACIÓN DICTAMEN USO DE SUELO ","descripcion"=>"En esta sección elabora el Dictamen Uso de Suelo","id_documento"=>1),//10
				array("Nombre"=>"ELABORACIÓN VISTO BUENO PROTECCIÓN CIVIL","descripcion"=>"En esta sección elabora el visto bueno protección civil","id_documento"=>1),//11
				array("Nombre"=>"ORDEN DE PAGO","descripcion"=>"Muestra la orden de pago","id_documento"=>1),//12
				array("Nombre"=>"ELABORACIÓN LICENCIA DE FUNCIONAMIENTO","descripcion"=>"En esta sección elabora la licencia de funcionamiento","id_documento"=>1),//13
				array("Nombre"=>"FIRMA SECRETARIO DE FINANZAS PÚBLICAS MUNICIPALES","descripcion"=>"En esta sección el tesorero firma la licencia","id_documento"=>1),//14
				array("Nombre"=>"FIRMA DE PRESIDENTE MUNICIPAL","descripcion"=>"En esta sección el presidente municipal firma la licencia","id_documento"=>1),//15
				array("Nombre"=>"ELABORACIÓN DE FACTIBILIDAD DE GIRO","descripcion"=>"En esta sección elabora factibilidad de giro","id_documento"=>5),//16
				array("Nombre"=>"ELABORACIÓN DE LICENCIA DE CONSTRUCCIÓN","descripcion"=>"En esta sección elabora licencia de construcción","id_documento"=>6),//17
				array("Nombre"=>"ELABORACIÓN DE INFORME USO DE SUELO","descripcion"=>"En esta sección se elabora el informe uso de suelo","id_documento"=>7),//18
				array("Nombre"=>"OPINIÓN TÉCNICA","descripcion"=>"En esta sección se sube el documento de opinión técnica","id_documento"=>7),//19
				array("Nombre"=>"REVISIÓN DE OPINIÓN TÉCNICA","descripcion"=>"En esta sección se revisan los archivos de la opinión técnica","id_documento"=>1),//20
				array("Nombre"=>"DICTAMEN","descripcion"=>"En esta sección se sube el dictamen","id_documento"=>1),//21
				array("Nombre"=>"REVISIÓN DE DICTAMEN","descripcion"=>"En esta sección se revisan los archivos de el dictamen","id_documento"=>1),//22
				array("Nombre"=>"INFORME DE CABILDO","descripcion"=>"En esta sección se sube el archivo del resultado de cabildo","id_documento"=>1),//23
				array("Nombre"=>"ACUERDO DE CABILDO","descripcion"=>"En esta sección se revisa el archivo del resulado de cabildo","id_documento"=>1),//24
				array("Nombre"=>"SOLICITAR OPINIÓN TÉCNICA","descripcion"=>"En esta sección se solicita opinión técnica a las áreas correspondientes","id_documento"=>7),//25
				array("Nombre"=>"DOCUMENTO OPINIÓN TÉCNICA","descripcion"=>"En esa parte se sube el archivo de la opinión técnica","id_documento"=>8),//26
				array("Nombre"=>"PERMISO PROVICIONAL DE ALCOHOL","descripcion"=>"En esa parte se sube el archivo de permiso de alcohol provicional","id_documento"=>9),//27
				array("Nombre"=>"PERMISO PROVICIONAL DE ALCOHOL","descripcion"=>"En esa parte se muestra el archivo de permiso de alcohol provicional","id_documento"=>9),//28
				array("Nombre"=>"FIRMA SUBDIRECTOR DU","descripcion"=>"Firma SubDirector","id_documento"=>1),//29

		);
		//insertamos los datos a la tabla
		$this->db->insert_batch("pestania",$data_pestania);
	}//up

	public function down() {
		$this->dbforge->drop_table("pestania");
	}//down

}//class

/* End of file 022_add_pestaña.php */
/* Location: ./application/migrations/022_add_pestaña.php */
