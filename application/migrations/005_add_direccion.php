<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_direccion extends CI_Migration {

public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'nombre' => array(
		                'type' => 'VARCHAR',
		                'unique' => TRUE,
		                'constraint' => '65',
		                'null'	=> FALSE,
		        ),
		        'descripcion' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '255',
		                'null'	=> TRUE,
		        ),
		        'status' => array(
		                'type' => 'int',
		                'constraint' => '2',
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('direccion');
		//Agregamos los campos iniciales para la tabla de direccón
		$campos_direccion = array(
		/*	array("nombre"=>"Dependencìa de Administrador!","id_direccion"=>1),//1
			array("nombre"=>"Desarrollo Urbano","id_direccion"=>2),//2
			array("nombre"=>"Protección Civil","id_direccion"=>3),//3
			array("nombre"=>"Ayuntamiento","id_direccion"=>4),//4
			array("nombre"=>"Ingresos","id_direccion"=>5),//5*/

			array("nombre"=>"H. Ayuntamiento", "descripcion"=>"Controlar las funciones de todo el sistema","status"=>0),//1
			array("nombre"=>"Secretaría de Desarrollo Urbano, Vivienda y Obras Públicas", "descripcion"=>"Dirigir e implementar políticas públicas para el desarrollo económico, desarrollo urbano, desarrollo rural y desarrollo agropecuario, en un marco de sustentabilidad ambiental","status"=>0),//2
			array("nombre"=>"Protección Civil", "descripcion"=>"Realizar acciones en función de los requerimientos de la ciudadanía, buscando el bien común, preservando la seguridad, los recursos naturales y promoviendo el desarrollo humano.","status"=>0),//3
			array("nombre"=>"Secretaría de Ayuntamiento", "descripcion"=>"Licencias de Funcionamiento.","status"=>0),//4
			array("nombre"=>"Secretaría de Finanzas Publicas Municipales", "descripcion"=>"Ingresos.","status"=>1),//5

		);
		//insertamos lod datos en ls DB
		$this->db->insert_batch("direccion", $campos_direccion);
	}//up

	public function down() {
		$this->dbforge->drop_table("direccion");
	}//down

}//class

/* End of file 006_add_direccion.php */
/* Location: ./application/migrations/006_add_direccion.php */
