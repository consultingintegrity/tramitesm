<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_concepto extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'concepto' => array(
	                'type' => 'CHAR',
	                'constraint' => '128',
	                'unique' => TRUE,
	                "null" => FALSE,
	        ),
	        'costo' => array(
	                'type' => 'DOUBLE',
	                'constraint' => '4,4',
	        ),
	        'id_tramite' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);		
		//creamos la tabla
		$this->dbforge->create_table('concepto');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `concepto` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");			
	}//up

	public function down() {
		$this->dbforge->drop_table("concepto");
	}//down

}//class

/* End of file 017_add_concepto.php */
/* Location: ./application/migrations/017_add_concepto.php */