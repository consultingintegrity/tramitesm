<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_pago extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),

            'id_solicitud' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
            'id_tramite'   => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
            'url'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '80',
                'null'       => false,
            ),
  	        'fecha_emision' => array(
  	                'type' => 'TIMESTAMP',
  	                "null" => TRUE,
  	        ),
  	        'fecha_vencimiento' => array(
  	                'type' => 'DATE',
  	                'null' => TRUE,
            ),
            'folio'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '30',
                'null'       => false,
            ),
  	        'linea_captura' => array(
                      'type' => 'VARCHAR',
                      'constraint' => '30',
  	                'null' => false,
  	        ),
            

        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('pago');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `pago` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `pago` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    		$this->db->query("ALTER TABLE `pago` CHANGE `fecha_emision` `fecha_emision` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
    	//	$this->db->query("ALTER TABLE `pago` CHANGE `fecha_vencimiento` `fecha_vencimiento` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("pago");
    } //down

} //class

/* End of file 033_add_pago.php */
/* Location: ./application/controllers/033_add_pago.php */
