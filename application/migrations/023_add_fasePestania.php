<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fasePestania extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'estatus' => array(
	                'type' => 'TINYINT',
	                'constraint' => '10',
	        ),
	        'id_fase' => array(
	                'type' => 'INT',
	                'constraint' => '11',
	                'unsigned' => TRUE,
	        ),
	        'id_pestania' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('fase_pestania');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `fase_pestania` ADD FOREIGN KEY (`id_fase`) REFERENCES `fase`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `fase_pestania` ADD FOREIGN KEY (`id_pestania`) REFERENCES `pestania`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//Creamos los datos para la tabla
		$data_fase_pestania = array(
			//tramite NO oficial
			//fase 1
			//sube informacion
			array("estatus"=>1,"id_fase"=>1,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>1,"id_pestania"=>2),
			//fase 2
			//valida informacion
			array("estatus"=>1,"id_fase"=>2,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>2,"id_pestania"=>2),
			//fase 3
			//realiza inspeccion
			array("estatus"=>1,"id_fase"=>3,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>3,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>3,"id_pestania"=>3),
			//fase 4
			//valida inspeccion
			array("estatus"=>1,"id_fase"=>4,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>4,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>4,"id_pestania"=>4),
			//fase 5
			//genera licencia
			array("estatus"=>1,"id_fase"=>5,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>5,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>5,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>5,"id_pestania"=>5),
			//fase 6
			//firma de sub director
			array("estatus"=>1,"id_fase"=>6,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>6,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>6,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>6,"id_pestania"=>29),
			//fase 7
			//firma de director
			array("estatus"=>1,"id_fase"=>7,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>7,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>7,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>7,"id_pestania"=>6),
			//fase 8
			//se pasas a ventanilla genera orden de pago
			array("estatus"=>1,"id_fase"=>8,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>8,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>8,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>8,"id_pestania"=>7),
			array("estatus"=>1,"id_fase"=>8,"id_pestania"=>8),
			//fase 9
			//se pasa a caja para pagar
			//fase 10
			//se entrega la licencia

			//Dictamen Uso de Suelo
			//fase 11 N°1
			//sube informacion
			array("estatus"=>1,"id_fase"=>11,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>11,"id_pestania"=>2),
			//fase 12 N°2
			//Revisión de información
			array("estatus"=>1,"id_fase"=>12,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>12,"id_pestania"=>2),
			//fase 13 N° 3
			//realiza inspeccion
			array("estatus"=>1,"id_fase"=>13,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>13,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>13,"id_pestania"=>3),
			//fase 14 N°4
			//valida inspeccion
			array("estatus"=>1,"id_fase"=>14,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>14,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>14,"id_pestania"=>4),
			//fase 15 N°5
			//genera licencia
			array("estatus"=>1,"id_fase"=>15,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>15,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>15,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>15,"id_pestania"=>10),
			//fase 16 N°6
			//firma de sub director
			array("estatus"=>1,"id_fase"=>16,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>16,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>16,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>16,"id_pestania"=>6),
			//fase 17 N°7
			//firma de director
			array("estatus"=>1,"id_fase"=>17,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>17,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>17,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>17,"id_pestania"=>9),
			//fase 18 N°8
			//se pasas a ventanilla genera orden de pago
			array("estatus"=>1,"id_fase"=>18,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>18,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>18,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>18,"id_pestania"=>7),
			array("estatus"=>1,"id_fase"=>18,"id_pestania"=>8),

			//Visto bueno proteccion civil
			//fase 21 N°1
			//sube informacion
			array("estatus"=>1,"id_fase"=>21,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>21,"id_pestania"=>2),
			//fase 22 N°2
			//Revisión de información
			array("estatus"=>1,"id_fase"=>22,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>22,"id_pestania"=>2),
			//fase 23 N° 3
			//realiza inspeccion
			array("estatus"=>1,"id_fase"=>23,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>23,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>23,"id_pestania"=>3),
			//fase 24 N°4
			//ventanilla genera orden de pago
			array("estatus"=>1,"id_fase"=>24,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>24,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>24,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>24,"id_pestania"=>8),
			//fase 26
			//se pasa a caja para pagar
			array("estatus"=>1,"id_fase"=>26,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>26,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>26,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>26,"id_pestania"=>12),
			array("estatus"=>1,"id_fase"=>26,"id_pestania"=>11),
			//fase 27 noFase = 7
			//Ventanilla entrega licencia emitida

			//fase 10
			//se entrega el documento

			//tramiteLicencia de funcionamiento
			//fase 27 N°1
			array("estatus"=>1,"id_fase"=>28,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>28,"id_pestania"=>2),
			//fase 28 N°2
			//Revisión de información
			array("estatus"=>1,"id_fase"=>29,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>29,"id_pestania"=>2),
			//fase 29 N° 3
			//realiza inspeccion
			array("estatus"=>1,"id_fase"=>30,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>30,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>30,"id_pestania"=>3),
			//fase 30 N°4
			//ventanilla genera orden de pago
			array("estatus"=>1,"id_fase"=>31,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>31,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>31,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>31,"id_pestania"=>8),
			//fase 31 N°5
			//se pasa a caja para pagar
			//fase 32 N°6
			//genera licencia
			array("estatus"=>1,"id_fase"=>33,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>33,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>33,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>33,"id_pestania"=>12),
			array("estatus"=>1,"id_fase"=>33,"id_pestania"=>13),
			//fase 33 N°7
			//firma tesorero
			array("estatus"=>1,"id_fase"=>34,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>34,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>34,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>34,"id_pestania"=>14),
			//fase 34 N°8
			//firma presidente
			array("estatus"=>1,"id_fase"=>35,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>35,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>35,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>35,"id_pestania"=>15),

			//tramite factibilidad de giro
			//fase 1
			//sube informacion
			array("estatus"=>1,"id_fase"=>37,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>37,"id_pestania"=>2),
			//fase 2
			//valida informacion
			array("estatus"=>1,"id_fase"=>38,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>38,"id_pestania"=>2),
			//fase 3
			//iNSPECTOR REALIZA INSPECCIÓN
			array("estatus"=>1,"id_fase"=>39 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>39,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>39,"id_pestania"=>3),
			//fase 4
			//jefe de inspeccion valida informacion
			array("estatus"=>1,"id_fase"=>40 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>40,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>40,"id_pestania"=>4),
			//fase 4
			//ventanilla genera factibilidad de giro
			array("estatus"=>1,"id_fase"=>41,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>41,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>41,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>41,"id_pestania"=>16),
			//fase 5
			//firma director
			array("estatus"=>1,"id_fase"=>42,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>42,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>42,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>42,"id_pestania"=>6),
			//fase 6
			//se pasas a ventanilla genera orden de pago
			array("estatus"=>1,"id_fase"=>43,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>43,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>43,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>43,"id_pestania"=>7),
			array("estatus"=>1,"id_fase"=>43,"id_pestania"=>8),
			//fase 7
			//se pasa a caja para pagar
			//fase 8
			//se entrega la licencia

			//##########################################################
			// Petañas relacionadas con licencia de construcción
			//##########################################################
			//fase 1
			//sube informacion
			array("estatus"=>1,"id_fase"=>46,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>46,"id_pestania"=>2),
			//fase 2
			//Ventillaa valida informacion
			array("estatus"=>1,"id_fase"=>47,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>47,"id_pestania"=>2),
			//fase 3
			//iNSPECTOR REALIZA INSPECCIÓN
			array("estatus"=>1,"id_fase"=>48 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>48,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>48,"id_pestania"=>3),
			//fase 4
			//jefe de inspeccion valida informacion
			array("estatus"=>1,"id_fase"=>49 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>49,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>49,"id_pestania"=>4),
			//fase 5
			//ventanilla genera factibilidad de giro
			array("estatus"=>1,"id_fase"=>50,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>50,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>50,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>50,"id_pestania"=>17),
			//fase 6
			//firma director
			array("estatus"=>1,"id_fase"=>51,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>51,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>51,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>51,"id_pestania"=>6),
			//fase 7
			//se pasas a ventanilla genera orden de pago
			array("estatus"=>1,"id_fase"=>52,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>52,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>52,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>52,"id_pestania"=>7),
			array("estatus"=>1,"id_fase"=>52,"id_pestania"=>8),
			//fase 8
			//se pasa a caja para pagar
			//fase 9
			//se entrega la licencia

			/*Petañas para el tramite informe uso de suelo*/
			//fase 1
			//sube informacion
			array("estatus"=>1,"id_fase"=>55,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>55,"id_pestania"=>2),
			//fase 2
			//Ventillaa valida informacion
			array("estatus"=>1,"id_fase"=>56,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>56,"id_pestania"=>2),
			//fase 3
			//iNSPECTOR REALIZA INSPECCIÓN
			array("estatus"=>1,"id_fase"=>57 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>57,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>57,"id_pestania"=>3),
			//fase 4
			//Área jurídica valida informacion
			array("estatus"=>1,"id_fase"=>58 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>58,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>58,"id_pestania"=>4),
			//fase 5
			//ventanilla genera informe uso de suelo
			array("estatus"=>1,"id_fase"=>59,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>59,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>59,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>59,"id_pestania"=>18),
			//fase 6
			//firma director
			array("estatus"=>1,"id_fase"=>60,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>60,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>60,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>60,"id_pestania"=>6),
			//fase 7
			//ventanilla genera orden de pago
			array("estatus"=>1,"id_fase"=>61,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>61,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>61,"id_pestania"=>4),
			array("estatus"=>1,"id_fase"=>61,"id_pestania"=>7),
			array("estatus"=>1,"id_fase"=>61,"id_pestania"=>8),
			//fase 8
			//se pasa a caja para pagar
			//fase 9
			//se entrega el informe

			/*Petañas para el tramite licencia de alcoholes*/
			//fase 1
			//sube informacion
			array("estatus"=>1,"id_fase"=>64,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>64,"id_pestania"=>2),
			//fase 2
			//Ventillaa valida informacion
			array("estatus"=>1,"id_fase"=>65,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>65,"id_pestania"=>2),
			//fase 3
			//comision
			array("estatus"=>1,"id_fase"=>66 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>66,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>66,"id_pestania"=>25),
			//fase 4
			//elaboracion de documento dictamen comision
			array("estatus"=>1,"id_fase"=>67 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>67,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>67,"id_pestania"=>20),
			array("estatus"=>1,"id_fase"=>67,"id_pestania"=>21),
			//fase 5
			//revision secretaria
			array("estatus"=>1,"id_fase"=>68 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>68,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>68,"id_pestania"=>22),
			array("estatus"=>1,"id_fase"=>68,"id_pestania"=>23),
			//fase 6
			//genera orden de pago ventanilla
			array("estatus"=>1,"id_fase"=>69 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>69,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>69,"id_pestania"=>22),
			array("estatus"=>1,"id_fase"=>69,"id_pestania"=>24),
			array("estatus"=>1,"id_fase"=>69,"id_pestania"=>8),
			//fase 9 Opinión técnica
			array("estatus"=>1,"id_fase"=>72,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>72,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>72,"id_pestania"=>26),

			/*Petañas para el tramite licencia de alcoholes (Provicional)*/
			//fase 1
			//sube informacion
			array("estatus"=>1,"id_fase"=>73,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>73,"id_pestania"=>2),
			//fase 2
			//Ventillaa valida informacion
			array("estatus"=>1,"id_fase"=>74,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>74,"id_pestania"=>2),
			//fase 3
			//ventanilla orden de pago
			array("estatus"=>1,"id_fase"=>75 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>75,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>75,"id_pestania"=>8),
			//fase 5
			//ventanilla envia a secretario
			array("estatus"=>1,"id_fase"=>77 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>77,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>77,"id_pestania"=>12),
			//fase 6
			//revision secretario
			array("estatus"=>1,"id_fase"=>78 ,"id_pestania"=>1),
			array("estatus"=>1,"id_fase"=>78,"id_pestania"=>2),
			array("estatus"=>1,"id_fase"=>78,"id_pestania"=>12),
			array("estatus"=>1,"id_fase"=>78,"id_pestania"=>27),

		);
		//insertamo los datos a la DB
		$this->db->insert_batch("fase_pestania",$data_fase_pestania);

	}//up

	public function down() {
		$this->dbforge->drop_table('fase_pestania');
	}//down

}//class

/* End of file 023_add_fasePestaña.php */
/* Location: ./application/migrations/023_add_fasePestaña.php */
