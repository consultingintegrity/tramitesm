<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_dependencia extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'nombre' => array(
		                'type' => 'VARCHAR',
		                'unique' => TRUE,
		                'constraint' => '65',
		                'null'	=> FALSE,
		        ),
		        'descripcion' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '130',
		                'null'	=> FALSE,
		        ),
		        'status' => array(
		                'type' => 'int',
		                'constraint' => '2',
		        ),
		        'id_direccion' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('dependencia');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `dependencia` ADD FOREIGN KEY (`id_direccion`) REFERENCES `direccion`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//Agregamos los datos iniciales para la tabla
		$data_dependencia = array(
			/*array("nombre"=>"H. Ayuntamiento", "descripcion"=>"Controlar las funciones de todo el sistema"),//1
			array("nombre"=>"Secretaría de Desarrollo Urbano, Vivienda y Obras Públicas", "descripcion"=>"Dirigir e implementar políticas públicas para el desarrollo económico, desarrollo urbano, desarrollo rural y desarrollo agropecuario, en un marco de sustentabilidad ambiental"),//2
			array("nombre"=>"Protección Civil", "descripcion"=>"Realizar acciones en función de los requerimientos de la ciudadanía, buscando el bien común, preservando la seguridad, los recursos naturales y promoviendo el desarrollo humano."),//3
			array("nombre"=>"Secretaría de Ayuntamiento", "descripcion"=>"Licencias de Funcionamiento."),//4
			array("nombre"=>"Secretaría de Finanzas Publicas Municipales", "descripcion"=>"Ingresos."),//5*/

			array("nombre"=>"Dependencìa de Administrador!","descripcion"=>"Controlar las funciones de todo el sistema","status"=>0,"id_direccion"=>1),//1
			array("nombre"=>"Desarrollo Urbano","descripcion"=>"Dirigir e implementar políticas públicas para el desarrollo económico, desarrollo urbano, desarrollo rural y desarrollo agropecuario, en un marco de sustentabilidad ambiental","status"=>0,"id_direccion"=>2),//2
			array("nombre"=>"Protección Civil", "descripcion"=>"Realizar acciones en función de los requerimientos de la ciudadanía, buscando el bien común, preservando la seguridad, los recursos naturales y promoviendo el desarrollo humano.","status"=>0,"id_direccion"=>3),//3
			array("nombre"=>"Ayuntamiento", "descripcion"=>"Licencias de Funcionamiento.","status"=>0,"id_direccion"=>4),//4
			array("nombre"=>"Ingresos", "descripcion"=>"Ingresos.","status"=>1,"id_direccion"=>5),//5
		);//array
		//Insertamos los datos
		$this->db->insert_batch("dependencia", $data_dependencia);
	}//up

	public function down() {
		$this->dbforge->drop_table("dependencia");
	}//down

}//class

/* End of file 007_add_dependencia.php */
/* Location: ./application/migrations/007_add_dependencia.php */
