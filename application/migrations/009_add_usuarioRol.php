<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_usuarioRol extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'id_rol' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		        ),
		        'id_usuario' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('usuario_rol');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `usuario_rol` ADD FOREIGN KEY (`id_rol`) REFERENCES `rol`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `usuario_rol` ADD FOREIGN KEY (`id_usuario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");

		$data_usuarioRol = array(
			//admin
			array("id_rol" => 1, "id_usuario" => 1),
			//DU
/*			array("id_rol" => 4, "id_usuario" => 2),
			array("id_rol" => 5, "id_usuario" => 3),
			array("id_rol" => 6, "id_usuario" => 4),
			array("id_rol" => 7, "id_usuario" => 5),
			array("id_rol" => 8, "id_usuario" => 6),
			array("id_rol" => 9, "id_usuario" => 7),
			array("id_rol" => 10, "id_usuario" => 8),
			array("id_rol" => 11, "id_usuario" => 9),
			array("id_rol" => 23, "id_usuario" => 10),
			array("id_rol" => 24, "id_usuario" => 11),
			array("id_rol" => 20, "id_usuario" => 12),
			//PC
			array("id_rol" => 4, "id_usuario" => 13),
			array("id_rol" => 8, "id_usuario" => 14),
			array("id_rol" => 20, "id_usuario" => 15),
			//SG
			array("id_rol" => 4, "id_usuario" => 16),
			array("id_rol" => 19, "id_usuario" => 17),
			array("id_rol" => 15, "id_usuario" => 18),
			array("id_rol" => 22, "id_usuario" => 19),
			//SF*/			
			array("id_rol" => 3, "id_usuario" => 2),
			/*array("id_rol" => 4, "id_usuario" => 21),
			array("id_rol" => 12, "id_usuario" => 22),
			array("id_rol" => 13, "id_usuario" => 23),
			array("id_rol" => 20, "id_usuario" => 24),
			array("id_rol" => 21, "id_usuario" => 25),*/
			//ciudadano
			array("id_rol" => 2, "id_usuario" => 3),
			//BD Predial
			array("id_rol" => 25, "id_usuario" => 4),
		);
		//ingresamos el registro en la base de datos
		$this->db->insert_batch("usuario_rol", $data_usuarioRol);
	}//up
	public function down() {
		$this->dbforge->drop_table("usuario_rol");
	}//down

}//class

/* End of file 009_add_usuarioRol.php */
/* Location: ./application/migrations/009_add_usuarioRol.php */
