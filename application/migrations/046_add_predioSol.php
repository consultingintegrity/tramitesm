<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_predioSol extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 111,
                'unsigned'       => true,
                'auto_increment' => true,
            ),
          
            'nombreContribuyente' => array(
                'type' => 'VARCHAR',
		        'constraint' => '100',
		        'null'	=> FALSE,
            ),

            'claveCatrastal' => array(
                'type' => 'VARCHAR',
		        'constraint' => '100',
		        'null'	=> FALSE,
            ),

            
            'FOLIO'  => array(
                'type'     => 'VARCHAR',
                'constraint' => 37,
                'null'     => FALSE,
            ),
            'lineaCaptura'  => array(
                'type'     => 'VARCHAR',
                'constraint' => 37,
                'null'     => FALSE,
            ),

            'total' => array(
                'type' => 'DOUBLE',
		        'unsigned' => TRUE,
		        'null'	=> FALSE,
            ),

            'url'  => array(
                'type'     => 'VARCHAR',
                'constraint' => 80,
                'null'     => true,
            ),
            'fecha_emision' => array(
                'type' => 'DATE',
                "null" => TRUE,
            ),
            'fecha_vencimiento' => array(
                    'type' => 'DATE',
                    'null' => TRUE,
            ),

            'estatus_vigente' => array(
                'type' => 'INT',
                'constraint' => 2,
                'null' => false,
            ),

            'estatus_pago' => array(
                'type' => 'INT',
                'constraint' => 2,
                'null' => false,
            ),
            
            'en_corteP' => array(
                'type' => 'INT',
                'constraint' => 2,
                'null' => false,
            ),
            'fecha' => array(
                'type' => 'DATE',
                "null" => TRUE,
            ),

        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('predioSol');
        //se agregan las claves foraneas a la tabla
        } //up

    public function down()
    {
        $this->dbforge->drop_table("prediosol");
    } //down

} //class

/* End of file 046_add_predioSol.php */
/* Location: ./application/controllers/046_add_predioSol.php */
