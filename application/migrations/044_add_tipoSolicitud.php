<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_TipoSolicitud extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'id_solicitud' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'tipo' => array(
	                'type' => 'INT',
	                'constraint' => 11,
            ),
            
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('tipo_solicitud');
		//Agregamos la clave foranea
        $this->db->query("ALTER TABLE `tipo_solicitud` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    }//up    

	public function down() {
		$this->dbforge->drop_table("tipo_solicitud");
	}//down

}//class