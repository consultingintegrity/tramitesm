<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_requisitoTramite extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'id_requisito' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_tramite' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'formato' => array(
	                'type' => 'CHAR',
	                'constraint' => 5,
	                'NULL' => FALSE,
	        ),
	        'obligatorio' => array(
	                'type' => 'TINYINT',
	                'constraint' => 1,
	                'NULL' => FALSE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('requisito_tramite');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `requisito_tramite` ADD FOREIGN KEY (`id_requisito`) REFERENCES `requisito`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `requisito_tramite` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");

		//creamos un array con los datos de los estados
		$data_requisito_tramite = array(
			//numero oficial = 1
			array("id_requisito" => 1, "id_tramite" => 1, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 2, "id_tramite" => 1, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 3, "id_tramite" => 1, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 4, "id_tramite" => 1, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 1, "formato" => "pdf", "obligatorio" => 0),
			array("id_requisito" => 7, "id_tramite" => 1, "formato" => "pdf", "obligatorio" => 1),
			//dictamen uso de suelo = 2
			//array("id_requisito" => 9, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 4, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 11, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 12, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 1, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 13, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 14, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			//array("id_requisito" => 15, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 3, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 7, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 10, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),
			//factibilidad de giro = 3
			array("id_requisito" => 16, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 1, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 17, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 18, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 31, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 4, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 77, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 20, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 13, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 0),
			array("id_requisito" => 7, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			//licencia de funcionamiento = 4
			array("id_requisito" => 22, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 23, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 25, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 26, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 31, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 28, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 0),
			//licencia de alcoholes = 5
			array("id_requisito" => 18, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 17, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 30, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 31, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 32, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 33, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 26, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 34, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 4, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 35, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 36, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 37, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 42, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 38, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 39, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 40, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 41, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 0),
			array("id_requisito" => 7, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			//Requisitos Protección Civil = 6
			array("id_requisito" => 43, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 36, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 23, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 22, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 20, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 45, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 18, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 31, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 28, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 8, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 46, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 42, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			//LICENCIA DE CONTRUCCIÓN = 7

			//OBRA Menor
			array("id_requisito" => 4, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 20, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 47, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 58, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 3, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 0),
			array("id_requisito" => 7, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),

			//Licencia para Ampliación, Modificación, Remodelación y Revalidación.
			array("id_requisito" => 54, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 0),
			array("id_requisito" => 55, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 56, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			//Aviso de terminación de obra
			array("id_requisito" => 57, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 12, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 59, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 60, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 61, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			//REGULARIZACIÓN DE OBRA
			array("id_requisito" => 2, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			//array("id_requisito" => 4, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			//array("id_requisito" => 10, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 62, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 63, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 64, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 32, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 49, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 50, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 65, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 66, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 15, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 51, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 68, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 67, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),

			//informe uso  de suelo = 8
			array("id_requisito" => 69, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 1, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 2, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 1),
			//array("id_requisito" => 70, "id_tramite" => 8, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 3, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 0),
			array("id_requisito" => 7, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 4, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 1),

			//complementos de todos los tramites
			array("id_requisito" => 12, "id_tramite" => 1, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 0),
			array("id_requisito" => 12, "id_tramite" => 3, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 12, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 12, "id_tramite" => 5, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 12, "id_tramite" => 6, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 12, "id_tramite" => 8, "formato" => "pdf", "obligatorio" => 1),

			//complementos de licencia de construccion
			//array("id_requisito" => 48, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 1, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			//complemento de licencia de funcionamiento
			array("id_requisito" => 71, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 72, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 73, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 74, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 42, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 75, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 76, "id_tramite" => 4, "formato" => "pdf", "obligatorio" => 1),

			//complemento de dictamen uso de suelo
			array("id_requisito" => 77, "id_tramite" => 2, "formato" => "pdf", "obligatorio" => 1),

			//complemento de Licencia de construccion
			array("id_requisito" => 78, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			//adicionales de construccion
			array("id_requisito" => 83, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 0),
			array("id_requisito" => 84, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 0),
			array("id_requisito" => 85, "id_tramite" => 7, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 86, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 87, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 88, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 89, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 90, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 91, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 92, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 93, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),
			array("id_requisito" => 94, "id_tramite" => 7, "formato" => "dwg", "obligatorio" => 1),

			//provicional de alcohol
			array("id_requisito" => 79, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 80, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 81, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 82, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 18, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 42, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 12, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 7, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 5, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 33, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
			array("id_requisito" => 34, "id_tramite" => 9, "formato" => "pdf", "obligatorio" => 1),
		);

		//ingresamos el registro en la base de datos
		$this->db->insert_batch("requisito_tramite", $data_requisito_tramite);

	}//up

	public function down() {
		$this->dbforge->drop_table("requisito_tramite");
	}//down

}//class

/* End of file 019_add_requisitoTramite.php */
/* Location: ./application/migrations/019_add_requisitoTramite.php */
