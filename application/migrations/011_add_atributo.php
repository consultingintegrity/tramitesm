<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_atributo extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
		        ),
		        'nombre' => array(
	                'type' => 'CHAR',
	                'constraint' => 15,
	                'unique' => TRUE,
	                'null' => FALSE
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('atributo');
		$data_atributo = array(
			array("nombre"=>"accept-charset"),//1
			array("nombre"=>"autocapitalize"),//2
			array("nombre"=>"autocomplete"),//3
			array("nombre"=>"enctype"),//4
			array("nombre"=>"target"),//5
			array("nombre"=>"input"),//6
			array("nombre"=>"textarea"),//7
			array("nombre"=>"button"),//8
			array("nombre"=>"select"),//9
			array("nombre"=>"option"),//10
			array("nombre"=>"label"),//11
			array("nombre"=>"fieldset"),//12
			array("nombre"=>"a href"),//13
			array("nombre"=>"date"),//14
			array("nombre"=>"file"),//15
			array("nombre"=>"h1"),//16
			array("nombre"=>"number"),//17
			array("nombre"=>"time"),//18
			array("nombre"=>"checkbox"),//19

		);//array
		//agregams los registros a la tabla
		$this->db->insert_batch("atributo",$data_atributo);
	}//up

	public function down() {
		$this->dbforge->drop_table("atributo");
	}//down

}//class

/* End of file 010_add_atributo.php */
/* Location: ./application/migrations/010_add_atributo.php */
