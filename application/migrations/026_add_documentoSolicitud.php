<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_documentoSolicitud extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'id_documento' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_solicitud' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'url' => array(
	                'type' => 'CHAR',
	                'constraint' => 85,
	                'NULL' => FALSE,
		),
		'folio' => array(
	                'type' => 'varchar',
	                'constraint' => 30,
	                'NULL' => FALSE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('documento_solicitud');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `documento_solicitud` ADD FOREIGN KEY (`id_documento`) REFERENCES `documento`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `documento_solicitud` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
	}//up

	public function down() {
		$this->dbforge->drop_table("documento_solicitud");
	}//down

}//class

/* End of file 026_add_documentoSolicitud.php */
/* Location: ./application/migrations/026_add_documentoSolicitud.php */
