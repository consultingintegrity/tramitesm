<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_campo extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'type' => array(
	                'type' => 'CHAR',
	                'constraint' => '20',
	                'null'	=> FALSE,
	        ),
	        'id_etiqueta' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_atributo' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        )
			);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('campo');
		$this->db->query("ALTER TABLE `campo` ADD FOREIGN KEY (`id_etiqueta`) REFERENCES `etiqueta`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `campo` ADD FOREIGN KEY (`id_atributo`) REFERENCES `atributo`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//creamos registros
		$data_fields = array(
			array("type"=>"label","id_etiqueta"=>1,"id_atributo"=>11),//1
			array("type"=>"email","id_etiqueta"=>1,"id_atributo"=>6),//2
			array("type"=>"label","id_etiqueta"=>2,"id_atributo"=>11),//3
			array("type"=>"password","id_etiqueta"=>2,"id_atributo"=>6),//4
			array("type"=>"label","id_etiqueta"=>5,"id_atributo"=>11),//5
			array("type"=>"checkbox","id_etiqueta"=>5,"id_atributo"=>6),//6
			array("type"=>"label","id_etiqueta"=>3,"id_atributo"=>11),//7
			array("type"=>"submit","id_etiqueta"=>3,"id_atributo"=>8),//8
			array("type"=>"a href","id_etiqueta"=>4,"id_atributo"=>13),//9
			array("type"=>"label","id_etiqueta"=>6,"id_atributo"=>11),//10
			array("type"=>"text","id_etiqueta"=>6,"id_atributo"=>6),//11
			array("type"=>"label","id_etiqueta"=>7,"id_atributo"=>11),//12
			array("type"=>"text","id_etiqueta"=>7,"id_atributo"=>6),//13
			array("type"=>"label","id_etiqueta"=>8,"id_atributo"=>11),//14
			array("type"=>"text","id_etiqueta"=>8,"id_atributo"=>6),//15
			array("type"=>"label","id_etiqueta"=>9,"id_atributo"=>11),//16
			array("type"=>"text","id_etiqueta"=>9,"id_atributo"=>6),//17
			array("type"=>"label","id_etiqueta"=>10,"id_atributo"=>11),//18
			array("type"=>"password","id_etiqueta"=>10,"id_atributo"=>6),//19
			array("type"=>"label","id_etiqueta"=>11,"id_atributo"=>11),//20
			array("type"=>"submit","id_etiqueta"=>11,"id_atributo"=>8),//21
			array("type"=>"label","id_etiqueta"=>12,"id_atributo"=>11),//22
			array("type"=>"select","id_etiqueta"=>12,"id_atributo"=>9),//23
			array("type"=>"label","id_etiqueta"=>13,"id_atributo"=>11),//24
			array("type"=>"label","id_etiqueta"=>14,"id_atributo"=>11),//25
			array("type"=>"submit","id_etiqueta"=>15,"id_atributo"=>8),//26
			array("type"=>"submit","id_etiqueta"=>16,"id_atributo"=>8),//27
			array("type"=>"submit","id_etiqueta"=>17,"id_atributo"=>8),//28
			array("type"=>"label","id_etiqueta"=>18,"id_atributo"=>11),//29
			array("type"=>"select","id_etiqueta"=>18,"id_atributo"=>9),//30
			array("type"=>"label","id_etiqueta"=>19,"id_atributo"=>11),//31
			array("type"=>"select","id_etiqueta"=>19,"id_atributo"=>9),//32
			array("type"=>"label","id_etiqueta"=>20,"id_atributo"=>11),//33
			array("type"=>"number","id_etiqueta"=>20,"id_atributo"=>6),//34
			array("type"=>"label","id_etiqueta"=>21,"id_atributo"=>11),//35
			array("type"=>"file","id_etiqueta"=>21,"id_atributo"=>6),//36
			array("type"=>"label","id_etiqueta"=>22,"id_atributo"=>11),//37
			array("type"=>"select","id_etiqueta"=>22,"id_atributo"=>9),//38
			array("type"=>"label","id_etiqueta"=>23,"id_atributo"=>11),//39
			array("type"=>"select","id_etiqueta"=>23,"id_atributo"=>9),//40
			array("type"=>"label","id_etiqueta"=>24,"id_atributo"=>11),//41
			array("type"=>"select","id_etiqueta"=>24,"id_atributo"=>9),//42
			array("type"=>"label","id_etiqueta"=>32,"id_atributo"=>11),//43
			array("type"=>"date","id_etiqueta"=>32,"id_atributo"=>14),//44
			array("type"=>"label","id_etiqueta"=>26,"id_atributo"=>11),//45
			array("type"=>"text","id_etiqueta"=>26,"id_atributo"=>6),//46
			array("type"=>"label","id_etiqueta"=>27,"id_atributo"=>11),//47
			array("type"=>"text","id_etiqueta"=>27,"id_atributo"=>6),//48
			array("type"=>"label","id_etiqueta"=>28,"id_atributo"=>11),//49
			array("type"=>"text","id_etiqueta"=>28,"id_atributo"=>6),//50
			array("type"=>"label","id_etiqueta"=>29,"id_atributo"=>11),//51
			array("type"=>"text","id_etiqueta"=>29,"id_atributo"=>6),//52
			array("type"=>"label","id_etiqueta"=>27,"id_atributo"=>11),//53
			array("type"=>"text","id_etiqueta"=>27,"id_atributo"=>6),//54
			array("type"=>"label","id_etiqueta"=>31,"id_atributo"=>11),//55
			array("type"=>"text","id_etiqueta"=>31,"id_atributo"=>6),//56
			array("type"=>"label","id_etiqueta"=>25,"id_atributo"=>11),//57
			array("type"=>"label","id_etiqueta"=>25,"id_atributo"=>6),//58
			array("type"=>"label","id_etiqueta"=>33,"id_atributo"=>11),//59
			array("type"=>"label","id_etiqueta"=>33,"id_atributo"=>15),//60
			array("type"=>"label","id_etiqueta"=>34,"id_atributo"=>11),//61
			array("type"=>"text","id_etiqueta"=>34,"id_atributo"=>6),//62
			array("type"=>"label","id_etiqueta"=>35,"id_atributo"=>11),//63
			array("type"=>"text","id_etiqueta"=>35,"id_atributo"=>6),//64
			array("type"=>"label","id_etiqueta"=>36,"id_atributo"=>11),//65
			array("type"=>"select","id_etiqueta"=>36,"id_atributo"=>9),//66
			array("type"=>"option","id_etiqueta"=>46,"id_atributo"=>10),//67
			array("type"=>"option","id_etiqueta"=>47,"id_atributo"=>10),//68
			array("type"=>"label","id_etiqueta"=>37,"id_atributo"=>11),//69
			array("type"=>"text","id_etiqueta"=>37,"id_atributo"=>6),//70
			array("type"=>"label","id_etiqueta"=>38,"id_atributo"=>11),//71
			array("type"=>"select","id_etiqueta"=>38,"id_atributo"=>9),//72
			array("type"=>"option","id_etiqueta"=>39,"id_atributo"=>10),//73
			array("type"=>"option","id_etiqueta"=>40,"id_atributo"=>10),//74
			array("type"=>"option","id_etiqueta"=>41,"id_atributo"=>10),//75
			array("type"=>"option","id_etiqueta"=>42,"id_atributo"=>10),//76
			array("type"=>"option","id_etiqueta"=>43,"id_atributo"=>10),//77
			array("type"=>"option","id_etiqueta"=>44,"id_atributo"=>10),//78
			array("type"=>"option","id_etiqueta"=>45,"id_atributo"=>10),//79
			array("type"=>"h1","id_etiqueta"=>46,"id_atributo"=>16),//80
			array("type"=>"h1","id_etiqueta"=>48,"id_atributo"=>16),//81
			array("type"=>"h1","id_etiqueta"=>49,"id_atributo"=>16),//82
			array("type"=>"label","id_etiqueta"=>50,"id_atributo"=>11),//83
			array("type"=>"text","id_etiqueta"=>50,"id_atributo"=>6),//84
			array("type"=>"label","id_etiqueta"=>51,"id_atributo"=>11),//85
			array("type"=>"text","id_etiqueta"=>51,"id_atributo"=>6),//86
			array("type"=>"label","id_etiqueta"=>52,"id_atributo"=>11),//87
			array("type"=>"text","id_etiqueta"=>52,"id_atributo"=>6),//88
			array("type"=>"label","id_etiqueta"=>53,"id_atributo"=>11),//89
			array("type"=>"text","id_etiqueta"=>53,"id_atributo"=>6),//90
			array("type"=>"label","id_etiqueta"=>54,"id_atributo"=>11),//91
			array("type"=>"text","id_etiqueta"=>54,"id_atributo"=>6),//92
			array("type"=>"label","id_etiqueta"=>20,"id_atributo"=>11),//93
			array("type"=>"number","id_etiqueta"=>55,"id_atributo"=>6),//94
			array("type"=>"label","id_etiqueta"=>56,"id_atributo"=>11),//95
			array("type"=>"text","id_etiqueta"=>56,"id_atributo"=>6),//96
			array("type"=>"label","id_etiqueta"=>57,"id_atributo"=>11),//97
			array("type"=>"text","id_etiqueta"=>57,"id_atributo"=>6),//98
			array("type"=>"label","id_etiqueta"=>58,"id_atributo"=>11),//99
			array("type"=>"text","id_etiqueta"=>58,"id_atributo"=>6),//100
			array("type"=>"label","id_etiqueta"=>59,"id_atributo"=>11),//101
			array("type"=>"number","id_etiqueta"=>59,"id_atributo"=>6),//102
			array("type"=>"label","id_etiqueta"=>60,"id_atributo"=>11),//103
			array("type"=>"text","id_etiqueta"=>60,"id_atributo"=>6),//104
			array("type"=>"label","id_etiqueta"=>61,"id_atributo"=>11),//105
			array("type"=>"text","id_etiqueta"=>61,"id_atributo"=>6),//106
			array("type"=>"label","id_etiqueta"=>62,"id_atributo"=>11),//107
			array("type"=>"text","id_etiqueta"=>62,"id_atributo"=>6),//108
			array("type"=>"label","id_etiqueta"=>63,"id_atributo"=>11),//109
			array("type"=>"text","id_etiqueta"=>63,"id_atributo"=>6),//110
			array("type"=>"label","id_etiqueta"=>64,"id_atributo"=>11),//111
			array("type"=>"time","id_etiqueta"=>64,"id_atributo"=>18),//112
			array("type"=>"label","id_etiqueta"=>65,"id_atributo"=>11),//113
			array("type"=>"text","id_etiqueta"=>65,"id_atributo"=>6),//114
			array("type"=>"label","id_etiqueta"=>66,"id_atributo"=>11),//115
			array("type"=>"date","id_etiqueta"=>66,"id_atributo"=>14),//116
			array("type"=>"label","id_etiqueta"=>67,"id_atributo"=>11),//117
			array("type"=>"text","id_etiqueta"=>67,"id_atributo"=>6),//118
			array("type"=>"label","id_etiqueta"=>68,"id_atributo"=>11),//119
			array("type"=>"date","id_etiqueta"=>68,"id_atributo"=>14),//120
			array("type"=>"label","id_etiqueta"=>69,"id_atributo"=>11),//121
			array("type"=>"text","id_etiqueta"=>69,"id_atributo"=>6),//122
			array("type"=>"label","id_etiqueta"=>70,"id_atributo"=>11),//123
			array("type"=>"text","id_etiqueta"=>70,"id_atributo"=>6),//124
			array("type"=>"label","id_etiqueta"=>71,"id_atributo"=>11),//125
			array("type"=>"text","id_etiqueta"=>71,"id_atributo"=>6),//126
			array("type"=>"label","id_etiqueta"=>72,"id_atributo"=>11),//127
			array("type"=>"date","id_etiqueta"=>72,"id_atributo"=>14),//128
			array("type"=>"label","id_etiqueta"=>73,"id_atributo"=>11),//129
			array("type"=>"text","id_etiqueta"=>73,"id_atributo"=>6),//130
			array("type"=>"label","id_etiqueta"=>74,"id_atributo"=>11),//131
			array("type"=>"number","id_etiqueta"=>74,"id_atributo"=>17),//132
			array("type"=>"text","id_etiqueta"=>19,"id_atributo"=>6),//133
			//Campos Form Solicitud de Licencia de Construcciónote
			array("type"=>"label","id_etiqueta"=>132,"id_atributo"=>11),//134
			array("type"=>"text","id_etiqueta"=>132,"id_atributo"=>6),//135
			array("type"=>"label","id_etiqueta"=>126,"id_atributo"=>11),//136
			array("type"=>"date","id_etiqueta"=>126,"id_atributo"=>14),//137
			// ###### FORM ANTECEDENTES
			array("type"=>"label","id_etiqueta"=>75,"id_atributo"=>11),//138
			array("type"=>"text","id_etiqueta"=>75,"id_atributo"=>6),//139
			array("type"=>"h1","id_etiqueta"=>76,"id_atributo"=>16),//140
			array("type"=>"label","id_etiqueta"=>77,"id_atributo"=>11),//141
			array("type"=>"text","id_etiqueta"=>77,"id_atributo"=>6),//142
			array("type"=>"label","id_etiqueta"=>59,"id_atributo"=>11),//143
			array("type"=>"number","id_etiqueta"=>59,"id_atributo"=>6),//144
			array("type"=>"label","id_etiqueta"=>78,"id_atributo"=>11),//145
			array("type"=>"text","id_etiqueta"=>78,"id_atributo"=>6),//146
			array("type"=>"label","id_etiqueta"=>31,"id_atributo"=>11),//147
			array("type"=>"text","id_etiqueta"=>31,"id_atributo"=>6),//148
			array("type"=>"label","id_etiqueta"=>79,"id_atributo"=>11),//149
			array("type"=>"text","id_etiqueta"=>79,"id_atributo"=>6),//150
			array("type"=>"label","id_etiqueta"=>80,"id_atributo"=>11),//151
			array("type"=>"text","id_etiqueta"=>80,"id_atributo"=>6),//152
			array("type"=>"label","id_etiqueta"=>81,"id_atributo"=>11),//153
			array("type"=>"text","id_etiqueta"=>81,"id_atributo"=>6),//154
			//##### DATOS DE LA OBRA
			array("type"=>"h1","id_etiqueta"=>82,"id_atributo"=>16),//155
			array("type"=>"h1","id_etiqueta"=>127,"id_atributo"=>16),//156
			array("type"=>"h1","id_etiqueta"=>83,"id_atributo"=>16),//157
			array("type"=>"label","id_etiqueta"=>84,"id_atributo"=>11),//158
			array("type"=>"text","id_etiqueta"=>84,"id_atributo"=>6),//159
			array("type"=>"label","id_etiqueta"=>85,"id_atributo"=>11),//160
			array("type"=>"text","id_etiqueta"=>85,"id_atributo"=>6),//1621
			array("type"=>"label","id_etiqueta"=>86,"id_atributo"=>11),//162
			array("type"=>"text","id_etiqueta"=>86,"id_atributo"=>6),//163
			array("type"=>"label","id_etiqueta"=>87,"id_atributo"=>11),//164
			array("type"=>"text","id_etiqueta"=>87,"id_atributo"=>6),//165
			array("type"=>"label","id_etiqueta"=>88,"id_atributo"=>11),//166
			array("type"=>"text","id_etiqueta"=>88,"id_atributo"=>6),//167
			array("type"=>"label","id_etiqueta"=>89,"id_atributo"=>11),//168
			array("type"=>"text","id_etiqueta"=>89,"id_atributo"=>6),//169
			array("type"=>"label","id_etiqueta"=>128,"id_atributo"=>11),//170
			array("type"=>"text","id_etiqueta"=>128,"id_atributo"=>6),//171
			array("type"=>"label","id_etiqueta"=>89,"id_atributo"=>11),//172
			array("type"=>"text","id_etiqueta"=>89,"id_atributo"=>6),//173
			array("type"=>"label","id_etiqueta"=>90,"id_atributo"=>11),//174
			array("type"=>"text","id_etiqueta"=>90,"id_atributo"=>6),//175
			array("type"=>"label","id_etiqueta"=>91,"id_atributo"=>11),//176
			array("type"=>"text","id_etiqueta"=>91,"id_atributo"=>6),//177
			array("type"=>"label","id_etiqueta"=>92,"id_atributo"=>11),//178
			array("type"=>"text","id_etiqueta"=>92,"id_atributo"=>6),//179
			array("type"=>"label","id_etiqueta"=>93,"id_atributo"=>11),//180
			array("type"=>"text","id_etiqueta"=>93,"id_atributo"=>6),//181
			array("type"=>"label","id_etiqueta"=>94,"id_atributo"=>11),//182
			array("type"=>"text","id_etiqueta"=>94,"id_atributo"=>6),//183
			//FORM PERITO RESPONSABLE
			array("type"=>"h1","id_etiqueta"=>129,"id_atributo"=>16),//184
			array("type"=>"h1","id_etiqueta"=>95,"id_atributo"=>16),//185
			array("type"=>"h1","id_etiqueta"=>96,"id_atributo"=>16),//186
			array("type"=>"label","id_etiqueta"=>97,"id_atributo"=>11),//187
			array("type"=>"checkbox","id_etiqueta"=>97,"id_atributo"=>19),//188
			array("type"=>"label","id_etiqueta"=>98,"id_atributo"=>11),//189
			array("type"=>"checkbox","id_etiqueta"=>98,"id_atributo"=>19),//190
			array("type"=>"h1","id_etiqueta"=>99,"id_atributo"=>16),//191
			array("type"=>"label","id_etiqueta"=>100,"id_atributo"=>11),//192
			array("type"=>"checkbox","id_etiqueta"=>100,"id_atributo"=>19),//193
			array("type"=>"label","id_etiqueta"=>101,"id_atributo"=>11),//194
			array("type"=>"checkbox","id_etiqueta"=>101,"id_atributo"=>19),//195
			array("type"=>"label","id_etiqueta"=>102,"id_atributo"=>11),//196
			array("type"=>"checkbox","id_etiqueta"=>102,"id_atributo"=>19),//197
			array("type"=>"label","id_etiqueta"=>130,"id_atributo"=>11),//198
			array("type"=>"text","id_etiqueta"=>130,"id_atributo"=>6),//199
			array("type"=>"label","id_etiqueta"=>103,"id_atributo"=>11),//200
			array("type"=>"text","id_etiqueta"=>103,"id_atributo"=>6),//201
			array("type"=>"h1","id_etiqueta"=>104,"id_atributo"=>16),//202
			array("type"=>"label","id_etiqueta"=>105,"id_atributo"=>11),//203
			array("type"=>"checkbox","id_etiqueta"=>105,"id_atributo"=>19),//204
			array("type"=>"label","id_etiqueta"=>106,"id_atributo"=>11),//205
			array("type"=>"checkbox","id_etiqueta"=>106,"id_atributo"=>19),//206
			array("type"=>"label","id_etiqueta"=>107,"id_atributo"=>11),//207
			array("type"=>"checkbox","id_etiqueta"=>107,"id_atributo"=>19),//208
			array("type"=>"label","id_etiqueta"=>108,"id_atributo"=>11),//209
			array("type"=>"checkbox","id_etiqueta"=>108,"id_atributo"=>19),//210
			array("type"=>"label","id_etiqueta"=>109,"id_atributo"=>11),//211
			array("type"=>"text","id_etiqueta"=>109,"id_atributo"=>6),//212
			array("type"=>"h1","id_etiqueta"=>110,"id_atributo"=>16),//213
			array("type"=>"label","id_etiqueta"=>105,"id_atributo"=>11),//214
			array("type"=>"checkbox","id_etiqueta"=>105,"id_atributo"=>19),//215
			array("type"=>"label","id_etiqueta"=>106,"id_atributo"=>11),//216
			array("type"=>"checkbox","id_etiqueta"=>106,"id_atributo"=>19),//217
			array("type"=>"label","id_etiqueta"=>107,"id_atributo"=>11),//218
			array("type"=>"checkbox","id_etiqueta"=>107,"id_atributo"=>19),//219
			array("type"=>"label","id_etiqueta"=>108,"id_atributo"=>11),//220
			array("type"=>"checkbox","id_etiqueta"=>108,"id_atributo"=>19),//221
			array("type"=>"label","id_etiqueta"=>131,"id_atributo"=>11),//222
			array("type"=>"checkbox","id_etiqueta"=>131,"id_atributo"=>19),//223
			array("type"=>"label","id_etiqueta"=>109,"id_atributo"=>11),//224
			array("type"=>"text","id_etiqueta"=>109,"id_atributo"=>6),//225
			// #### FORM INFORACIÓn ADICIONAL EsTADÍSTICA
			array("type"=>"label","id_etiqueta"=>111,"id_atributo"=>11),//226
			array("type"=>"text","id_etiqueta"=>111,"id_atributo"=>6),//227
			array("type"=>"label","id_etiqueta"=>112,"id_atributo"=>11),//228
			array("type"=>"text","id_etiqueta"=>112,"id_atributo"=>6),//229
			//##### DATOS DEL PERITO
			// FORM SOLICITUD TERMINACIÓN DE OBRA
			array("type"=>"h1","id_etiqueta"=>113,"id_atributo"=>16),//230
			array("type"=>"label","id_etiqueta"=>114,"id_atributo"=>11),//231
			array("type"=>"text","id_etiqueta"=>114,"id_atributo"=>6),//232
			array("type"=>"h1","id_etiqueta"=>115,"id_atributo"=>16),//233
			array("type"=>"label","id_etiqueta"=>116,"id_atributo"=>11),//234
			array("type"=>"text","id_etiqueta"=>116,"id_atributo"=>6),//235
			array("type"=>"label","id_etiqueta"=>117,"id_atributo"=>11),//236
			array("type"=>"text","id_etiqueta"=>117,"id_atributo"=>6),//237
			array("type"=>"label","id_etiqueta"=>118,"id_atributo"=>11),//238
			array("type"=>"text","id_etiqueta"=>118,"id_atributo"=>6),//239
			array("type"=>"label","id_etiqueta"=>119,"id_atributo"=>11),//240
			array("type"=>"date","id_etiqueta"=>119,"id_atributo"=>14),//241
			array("type"=>"label","id_etiqueta"=>120,"id_atributo"=>11),//242
			array("type"=>"date","id_etiqueta"=>120,"id_atributo"=>14),//243
			array("type"=>"label","id_etiqueta"=>121,"id_atributo"=>11),//244
			array("type"=>"text","id_etiqueta"=>121,"id_atributo"=>6),//245
			//### Form REvisión y Superviión de Obra
			array("type"=>"h1","id_etiqueta"=>122,"id_atributo"=>16),//246
			array("type"=>"label","id_etiqueta"=>123,"id_atributo"=>11),//247
			array("type"=>"text","id_etiqueta"=>123,"id_atributo"=>6),//248
			array("type"=>"h1","id_etiqueta"=>125,"id_atributo"=>16),//249
			array("type"=>"label","id_etiqueta"=>125,"id_atributo"=>11),//250
			array("type"=>"text","id_etiqueta"=>125,"id_atributo"=>6),//251
			array("type"=>"h1","id_etiqueta"=>124,"id_atributo"=>16),//252
			//licencia de funcionamiento
			array("type"=>"label","id_etiqueta"=>133,"id_atributo"=>11),//253
			array("type"=>"time","id_etiqueta"=>133,"id_atributo"=>18),//254
			array("type"=>"label","id_etiqueta"=>68,"id_atributo"=>11),//255
			array("type"=>"date","id_etiqueta"=>68,"id_atributo"=>14),//256

			array("type"=>"label","id_etiqueta"=>134,"id_atributo"=>11),//257
			array("type"=>"label","id_etiqueta"=>134,"id_atributo"=>15),//258

			array("type"=>"label","id_etiqueta"=>135,"id_atributo"=>11),//259
			array("type"=>"label","id_etiqueta"=>135,"id_atributo"=>15),//260
			array("type"=>"label","id_etiqueta"=>136,"id_atributo"=>11),//261
			array("type"=>"text","id_etiqueta"=>136,"id_atributo"=>6),//262
			array("type"=>"text","id_etiqueta"=>137,"id_atributo"=>11),//263

			//para razón social
			array("type"=>"text","id_etiqueta"=>138,"id_atributo"=>11),//264
			array("type"=>"text","id_etiqueta"=>138,"id_atributo"=>6),//265
		);
		//insertamos los datos en l tabla
		$this->db->insert_batch("campo",$data_fields);
	}//up

	public function down() {
		$this->dbforge->drop_table("campo");
	}//doen

}//class

/* End of file 014_add_campo.php */
/* Location: ./application/migrations/014_add_campo.php */
