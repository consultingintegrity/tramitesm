<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_checkpredio extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id_funcionario' => array(
                'type'       => 'varchar',
                'constraint' => 11,
            ),

            'date' => array(
                'type'       => 'varchar',
                'constraint' => 10,
            ),
  	        'val' => array(
  	                'type' => 'varchar',
                    'constraint' => 5,
              ),
        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        //creamos la tabla
        $this->dbforge->create_table('checkpredio');
        $this->subir();
        
    } //up

    public function down()
    {
        $this->dbforge->drop_table("checkpredio");
        $this->bajar();
    } //down

    public function subir(){
        $campos = array(
            'FOLIO' => array(
                'name' => 'FOLIO',
                'type' => 'varchar',
                'constraint' => 37,
                'NULL' => TRUE,
            ),
            'lineaCaptura' => array(
                'name' => 'lineaCaptura',
                'type' => 'varchar',
                'constraint' => 37,
                'NULL' => TRUE,
            ),
        ); //campos
        $this->dbforge->modify_column('predioSol',$campos);
    }
   
    public function bajar(){
        $campos = array(
            'FOLIO' => array(
                'name' => 'FOLIO',
                'type' => 'varchar',
                'constraint' => 37,
                'NULL' => FALSE,
            ),
            'lineaCaptura' => array(
                'name' => 'lineaCaptura',
                'type' => 'varchar',
                'constraint' => 37,
                'NULL' => FALSE,
            ),
        ); //campos
        $this->dbforge->modify_column('predioSol',$campos);
    }


} //class

/* End of file 052_add_checkpredio.php */
/* Location: ./application/controllers/ */
