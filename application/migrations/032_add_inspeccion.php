<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_inspeccion extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),
            'url'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '80',
                'null'       => true,
            ),
            'id_solicitud' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
            'id_usuario'   => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
            'estatus'      => array(
                'type'       => 'TINYINT',
                'constraint' => 1,
                'unsigned'   => true,
                'null'       => false,
            ),
            'fecha'        => array(
                'type'    => 'TIMESTAMP',
                'null'    => false,
                
            ),
        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('inspeccion');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `inspeccion` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `inspeccion` ADD FOREIGN KEY (`id_usuario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("inspeccion");
    } //down

} //class

/* End of file 032_add_inspeccion.php */
/* Location: ./application/controllers/032_add_inspeccion.php */
