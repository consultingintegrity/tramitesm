<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_persona extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'primer_nombre' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> FALSE,
		        ),
		        'segundo_nombre' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> TRUE,
		        ),
		        'primer_apellido' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> FALSE,
		        ),
		        'segundo_apellido' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> TRUE,
		        ),
		        'fecha_nacimiento' => array(
		                'type' => 'DATE',
		                'null'	=> TRUE,
		        ),
		        'genero' => array(
		                'type' => 'CHAR',
		                'constraint' => '1',
		                'null'	=> TRUE,
		        ),
		        'id_municipio' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		        ),
		        'calle' => array(
		                'type' => 'CHAR',
		                'constraint' => '50',
		                'null'	=> TRUE,
		        ),
		        'colonia' => array(
		                'type' => 'CHAR',
		                'constraint' => '50',
		                'null'	=> TRUE,
		        ),
		        'numero_interior' => array(
		                'type' => 'CHAR',
		                'constraint' => '15',
		                'null'	=> TRUE,
		        ),
		        'numero_exterior' => array(
		                'type' => 'CHAR',
		                'constraint' => '15',
		                'null'	=> TRUE,
		        ),
		        'rfc' => array(
		                'type' => 'CHAR',
		                'constraint' => '13',
		                'null'	=> TRUE,
		        ),
		        'razon_social' => array(
		                'type' => 'CHAR',
		                'constraint' => '100',
		                'null'	=> TRUE,
		        ),
		        'tipo_persona' => array(
		                'type' => 'TINYINT',
		                'constraint' => '1',
		                'null'	=> TRUE,
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('persona');
		//Agregamos la clavce foranea
		$this->db->query("ALTER TABLE `persona` ADD FOREIGN KEY (`id_municipio`) REFERENCES `municipio`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		 //creamos un array con los datos de los paìses
		 $data_persona = array(
			//admin
			array("primer_nombre"=>'Administrador',"segundo_nombre"=>"","primer_apellido" =>'Administrador',"segundo_apellido" =>"ESTRELLA","id_municipio" => 10),//1//Administrador
			//DU
/*			array("primer_nombre"=>"ventanilla","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//2//ventanillaDU
			array("primer_nombre"=>"Encargado","segundo_nombre"=>"","primer_apellido" =>"NFDU","segundo_apellido" =>"","id_municipio" => 10),//3//EncargadoNFDU
			array("primer_nombre"=>"Director","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//4//DirectorDU
			array("primer_nombre"=>"SecretarioDU","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//5//SecretarioDU
			array("primer_nombre"=>"InspeccionDU","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//6//InspeccionDU
			array("primer_nombre"=>"JefeInspeccionDU","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//7//JefeInspeccionDU
			array("primer_nombre"=>"secretariaDU","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//8//secretariaDU
			array("primer_nombre"=>"encargadoDictamen","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//9 //encargadoDictamen
			array("primer_nombre"=>"SubDirectorDu","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//10//SubDirectorDu
			array("primer_nombre"=>"coordinacionVentanillaDU","segundo_nombre"=>"","primer_apellido" =>"DU","segundo_apellido" =>"","id_municipio" => 10),//11//coordinacionVentanillaDU
			array("primer_nombre"=>"Opinión Técnica","segundo_nombre"=>"","primer_apellido" =>"Desarrollo Urbano","segundo_apellido" =>"","id_municipio" => 10),//12//OpinionTecnicaDU
			//usuarios para Protección Civil
			array("primer_nombre"=>"VentanillaPC","segundo_nombre"=>"","primer_apellido" =>"PC","segundo_apellido" =>"","id_municipio" => 10),//13//VentanillaPC
			array("primer_nombre"=>"InspeccionPC","segundo_nombre"=>"","primer_apellido" =>"PC","segundo_apellido" =>"","id_municipio" => 10),//14//InspeccionPC
			array("primer_nombre"=>"OpinionTecnicaPC","segundo_nombre"=>"","primer_apellido" =>"PC","segundo_apellido" =>"","id_municipio" => 10),//15//OpinionTecnicaPC
			//secretaria General
			array("primer_nombre"=>"VentanillaSG","segundo_nombre"=>"","primer_apellido" =>"SG","segundo_apellido" =>"","id_municipio" => 10),//16//VentanillaSG
			array("primer_nombre"=>"ComisionSG","segundo_nombre"=>"","primer_apellido" =>"SG","segundo_apellido" =>"","id_municipio" => 10),//17//ComisionSG
			array("primer_nombre"=>"SecretariaSG","segundo_nombre"=>"","primer_apellido" =>"SG","segundo_apellido" =>"","id_municipio" => 10),//18//SecretariaSG
			array("primer_nombre"=>"SecretarioGeneralSG","segundo_nombre"=>"","primer_apellido" =>"SG","segundo_apellido" =>"","id_municipio" => 10),//19//SecretarioGeneralSG
			//finanzas*/
			array("primer_nombre"=>"CajaIn","segundo_nombre"=>"","primer_apellido" =>"FI","segundo_apellido" =>"","id_municipio" => 10),//20//CajaIn
			/*array("primer_nombre"=>"VentanillaIN","segundo_nombre"=>"","primer_apellido" =>"FI","segundo_apellido" =>"","id_municipio" => 10),//21 VentanillaIN
			array("primer_nombre"=>"TesoreroIN","segundo_nombre"=>"","primer_apellido" =>"FI","segundo_apellido" =>"","id_municipio" => 10),//22//TesoreroIN
			array("primer_nombre"=>"presidenteIN","segundo_nombre"=>"","primer_apellido" =>"FI","segundo_apellido" =>"","id_municipio" => 10),//23//presidenteIN
			array("primer_nombre"=>"OpinonTecnicaIN","segundo_nombre"=>"","primer_apellido" =>"FI","segundo_apellido" =>"","id_municipio" => 10),//24//OpinonTecnicaIN
			array("primer_nombre"=>"InspeccionSecGob","segundo_nombre"=>"","primer_apellido" =>"FI","segundo_apellido" =>"","id_municipio" => 10),//25//InspeccionSecGob*/		
			//Ciudadano de prueba
			array("primer_nombre"=>"Usuario","segundo_nombre"=>"","primer_apellido" =>"Testing","segundo_apellido" =>"","id_municipio" => 10),//26//ciudadan
			//encargado de DB
			array("primer_nombre"=>"Predio","segundo_nombre"=>"","primer_apellido" =>"Base de datos","segundo_apellido" =>"","id_municipio" => 10),//27//encargado de DB

		);
		 //ingresamos el registro en la base de datos
		 $this->db->insert_batch('persona', $data_persona);
	}//up

	public function down() {
		$this->dbforge->drop_table("persona");
	}//down

}//class

/* End of file 004_add_persona.php */
/* Location: ./application/migrations/004_add_persona.php */
