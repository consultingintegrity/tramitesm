<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_requisito extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'nombre' => array(
	                'type' => 'CHAR',
	                'constraint' => '180',
	                "null" => FALSE,
	        ),
	        'descripcion' => array(
	                'type' => 'VARCHAR',
	                'constraint' => '250',
		),
		'status' => array(
	                'type' => 'INT',
	                'constraint' => '2',
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('requisito');
		//creamos un array con los datos de los REQUISITOS
		$data_requisito = array(
			array("nombre" => "Croquis de localización del predio", "descripcion" => "Croquis de localización del predio, que contenga nombre de la calle, puntos de referencia y superficies.","status" => 0),//1
			array("nombre" => "Escrituras,título de propiedad o constancia notariada", "descripcion" => "escrituras,título de propiedad o constancia notariada.","status" => 0),//2
			array("nombre" => "Identificación del propietario", "descripcion" => "identificación del propietario","status" => 0),//3
			array("nombre" => "Recibo de pago de predial vigente", "descripcion" => "recibo de pago de predial vigente","status" => 0),//4
			array("nombre" => "Poder notarial", "descripcion" => "En caso de que la persona sea moral","status" => 1),//5
			array("nombre" => "Alta en Hacienda xxxx", "descripcion" => "alta en Hacienda","status" => 0),//43
			array("nombre" => "Acta constitutiva", "descripcion" => "Acta constitutiva para personas morales","status" => 1),//7
			array("nombre" => "Documento de poder Notarial", "descripcion" => "Poder Notarial","status" => 1),//8
			//tramite 2
			array("nombre" => "Escrito Ubicación del Predio", "descripcion" => "Especifique ubicación del predio, descripción del uso pretendido(tipo A o B), firmado por el propietario o representante legal; dirigido al Director de Desarrollo Urbano y Vivienda.","status" => 0),//9
			array("nombre" => "Alineamiento y Número Oficial", "descripcion" => "Documento obtenido en el tramite de Número Oficial","status" => 0),//10
			array("nombre" => "Ecrituras, titulo de propiedad o constancia notarial", "descripcion" => "escrituras, titulo o constancia notarial En caso de que la solicitud sea presentada por un tercero.","status" => 0),//11
			array("nombre" => "Poder notariado o Carta poder notariada", "descripcion" => "poder notariado en caso de que la solicitud sea presentada por un tercero","status" => 2),//12
			array("nombre" => "Reporte fotográfico de fachada e interior del predio", "descripcion" => "Reporte fotográfico de fachada e interior del predio","status" => 0),//13
			array("nombre" => "VoBo de la asociación de colonos", "descripcion" => "VoBo de la asociación de colonos, de no existir, solicitarlo a los vecinos de enfrente o colindantes","status" => 0),//14
			array("nombre" => "Prefactibilidad de servicios de agua potable expedido por la CEA o recibo de pago de agua", "descripcion" => "pre factibilidad de servicios de agua potable expedido por la CEA o recibo de pago de agua","status" => 0),//15
			//tramite 3
			array("nombre" => "Oficio dirigido a Director de Desarrollo Urbano y Vivienda", "descripcion" => "*Arq. Luis Eduardo Barajas Ortiz* Especifique el giro(s) pretendido(s) y ubicación del predio","status" => 0),//16
			array("nombre" => "Documento dictamen de uso de suelo", "descripcion" => "dictamen de uso de suelo","status" => 0),//17
			array("nombre" => "Contrato de arrendamiento", "descripcion" => "Contrato de arrendamiento","status" => 0),//18
			array("nombre" => "Credencial de elector del solicitante", "descripcion" => "credencial de elector del solicitante","status" => 0),//19
			array("nombre" => "Alineamiento y Número Oficial", "descripcion" => "Alineamiento y No. Oficial","status" => 0),//20
			array("nombre" => "Formato de solicitud de Licencia Municipal", "descripcion" => "Formato de solicitud de Licencia Municipal","status" => 0),//21
			array("nombre" => "Dictamen de uso de suelo", "descripcion" => "Dictamen de uso de suelo","status" => 0),//22
			array("nombre" => "Documento de factibilidad de giro", "descripcion" => "factibilidad de giro","status" => 0),//23
			array("nombre" => "Aviso de inscripción en el SAT", "descripcion" => "Aviso de inscripción en el SAT apertura de establecimiento o cambio de domicilio","status" => 0),//24
			array("nombre" => "Visto Bueno de Protección Civil", "descripcion" => "Visto Bueno de Protección Civil renovar cada año","status" => 0),//25
			array("nombre" => "Licencia sanitaria", "descripcion" => "Licencia sanitaria en caso de que el giro sea con venta de alimentos y otros","status" => 0),//26
			array("nombre" => "INE", "descripcion" => "INE","status" => 0),//27
			array("nombre" => "Acta Constitutiva", "descripcion" => "acta constitutiva","status" => 1),//28
			array("nombre" => "Solicitud por escrito dirigida al H. Ayuntamiento de Tequisquiapan, Qro.", "descripcion" => "debe contener nombre o razón social de quien promueve o representante legal, domicilio para oír y recibir notificaciones y personas autorizadas para recibirlas, especificar el giro comercial, lugar, fecha, firma","status" => 0),//29
			array("nombre" => "Factibilidad de giro", "descripcion" => "factibilidad de giro","status" => 0),//30
			array("nombre" => "Identificación oficial con fotografía", "descripcion" => "identificación oficial con fotografía","status" => 0),//31
			array("nombre" => "Croquis de localización", "descripcion" => "debiendo especificar a qué distancia se encuentra el negocio de escuelas, centros de salud, hospitales, iglesias o giros comerciales iguales al solicitante","status" => 0),//32
			array("nombre" => "Documento que acredite la propiedad", "descripcion" => "este puede ser título de propiedad, escrituras públicas, contrato de arrendamiento, constancia notarial de escrituración","status" => 0),//33
			array("nombre" => "VoBo otorgado por la Dirección de Protección Civil Municipal", "descripcion" => "visto bueno otorgado por la Dirección de Protección Civil Municipal","status" => 0),//34
			array("nombre" => "Registro federal de contribuyentes (RFC)", "descripcion" => "registro federal de contribuyentes (RFC), alta en hacienda, y último pago","status" => 0),//35
			array("nombre" => "Comprobante de domicilio", "descripcion" => "comprobante de domicilio (solicitante)","status" => 0),//36
			array("nombre" => "Recibo de agua del negocio", "descripcion" => "recibo de agua del negocio, tarifa comercial","status" => 0),//37
			array("nombre" => "Escritos originales de los vecinos inmediatos (cuatro o seis)", "descripcion" => "Escritos originales de los vecinos inmediatos (cuatro o seis), con copia de identificación oficial en donde manifiesten su conformidad (anuencia) para que el establecimiento obtenga el visto bueno de venta de bebidas alcohólicas (señalando que tipo de bebidas).","status" => 0),//38
			array("nombre" => "Visto bueno expedido por la delegación o sub delegación de la comunidad o colonia en la que se encuentre el local", "descripcion" => "Visto bueno expedido por la delegación o sub delegación de la comunidad o colonia en la que se encuentre el local","status" => 0),//39
			array("nombre" => "Dos fotografías originales del exterior y dos del interior del lugar donde se encuentra establecido el negocio o donde se pretende establecer", "descripcion" => "Dos fotografías originales del exterior y dos del interior del lugar donde se encuentra establecido el negocio o donde se pretende establecer","status" => 0),//40
			array("nombre" => "Visto bueno expedido por bomberos voluntarios de Tequisquiapan", "descripcion" => "Visto bueno expedido por bomberos voluntarios de Tequisquiapan","status" => 0),//41
			array("nombre" => "Licencia de funcionamiento", "descripcion" => "licencia de funcionamiento (si es el caso)","status" => 0),//42
			//Proteccikón Civil
			array("nombre" => "Alta en Hacienda", "descripcion" => "alta en Hacienda","status" => 0),//43
			array("nombre" => "XXXXalineamiento y Número OficialXXXXX", "descripcion" => "expedicion de Alineamiento y Número Oficial","status" => 0),//44
			array("nombre" => "Croquis de Ubicación", "descripcion" => "Croquis de Ubicación","status" => 0),//45
			array("nombre" => "Carpeta de programa interno de protección civil", "descripcion" => "Carpeta de programa interno de protección civil","status" => 1),//46
			//######################################################
			//Trámite 7 Licencia de Construcción
			//REQUISITOS  DE OBRA MENOR Obra Menor, Licencia de Bardeo, Licencia de Acabados.
			array("nombre" => "Constancia de propiedad inscrita en el Registro Público de la Propiedad o constancia notarial", "descripcion" => "constancia de propiedad, inscrita en el Registro Público de la Propiedad o constancia notarial;","status" => 0),//47
			//Modificación de Fachada
			array("nombre" => "Licencia y planos anteriores autorizados", "descripcion" => "licencia y planos anteriores autorizados (en su caso)","status" => 0),//48
			//LICENCIA DE CONSTRUCCIÓN CASA HABITACIÓN
			array("nombre" => "Croquis en donde se muestre la fachada anterior y la propuesta", "descripcion" => "proyecto o croquis en donde se muestre la fachada anterior y la propuesta","status" => 0),//49
			array("nombre" => "Escrituras, título de propiedad o constancia notarial de trámite de escritura", "descripcion" => "escrituras, título de propiedad o constancia notarial de trámite de escritura","status" => 0),//50
			array("nombre" => "Credencial vigente del colegio al que está inscrito el perito responsable", "descripcion" => "credencial vigente del colegio al que está inscrito","status" => 0),//51
			array("nombre" => "Planos arquitectónicos que contengan (plantas de conjunto, plantas arquitectónicas", "descripcion" => "Planos arquitectónicos que contengan (plantas de conjunto, plantas arquitectónicas, plantas de azotea, cortes y fachadas, croquis de localización en planos con calles colindantes norte y algunos puntos de referencia, proyecto de instalación hidráulica y sanitaria indicando diámetros de tuberías, separación de aguas negras de aguas grises y hacer detalle de fosa sética)","status" => 0),//52
			array("nombre" => "XXplanos estructuralesXX", "descripcion" => "planos estructurales que contengan (cimentación, armado de cadenas, castillos, trabes, columnas, etc., armado de losas, destalles constructivos, especificaciones","status" => 0),//53
			//LICENCIA PARA AMPLIACIÓN, REMODELACIÓN
			array("nombre" => "Licencia de conexión al drenaje", "descripcion" => "licencia de conexión al drenaje Nota*No obligatorio y en su caso*","status" => 0),//54
			array("nombre" => "Planos anteriores autorizados", "descripcion" => "en caso de ser revalidación deberá presentar dos planos anteriores incluyendo planos estructurales","status" => 0),//55
		  	array("nombre" => "proyecto de instalación hidráulica y sanitaria, indicando diámetros de tuberías, separación de aguas negras, grises y hacer detalle de la fosa séptica)", "descripcion" => "proyecto de instalación hidráulica y sanitaria, indicando diámetros de tuberías, separación de aguas negras, grises y hacer detalle de la fosa séptica","status" => 0),//56
			//Terminación de Obra
			array("nombre" => "Licencia de construcción autorizada", "descripcion" => "licencia de construcción autorizada","status" => 0),//57
			array("nombre" => "Planos arquitectónicos originales autorizados que contengan plantas y fachadas", "descripcion" => "Planos arquitectónicos originales autorizados que contengan plantas y fachadas","status" => 0),//58
			array("nombre" => "Bitácora de obra", "descripcion" => "Bitácora de obra, firmada por el propietario o representante legal y Director Responsable de Obra.","status" => 0),//59
			array("nombre" => "Placa de identificación de obra", "descripcion" => "Placa de identificación de obra", "status" => 0),//60
			array("nombre" => "Planos autorizados arquitectónicos donde contenga la planta de conjunto", "descripcion" => "planos autorizados arquitectónicos donde contenga la planta de conjunto.","status" => 0),//61

			//REGULARIZACIÓN DE OBRA
			array("nombre" => "Planos arquitectónicos", "descripcion" => "Planos arquitectónicos ","status" => 0),//62
			array("nombre" => "Cédula del perito", "descripcion" => "cédula del perito","status" => 0),//63
			array("nombre" => "Credencial del colegio al que esté inscrito el DRO", "descripcion" => "credencial del colegio al que esté inscrito el D.R.O","status" => 0),//64
			array("nombre" => "Visto Bueno del INAH", "descripcion" => "Visto Bueno del INAH; solo aplicable a obras dentro del perímetro de la zona de Monumentos.","status" => 0),//65
			array("nombre" => "Licencia de construcción anterior autorizada", "descripcion" => "licencia de construcción anterior autorizada.","status" => 0),//66
			array("nombre" => "Planos estructurales que contengan Cimentación", "descripcion" => "planos estructurales que contengan Cimentación","status" => 0),//67
			array("nombre" => "Proyecto arquitectónico", "descripcion" => "debe contener la ampliación, modificación o remodelación, etc; por el propietario o reperesentante legal y perito responsable, este punto no aplica a la revalidación","status" => 0),//68
			//INFORME USO DE SUELO
			array("nombre" => "Oficio dirigido a ARQUITECTO LUIS EDUARDO BARAJAS ORTIZ", "descripcion" => "Oficio dirigido a: ARQ. LUIS EDUARDO BARAJAS ORTIZ, Director de Desarrollo Urbano, Vivienda y Ecología especificando el tipo de uso(s) de suelo(s) pretendidos(s).","status" => 0),//69
			array("nombre" => "Planos de escrituras", "descripcion" => "planos de escrituras","status" => 0),//70
			array("nombre" => "Visto Bueno de Cabildo (Provisional)", "descripcion" => "Gaceta Municipal","status" => 0),//71

			//requisito para licencia de funcionamiento
			array("nombre" => "Registro nacional de turismo", "descripcion" => "Constancia de inscripción en el registro nacional de turismo","status" => 0),//72
			array("nombre" => "Constancia de Impacto Ambiental", "descripcion" => "Constancia de Impacto Ambiental","status" => 0),//73
			array("nombre" => "Constancia de Situación Fiscal en el SAT", "descripcion" => "Constancia de Situación Fiscal en el SAT","status" => 0),//74
			array("nombre" => "Licencia de Alcoholes Estatal", "descripcion" => "Licencia de Alcoholes Estatal del año anterior","status" => 0),//75
			array("nombre" => "Documento que acredite el inicio del trámite Estatal de alcohol", "descripcion" => "Algun documento que acredite que ya está en trámite en conseciones y licencias del Estado","status" => 0),//76

			//Complemento Dicatamen Uso de Suelo
			array("nombre" => "Dictamen de Uso de suelo Anterior", "descripcion" => "Dictamen de uso de Suelo anterior","status" => 0),//77

			//Complemento Licencia de Construccion
			array("nombre" => "Croquis Constructivo", "descripcion" => "Croquis Constructivo","status" => 0),//78
			//provicional de alcohol
			array("nombre" => "Solicitud por escrito con la descripción del evento a realizar", "descripcion" => "Solicitud por escrito con la descripción del evento a realizar","status" => 0),//79
			array("nombre" => "Vo Bo del Delegado", "descripcion" => "Vo Bo del Delegado","status" => 0),//80
			array("nombre" => "Contrato de servicios de ambulancia", "descripcion" => "Documento que acredite la constratación de servicios de ambulancia","status" => 0),//81
			array("nombre" => "Contrato de seguridad privada", "descripcion" => "Documento que acredite la contratación de seguridad privada","status" => 0),//82
			//complemento de construccion adicionales
			array("nombre" => "Adicional", "descripcion" => "Documento o planos adicionales nota*No obligatorios*","status" => 0),//83
			array("nombre" => "Adicional 2", "descripcion" => "Documento o planos adicionales nota*No obligatorios*","status" => 0),//84
			//complemento de construccion normales
			array("nombre" => "Cédula de perito responsable", "descripcion" => "cédula de perito responsable","status" => 0),//85

			array("nombre" => "Planos arquitectónicos de Planta de conjuntos", "descripcion" => "Planos arquitectónicos de Planta de conjuntos","status" => 0),//86
			array("nombre" => "Planos arquitectónicos de Plantas arquitectonicas", "descripcion" => "Planos arquitectónicos de Plantas arquitectonicas","status" => 0),//87
			array("nombre" => "Planos arquitectónicos de Azotea", "descripcion" => "Planos arquitectónicos de Azotea","status" => 0),//88
			array("nombre" => "Planos arquitectónicos de cortes", "descripcion" => "Planos arquitectónicos de cortes","status" => 0),//89
			array("nombre" => "Planos arquitectónicos de fachadas", "descripcion" => "Planos arquitectónicos de fachadas","status" => 0),//90
			//planos estructurales
			array("nombre" => "planos estructurales que contengan armado de cadenas", "descripcion" => "planos estructurales que contengan armado de cadenas, castillos, trabes, columnas,etc","status" => 0),//91
			array("nombre" => "planos estructurales que contengan Armado de losas", "descripcion" => "planos estructurales que contengan Armado de losas","status" => 0),//92		
			array("nombre" => "planos estructurales que contengan detalles contructivos", "descripcion" => "planos estructurales que contengan detalles contructivos","status" => 0),//93	
			array("nombre" => "planos estructurales que contengan especificaciones", "descripcion" => "planos estructurales que contengan especificaciones","status" => 0),//94	
		);
	        //ingresamos el registro en la base de datos
		 $this->db->insert_batch("requisito", $data_requisito);
	}//up

	public function down() {
		$this->dbforge->drop_table("requisito");
	}//down

}//class

/* End of file 018_add_requisito.php */
/* Location: ./application/migrations/018_add_requisito.php */
