<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_rol extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'nombre' => array(
		                'type' => 'VARCHAR',
		                'unique' => TRUE,
		                'constraint' => '65',
		                'null'	=> FALSE,
		        ),
		        'descripcion' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '150',
		                'null'	=> TRUE,
		        )
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('rol');
		//Agregamos registros iniciales para la base de datos
		$data_rol =  array(
			array("nombre"=>"Administrador","descripcion"=>"Acceso a todas las funciones del sistema"),//1
			array("nombre"=>"Ciudadano","descripcion"=>"Usuario encargado de realizar un trámite"),//2
			array("nombre"=>"Caja","descripcion"=>"Persona Encargada de Cobrar en Caja los Trámites"),//3
			array("nombre"=>"Ventanilla","descripcion"=>"Es aquella que valida la documentación, archiva y atiende al ciudadano"),//4
			array("nombre"=>"Encargado de Números Oficiales","descripcion"=>"Asignación de Numeros Oficiales"),//5
			array("nombre"=>"Director de Desarrollo Urbano","descripcion"=>"Autoriza el támite"),//6
			array("nombre"=>"Secretario de Desarrollo Urbano","descripcion"=>"Auoriza el trámite"),//7
			array("nombre"=>"Inspección","descripcion"=>"Persona Encargada de Realizar Inspecciones para Generar Reportes"),//8
			array("nombre"=>"Jefe de Inspección","descripcion"=>"Persona Encargada de Autorizar Inspecciones"),//9
			array("nombre"=>"Secretaria de Director","descripcion"=>"Persona Encargada de Recabar firmas"),//10
			array("nombre"=>"Encargado Dictamen Uso de Suelo","descripcion"=>"Persona encargada de dictamen uso de suelo"),//11
			array("nombre"=>"Secretario de finanzas públicas municipales","descripcion"=>"Tesorero de ayuntamiento"),//12
			array("nombre"=>"Presidente municipal","descripcion"=>"Presiente del municipio de Tequisquiapan"),//13
			array("nombre"=>"Área jurídica","descripcion"=>"Área jurídica"),//14
			array("nombre"=>"Comisión","descripcion"=>"Comisión para análisis"),//15
			array("nombre"=>"Área Técnica","descripcion"=>"Área técnica"),//16
			array("nombre"=>"Área Cablido","descripcion"=>"Área cabildo"),//17
			array("nombre"=>"Sesión Ordinária","descripcion"=>"Sesión ordinária"),//18
			array("nombre"=>"Secretaria","descripcion"=>"Secretaria de ayuntamiento"),//19
			array("nombre"=>"Opinión Técnica","descripcion"=>"Inspector Encargado de Generar la Opinión Técnica Para El Permiso De Venta De Bebidas Alcoholicas"),//20
			array("nombre"=>"Inspector de Sec de Gobierno","descripcion"=>"Inspector de Sec de Gob encargado de Finanzas"),//21
			array("nombre"=>"Secretario General","descripcion"=>"Secretario General"),//22
			array("nombre"=>"Sub Director","descripcion"=>"Sub Director"),//23
			array("nombre"=>"Coordinacion Ventanilla DU","descripcion"=>"Coordinación de Ventanilla"),//24
			array("nombre"=>"Encargado DB Predial","descripcion"=>"Encargado de Base de Datos Predial"),//25
		);//dataRol
		$this->db->insert_batch("rol", $data_rol);
	}//up

	public function down() {
		$this->dbforge->drop_table("rol");
	}//down

}//class

/* End of file 006_add_rol.php */
/* Location: ./application/migrations/006_add_rol.php */
