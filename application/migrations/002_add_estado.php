<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_estado extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'nombre' => array(
		                'type' => 'CHAR',
		                'constraint' => '100',
		                'unique' => TRUE,
		                'null'	=> FALSE,
		        ),		        
		        'id_pais' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		//creamos la tabla
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('estado');
		$this->db->query("ALTER TABLE `estado` ADD FOREIGN KEY (`id_pais`) REFERENCES `pais`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		 //creamos un array con los datos de los estados
		$data_estado = array(
		 	array("nombre" => "Aguascalientes", "id_pais" => 110),
		 	array("nombre" => "Baja California", "id_pais" => 110),
		 	array("nombre" => "Baja California Sur", "id_pais" => 110),
		 	array("nombre" => "Campeche", "id_pais" => 110),
		 	array("nombre" => "Chihuahua", "id_pais" => 110),
		 	array("nombre" => "Chiapas", "id_pais" => 110),
		 	array("nombre" => "Coahuila", "id_pais" => 110),
		 	array("nombre" => "Colima", "id_pais" => 110),
		 	array("nombre" => "Durango", "id_pais" => 110),
		 	array("nombre" => "Guanajuato", "id_pais" => 110),
		 	array("nombre" => "Guerrero", "id_pais" => 110),
		 	array("nombre" => "Hidalgo", "id_pais" => 110),
		 	array("nombre" => "Jalisco", "id_pais" => 110),
		 	array("nombre" => "México", "id_pais" => 110),
		 	array("nombre" => "Michoacán", "id_pais" => 110),
		 	array("nombre" => "Morelos", "id_pais" => 110),
		 	array("nombre" => "Nayarit", "id_pais" => 110),
		 	array("nombre" => "Nuevo León", "id_pais" => 110),
		 	array("nombre" => "Oaxaca", "id_pais" => 110),
		 	array("nombre" => "Puebla", "id_pais" => 110),
		 	array("nombre" => "Querétaro", "id_pais" => 110),
		 	array("nombre" => "Quintana Roo", "id_pais" => 110),
		 	array("nombre" => "San Luis Potosí", "id_pais" => 110),
		 	array("nombre" => "Sinaloa", "id_pais" => 110),
		 	array("nombre" => "Sonora", "id_pais" => 110),
		 	array("nombre" => "Tabasco", "id_pais" => 110),
		 	array("nombre" => "Tamaulipas", "id_pais" => 110),
		 	array("nombre" => "Tlaxcala", "id_pais" => 110),
		 	array("nombre" => "Veracruz", "id_pais" => 110),
		 	array("nombre" => "Yucatán", "id_pais" => 110),
		 	array("nombre" => "Zacatecas", "id_pais" => 110)
		);
		 //ingresamos el registro en la base de datos
		 $this->db->insert_batch("estado", $data_estado);		
	}//up

	public function down() {
		$this->dbforge->drop_table("estado");
	}//down

}

/* End of file 001_add_estado.php */
/* Location: ./application/migrations/001_add_estado.php */