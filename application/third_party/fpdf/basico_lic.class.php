<?php
class PDF extends MYPDF
{
	protected $_server_link = NULL;
	
  function setServerLink($server_link)
	{
		$this->_server_link = $server_link;
  }  
	function head($dependencia,$coordinacion,$folio_mascaraR)
	{
		$this->Vdependencia = $dependencia;
		$this->Vcoordinacion = $coordinacion; 
		$this->Vfolio_mascaraR = $folio_mascaraR;
  }
	

  function getServerLink(){
        return $this->_server_link;
  }
	function Header()
	{
		$this->Image(''.$this->getServerLink().'elmarques/control/images/logo_gobmunicipal.jpg',85,5,40,16);
		$this->SetFont('Arial','',10);
		$this->Cell(190,4,utf8_decode(mb_strtoupper($this->Vdependencia)),0,1,'R');
		$this->Cell(190,4,utf8_decode(mb_strtoupper($this->Vcoordinacion)),0,1,'R');
		$this->Cell(190,4,utf8_decode(mb_strtoupper('Folio No: '.$this->Vfolio_mascaraR)),0,1,'R');
		$this->ln(4);


	}
	// Pie de página
	function Footer()
	{
		$this->SetY(-40);
		$this->SetX(null);
		$this->SetFillColor(255,179,179);
		$this->SetY(-22);
		$this->SetFont('Arial','',6);
    $this->Image(''.$this->getServerLink().'elmarques/control/images/logo_progreso.jpg','83',235,40,30);	
		$this->Rect(15,262,185,0,'F');
		$this->Rect(15,263,185,2,'F');
		$this->SetY(-15);
		$this->SetFont('Arial','',6);
		$this->Cell(65,5,'',0,0,'C',0);
		$this->Cell(60,5,utf8_decode('Venustiano Carranza No. 2, La Cañada, El Marqués, Querétaro'),0,0,'C');
		$this->Cell(65, 5, utf8_decode("Página " . $this->PageNo() . " de {nb}"), 0, 0,'R'); 
		$this->SetY(-12); 
		$this->Cell(20,5,' ',0,0,'C');
		$this->Cell(140,5,'CP 76240, TEL. (442) 238 84 00',0,0,'C');
				$this->Cell(20,5,' ',0,0,'C');

		$this->SetY(-10); 
		$this->Cell(20,5,'',0,0,'C');
		$this->Cell(140,5,'www.elmarques.gob.mx',0,1,'C');
				$this->Cell(20,5,'',1,0,'C');

		
	}

}
?>