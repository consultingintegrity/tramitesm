<?php
/**
 * @author Pepe-Droid
 */
class LoadFile{
    /**
     * summary
     */
   public function __construct()
    {
    	//Obtenemos la instancia ci para cceder a las librerias de codeigniter
        $this->ci =& get_instance();
    }//construct

    public function upload_files($config,$input){
		    $this->ci->load->library('upload', $config);
        $this->ci->upload->initialize($config);
        if (!$this->ci->upload->do_upload($input)) {
            //*** ocurrio un error
            $data['uploadError'] = $this->ci->upload->display_errors();
            $data["valid"] = false;
        }//if
        else{
            $data['uploadSuccess'] = $this->ci->upload->data();
            $data["valid"] = true;
        }//else
        return $data;
      }//upload_files

    public function dir_exist($dir){
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }//if
    }//dir_exist
}//class
