<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";

    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
    class Pdf extends FPDF {
      protected $B = 0;
      protected $I = 0;
      protected $U = 0;
      protected $HREF = '';

      public function __construct() {
            parent::__construct();
        }
        // El encabezado del PDF
        function Header(){
            $this->Image(base_url().'plantilla/images/img-tequis/logo.png',168,9,30);
            $this->Image(base_url().'plantilla/images/img-tequis/escudo.png',17,8,22);
            $this->SetFont('Arial','B',18);
      }
       // El pie del pdf
      function Footer(){
           $this->SetY(-15);
           $this->SetFont('Arial','I',8);
           $this->Cell(0,10,'Pag. '.$this->PageNo().'/{nb}',0,0,'C');
        }

      function WriteHTML($html){
        // HTML parser
        $html = str_replace("\n",' ',$html);
        $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
        foreach($a as $i=>$e){
            if($i%2==0){
                // Text
                if($this->HREF)
                    $this->PutLink($this->HREF,$e);
                else
                    $this->Write(5,$e);
            }
            else{
              // Tag
              if($e[0]=='/')
                  $this->CloseTag(strtoupper(substr($e,1)));
              else{
                // Extract attributes
                $a2 = explode(' ',$e);
                $tag = strtoupper(array_shift($a2));
                $attr = array();
                foreach($a2 as $v){
                  if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])] = $a3[2];
                }//foreach
                $this->OpenTag($tag,$attr);
              }//else
            }//else
          }//foreach
      }//writeHtml

      function OpenTag($tag, $attr){
        // Opening tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,true);
        if($tag=='A')
            $this->HREF = $attr['HREF'];
        if($tag=='BR')
          $this->Ln(5);
      }

      function CloseTag($tag){
        // Closing tag
        if($tag=='B' || $tag=='I' || $tag=='U')
            $this->SetStyle($tag,false);
        if($tag=='A')
            $this->HREF = '';
      }

      function SetStyle($tag, $enable){
        // Modify style and select corresponding font
        $this->$tag += ($enable ? 1 : -1);
        $style = '';
        foreach(array('B', 'I', 'U') as $s)
        {
            if($this->$s>0)
                $style .= $s;
        }
        $this->SetFont('',$style);
      }

      function PutLink($URL, $txt){
        // Put a hyperlink
        $this->SetTextColor(0,0,255);
        $this->SetStyle('U',true);
        $this->Write(5,$txt,$URL);
        $this->SetStyle('U',false);
        $this->SetTextColor(0);
      }
      function WriteHtmlCell($cellWidth, $html){
        $rm = $this->rMargin;
        $this->SetRightMargin($this->w - $this->GetX() - $cellWidth);
        $this->WriteHtml($html);
        $this->SetRightMargin($rm);
      }
    }//class
?>
