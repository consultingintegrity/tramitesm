<?php
/**
 * @author Daniel Salinas
 * @version 1.0
 */
class CorreoElectronico{
  private $contenido;

    public function __construct()
    {
      //Obtenemos la instancia ci para cceder a las librerias de codeigniter
      $this->ci = &get_instance();
      $this->estilo = '<body style="font-family: Gotham, Helvetica Neue, Helvetica, Arial, sans-serif;">
                        <div style="margin-top: 0;margin-bottom: 0;margin-left: auto;margin-right: auto;background-color: #F2F2F2;width: 600px;min-height: 500px;position: relative" >';
    }
    //funcion que envia correo electronico
    public function mandarCorreo($direccion, $titulo, $contenido)
    {
        //se carga libreria email
        $this->ci->load->library('email');

        $config['protocol'] = 'mail';
        //El servidor de correo que utilizaremos
        $config["smtp_host"] = 'mail.plataformasdinamicas.com.mx';
        //Nuestro usuario
        $config["smtp_user"] = 'no-reply@plataformasdinamicas.com.mx';
        //Nuestra contraseña
        $config["smtp_pass"] = 'txMunicipio';
        //tipo de mail
        $config['mailtype'] = 'html';
        //El puerto que utilizará el servidor smtp
        $config["smtp_port"] = '465';
        //El juego de caracteres a utilizar
        $config['charset'] = 'utf-8';
        //se carga la configuracion
        $this->ci->email->initialize($config);
        //de donde se enviara el correo
        $this->ci->email->from('no-reply@plataformasdinamicas.com.mx', 'Municipio');
        //a que direccion se enviara el correo
        $this->ci->email->to($direccion);
        //el titulo que llevara el correo
        $this->ci->email->subject($titulo);
        //el contenido que llevara el correo
        $this->ci->email->message($contenido);
        //se comprueba que se envio el correo
        if ($this->ci->email->send()) {
            //si el correo es enviado retorna un true
            return true;
        } //if de mensaje enviado

        else {
            //si algo fallo al enviar retorna un false
            return false;
        }

    }

    /**
     * Correo para mandar cada que un ciudadano da de alta una solicitud
     */
    public function inicioTramite($subTitulo,$texto)
    {
      
       $header=base_url() . 'plantilla/images/correo/header.png';
       $logos=base_url() . 'plantilla/images/correo/logos.png';

       $this->contenido .= $this->estilo;
       $this->contenido .=' <header> <img src="';

       $this->contenido .=  $header;
       $this->contenido .='" width="600" height="auto"></header>

                              <div style="padding:40px"><h2>';
       $this->contenido .= $subTitulo;
       $this->contenido .= '</h2><p style="text-align:justify; line-height: 2em;">';
       $this->contenido .= $texto;
       $this->contenido .= '</p><p style="text-align:justify; line-height: 2em;">Para cualquier duda y/o aclaración comunícate al siguiente número: (### 2732327)</p>
                            <div align="center"><img src="';
       $this->contenido .=  $logos;
       $this->contenido .= '" width="215" height="107"></div>
                              </div>

                          <div style="position:relative; left: 0; bottom: 0; width: 100%; background-color: black; color: white;text-align: center;font-size: 8px;">
                            <div style="padding:40px">
                              <table width="100%" border="0">
                                <tbody>
                                  <tr>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Atención Ciudadana</strong><br><br> Querétaro.</p></td>
                                    <td><p style="text-align:justify; line-height: 2em;"><strong>Síguenos en redes sociales</strong><br><br></p></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>

                        </div>
                      </body>
                         ';
       return $this->contenido;
  }//inicioTramite

}//class
/* End of file CorreoElectronico.php */
/* Location: ./application/controllers/CorreoElectronico.php */
