<?php
ini_set('memory_limit', '1024M'); // or you could use 1G
defined('BASEPATH') OR exit('No direct script access allowed');

class Predio extends CI_Model {

	public function __construct(){
    	parent::__construct();
    	//Codeigniter : Write Less Do More
 	}


	public function insertBatch($arr_data){
		$this->db->truncate('predio');
		return $this->db->insert_batch('predio', $arr_data);
	}//insertBatch

	//verifica que existan registros
	public function verifica(){
		$this->db->select("*")
		->from("predio")
		->limit(30);
		$sql = $this->db->get();
		return $sql->result();
	}

	//guardar registro de valid predial
	public function check($arr_data){
		return $this->db->insert('checkpredio', $arr_data);
	}

}//Model

/* End of file Predio.php */
/* Location: ./application/models/Predio.php */