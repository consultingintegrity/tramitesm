<?php
/**
 * @author Guadalupe Valerio
 * @version 1.0
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulario extends CI_Model {
	//Atributos de la clase
	private $idFormulario;

	public function __construct(){


	}//consstruct

	/**
	 * @return object stdClass
	 */
	public function getForm(){
		//obtenemos los datos del formulario
		$this->db->where_in("id",$this->idFormulario);
		$query = $this->db->get("formulario");
		$result = $query->result();
		return $result;
	}//getForm

	/**
	 * @return object stdClass
	 */
	public function getAributoFormulario(){
		//Obtenemos los atributos del frmulario

		$this->db->select("nombre,value, id_formulario")
				 ->from("atributo_formulario")
				 ->join("atributo","atributo_formulario.id_atributo = atributo.id")
				 ->where_in("atributo_formulario.id_formulario",$this->idFormulario);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}//getAtributoFormulario

	/**
	 * @return object stdClass
	 */
	public function getCamposFormulario(){
		//Obtenemos los campos del formulario
		$this->db->select("
			atr.nombre atributo,
			c.type,
			lbl.nombre etiqueta,
			cf.*")
			->from("campo_formulario cf")
			->join("campo c","cf.id_campo = c.id")
			->join("etiqueta lbl","lbl.id = c.id_etiqueta")
			->join("atributo atr", "atr.id = c.id_atributo")
			->where_in("cf.id_formulario",$this->idFormulario)
			->order_by("cf.id", "asc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}//getCamposFormulario

	/**
	 * @return object stdClass
	 */
	public function getFormPestania($arr_pestania,$perfilado_form,$id_tramite){
		//Obtenemos los frm relacionasos con las pestañas
		if($perfilado_form != null){
				$this->db->where_in("id_pestania",$arr_pestania)
									->where_not_in("id_formulario",$perfilado_form);
			}else{
				$this->db->where_in("id_pestania",$arr_pestania);
			}
				$this->db->where('id_tramite', $id_tramite);
		/*$query = $this->db->where_in("id_pestania",$arr_pestania)->get("pestania_form");
		$query = $this->db->where_not_in("id_pestania",$arr_pestania)->get("pestania_form");*/
		$query = $this->db->get('pestania_form');
		$result = $query->result();
		return $result;
	}//getCamposFormulario


	//Getters & Setters
	/**
	 * @return int
	 */
	public function getIdFormulario()
	{
	    return $this->idFormulario;
	}

	/**
	 * @param int $idForulario
	 */
	public function setIdFormulario($idFormulario)
	{
	    $this->idFormulario = $idFormulario;
	    return $this;
	}

	/**
	 * @return object stdClass
	 */
	public function getCamposForm($id){
		//Obtenemos los campos del formulario
		$this->db->select("
			c.type,
			cf.name")
			->from("campo_formulario cf")
			->join("campo c","cf.id_campo = c.id")
			->where("cf.id_formulario",$id)
			->order_by("cf.id", "asc");
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}//getCamposFormulario


}//class

/* End of file Formulario.php */
/* Location: ./application/models/Formulario.php */
