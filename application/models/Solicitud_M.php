<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitud_M extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  /**
   * Insertamos la inspección de una solicitud
   * return Bool $success
   */
  public function insertInspeccion($id_solicitud,$id_usuario,$url)
  {
    $data = array (
      "url" => $url,
      "id_solicitud" => $id_solicitud,
      "id_usuario" => $id_usuario
    ); //Campos de la tabla
    $query = $this->db->insert('inspeccion', $data);
    return $query;
  }//insertInspeccion

  /**
   * return INT $id_solicitud
   */
  public function insertSolicitud($id_tramite,$id_ciudadano,$id_funcionario,$id_fase,$informacion){
    $id_solicitud = 0;
    $data = array(
      "id_tramite"=>$id_tramite,
      "id_ciudadano"=>$id_ciudadano,
      "id_funcionario"=>$id_funcionario,
      "estatus"=>4,
      "id_fase"=>$id_fase
    );
    $this->db->trans_begin();
    $id_solicitud = ($this->db->insert('solicitud', $data)) ?  $this->db->insert_id() : 0;
    if ($id_solicitud > 0) {
      $data = array(
        "informacion"=>$informacion,
        "id_solicitud"=>$id_solicitud
      );
      if (!$this->db->insert('infoSolicitud', $data)){
        $this->db->trans_rollback();
        $id_solicitud = 0;
      }//if
      else{
        $this->db->trans_commit();
      }//else
    }//if > 0
    else{
      $this->db->trans_rollback();
    }//else
    $this->db->trans_complete();
    return $id_solicitud;
  }//insertSolicitud


  /**
   * return INT $id_solicitud
   */
  public function insertSolicitudGiro($id_solicitud,$id_giro){
    $data = array(
      "id_solicitud"=>$id_solicitud,
      "id_giro"=>$id_giro
    );
    $query = $this->db->insert('solicitud_giro', $data);
    return $query;
  }//insertSolicitud


  /**
   * @return Boolean true || false
   * @param INT $id_tipo_construccion
   */
  public function insertSolicitudConstruccion($id_solicitud,$id_tipo_construccion){
    $data = array(
      "id_solicitud"=>$id_solicitud,
      "id_tipoConstruccion"=>$id_tipo_construccion
    );
    $query = $this->db->insert('solicitud_construccion', $data);
    return $query;
  }//insertSolicitud

  /**
   * Obtenemos el archivo de inspección
   * return String $urlDoc
   */
  public function getDocInspeccion($id_solicitud)
  {
    $this->db->select('url,primer_nombre, primer_apellido,fecha')
              ->from("inspeccion")
              ->join("usuario","usuario.id = inspeccion.id_usuario")
              ->join("persona","persona.id = usuario.id_persona")
              ->where("id_solicitud",$id_solicitud)
              ->order_by("inspeccion.id ","DESC");
    $query = $this->db->get();
    return  $query->row();
  }//getSolicitud

  /**
   * Obtenemos la/s licencia/s o documento/s de el trámite
   * return Object stdClass
   */
  public function getLicencia($id_solicitud)
  {
    $this->db->select('d.nombre, ds.url')
            ->from('documento_solicitud ds')
            ->join('documento d','ds.id_documento = d.id','LEFT')
            ->where('ds.id_solicitud',$id_solicitud);
    $query = $this->db->get();
    return $query->result();
  }//getLicencia

   /* Obtenemos el archivo de licencia
   * return String $urlDoc
   */
  public function docLicencia($id_solicitud){
    $this->db->select('url')
              ->from("documento_solicitud")
              ->where("id_solicitud",$id_solicitud);
    $query = $this->db->get();
    return  $query->row();
  }//getlicencia


  /**
   * return Bool $success
   * insertamos el seguimiento de un funcionario con el trámite de una solicitud
   */
   public function update($id_solicitud, $id_funcionario,$id_fase,$estatus,$costo)
   {
     $success = false;
     if ($estatus != 1) {   //Procedemos a nsertar en la tabla funcionario_solicitud
        $data = array(
          "estatus"=>0,
          "fase"=> $id_fase,
          "id_solicitud"=>$id_solicitud,
          "id_usuario"=>$id_funcionario
        );
        $this->db->trans_begin();
        if ($this->db->insert('funcionario_solicitud', $data)) {
          //si se registro, actualizamos en la tabla solicitud
          $data = array(
            "estatus"=>$estatus,
            "id_funcionario"=>$id_funcionario,
            "id_fase"=>$id_fase,
            "costo"=>$costo
          );
          $query_update = $this->db->where('id', $id_solicitud)->update("solicitud",$data);
          if (!$query_update){
            $this->db->trans_rollback();
          }//if
          else{
            $this->db->trans_commit();
            $success = true;
          }//else
        }//if > 0
        else{
          $this->db->trans_rollback();
        }//else
        $this->db->trans_complete();
     }
     else{
       $data = array(
         "estatus"=>$estatus,
         "id_funcionario"=>$id_funcionario,
         "id_fase"=>$id_fase
       );
       $this->db->trans_begin();
       $query_update = $this->db->where('id', $id_solicitud)->update("solicitud",$data);
       if (!$query_update){
         $this->db->trans_rollback();
       }//if
       else{
         $this->db->trans_commit();
         $success = true;
       }//else
     }//else

     return $success;
   }//update

  /**
   * return Boolean valid
   */
   public function insertaRequisitoSolicitud($url,$id_ciudadano,$id_requisito,$id_solicitud)
   {
     $data = array(
       "url"=>$url,
       "id_ciudadano"=>$id_ciudadano,
       "id_requisito"=>$id_requisito,
       "estatus"=>0,
       "id_solicitud"=>$id_solicitud
     );
     $query = ($this->db->insert('requisito_solicitud', $data));
     return $query;
   }//insertaRequisitoSolicitud

  /**
   * return INT idFuncionario
   */
  public function getIdFuncionario($id_fase,$id_dependencia){
    //obtenemos el valor random de un ciudadano por su fase
    $this->db->select('usr.id, ur.id_rol')
          ->from("usuario usr")
          ->join('usuario_rol ur','usr.id = ur.id_usuario','LEFT')
          ->join('rol r','ur.id_rol = r.id','LEFT')
          ->join('fase f','f.id_rol=r.id')
          ->where('f.id',$id_fase)
          ->where('usr.id_dependencia',$id_dependencia)
          ->where('usr.status',1)
    			->order_by("RAND()");
    $query =  $this->db->get();
    return $query->row();
  }//getIdFuncionario

  /**
   * return Boolean valid
   */
  public function funcionarioSolicitud($fase, $id_solicitud, $id_usuario)
  {
    $data = array('fase' => $fase,
                  'id_solicitud'=>$id_solicitud,
                  'id_usuario'=>$id_usuario);
    $query = $this->db->insert('funcionario_solicitud',$data);
    return $query;
  }//funcionarioSolicitud

  /**
   * return fetchRow
   */
   public function getSolicitud($id_funcionario,$estatus,$id_solicitud)
   {
     $data = array('id_funcionario'=>$id_funcionario);
     if($id_solicitud == '') {
       $this->db->select('*')
            ->from('solicitud')
            ->where('id_funcionario',$id_funcionario)
            ->where('estatus',$estatus)
            ->order_by('id','ASC');
      }//if
      else{
        $this->db->select('*')
            ->from('solicitud')
            ->where('id_funcionario',$id_funcionario)
            ->where('estatus',$estatus)
            ->where('id',$id_solicitud);
      }//else

     $query = $this->db->get();
     return $query->row();
   }//getSolicitud

   /**
    * return json row
    */
   public function getDataSolicitud($id_solicitud)
   {
     //obtenemos la info de la Solicitud
     $query =  $this->db->get_where('infoSolicitud',['id_solicitud'=>$id_solicitud],1);

    return $query->row();
   }//getDataSolicitud

   /**
    * Obtenemos los requisitos de una solicitud
    * reurn object stdClass
    */
   public function getRequisitosSolicitud($id_solicitud)
   {
     $this->db->select('rs.id, rs.url, rs.id_requisito, rs.estatus, r.nombre, r.descripcion')
              ->from('requisito_solicitud rs')
              ->join('requisito r','rs.id_requisito = r.id','LEFT')
              ->where('rs.id_solicitud',$id_solicitud);
     $query = $this->db->get();
     return $query->result();
   }//getRequisitosSolicitud

   /**
    * @param INT $id_rol
    * @return INT $id_funcionario
    */
   public function getFuncionarioSeguimientoSolicitud($id_rol,$id_solicitud){
     $id_funcionario = 0;
     $this->db->select('fs.id_usuario')
              ->from('funcionario_solicitud fs')
              ->join('usuario_rol ur', 'fs.id_usuario = ur.id_usuario')
              ->where('ur.id_rol',$id_rol)
              ->where('fs.id_solicitud',$id_solicitud);
      $query = $this->db->get();
      return $query->row();
   }//getFuncionarioSeguimientoSolicitud

   /**
    * @param INT $id_funcionario
    *  @return Object stdClass
    */
   public function pasesCaja($id_funcionario)
   {
     $this->db->select('s.id,s.fecha_inicio,s.costo,
                        u.correo_electronico,u.telefono,
                        p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
                        t.logo, t.nombre,
                        pa.url,pa.fecha_emision, pa.fecha_vencimiento')
              ->from('solicitud s')
              ->join('tramite t','s.id_tramite = t.id','LEFT')
              ->join('usuario u','s.id_ciudadano = u.id','LEFT')
              ->join('persona p','u.id_persona = p.id','LEFT')
              ->join('pago pa','pa.id_solicitud = s.id','LEFT')
              ->where('s.id_funcionario',$id_funcionario)
              ->where('s.estatus',3)
              ->where('s.costo <>',"NULL");
    $query = $this->db->get();
    return $query->result();
   }//pasesCaja

   /**
    * @param INT $id_funcionario
    *  @return Object stdClass
    */
   public function infoSolicitud($id_solicitud,$estatus)
   {
     $this->db->select('s.id,s.fecha_inicio, s.fecha_fin, s.estatus, s.costo, s.id_ciudadano, s.id_funcionario, s.id_fase,s.id_tramite,
                        u.correo_electronico, u.imagen,
                        p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
                        t.logo, t.nombre,
                        g.descripcion as giro, tc.nombreSubTramite as tipoConstruccion')
              ->from('solicitud s')
              ->join('tramite t','s.id_tramite = t.id','LEFT')
              ->join("solicitud_giro sg", "s.id = sg.id_solicitud","LEFT")
              ->join("giro g", "sg.id_giro = g.id","LEFT")
              ->join("solicitud_construccion sc", "s.id = sc.id_solicitud","LEFT")
              ->join("subTramite tc", "sc.id_tipoConstruccion = tc.id","LEFT")
              ->join('usuario u','s.id_ciudadano = u.id','LEFT')
              ->join('persona p','u.id_persona = p.id','LEFT')
              ->where('s.id',$id_solicitud)
              ->where('s.estatus',$estatus);
    $query = $this->db->get();
    return $query->row();
   }//pasesCaja

   public function entregaTramite($id_funcionario)
   {
    $this->db->select('s.id,s.fecha_inicio,s.costo,
                      u.correo_electronico,
                      p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
                      t.logo, t.nombre,t.id as id_tramite,
                      d.url as doc')
    ->from('solicitud s')
    ->join('tramite t','s.id_tramite = t.id','LEFT')
    ->join('usuario u','s.id_ciudadano = u.id','LEFT')
    ->join('persona p','u.id_persona = p.id','LEFT')
    ->join('documento_solicitud d','s.id = d.id_solicitud','LEFT')
    ->where('s.id_funcionario',$id_funcionario)
    ->where('s.estatus',2);
    $query = $this->db->get();
    return $query->result();
   }

   /**
    * @param Int $id_solicitud
    * @param Int $id_ciudadano
    * @return bool $success
    */
   public function delete($id_solicitud,$id_ciudadano,$estatus)
   {
     $success = false;
     $data = array("estatus"=>$estatus);
     $success = $this->db->where("id",$id_solicitud)->where("id_ciudadano",$id_ciudadano)->update('solicitud', $data);
     return $success;
   }//delete

   /**
    * @param Int $id_solicitud
    * @param Int $id_ciudadano
    * @param String $obervaciones
    * @return bool $success
    */
   public function correccion($id_solicitud,$id_funcionario,$observaciones)
   {
     $success = false;
     $data = array("id_solicitud"=>$id_solicitud, "id_funcionario" => $id_funcionario, "observaciones" => $observaciones);
     $success = $this->db->insert('solicitud_correccion', $data);
     return $success;
   }//correccion

   public function existeDocumentoInspeccion($id_solicitud)
   {
     $query =  $this->db->get_where('inspeccion',['id_solicitud'=>$id_solicitud]);
     return $query->row();
   }

   /**
    * actualizamos la inspección de una solicitud
    * return Bool $success
    */
   public function updateInspeccion($id_solicitud,$id_usuario,$url)
   {
     $data = array (
       "url" => $url
     ); //Campos de la tabla
     $query = $this->db->where("id_solicitud",$id_solicitud)->where("id_usuario",$id_usuario)->update('inspeccion', $data);
     return $query;
   }//insertInspeccion

   /**
    * Obtenemos la información de una solicitud
    * return mysql row
    */
   public function getSolicitudCosto($id_solicitud)
   {
     $query = $this->db->get_where('solicitud',['id'=>$id_solicitud],1);
     return $query->row();
   }//insertInspeccion

   /**
    * Obtenemos el pago de una solicitudd
    * return mysql row
    */
   public function getPago($id_solicitud)
   {
     $query = $this->db->get_where('pago',['id_solicitud'=>$id_solicitud],1);
     return $query->row();
   }//insertInspeccion

    /**
     * Obtenemos el giro de la solicitud
     * @param INT id_solicitud
     * @return mysql row
     */
    public function giroSolicitud($id_solicitud)
    {
      $this->db->select('gs.*, g.descripcion,g.vta_alcohol')
              ->from('solicitud_giro gs')
              ->join("giro g","gs.id_giro = g.id","LEFT")
              ->where("gs.id_solicitud",$id_solicitud);
      $query = $this->db->get();
      return $query->row();
    }//insertInspeccion

    /**
     * Obtenemos el tipo de licencia de construccion
     * @param INT id_solicitud
     * @return mysql row
     */
    public function tipoConstruccionSoicitud($id_solicitud)
    {
      $this->db->select('sc.*, tp.nombreSubTramite as tipo')
              ->from('solicitud_construccion sc')
              ->join("subTramite tp","sc.id_tipoConstruccion = tp.id","LEFT")
              ->where("sc.id_solicitud",$id_solicitud);
      $query = $this->db->get();
      return $query->row();
    }//insertInspeccion

    /**
     * Actualizamos el impacto del giro de la solicitud
     * @param INT id_solicitud
     * @param STRING impacto
     * @return mysql row
     */
    public function updateGiroSolicitud($id_solicitud,$impacto)
    {
      $data = array (
        "impacto" => $impacto
      ); //Campos de la tabla
      $query = $this->db->where("id_solicitud",$id_solicitud)->update('solicitud_giro', $data);
      return $query;
    }//insertInspeccion
      //Apartir de aquí las funciones se ocupan para el controlador Administrador

    /**
     * Obtenemos todas las solicitudes
     * @return mysql array StdClass
     */
    public function getAllSolicitud($estatus,$fecha = ""){
      $this->db->where("estatus",$estatus);
      if ($fecha != "") {
        $this->db->where("MONTH(fecha_fin)",$fecha);
      }
      $query = $this->db->get('solicitud');
      return $query->result();
    }//getAllSolicitud
    /**
     * Obtenemos l dinero generado
     * @return Int $dinero
    */
    public function getDineroGenerado($fecha){
      $query = $this->db->select_sum("costo")
          ->where("MONTH(fecha_fin)",$fecha)
          ->get('solicitud');
      return $query->row();
    }//getAllSolicitud

    /**
     * Obtenemos todas las solicitudes con un estatus
     * @return mysql array StdClass
     */
    public function solicitudes($date = ""){
      //se hace la consulta correspondiente para obtener los datos que contendra la tabla
      $this->db->select("s.*,
                        t.nombre as tramite,
                        p.primer_nombre as primer_nombreC, p.segundo_nombre as segundo_nombreC, p.primer_apellido as primer_apellidoC, p.segundo_apellido as segundo_apellidoC, u.correo_electronico as correo_electronicoC, pp.primer_nombre as primer_nombreF,pp.segundo_nombre as segundo_nombreF, pp.primer_apellido as primer_apellidoF, pp.segundo_apellido as segundo_apellidoF, uu.correo_electronico as correo_electronicoF, d.nombre as dependencia ,g.descripcion as giro, tc.nombreSubTramite as tipoConstruccion")
          ->from("solicitud s")
          ->join("tramite t", "t.id=s.id_tramite")
          ->join("solicitud_giro sg", "s.id = sg.id_solicitud","LEFT")
          ->join("giro g", "sg.id_giro = g.id","LEFT")
          ->join("solicitud_construccion sc", "s.id = sc.id_solicitud","LEFT")
          ->join("subTramite tc", "sc.id_tipoConstruccion = tc.id","LEFT")
          ->join("usuario u", "s.id_ciudadano=u.id")
          ->join("persona p", "u.id_persona=p.id")
          ->join("usuario uu", "s.id_funcionario=uu.id")
          ->join("dependencia d","d.id = uu.id_dependencia")
          ->join("persona pp", "uu.id_persona=pp.id");
          if ($date != "") {
            $this->db->where("MONTH(s.fecha_fin)", $date);
          }
          $this->db->order_by("s.id");
      //el resultado de la consulta se almacena en la variable query
      $query = $this->db->get();
      //se regresa un result que se guarda en la variable query
      return $query->result();
    }//solicitudes

    /**
     * @param Date $date
     * @return Object stdClass Mysql
     *  Obtenemos el proreso de las solicitudes
     */
    public function progresoSolictud($date)
    {
      $this->db->select("t.nombre, t.logo,
                        s.id, s.fecha_inicio, s.estatus,
                        (SELECT 100 / COUNT(tramite_fase.id_fase) * f.noFase FROM tramite_fase WHERE tramite_fase.id_tramite = t.id ) as progreso,
                        g.descripcion as giro, tc.nombreSubTramite as tipoConstruccion")
          ->from("solicitud s")
          ->join("tramite_fase tf","s.id_fase = tf.id_fase")
          ->join("tramite t", "s.id_tramite = t.id")
          ->join("fase f", "s.id_fase = f.id")
          ->join("solicitud_giro sg", "s.id = sg.id_solicitud","LEFT")
          ->join("giro g", "sg.id_giro = g.id","LEFT")
          ->join("solicitud_construccion sc", "s.id = sc.id_solicitud","LEFT")
          ->join("subTramite tc", "sc.id_tipoConstruccion = tc.id","LEFT")
          ->where("MONTH(s.fecha_fin)", $date)
          ->order_by('s.id' );
      $query = $this->db->get();
      return $query->result();
    }//progresoSolictud

    /**
     * @param Date $date
     * @return Object stdClass Mysql
     *  Obtenemos los documentos y licencias que se han generado
     */
    public function getCountdocumentosGenerados($date)
    {
      $this->db->select("COUNT(ds.id) as documentos_generados,
                        t.id as id_tramite, t.nombre as tramite,
                        g.descripcion as giro,
                        tc.nombreSubTramite as tipoConstruccion")
          ->from("documento_solicitud ds")
          ->join("solicitud s","s.id = ds.id_solicitud","LEFT")
          ->join("tramite t", "s.id_tramite = t.id","LEFT")
          ->join("solicitud_giro sg", "s.id = sg.id_solicitud","LEFT")
          ->join("giro g", "sg.id_giro = g.id","LEFT")
          ->join("solicitud_construccion sc", "s.id = sc.id_solicitud","LEFT")
          ->join("subTramite tc", "sc.id_tipoConstruccion = tc.id","LEFT")
          ->where("MONTH(s.fecha_fin)", $date)
          ->group_by("t.nombre")
          ->order_by('s.id' );
      $query = $this->db->get();
      return $query->result();
    }//progresoSolictud

    /**
     * @param Int $estatus
     * @return Object stdClass Mysql
     *  Obtenemos el total de solicitudes por estatus
     */
    public function getCountSolicitudes($estatus){
      $query = $this->db->get_where('solicitud',['estatus'=>$estatus]);
      return $query->num_rows();
    }//getCountSolicitudes

    /**
     * @param Int $id_solicitud
     * @return Object stdClass Mysql
     *  Obtenemos los funcionarios que revisarón una solicitud
     */
    public function seguimientoFuncionarios($id_solicitud){
      $this->db->select('fs.fecha as fecha_revision_funcionario,
                        p.primer_nombre, p.primer_apellido,
                        u.imagen,
                        r.nombre as rol,
                        d.nombre as dependencia,
                        s.fecha_inicio')
                ->from("funcionario_solicitud fs")
                ->join("solicitud s","fs.id_solicitud = s.id","LEFT")
                ->join("usuario u","fs.id_usuario = u.id","LEFT")
                ->join("dependencia d","d.id = u.id_dependencia","LEFT")
                ->join("persona p","p.id = u.id_persona","LEFT")
                ->join("usuario_rol ur","ur.id_usuario = u.id","LEFT")
                ->join("rol r","r.id = ur.id_rol","LEFT")
                ->where("fs.id_solicitud",$id_solicitud);
      $query = $this->db->get();
      return $query->result();
    }//seguimientoFuncionarios

    /**
     * Funciones para llenar las gráficas :V
    */

    public function tramitesSolicitados(){
      $this->db->select('COUNT(s.id) as total, t.nombre as tramite')
              ->from("solicitud s")
              ->join("tramite t","s.id_tramite = t.id","LEFT")
              ->group_by("t.nombre");
      $query = $this->db->get();
      return $query->result();

    }//tramitesSolicitados

    public function dineroPorTramites(){
      $this->db->select('SUM(s.costo) as ingreso, t.nombre as tramite')
              ->from("solicitud s")
              ->join("tramite t","s.id_tramite = t.id","LEFT")
              ->group_by("t.nombre");
      $query = $this->db->get();
      return $query->result();
    }//dineroPorTramites


     /**
   * Insertamos la opinion tecnica de una solicitud
   * return Bool $success
   */
  public function insertOpinion($id_solicitud,$id_usuario,$url){
    //guardamos en la tabla de opiniones
    $data = array (
      "id_solicitud" => $id_solicitud,
      "url" => $url,
      "id_funcionario" => $id_usuario
    ); //Campos de la tabla
    $query = $this->db->insert('opinion_tecnica', $data);
    return $query;
  }//insertOpinion

  public function getOpinionTec($id_solicitud){
    $this->db->select('ot.url,r.nombre,d.nombre as dependencia')
    ->from("opinion_tecnica ot")
    ->join("usuario_rol usr","usr.id_usuario = ot.id_funcionario","LEFT")
    ->join("rol r","usr.id_rol = r.id","LEFT")
    ->join("usuario us","us.id = usr.id_usuario","LEFT")
    ->join("dependencia d","d.id = us.id_dependencia","LEFT")
    ->where("id_solicitud",$id_solicitud);
    $sql = $this->db->get();
    return $sql->result();
} //getopiniones

 /**
   * Insertamos dictamen de alcohol
   * return Bool $success
   */
  public function insertDictamenAlcohol($id_solicitud,$id_usuario,$url)
  {
    $data = array (
      "id_solicitud" => $id_solicitud,
      "id_funcionario" => $id_usuario,
      "url" => $url
    ); //Campos de la tabla
    $query = $this->db->insert('dictamen_alcohol', $data);
    return $query;
  }//dictamen_alcohol

  //traer el dictamen
  public function getDocDictamen($id_solicitud){

    $this->db->select('url')
    ->from("dictamen_alcohol")
    ->where("id_solicitud",$id_solicitud);
    $sql = $this->db->get();
     return $sql->result();
  }//dictamen_alcohol

  public function getFuncionarioSeguimientoSolicitudAlcohol($id_rol,$id_solicitud){
    $id_funcionario = 0;
    $this->db->select('fs.id_usuario')
             ->from('funcionario_solicitud fs')
             ->join('usuario_rol ur', 'fs.id_usuario = ur.id_usuario')
             ->where('ur.id_rol',$id_rol)
             ->where('fs.id_solicitud',$id_solicitud)
             ->order_by('fs.id_usuario','DESC');
     $query = $this->db->get();
     return $query->row();
  }//getFuncionarioSeguimientoSolicitudAlcohol

  public function getOpinionTecFuncionario($id_solicitud,$id_funcionario){
    $this->db->select('*')
    ->from("opinion_tecnica ot")
    ->where("id_solicitud",$id_solicitud)
    ->where("id_funcionario",$id_funcionario);
    $sql = $this->db->get();
    return $sql->row();
} //getopiniones

    /**
   * Insertamos la opinion tecnica de una solicitud
   * return Bool $success
   */
  public function insertarFuncionarioOpinion($id_solicitud,$id_funcionario,$fechaDictamen){
    //guardamos en la tabla de opiniones
    $data = array (
      "id_solicitud" => $id_solicitud,
      "id_funcionario" => $id_funcionario,
      "fecha_dictamen" => $fechaDictamen,
    ); //Campos de la tabla
    $query = $this->db->insert('usuariosopinion', $data);
     return $query;
  }//insertOpinion

  public function idFuncionarioOpinion($id_solicitud){
    //guardamos en la tabla de opiniones
    $this->db->select('id')
    ->from("usuariosopinion")
    ->where("id_solicitud",$id_solicitud);
    $sql = $this->db->get();
    return $sql->row();
  }//insertOpinion

  /**
   * Insertamos la ruta del archivo de opinion técnica
   * return Bool $success
   */
  public function insertOpinionTecnica($id_solicitud,$id_funcionario,$url)
  {
    $data = array (
      "id_solicitud" => $id_solicitud,
      "url" => $url,
      "id_funcionario" => $id_funcionario
    ); //Campos de la tabla
    $query = $this->db->insert('opinion_tecnica', $data);
    return $query;
  }//insertInspeccion

  /**
   * actualizamos el estatus del usuario para que no se le muestre mas ese trámite
   * return Bool $success
   */
  public function updateUsuarioOpinionTecnica($id_solicitud,$id_funcionario)
  {
    $data = array (
      "estatus" => 0
    ); //Campos de la tabla
    $query = $this->db->where("id_solicitud",$id_solicitud)->where("id_funcionario",$id_funcionario)->update('usuariosopinion', $data);
    return $query;
  }//insertInspeccion

  public function FuncionariosOpinion($id_solicitud){
    //guardamos en la tabla de opiniones
    $this->db->select('*')
    ->from("usuariosopinion")
    ->where("id_solicitud",$id_solicitud);
    $sql = $this->db->get();
    return $sql->result();
  }//insertOpinion

//trae si el giro es venta de alcohol
  public function VentaDeAlcohol($id){
    $this->db->select('vta_alcohol')
    ->from("giro")
    ->where("id", $id)
    ->where("vta_alcohol", 1);
    $sql = $this->db->get();
    return $sql->result();
    if (isset($sql))
        {
            return $sql->vta_alcohol;//write your column name
        }
        else
        {
            return 0;
        }
  }

  //trae si el giro es venta de comida
  public function VentaDeComida($id){
    $this->db->select('alimentos')
    ->from("giro")
    ->where("id", $id)
    ->where("alimentos", 1);
    $sql = $this->db->get();
    return $sql->result();
    if (isset($sql))
        {
            return $sql->alimentos;//write your column name
        }
        else
        {
            return 0;
        }
  }
  //inserta el subtramite
  public function insertSubTramite($id_solicitud,$id_subTramite){
    $data = array (
      "id_solicitud" => $id_solicitud,
      "id_subTramite" => $id_subTramite
    ); //Campos de la tabla
    $query = $this->db->insert('solicitudSubTramite', $data);
    return $query;
  }

  //inserta tipo solicitud
  public function insertaTipoSolicitud($id_solicitud, $tipo){
    $data = array(
      "id_solicitud" => $id_solicitud,
      "tipo" => $tipo
    );
    $query = $this->db->insert('tipo_solicitud', $data);
    return $query;
  }

  //consulta que tipo de solicitud se realiza
  public function getTipoSolicitud($id){
    $this->db->select('tipo')
    ->from('tipo_solicitud')
    ->where('id_solicitud', $id);
    $query = $this->db->get();
    $result = $query->row();
    if (isset($result))
        {
            return $result->tipo;//write your column name
        }
        else
        {
            return 1;
        }
  }

    public function insertaTipoPago($id_solicitud, $folio, $fecha, $en_corte, $tipo_pago){
      
      $data = array(
        "id_solicitud" => $id_solicitud,
        "folio" => $folio,
        "fecha" => $fecha,
        "corte" => $en_corte,
        "tipo_pago" => $tipo_pago
      );
      $query = $this->db->insert('tipo_pago', $data);
      return $query;
    }//end insertBanco

    public function getFolio($id_solicitud){
      $this->db->select('folio')
      ->from('pago')
      ->where('id_solicitud', $id_solicitud);
      $query = $this->db->get();
    $result = $query->row();
    if (isset($result))
        {
            return $result->folio;//write your column name
        }
        else
        {
            return 1;
        }
    }//consultaLineCap

    public function getlinea($id_solicitud){
      $this->db->select('linea_captura')
      ->from('pago')
      ->where('id_solicitud', $id_solicitud);
      $query = $this->db->get();
    $result = $query->row();
    if (isset($result))
        {
            return $result->linea_captura;//write your column name
        }
        else
        {
            return 1;
        }
    }

    public function getCorte(){
      $query = $this->db->query('SELECT tipo_pago.id, tipo_pago.id_solicitud, tipo_pago.folio, tipo_pago.fecha, solicitud.costo,
      CASE
        WHEN  tipo_pago.tipo_pago = "B"
          THEN "Banco"
          ELSE "Caja"
        END AS tipo_pago
      FROM tipo_pago, solicitud
      WHERE  tipo_pago.corte = 0
      AND tipo_pago.id_solicitud=solicitud.id');

      return $query->result();
    }//getCorte

    public function fechaFuncionariosOpinion($id_solicitud,$funcionario){
      //guardamos en la tabla de opiniones
      $this->db->select('fecha_dictamen')
      ->from("usuariosopinion")
      ->where("id_funcionario",$funcionario)
      ->where("id_solicitud",$id_solicitud);
      $sql = $this->db->get();
      return $sql->row();
    }//insertOpinion
    public function generaCorte($en_corte){//actualiza el estatus en la tabla tipo_pago en la columna corte
      $data = array (
        "corte" => 1
      ); //Campos de la tabla
      $query = $this->db->where("corte",$en_corte)->update('tipo_pago', $data);
      return $query;
    }

    public function corte(){
      
      $query = $this->db->query('SELECT corte.id, tipo_pago.folio, tipo_pago.fecha, corte.costo_corte,
      CASE
        WHEN  tipo_pago.tipo_pago = "B"
          THEN "Banco"
          ELSE "Caja"
        END AS tipo_pago
      FROM corte, tipo_pago
      WHERE corte.fecha_corte=CURDATE() AND corte.id_tipo_pago=tipo_pago.id');

      return $query->result();
    }//corte

    public function insertaCorte(){//inserta los registres que tenga el status 1 en corte

      $query = $this->db->query("INSERT INTO corte(id_tipo_pago, fecha_corte, costo_corte)
      SELECT tipo_pago.id, NOW(), solicitud.costo
      FROM tipo_pago, solicitud
      WHERE tipo_pago.corte = 1
      AND tipo_pago.id_solicitud=solicitud.id");

      return true;
    }//end insertaCorte

    

    /**
     * return Boolean valid
     */
    public function insertCorreccionCiudadano($id_solicitud, $id_ciudadano, $id_funcionario,$url,$observaciones)
    {
      $data = array('id_solicitud' => $id_solicitud,
                    'id_ciudadano'=>$id_solicitud,
                    'id_funcionario'=>$id_funcionario,
                    'observaciones' => $observaciones,
                    'url' => $url);
      $query = $this->db->insert('correcciones_ciudadano_inspeccion',$data);
      return $query;
    }//insertCorreccionCiudadano

    /**
     * return Boolean valid
     */
    public function updateCorreccionCiudadano($id_solicitud,$url)
    {
      $data = array('url' => $url);
      $query = $this->db->where('id_solicitud',$id_solicitud)->update('correcciones_ciudadano_inspeccion',$data);
      return $query;
    }//insertCorreccionCiudadano
    public function sumCorte(){
      $query = $this->db->query("SELECT SUM(costo_corte) as 'total' FROM corte WHERE fecha_corte=CURDATE()");
      return $query->row('total');
    }


    /**
     * @param Int $id_solicitud
     * @return Object stdClass Mysql
     *  Obtenemos los funcionarios que revisarón una solicitud
     */
    public function infoSolicitudByTramite($id_tramite){
      $this->db->select('s.id,t.nombre, is.informacion,tc.nombreSubTramite as tipo')
               ->from('solicitud s')
               ->join('tramite t','s.id_tramite = t.id','LEFT')
               ->join('infoSolicitud is','s.id = is.id_solicitud','LEFT')
               ->join('solicitud_construccion sc','sc.id_solicitud = s.id','LEFT')
               ->join('subTramite tc','tc.id = sc.id_tipoConstruccion','LEFT')
               ->where('s.id_tramite',$id_tramite);
      $query = $this->db->get();
      return $query->result();
    }//function


    /**
     * @param Int $id_solicitud
     * @return Object stdClass Mysql
     *  Obtenemos los funcionarios que revisarón una solicitud
     */
    public function getSolicitudSubTramite($id_tramite){
      $this->db->select('s.id,t.nombre, st.nombreSubTramite, count(st.nombreSubTramite) as TOTAL')
               ->from('solicitud s')
               ->join('tramite t','s.id_tramite = t.id','LEFT')
               ->join('solicitudSubTramite sbt','s.id = sbt.id_solicitud','LEFT')
               ->join('subTramite st','st.id = sbt.id_subTramite','LEFT')
               ->where('s.id_tramite',$id_tramite)
               ->group_by('st.nombreSubTramite')
               ->order_by('TOTAL', 'desc'); ;
      $query = $this->db->get();
      return $query->result();
    }//function
    //inserto razon social
    public function insertarazonSocial($id_solicitud, $razonSocial){
      $data = array(
        "id_solicitud" => $id_solicitud,
        "tipo" => $razonSocial
      );
      $query = $this->db->insert('tipo_persona', $data);
      return $query;
    }
//consulto
    public function consultarazonSocial($id_solicitud){
      $this->db->select('tipo')
    ->from('tipo_persona')
    ->where('id_solicitud', $id_solicitud);
    $query = $this->db->get();
    $result = $query->row();
    if (isset($result))
        {
            return $result->tipo;//write your column name
        }
        else
        {
            return 1;
        }
    }

    //contar visitas
    public function contarVisitas(){
      $this->db->select('count(id) as visitas')
                ->from('ci_session');
      $query = $this->db->get();
      $result = $query->row()->visitas;
      return $result;
    }
}//class
