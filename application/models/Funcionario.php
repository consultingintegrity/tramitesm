<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * En esta clase vamos a obtener inforación del Funcionario
 */
class Funcionario extends CI_Model
{


    //funcion que obtine los datos de todos los funcionarios que no esten dados de baja
    public function getFun()
    {

        //se hace la consulta correspondiente para obtener los datos de los funcionarios
        $this->db->select("u.id as id_usuario, u.imagen,u.correo_electronico, u.status,
                          p.id as id_persona, p.primer_nombre, segundo_nombre, p.primer_apellido ,p.segundo_apellido ,p.genero,
                          r.nombre, r.descripcion,
                          d.nombre as dependencia,
                          dir.nombre as direccion
                          ")
            ->from("persona p")
            ->join("usuario u", "p.id=u.id_persona", "left")
            ->join("usuario_rol ur", "u.id=ur.id_usuario", "left")
            ->join("rol r", "r.id=ur.id_rol", "left")
            ->join("dependencia d", "d.id = u.id_dependencia", "left")
            ->join("direccion dir", "dir.id = d.id_direccion", "left")
            ->where("ur.id_rol <> 2 ")
            ->where("ur.id_rol <> 1 ")
            ->where("u.status <> 3 ");
        $query = $this->db->get();
        //se regresa un result que se guarda en la variable query
        return $query->result();
    } //fin de la funcion getFun
    //funcion que obtiene los datos de de un funcionario en especifico en base a el id de persona
    public function getFunId($id_usuario)
    {
        //se hace la consulta correspondiente para obtener los datos de ese funcionario
        $this->db->select("p.*,
                          u.id as id_usuario,correo_electronico,contrasenia,u.status,id_rol,telefono,extension,telefono_oficina,
                          e.id as id_estado,
                          dir.id as id_direccion,
                          de.id as id_dependencia,")
            ->from("persona p")
            ->join("usuario u", "p.id = u.id_persona ", "left")
            ->join("usuario_rol r", "u.id = r.id_usuario", "left")
            ->join("municipio m", "m.id =p.id_municipio", "left")
            ->join("estado e", "e.id =m.id_estado", "left")
            ->join("dependencia de", "de.id = u.id_dependencia", "left")
            ->join("direccion dir", "dir.id = de.id_direccion", "left")
            ->where("u.id", $id_usuario);
        $query = $this->db->get();
        //se regresa un result que se guarda en la variable query
        return $query->row();
    } //fin de la funcion getFunId
    //funcion que actualiza la contraseña en base a un id usuario
    public function cambiarcontra($contrasenia, $id_usuario)
    {
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "contrasenia" => $contrasenia,

        );
        //se actualiza la contraseña en la tabla de usuario donde id de esa tabla sea igual a la variable id_usuario
        $this->db->where('id', $id_usuario);
        //if que comprueba que el update se realizo correctamente
        if ($this->db->update('usuario', $data)) {
            return true;
        } //fin if update
        //else de el if de update
        else {
            return false;
        } //fin del else de update
    } //fin de la funcion cambiarcontra
    //funcion que modifica los datotos de la tabla persona,usuario y usuario_rol en base a id_persona e id_usuario
    public function modFun($primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido, $genero, $id_municipio, $id_persona, $correo, $rol, $numi, $nume, $calle, $colonia, $fecha_nacimiento,
                          $id_usuario,  $txtMovil, $txtTelOficina, $txtExtension, $optDireccion, $optDependencia)
    {
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "primer_nombre"    => $primer_nombre,
            "segundo_nombre"   => $segundo_nombre,
            "primer_apellido"  => $primer_apellido,
            "segundo_apellido" => $segundo_apellido,
            "fecha_nacimiento" => $fecha_nacimiento,
            "genero"           => $genero,
            "id_municipio"     => $id_municipio,
            "numero_interior"  => $numi,
            "numero_exterior"  => $nume,
            "calle"            => $calle,
            "colonia"          => $colonia,
        );
        //se actualizan los datos de la tabla persona con los datos del array en base a id de la persona
        $this->db->where('id', $id_persona);
        $this->db->update('persona', $data);
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "correo_electronico" => $correo,
            "telefono"         => $txtMovil,
            "telefono_oficina" => $txtTelOficina,
            "extension"        => $txtExtension
        ); //data
        //se actualizan los datos de la tabla usuario con los datos del array en base a id de la usuario
        $this->db->where('id_persona', $id_persona);
        $this->db->update('usuario', $data);
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "id_rol" => $rol,
        );
        //se actualizan los datos de la tabla usuario_rol con los datos del array en base a id de la usuario
        $this->db->where('id_usuario', $id_usuario);
        if ($this->db->update('usuario_rol', $data)) {
            return true;
        } //if update
        else {
            return false;
        } //else

    } //fin de la funcion modFun
    //funcion que registra un nuevo funcionario
    public function registro($primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido, $genero, $muni, $correo_electronico, $contrasenia, $id_rol, $numero_interior, $numero_exterior, $status, $calle, $colonia, $fecha_nacimiento,$telefono,$telOficina,$extension,$id_dependencia)
    {
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "primer_nombre"    => $primer_nombre,
            "segundo_nombre"   => $segundo_nombre,
            "primer_apellido"  => $primer_apellido,
            "segundo_apellido" => $segundo_apellido,
            "fecha_nacimiento" => $fecha_nacimiento,
            "genero"           => $genero,
            "id_municipio"     => $muni,
            "calle"            => $calle,
            "colonia"          => $colonia,
            "numero_interior"  => $numero_interior,
            "numero_exterior"  => $numero_exterior,

        );
        //if que valida si la funcion insert se efectuo correctamente
        if ($this->db->insert('persona', $data)) {
            //se guarda en la variable id_persona el id de la ultima persona registrada
            $id_persona = $this->db->insert_id();
            $ban        = true;
        } //fin del if de validacion de insert
        else {
            $ban = false;
        } //else
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "correo_electronico" => $correo_electronico,
            "contrasenia"        => $contrasenia,
            "id_persona"         => $id_persona,
            "status"             => $status,
            "telefono"  => $telefono,
            "telefono_oficina"  => $telOficina,
            "extension"  => $extension,
            "id_dependencia"  => $id_dependencia
        ); //data
        //if que valida si la funcion insert se efectuo correctamente
        if ($this->db->insert('usuario', $data)) {
            //se guarda en la variable id_usuario el id de el ultimo usuario registrado
            $id_usuario = $this->db->insert_id();
            $ban2       = true;
        } //if
        else {
            $ban2 = false;
        } //else
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "id_rol"     => $id_rol,
            "id_usuario" => $id_usuario,

        ); //data
        //if que valida si la funcion insert se efectuo correctamente
        if ($this->db->insert('usuario_rol', $data)) {

            $ban3 = true;
        } //if
        else {
            $ban3 = false;
        }
        if ($ban && $ban2 && $ban3) {
            return true;
        } //if
        else {
            return false;
        } //else

    } //fin del if de la funcion registro
    //funcion que actualiza el estatus de un usuario en base a su id
    public function actualizaStatus($id_usuario,$estatus)
    {
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "status" => $estatus,

        );
        //se actualizan los datos de la tabla usuario_rol con los datos del array en base a id de usuario
        $this->db->where('id', $id_usuario);
        if ($this->db->update('usuario', $data)) {
            return true;
        } //if update
        else {
            return false;
        } //else
    } //fin de la funcion actualizaStatus

    /**
     * return row $query
     */
    public function rol($id_rol)
    {
      //obtenemos la informacikónd de un rol
      $query = $this->db->where("id",$id_rol)->get('rol');
      return $query->row();
    }
     public function dependencia($id_tramite)
    {
        //se hace la consulta correspondiente para obtener los datos de ese funcionario
        $this->db->select("d.id as id_dependencia, d.nombre as nombre_dependencia")
            ->from("dependencia d")
            ->join("tramite t", "t.id_dependencia = d.id")
            ->where("t.id", $id_tramite);
        $query = $this->db->get();
        //se regresa un result que se guarda en la variable query
        return $query->row();
    } //fin de la funcion getFunId

    /**
     * @
     */
    public function actualizarInfoAdministrador($primer_nombre, $segundo_nombre, $primer_apellido, $segundo_apellido, $genero, $id_persona, $correo)
    {
      $result = false;
      //se crea un array con los datos a actualizar y se guardan el la variable data
      $data = array(
          "primer_nombre"    => $primer_nombre,
          "segundo_nombre"   => $segundo_nombre,
          "primer_apellido"  => $primer_apellido,
          "segundo_apellido" => $segundo_apellido,
          "genero"           => $genero
      );
      //se actualizan los datos de la tabla persona con los datos del array en base a id de la persona
      $this->db->where('id', $id_persona);
      if ($this->db->update('persona', $data)) {
        //se crea un array con los datos a actualizar y se guardan el la variable data
        $data = array(
            "correo_electronico" => $correo
        ); //data
        //se actualizan los datos de la tabla usuario con los datos del array en base a id de la usuario
        $this->db->where('id_persona', $id_persona);
        if ($this->db->update('usuario', $data)) {
          $result = true;
        }
      }//if
      return $result;
    } //actualizarInfoAdministrador

    public function eliminarFuncionario($id,$estatus){
        $data = array("status"=>$estatus);
        $query = $this->db->where("id",$id)->update('usuario', $data);
     return $query;
    }
} //class
