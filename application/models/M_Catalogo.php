<?php
/**
 * @author Guadalupe-Valerio
 */
defined('BASEPATH') or exit('No direct script access allowed');

class M_Catalogo extends CI_Model
{
    public function getDireccion()
    {
      $query = $this->db->get_where('direccion',['status'=> 1]);
      return $query->result();
    }//getDireccion

    public function getDependencia($id_direccion)
    {
      $query = $this->db->get_where('dependencia',['id_direccion'=>$id_direccion]);
      return $query->result();
    }//getDependencia

    public function getEstados()
    {
        $this->db->select("id,nombre")
        ->from("estado")
        ->where("id=21");
        $sql = $this->db->get();
        return $sql->result();

    } //getestados
    public function getMunicipios($id_estado)
    {

        $this->db->select("id,nombre,id_estado")
            ->where("id_estado", $id_estado)
            ->from("municipio")
            ->where("id=10");
        $sql = $this->db->get();
        return $sql->result();
    } //municipios

    public function getRol()
    {

        $this->db->select("id,nombre")

            ->from("rol")
            ->where("id <> 1 and id <> 2");
        $sql = $this->db->get();
        return $sql->result();

    } //get rol

    public function getGiros(){
        $this->db->select("*")
        ->from("giro")
        ->order_by("descripcion");
        $sql = $this->db->get();
        return $sql->result();
    } //getgiros

    /**
     * @param INT id_giro
     * @return Mysql Object row
     */
    public function giro($id_giro){
      $query = $this->db->get_where('giro',['id'=>$id_giro],1);
      return $query->row();
    }//giro

    /**

     * @return Object Mysql row
     */
    public function getTipoConstruccion(){
      $sql = $this->db->order_by("nombreSubTramite")->get('subTramite');
      return $sql->result();
    } //getTipoConstrucción

    /**
     * @param INT id_tipo_construccion
     * @return Mysql Object row
     */
    public function tipo_construccion($id_tipo_construccion){
      $query = $this->db->get_where('subTramite',['id'=>$id_tipo_construccion],1);
      return $query->row();
    }//giro

    /*
     * @param INT id_giro
     * @return Mysql Object result
     */
    public function solGiro($id_solicitud){
         $this->db->select("*")
        ->from("solicitud_giro")
        ->where("id_solicitud=".$id_solicitud);
        $sql = $this->db->get();
        $id = $sql->row();

        $this->db->select("*")
                ->from("giro")
                ->where("id", $id->id_giro);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
      }//giro

      /*
       * @param INT id_solicitud
       * @return Mysql Object result
       */
      public function solictud_tipoConstruccion($id_solicitud){
           $this->db->select("id_tipoConstruccion,nombreSubTramite as tipo")
          ->from("solicitud_construccion")
          ->join("subTramite","subTramite.id = solicitud_construccion.id_tipoConstruccion")
          ->where("id_solicitud",$id_solicitud);
          $sql = $this->db->get();
          $result = $sql->row();
          return $result;
      }//solicitud_tipoConstruccion

      //tipo_subTramite
      public function tipo_subTramite ($id_tramite){
        $this->db->select("id, nombreSubTramite, subNumero")
        ->from("subTramite")
        ->where("id_tramite",$id_tramite);
        $sql = $this->db->get();
        $result = $sql->result();
        return $result;
      }

       //tipo_subTramite_solicitud
       public function subTramiteSol ($id_solicitud){
        $this->db->select("subNumero")
        ->from("solicitudSubTramite")
        ->join("subTramite","solicitudSubTramite.id_subTramite = subTramite.id")
        ->where("solicitudSubTramite.id_solicitud",$id_solicitud);
        $sql = $this->db->get();
        $result = $sql->row();
        if(isset($result)){
            return $result->subNumero;
        }
        return 1;
      }

      //provicional si y/o no
      public function provicional($id_solicitud){
        $this->db->select("tipo")
        ->from("tipo_solicitud")
        ->where("id_solicitud",$id_solicitud);
        $sql = $this->db->get();
        $result = $sql->row();        
        return $result;
      }
} //class

/* End of file Catalogo.php */
/* Location: ./application/models/Catalogo.php */
