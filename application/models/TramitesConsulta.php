<?php
class TramitesConsulta extends CI_Model{

    public function __construct(){
        parent::__construct();

    }

    //consulta para la pagina principal (descripcion basica de tramite)
    //retorno todos los tramites almacenados en la base de datos
    public function consulta(){

        $this->db->select('id,nombre,descripcion,logo');
        $this->db->from('tramite');
        $this->db->where("id <> 9");
        $this->db->where("status <> 0");
        $this->db->order_by("nombre");
        $consulta = $this->db->get();
        $resultado = $consulta->result();

        return $resultado;
    }

    //consulto un tramite en especifico para mostrar la informacion concreta
    //en la vista de tramites en linea
    public function descTramite($id){

        $this->db->select('*');
        $this->db->from('tramite');
        $this->db->where("id=".$id);
        $consulta = $this->db->get();
        $resultado = $consulta->row();

        return $resultado;
    }

    //consulto los requisitos relacionados con un tramite en especifico
    //regreso el arreglo correspondiente
    public function requisitosTramite($id,$perfilado_requisitos){
        $this->db->select('*');
        $this->db->from('requisito');
        $this->db->join('requisito_tramite','requisito_tramite.id_requisito = requisito.id');
        $this->db->where("requisito_tramite.id_tramite=".$id);
        if (!empty($perfilado_requisitos)) {
        $this->db->where_not_in("requisito_tramite.id_requisito",$perfilado_requisitos);
        }
        $consulta = $this->db->get();
        $resultado = $consulta->result();

        return $resultado;
    }


    public function ConsultaPendientes($id){
        $this->db->select('s.id,s.fecha_inicio as fecha,t.nombre,s.estatus,concat(p.primer_nombre," ",  p.primer_apellido) as persona, u.telefono, u.correo_electronico');
        $this->db->from('solicitud s');
        $this->db->join('usuario u','u.id = s.id_ciudadano');
        $this->db->join('persona p','p.id = u.id_persona');
        $this->db->join('tramite t','t.id = s.id_tramite');
        $this->db->where("s.id_funcionario = ".$id);
        $this->db->where("s.estatus <>",0);
        $this->db->where("s.estatus <>",1);
        $this->db->where("s.id_funcionario = ".$id);
        $this->db->order_by("s.fecha_inicio ","desc");
        $consulta = $this->db->get();
        $resultado = $consulta->result();

        return $resultado;
        }

    //tramites pendientes de la persona actual
    public function tramitesPendientes($idUsuario){
        //todos los resultados diferentes de fianlizado = 3
        $this->db->select("s.id,s.fecha_inicio,t.nombre,s.estatus,s.costo,g.descripcion as giro, tc.nombreSubTramite as tipoConstruccion");
        $this->db->from('solicitud s');
        $this->db->join('tramite t','t.id = s.id_tramite');
        $this->db->join("solicitud_giro sg", "s.id = sg.id_solicitud","LEFT");
        $this->db->join("giro g", "sg.id_giro = g.id","LEFT");
        $this->db->join("solicitud_construccion sc", "s.id = sc.id_solicitud","LEFT");
        $this->db->join("subTramite tc", "sc.id_tipoConstruccion = tc.id","LEFT");
        $this->db->where('s.id_ciudadano='.$idUsuario);
        $this->db->order_by("s.fecha_inicio ","asc");
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        //retorno toda la consulta
        return $resultado;
    }//.tramites pendientes

    /**
     * @param Int $id_solicitud
     * @return string $url Folio Pago
     */
    public function getFolioPago($id_solicitud,$id_usuario)
    {
      $this->db->select('url')
              ->from('pago p')
              ->join('solicitud s','p.id_solicitud = s.id','LEFT')
              ->where('p.id_solicitud',$id_solicitud)
              ->where('s.id_ciudadano',$id_usuario);
      $query = $this->db->get();
      return $query->row();
    }//getFolioPago

    /**
     * @return Object Mysql row Data
     */
    public function tramitesConculuidos(){
      //Segun yo nos debe traer la suma de todos los registros con estatus 1
      $query = $this->db->get_where('solicitud',['estatus'=>1]);
      return $query->result();
    }//tramites concluidos

    /**
     * @param INT $id_funcionario
     * @return ArrayObject $data
     * @param INT $estatus
     * Nos devuelve las solicitudes que tiene correcciones
     */
    public function solicitudCorreccion($id_funcionario,$estatus){
     $this->db->select('s.id,s.fecha_inicio,
                       u.correo_electronico,u.telefono,
                       p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
                       t.nombre,
                       sc.fecha as fechaCorreccion, sc.observaciones')
     ->from('solicitud s')
     ->join('tramite t','s.id_tramite = t.id','LEFT')
     ->join('usuario u','s.id_ciudadano = u.id','LEFT')
     ->join('persona p','u.id_persona = p.id','LEFT')
     ->join('solicitud_correccion sc','s.id = sc.id_solicitud','LEFT')
     ->where('s.id_funcionario',$id_funcionario)
     ->where('s.estatus',$estatus);
     $query = $this->db->get();
     return $query->result();
   }//solicitudCorreccion

   /**
    * @param INT $id_funcionario
    * @return ArrayObject $data
    * @param INT $estatus
    * Nos devuelve las solicitudes que tiene correcciones
    */
   public function solicitudCorreccionCiudadano($id_funcionario,$estatus){
    $this->db->select('s.id,s.fecha_inicio,
                      u.correo_electronico,u.telefono,
                      p.primer_nombre, p.segundo_nombre, p.primer_apellido, p.segundo_apellido,
                      t.nombre,
                      cci.fecha as fechaCorreccion, cci.observaciones, cci.url as docCorreccionCiudadano')
    ->from('solicitud s')
    ->join('tramite t','s.id_tramite = t.id','LEFT')
    ->join('usuario u','s.id_ciudadano = u.id','LEFT')
    ->join('persona p','u.id_persona = p.id','LEFT')
    ->join('correcciones_ciudadano_inspeccion cci','s.id = cci.id_solicitud','LEFT')
    ->where('s.id_funcionario',$id_funcionario)
    ->or_where('s.id_ciudadano',$id_funcionario)
    ->where('s.estatus',$estatus);
    $query = $this->db->get();
    return $query->result();
  }//solicitudCorreccion



  /**
   * @author Pepe-Droid
   * @param INT $id_solicitud
   * @return Object Mysql row data
   * consulamos el tipo de licencia de construccion o tipo de licencia de funcionamiento, si y solo si esta tiene
   */
   public function tipoLicenciaSolicitud($id_solicitud){

    $this->db->select("t.nombre, g.descripcion as giro, tc.nombreSubTramite as tipoConstruccion");
    $this->db->from('solicitud s');
    $this->db->join('tramite t','t.id = s.id_tramite');
    $this->db->join("solicitud_giro sg", "s.id = sg.id_solicitud","LEFT");
    $this->db->join("giro g", "sg.id_giro = g.id","LEFT");
    $this->db->join("solicitud_construccion sc", "s.id = sc.id_solicitud","LEFT");
    $this->db->join("subTramite tc", "sc.id_tipoConstruccion = tc.id","LEFT");
    $this->db->where('s.id',$id_solicitud);
    $consulta = $this->db->get();
    $resultado = $consulta->row();
    //devolvemos los valores
    return $resultado;
   }//tipoLicenciaSolicitud

  /**
   * @param Int $id_rol
   * Obtenemos los trámites para subir opinión técnica
   */
  public function opinionTecnica($id_funcionario){
    $query = $this->db->select('id_solicitud')
            ->from("usuariosopinion")
            ->where('id_funcionario',$id_funcionario)
            ->where("estatus",1)
            ->get();
     $resultado = $query->result();
     return $resultado;
  }//opinionTecnica


  public function solicitudesOpinionTecnica($arrayIdSolicitud){
    $this->db->select('s.id,s.fecha_inicio as fecha,t.nombre,s.estatus,s.id_funcionario,concat(p.primer_nombre," ",  p.primer_apellido) as persona, u.telefono, u.correo_electronico');
    $this->db->from('solicitud s');
    $this->db->join('usuario u','u.id = s.id_ciudadano');
    $this->db->join('persona p','p.id = u.id_persona');
    $this->db->join('tramite t','t.id = s.id_tramite');
    $this->db->where_in("s.id",$arrayIdSolicitud);
    $this->db->where("s.estatus <>",0);
    $this->db->where("s.estatus <>",1);
    $this->db->order_by("s.fecha_inicio ","asc");
    $consulta = $this->db->get();
    $resultado = $consulta->result();
    return $resultado;
  }

    /**
   * @param Int $id_solicitud
   * Obtenemos los Subtrámites para subir orden de pago
   */
  public function subTramitePago($id_solicitud){
    $query = $this->db->select('SubT.nombreSubTramite')
            ->from("solicitudSubTramite solSubT")
            ->join('subTramite SubT','SubT.id = solSubT.id_subTramite')
            ->where('solSubT.id_solicitud',$id_solicitud);
            $consulta = $this->db->get();
     $r = $consulta->row();
    return $r;
  }//subtramite pago

}
?>
